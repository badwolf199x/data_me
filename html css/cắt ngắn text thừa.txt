.tin-tuc-right .elementor-grid {
    grid-template-columns: auto!important;
}
.tin-tuc-right h3.elementor-post__title a {
    text-align: start;
    text-overflow: ellipsis;
    -webkit-line-clamp: 2;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    height: 40px;
    overflow: hidden;
    margin-bottom: 10px;
    text-align: left;
    font-size: 16px;
    line-height: 21px;
    font-weight: 700;
}
.tin-tuc-right h3.elementor-post__title {
    height: 48px;
    display: flex;
    align-items: center;
}
.tin-tuc-right .elementor-post__excerpt p {
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 21px;
    -webkit-line-clamp: 3;
    max-height: 63px;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    text-align: justify;
}
.tin-tuc-right .elementor-post__excerpt {
    height: 63px;
    display: flex;
    align-items: center;
}

@media screen and (max-width: 768px )
{
    
}

@media screen and (max-width: 500px )
{
    
}
