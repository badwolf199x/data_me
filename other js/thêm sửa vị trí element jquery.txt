a.append('b') - thêm nội dung b vào a
prepend('b') - Inserts content at the beginning of the selected elements
after('b') - Inserts content after the selected elements
before('b') - Inserts content before the selected elements

a.appendTo('b'); thêm a vào b

a.wrap('b'); thêm cha b trước con a
a.unwrap(); xóa cha a
a.replaceWith('b') thay nội dung a bằng b
a.replaceAll('b') thay tất cả  a bằng b