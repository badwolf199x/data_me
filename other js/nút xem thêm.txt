
	jQuery(function($)
	{
<!--sub-megamenu-->
	$(document).ready(function(){
	if($(window).width()>1100)
	{
	  $('li.subdrop').mouseenter(function(e){
		var classList = $(this).attr('class').split(/\s+/);
	   $.each(classList, function(index, item) {
      if ($('.grid-sub-megamenu').hasClass(item)) 
	  {
        $('.grid-sub-megamenu.'+item).first().css('display','flex');
		$('.grid-sub-megamenu.'+item).first().mouseenter(function(e){
			$(this).css('display','flex');
		});
		$('.grid-sub-megamenu.'+item).first().mouseleave(function(e){
			$(this).css('display','none');
		});
		$('li.menu-item').mouseenter(function(e){
			if(!$(this).hasClass(item))
			{
				$('.grid-sub-megamenu.'+item).first().css('display','none');
			}
		})
		$('.elementor-sticky').mouseleave(function(e){
			$('.grid-sub-megamenu.'+item).first().css('display','none');
		})
       }
      });
	});
	}});
<!--slider image gallery product-->
$(document).ready(function(){
if($(window).width()>1100)
{
var flex_width=$('.product-pview ol.flex-control-nav.flex-control-thumbs li:first-child').height()+20;
var li_all_element=$('.product-pview ol.flex-control-nav.flex-control-thumbs li');	
function change_img()
{
	for(var i=1;i<li_all_element.length+1;i++)
		{
         if($('ol.flex-control-nav.flex-control-thumbs li:nth-child('+i+') img').hasClass('flex-active'))
			 {
				 if(i==1 || i==2){ $('.product-pview ol.flex-control-nav.flex-control-thumbs').scrollTop(0); }
				 else { $('.product-pview ol.flex-control-nav.flex-control-thumbs').scrollTop(((i-2)*flex_width)-10); }
			 }
		}
}
		$('ol.flex-control-nav.flex-control-thumbs li img').click(function(){ setTimeout( change_img , 100 ); });
		$('ul.flex-direction-nav li ').click(function(){ change_img(); });
}
})
<!--product mo ta-->
$(document).ready(function(){
if($('.product-mota-tab').height()>600)
{
  var show=0;
  $('.woocommerce-tabs.wc-tabs-wrapper').after('<p class="show-more text-center readmore"><a class="btn-custom">Đọc thêm</a></p>');
  $('.woocommerce-tabs.wc-tabs-wrapper').css('max-height','200px');
  $('.btn-custom').click(function(){
	  if(show==0)
	 {
	  $(this).parents('.show-more').removeClass('readmore');$(this).parents('.show-more').addClass('hidden');$(this).text('Thu gọn');
	  $('.product-mota-tab').css('max-height','unset');
	  show++;
	 }
	  else
	{
	    $(this).parents('.show-more').removeClass('hidden');$(this).parents('.show-more').addClass('readmore');$(this).text('Đọc thêm');
		$('.product-mota-tab').css('max-height','200px');
		show--;
	}
  });
}

if($('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').height()>0)
	  {
		  $('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').each(function()
		  {
			  if($(this).height()>400)
			  {
			  $(this).append('<p class="show-more text-center readmore"><a class="btn-custom">Đọc thêm</a></p>');
			  $(this).css('max-height','400px');
			  $('.btn-custom').click(function(){
				  if($(this).text()=='Đọc thêm')
				  {
					  $(this).parents('.show-more').removeClass('readmore');
					  $(this).parents('.show-more').addClass('hidden');
					  $(this).text('Thu gọn');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('max-height','unset');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('padding-bottom','126px');
					  show++;
				  }
				  else
				  {
					  $(this).parents('.show-more').removeClass('hidden');$(this).parents('.show-more').addClass('readmore');
					  $(this).text('Đọc thêm');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('max-height','400px');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('padding-bottom','');
					  show--;
				  }
			  });
			  }
		  });
	  }
})	
	})
	