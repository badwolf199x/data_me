<?php function cptui_register_my_cpts_settings_theme() {

	/**
	 * Post Type: Cài đặt hiển thị.
	 */

	$labels = [
		"name" => __( "Cài đặt hiển thị", "ecomcx-theme" ),
		"singular_name" => __( "Cài đặt hiển thị", "ecomcx-theme" ),
	];

	$args = [
		"label" => __( "Cài đặt hiển thị", "ecomcx-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"delete_with_user" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => false,
		"query_var" => true,
		"menu_position" => 3,
		"menu_icon" => "dashicons-admin-generic",
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "settings_page", $args );
}

add_action( 'init', 'cptui_register_my_cpts_settings_theme' );

?>