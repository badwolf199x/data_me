<?php 

add_action( 'pre_get_posts', 'exclude_post_slider', 1 );
function exclude_post_slider( $query ) 
{
    if (!$query->is_admin && $query->is_main_query() && $query->is_archive && !$query->is_search ) 
	{
		$tax=(array)$query->tax_query;
        $sticky = get_option( 'sticky_posts' );
		$args_slider = array(
			'post_status' =>'publish',
            'nopaging' => true,
            'tax_query' => array(
			   'relation' =>$tax['relation'],
			    $tax['queries'][0]),
             );
		$all_post=query_posts($args_slider);
		wp_reset_query();
		$have_sticky=0;
		foreach($all_post as $key => $value)
		{
			if(in_array($value->ID,$sticky))
			   {
				  $have_sticky=1;
				  $GLOBALS['sticky_slider'][]=$value->ID;
			   }
		}
	    if($have_sticky==1)
	    {
			$query->set('nopaging', false );   
		    $query->set('posts_per_archive_page', 8 );
			$query->set('post__not_in', $sticky );  
	    }
		else 
		{
			$query->set('nopaging', false );   
			$query->set('offset', 6 ); 
		    $query->set('posts_per_archive_page', 8 );
		}
        return;
    }
}

