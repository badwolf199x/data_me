<?php 

if ( ! function_exists( 'f_popup_form' ) ) 

{

	function popup_form() 

	{   ?>

		<div class="wrap-popup">

			<div class="popup-form">

				<div class="button-close">

					<i class="bi bi-x-circle"></i>

			    </div>

				<div class="col-md-4 col-sm-4 col-xs-12 popup-title-wrap">

					<h4 class="sub-title fst-italic">Từ đội ngũ tư vấn giàu kinh nghiệm</h4>

					<h2 class="title">NHẬN GIẢI PHÁP MIỄN PHÍ</h2>

				</div>

				<div class="col-md-8 col-sm-8 col-xs-12 popup-content-wrap">

					<?php echo do_shortcode('[contact-form-7 id="2142" title="Form subcribe"]'); ?>

					<div class="text-justify text-italic">

						<span><p>Để chúng tôi có thể hỗ trợ bạn được tốt nhất, bạn vui lòng để lại thông tin, đội ngũ tư vấn với kinh nghiệm trên 5 năm triển khai TMĐT của chúng tôi sẽ liên hệ lại tư vấn giúp bạn hoàn toàn miễn phí</p>Chúng tôi cam kết <b>không spam</b> dưới mọi hình thức</span>

				    </div>

			    </div>

			</div>

		</div>

	<?php 	}

	add_action('wp_footer','popup_form');

}