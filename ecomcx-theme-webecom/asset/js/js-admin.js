jQuery(document).ready( function($){



    var mediaUploader ;



    function myTheme_setImageFromURL(url, attachmentId, width, height) {



    var choice, data = {};







    data.url = url;



    data.thumbnail_url = url;



    data.timestamp = _.now();







    if (attachmentId) {



        data.attachment_id = attachmentId;



    }







    if (width) {



        data.width = width;



    }







    if (height) {



        data.height = height;



    }



     $('#site_icon').val(data.attachment_id);



     $('#profile_icon').val(data.url);



     $("#mini_icon").prop("src", data.url);   



     $('#icon-logo').css('background-image', 'url("'+data.url+'")');     



}



function myTheme_setImageFromAttachment(attachment) {







     $('#site_icon').val(attachment.id);



     $('#profile_icon').val(attachment.url );



     $("#mini_icon").prop("src", attachment.url); 



     $('#icon-logo').css('background-image', 'url("'+attachment.url +'")');            



}







function myTheme_setImageFromURL_avt(url, attachmentId, width, height) {



    var choice, data = {};







    data.url = url;



    data.thumbnail_url = url;



    data.timestamp = _.now();







    if (attachmentId) {



        data.attachment_id = attachmentId;



    }







    if (width) {



        data.width = width;



    }







    if (height) {



        data.height = height;



    }



    $('#avt').val(data.url);      



    $('#avt-icon').css('background-image', 'url("'+data.url+'")');    



}



function myTheme_setImageFromAttachment_avt(attachment) {



   $('#avt').val(attachment.url);      



    $('#avt-icon').css('background-image', 'url("'+attachment.url +'")');  



    }



    



function myTheme_calculateImageSelectOptions(attachment, controller) {







    var control = controller.get( 'control' );







    var flexWidth = !! parseInt( control.params.flex_width, 10 );



    var flexHeight = !! parseInt( control.params.flex_height, 10 );







    var realWidth = attachment.get( 'width' );



    var realHeight = attachment.get( 'height' );







    var xInit = parseInt(control.params.width, 10);



    var yInit = parseInt(control.params.height, 10);







    var ratio = xInit / yInit;







    controller.set( 'canSkipCrop', ! control.mustBeCropped( flexWidth, flexHeight, xInit, yInit, realWidth, realHeight ) );







    var xImg = xInit;



    var yImg = yInit;







    if ( realWidth / realHeight > ratio ) {



        yInit = realHeight;



        xInit = yInit * ratio;



    } else {



        xInit = realWidth;



        yInit = xInit / ratio;



    }        







    var x1 = ( realWidth - xInit ) / 2;



    var y1 = ( realHeight - yInit ) / 2;        







    var imgSelectOptions = {



        handles: true,



        keys: true,



        instance: true,



        persistent: true,



        imageWidth: realWidth,



        imageHeight: realHeight,



        minWidth: xImg > xInit ? xInit : xImg,



        minHeight: yImg > yInit ? yInit : yImg,            



        x1: x1,



        y1: y1,



        x2: xInit + x1,



        y2: yInit + y1



    };



    imgSelectOptions.aspectRatio = xInit + ':' + yInit;



    return imgSelectOptions;



}  



	



//img//



jQuery('#image-upload-button').click(function(e){



    



	e.preventDefault();



    mediaUploader = new wp.media({



    title: 'Chọn ảnh',



    button:{



        text: 'Chọn ảnh này'



    },



    multiple: false



    }); 



    



  mediaUploader.on('select', function(){



    attachment = mediaUploader.state().get('selection').first().toJSON();



    $('#profile_picture').val(attachment.url);



    $('#img-logo').css('background-image', 'url("'+attachment.url+'")');



  });



         mediaUploader.open();



  });



jQuery('#image-delete-button').click(function(e){          



    $('#profile_picture').val(null);



    $('#img-logo').css('background-image', 'url("")');



  });



//Logo đơn sắc//



jQuery('#logo_monochrome-upload-button').click(function(e){

    

    e.preventDefault();

    mediaUploader = new wp.media({

    title: 'Chọn ảnh',

    button:{

        text: 'Chọn ảnh này'

    },

    multiple: false

    }); 

    

  mediaUploader.on('select', function(){

    attachment = mediaUploader.state().get('selection').first().toJSON();

    $('#profile_logo_monochrome').val(attachment.id);

    $('#img-logo-monochrome').css('background-image', 'url("'+attachment.url+'")');

  });

         mediaUploader.open();

  });

    

jQuery('#logo-monochrome-delete-button').click(function(e){          

    $('#profile_logo_monochrome').val(null);

    $('#img-logo-monochrome').css('background-image', 'url("")');

  });





//icon//



jQuery('#icon-upload-button').click(function(e){







    e.preventDefault();



 var cropControl = {



       id: "control-id",



       params : {



         flex_width : false,  



         flex_height : true, 



         width : 512,  



         height : 512, 



       }



   };



   cropControl.mustBeCropped = function(flexW, flexH, dstW, dstH, imgW, imgH) {



    if ( true === flexW && true === flexH ) {



        return false;



    }



    if ( true === flexW && dstH === imgH ) {



        return false;



    }       



    if ( true === flexH && dstW === imgW ) {



        return false;



    }             



    if ( dstW === imgW && dstH === imgH ) {



        return false;



    }



    if ( imgW <= dstW ) {



        return false;



    }



    return true;        



   };     



    mediaUploader = new wp.media({



    button:{



        text: 'Chọn ảnh và cắt',



        close: false



    },



        states: [



            new wp.media.controller.Library({



                title:     'Chọn ảnh và cắt', 



                library:   wp.media.query({ type: 'image' }),



                multiple:  false,



                date:      false,



                priority:  20,



                suggestedWidth: 512,



                suggestedHeight: 512



            }),



            new wp.media.controller.CustomizeImageCropper({ 



                imgSelectOptions: myTheme_calculateImageSelectOptions,



                control: cropControl



            })



        ]



    }); 



    mediaUploader.on('cropped', function(croppedImage) {



        var url = croppedImage.url,



            attachmentId = croppedImage.id,



            w = croppedImage.width,



            h = croppedImage.height;



            myTheme_setImageFromURL(url, attachmentId, w, h);            



    });







    mediaUploader.on('skippedcrop', function(selection) {



        var url = selection.get('url'),



            w = selection.get('width'),



            h = selection.get('height');



            myTheme_setImageFromURL(url, selection.id, w, h);            



    });        



    



  mediaUploader.on('select', function(){



   var attachment = mediaUploader.state().get( 'selection' ).first().toJSON();



   if (     cropControl.params.width  === attachment.width 



            &&   cropControl.params.height === attachment.height 



            && ! cropControl.params.flex_width 



            && ! cropControl.params.flex_height ) {



                myTheme_setImageFromAttachment( attachment );



            mediaUploader.close();



        } else {



            mediaUploader.setState( 'cropper' );



        }



  });



  mediaUploader.open();



  });



jQuery('#icon-delete-button').click(function(e){          



      $('#site_icon').val(null);



        $("#mini_icon").prop("src", "");       



    $('#profile_icon').val(null);



    $('#icon-logo').css('background-image', 'url("")');



  });



	



//avt//



jQuery('#avt-upload-button').click(function(e){



           



     e.preventDefault();



 var cropControl = {



       id: "control-id",



       params : {



         flex_width : false,  



         flex_height : true, 



         width : 96,  



         height : 96, 



       }



   };



   cropControl.mustBeCropped = function(flexW, flexH, dstW, dstH, imgW, imgH) {



    if ( true === flexW && true === flexH ) {



        return false;



    }



    if ( true === flexW && dstH === imgH ) {



        return false;



    }       



    if ( true === flexH && dstW === imgW ) {



        return false;



    }             



    if ( dstW === imgW && dstH === imgH ) {



        return false;



    }



    if ( imgW <= dstW ) {



        return false;



    }



    return true;        



   };     



    mediaUploader = new wp.media({



    button:{



        text: 'Chọn ảnh và cắt',



        close: false



    },



        states: [



            new wp.media.controller.Library({



                title:     'Chọn ảnh và cắt', 



                library:   wp.media.query({ type: 'image' }),



                multiple:  false,



                date:      false,



                priority:  20,



                suggestedWidth: 96,



                suggestedHeight: 96



            }),



            new wp.media.controller.CustomizeImageCropper({ 



                imgSelectOptions: myTheme_calculateImageSelectOptions,



                control: cropControl



            })



        ]



    }); 



    mediaUploader.on('cropped', function(croppedImage) {



        var url = croppedImage.url,



            attachmentId = croppedImage.id,



            w = croppedImage.width,



            h = croppedImage.height;



            myTheme_setImageFromURL_avt(url, attachmentId, w, h);            



    });







    mediaUploader.on('skippedcrop', function(selection) {



        var url = selection.get('url'),



            w = selection.get('width'),



            h = selection.get('height');



            myTheme_setImageFromURL_avt(url, selection.id, w, h);            



    });        



    



  mediaUploader.on('select', function(){



   var attachment = mediaUploader.state().get( 'selection' ).first().toJSON();



   if (     cropControl.params.width  === attachment.width 



            &&   cropControl.params.height === attachment.height 



            && ! cropControl.params.flex_width 



            && ! cropControl.params.flex_height ) {



                myTheme_setImageFromAttachment_avt( attachment );



            mediaUploader.close();



        } else {



            mediaUploader.setState( 'cropper' );



        }



  });



  mediaUploader.open();



  });



jQuery('#avt-delete-button').click(function(e){          



      $('#avt').val(null);      



    $('#avt-icon').css('background-image', 'url("")');



  });



//color pick//



    $('.ir').wpColorPicker();



jQuery('#site').change(function(e){          



var websitee=$(this).val();



 $('#websitee').val(websitee);



  });



//tabs content//



    $("#tabs li:first").attr("id","current"); 



    $("#content div:first").fadeIn(); 



    $('#tabs a').click(function(e) {



        e.preventDefault();



        $(".tab").css('display', 'none'); 



        $("#tabs li").attr("id",""); 



        $(this).parent().attr("id","current"); 



        $('#' + $(this).attr('title')).fadeIn(); 



    });



});