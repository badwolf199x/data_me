jQuery(function($){



//Add class to header

    //caches a jQuery object containing the header element

    var header = $(".main-header");

    $(window).scroll(function() {

        var scroll = $(window).scrollTop();

        function cssbackground()
        {

        }

        if( $(window).width() >991 ){

            if (scroll >= 120) {

                header.addClass("affix-mobile");

            } else {
                header.removeClass("affix-mobile");
            }

        }else{

            if (scroll >= 120) {

                header.addClass("scroll-menu");

            } else {

                header.removeClass("scroll-menu");

            }

        }

    });



//Add element to menu

$(document).ready(function()

{

    $("body").wrapInner(' <div class="main-body"></div> ');



    $(".menu-item-has-children ul li:first-child a").removeAttr("href");



    $(".wpcf7-submit").wrap(' <a class="ecx-button dark"></a> ');



    if ( $(window).width() >991 ){



        $( ".menu-item-has-children > a" ).append(' <span class="icon-nav icon-nav--down"><i class="bi bi-chevron-down"></i></span> ');

        $(".menu-item-has-children > ul").wrap(' <div class="dropdown-wrapper dropdown-width--auto"><div class="dropdown-content"><div class="dropdown-list"></div></div></div> ');

        

    }else {



        $("ul#menu-main-menu:not(.subnav-children) > .menu-item-has-children > a").removeAttr("href");

        $( ".menu-item-has-children > a" ).after(' <span class="icon-subnav"><i class="bi bi-chevron-right"></i></span> ');

        $( ".menu-item-has-children > ul" ).addClass(' subnav-children ');

    }



})



//Add class active when click hamburger menu

$( '.ecx-hamburger-menu' ).on( "click", function(e) {

    $( '.main-body' ).toggleClass( "sidebar-move" );

    $( '.site-overlay' ).toggleClass( "active" );

    e.stopPropagation()

});



$( '.site-close-handle' ).on( "click", function(e) {

    $( '.site-sidebar' ).removeClass( "active" );

    $( '.site-overlay' ).removeClass( "active" );

    $( '.main-body' ).removeClass( "sidebar-move" );

    e.stopPropagation()

});



$( '.ecx-hamburger-menu' ).on( "click", function(e) {

    if( $(window).width() >991 ) {

        $( '#site-sidebar-modal' ).toggleClass( "active" );

        e.stopPropagation()

    }else {

        $( '#site-sidebar-nav-mb' ).toggleClass( "active" );

    }

})



$( '.primary-menu li.menu-item-has-children' ).on( "click", function(e) {

    $(this).toggleClass( "active" );

    $( 'li.menu-item-has-children' ).not(this).removeClass( "active" );

    e.stopPropagation()



})



//hidden sidebar
$(document).click(function (e){
    let drop = $('.site-sidebar');
    if (!drop.is(e.target) && drop.has(e.target).length === 0) {
        $( '.site-sidebar' ).removeClass( "active" );
        $( '.main-body' ).removeClass( "sidebar-move" );
        $( '.site-overlay' ).removeClass( "active" );
    }
})


//search
if ( $(window).width() >991 ) {
$( '.main-header.header-transparent a.search_modal_button.header-button' ).on( "mouseenter", function(e) {
    if($('.wrap-button-search').hasClass('active'))
	{
		$('.wrap-button-search').removeClass('active');
	}
	else
	{
		$('.wrap-button-search').addClass('active');
	}
});
}
else
{
	$( '.main-header.header-transparent a.search_modal_button.header-button' ).on( "click", function(e) {
    if($('.wrap-button-search').hasClass('active'))
	{
		$('.wrap-button-search').removeClass('active');
	}
	else
	{
		$('.wrap-button-search').addClass('active');
	}
});
}
$(document).on('click', function (e) {
                            if ($(e.target).closest(".wrap-button-search.active .searchform").length === 0 && $(e.target).closest(".main-header.header-transparent a.search_modal_button.header-button").length === 0) {
                             $('.wrap-button-search').removeClass('active');
                              }
                          });

$(document).ready(function()
{
//show more portfolio
var paged_show_more_portfolio=2;
$(".show-more-portfolio").on("click", function(t) {
				$.ajax({
					type: "post",
					dataType: "json",
					url: window.location.protocol + "//" + window.location.host + "/" + "wp-admin/admin-ajax.php",
					data: {
						action: "portfolio_show_more",
					    paged: paged_show_more_portfolio,
					},
					success: function(response) {
                        if(response.success)
                        {
                        	if(response.data.html!='')
					    	{
					     		$('div#portfolio-items').append(response.data.html);
					     		var t = document.querySelector(".portfolio-items"),
					     		e = document.querySelectorAll(".ecx-portfolio__terms button");
					     		if (t && e.length && "undefined" != typeof Isotope) 
					     		{
					     			var r = new Isotope(t, {
					     				itemSelector: ".portfolio-item",
					     				layoutMode: "fitRows"
					     			});
					     			imagesLoaded(t).on("progress", (function() {
					     				r.layout()
					     			})), e.forEach((function(t) {
					     				t.addEventListener("click", (function(t) {
					     					document.querySelector(".ecx-portfolio__terms .is-active").classList.remove("is-active");
					     					var e = t.target;
					     					e.classList.add("is-active");
					     					var o = e.getAttribute("data-filter");
					     					r.arrange({
					     						filter: o
					     					})
					     				}))
					     			}))
					     		}
					    		paged_show_more_portfolio++;
					    		if(response.data.count<6)
					    		{
					    			$('.show-more-portfolio').remove();
					    		}
					    	}
					      	else
					    	{
							    $('.show-more-portfolio').remove();
						    }
                        }
                    },
                    error: function(){
                    	console.log('lỗi ajax');
                    }
		});
});
//table of content single
if($('.single.single-post').width()>0)
{
		var title=$('div#toc_container p.toc_title').text();
		var content=$('div#toc_container ul.toc_list');
	if(title!="")
		{
		$('.wrap-toc h4#accordion-headingOne button').text(title);
		$('.wrap-toc div#flush-collapseOne .accordion-body').append(content);
		$('div#toc_container').remove();
		}
    else
		{
			$('.wrap-toc').remove();
		}
}
	//autocomplete search
$(".searchform input#s").on("keyup", function(t) {
    		t.preventDefault();
    		var a = $(".searchform input#s").val().replace(/:|;|!|@@|#|\$|%|\^|&|\*|'|"|>|<|,|\.|\?|`|~|\+|=|_|\(|\)|{|}|\[|\]|\\|\|/gi, "");
    		if (a.length >= 3) {
				var data = {
					action: "autocomplete_search",
				    text: a,
				};
				$.ajax({
					type: "post",
					dataType: "json",
					url: window.location.protocol + "//" + window.location.host + "/" + "wp-admin/admin-ajax.php",
					data: data,
					success: function(t) {
						     var data = t.data;
							 $("#search-result").html(data.html);
							 $("#search-result").show();
						     var height_max= $(window).height()*40/100;
						     if($("#search-result").height()>height_max)
								 {
									 $('div#search-result ul').css('max-height','40vh');
									 $('div#search-result ul').css('overflow-y','scroll');
								 }
					}
				});
    		}else{
				 $("#search-result").html("");
			}
		});
})

//popup
function closepopup()
{
$('.wrap-popup .button-close').click(function(){
				$('.wrap-popup').removeClass('active');
			});
$('.wrap-popup').click(function (e){
    let drop = $('.wrap-popup .popup-form');
    if (!drop.is(e.target) && drop.has(e.target).length === 0) {
        $('.wrap-popup').removeClass('active');
    }
})
}

function showpopup()
{
	$('.wrap-popup').addClass('active');
}
$('a.call-in-action-btn.showpopup').click(function(){showpopup();closepopup();});

//animation
function add_animation()
{
    $('.corner-line-wrap').each(function() {
    	var top_of_element = $(this).offset().top;

		var element_height =  $(this).height();

        var bottom_of_element = $(this).offset().top + $(this).outerHeight();

		var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();

		var top_of_screen = $(window).scrollTop();

		if ((bottom_of_screen > top_of_element + element_height) && (top_of_screen < bottom_of_element)){

			if(!$(this).hasClass('added-animation'))

				{

					$(this).addClass('added-animation');

				}
		}

    });

	$('.has-animation').each(function() {

		var top_of_element = $(this).offset().top;

		var element_height =  $(this).height();

        var bottom_of_element = $(this).offset().top + $(this).outerHeight();

		var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();

		var top_of_screen = $(window).scrollTop();

		if ((bottom_of_screen > top_of_element + element_height) && (top_of_screen < bottom_of_element)){

			if(!$(this).hasClass('added-animation'))

				{

					$(this).addClass('added-animation');

					var animation_name=$(this).attr('animation-name');

					var animation_duration=$(this).attr('animation-duration');

					var animation_timing_function=$(this).attr('animation-timing-function');

					var animation_delay=$(this).attr('animation-delay');

					var animation_direction=$(this).attr('animation-direction');

					var animation_iteration_count=$(this).attr('animation-iteration-count');

					var animation_fill_mode=$(this).attr('animation-fill-mode');

					var animation_play_state=$(this).attr('animation-play-state');


					if(animation_name!=null)

						{

							$(this).css('animation-name',animation_name);

						}

					if(animation_duration!=null)

						{

							$(this).css('animation-duration',animation_duration);

						}

					if(animation_timing_function!=null)

						{

							$(this).css('animation-timing-function',animation_timing_function);

						}

					if(animation_delay!=null)

						{

							$(this).css('animation-delay',animation_delay);

						}

					if(animation_direction!=null)

						{

							$(this).css('animation-direction',animation_direction);

						}

					if(animation_iteration_count!=null)

						{

							$(this).css('animation-iteration-count',animation_iteration_count);

						}

					if(animation_fill_mode!=null)

						{

							$(this).css('animation-fill-mode',animation_fill_mode);

						}

					if(animation_play_state!=null)

						{

							$(this).css('animation-play-state',animation_play_state);

						}

				}

		} else {

        

		}

	});

}

$(window).scroll(function() {
	add_animation();
});

//swiper home slider
const  home_slider_swiper = new Swiper('.home-slider.swiper', {
	slidesPerView: 1,
	spaceBetween: 0,
	loop: true,
	direction:    'horizontal',
	initialSlide: 0,
	PreventInteractionOnTransition: true,
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: {
    	delay: 5000,
        disableOnInteraction	:false,
        pauseOnMouseEnter: false,
    },
    keyboard: {
        enabled: true,
        onlyInViewport: false,
    }
});
home_slider_swiper.on('slideChangeTransitionStart', function () {
  var data_html=$('.swiper-slide-active .slider-info-wrap').clone();
  $('.home-slider.swiper .background-info').html(data_html); 
  $('.home-slider.swiper .background-info .title.slider-title').addClass('sideUp');
});
if($('.home-slider.swiper').width()>0)
{
$('.home-slider.swiper .background-info').html($('.swiper-slide-active .slider-info-wrap').clone()); 
$('.home-slider.swiper .background-info .title.slider-title').addClass('sideUp');
var timeout_sideDown = setTimeout(function()
  	    {
  		    $('.home-slider.swiper .background-info .title.slider-title').addClass('sideDown');
  	    },4350);  
var $div = $('.home-slider.swiper .swiper-wrapper .swiper-slide');
var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    if (mutation.attributeName === "class") {
    	clearTimeout(timeout_sideDown);
    	timeout_sideDown = setTimeout(function()
  	    {
  		    $('.home-slider.swiper .background-info .title.slider-title').addClass('sideDown');
  	    },4500);   
    }
  });
});
observer.observe($div[0], {
  attributes: true
});
}

//swiper slider core values
var swiper_core_values = new Swiper('#core-values .swiper', {
  loop:true,
  slidesPerView: 1,
  spaceBetween: 20,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
  autoplay: {
   delay: 5000,
   disableOnInteraction	:false,
   pauseOnMouseEnter: true,
 },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });

//swiper home slider partner
const  home_slider_partner_swiper = new Swiper('.home-partner-slider.swiper', {
	slidesPerView: 1,
    spaceBetween: 30,
	breakpoints: {
	1400: {
      slidesPerView: 4,
      spaceBetween: 30,
    },	
    1200: {
      slidesPerView: 4,
      spaceBetween: 30,
    },
    992: {
      slidesPerView: 3,
      spaceBetween: 30
    },
	768: {
      slidesPerView: 3,
      spaceBetween: 30
    },
	500: {
      slidesPerView: 2,
      spaceBetween: 30
    },
    },
	loop: true,
	direction:    'horizontal',
	initialSlide: 0,
	PreventInteractionOnTransition: true,
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    },
    autoplay: {
    	delay: 5000,
        disableOnInteraction	:false,
        pauseOnMouseEnter: true,
    },
    keyboard: {
        enabled: true,
        onlyInViewport: false,
    }
});

    //counter 
	add_animation();

	$(window).scroll(widthheader);
	widthheader();
	function widthheader() 
	{
		if($('.header-corner-line-wrap').length>0)
 		{
 			$('.header-corner-line-wrap').each(function() {
 				var top_of_element = $(this).offset().top;
 				var element_height =  $(this).height();
 				var bottom_of_element = $(this).offset().top + $(this).outerHeight();
 				var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
 				var top_of_screen = $(window).scrollTop();
 				if ((bottom_of_screen > top_of_element + element_height) && (top_of_screen < bottom_of_element)){
 					if(!$(this).hasClass('added-animation'))
 					{
 						$(this).addClass('added-animation');
 						$(this).find('.header-corner-line-wrap-before').animate({height: "15px",width: "15px"}, 500);
 			            $(this).find('.header-corner-line-wrap-after').animate({height: "15px",width: "15px"}, 500);
 					}
 			    }
 			});
	    }
    }

	$(window).scroll(startProgress);
	startProgress();
	function startProgress() 
	{
		if($('.skill-wrap').length>0)
 		{
 			function run_progress($this,duration)
 			{
 				jQuery({
		    			Counter: 0
	    			}).animate({
		    			Counter: $this.find('.skill-percent').text().replace(/%/g, '')/100
		     		}, {
			    		duration: duration,
			    		easing: 'swing',
			    		step: function() {
			    			$this.find('.skill-line').css('transform','scale('+this.Counter+',1)');
			    		},
			    		complete: function() {
				    		$this.find('.skill-line').css('transform','scale('+$this.find('.skill-percent').text().replace(/%/g, '')/100+',1)');
				     	}
				    });
 			}
            var top_of_element = $('.skill-wrap').offset().top;
	    	var element_height =  $('.skill-wrap').height();
            var bottom_of_element = $('.skill-wrap').offset().top + $('.skill-wrap').outerHeight();
	    	var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
	    	var top_of_screen = $(window).scrollTop();
	    	if ((bottom_of_screen > top_of_element + element_height) && (top_of_screen < bottom_of_element)) {
	    		$(window).off("scroll", startProgress);
	    		$('.skill').each(function() {
	    			var $this = $(this);
	    			setTimeout(function()
	    			{
	    				run_progress($this,$(this).attr('duration'));
	    			},$(this).attr('delay'))
		    	});
		    }
	    }
    }

    $(window).scroll(startCounter);
	startCounter();
	function startCounter() 
	{
		if($('.statistics-content-wrap').length>0)
 		{
            var top_of_element = $('.statistics-content-wrap').offset().top;
	    	var element_height =  $('.statistics-content-wrap').height();
            var bottom_of_element = $('.statistics-content-wrap').offset().top + $('.statistics-content-wrap').outerHeight();
	    	var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
	    	var top_of_screen = $(window).scrollTop();
	    	if ((bottom_of_screen > top_of_element + element_height) && (top_of_screen < bottom_of_element)) {
	    		$(window).off("scroll", startCounter);
	    		$('.counter-value').each(function() {
	    			var $this = $(this);
	    			jQuery({
		    			Counter: 0
	    			}).animate({
		    			Counter: $this.text().replace(/,/g, '')
		     		}, {
			    		duration: 1000,
			    		easing: 'swing',
			    		step: function() {
			    			$this.text(commaSeparateNumber(Math.floor(this.Counter)));
			    		},
			    		complete: function() {
				    		$this.text(commaSeparateNumber(this.Counter));
				     	}
				    });
		    	});
		    	function commaSeparateNumber(num) {
		    		return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    }
		    }
	    }
    }
});