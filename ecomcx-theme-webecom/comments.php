<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @package Ecomcx-theme
 */

// Comment Reply Script.
if ( comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}
?>
<div id="comments" class="comments-area">
<?php if ( have_comments() ) : ?>
		<div class="heading-wrap line-after">
            <h2 class="title text-left text-uppercase">
				BÌNH LUẬN
            </h2>
        </div>
		<?php the_comments_navigation(); ?>
        <div class="post-comments-content-wrap">
			<ol class="comment-list">
				<?php
				wp_list_comments(
					[
						'style'       => 'ol',
				        'short_ping'  => true,
				        'avatar_size' => 42,
			        ]
				);
				?>
			</ol>
	    </div>
		<?php the_comments_navigation(); ?>
<?php endif; ?>

<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
	<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'ecomcx-theme' ); ?></p>
<?php endif; ?>
	    <div class="post-comments-form-wrap">
<?php
			
$args_comment_form = array(
    'fields' => apply_filters(
        'comment_form_default_fields', array(
            'author' =>'<div class="comment-form-author">' . '<input id="author" placeholder="Tên của bạn '. ( $req ? '*' : '' ).'" name="author" type="text" value="' .
                esc_attr( $commenter['comment_author'] ) . '" aria-required="true" /></div>'
                ,
            'email'  => '<div class="comment-form-email"><input id="email" placeholder="Email của bạn '.( $req ? '*' : '' ).'" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ). '" aria-required="true" /></div>',
            'url'    => '',
            'cookies'=> '',
        )
    ),
    'comment_field' => '<div class="comment-form-comment"><textarea id="comment" name="comment" placeholder="Viết bình luận…" cols="100" rows="6" aria-required="true"></textarea></div>',
    'comment_notes_after' => '',
    'comment_notes_before' => '',
    'title_reply' => 'viết BÌNH LUẬN',
    'title_reply_to' => 'Trả lời BÌNH LUẬN',
    'title_reply_before'=>'<div class="heading-wrap line-after"><h2 class="title text-left text-uppercase">',
    'title_reply_after'=>'</h2></div>',
    'label_submit'=>'gửi bình luận'

);
comment_form( $args_comment_form );
?>
	   </div>
</div>
