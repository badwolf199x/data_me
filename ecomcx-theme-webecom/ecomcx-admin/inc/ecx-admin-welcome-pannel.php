<?php

//*Xóa widget mặc định trong trang quản trị*/

    function ecx_remove_default_admin_widget() {
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    }
    
    add_action( 'wp_dashboard_setup', 'ecx_remove_default_admin_widget' );  
    
    remove_action( 'welcome_panel', 'wp_welcome_panel' );

//* Widget chào mừng */

   function ecx_welcome_pannel() { ?>

    	<div class="welcome-panel-content">
    		<h2 class="welcome-title"> 
    			<?php echo 'Xin chào <b><a href="'.get_edit_profile_url().'">'.get_the_author_meta('display_name', get_current_user_id()) .'</a></b>, chúc bạn một ngày mới tốt lành';
    			?>
         	</h2>
   		 	<p class="about-description"> 
   		 		<?php echo 'Chào mừng bạn đến với trang quản trị <b><a href="'.get_home_url().'" target="blank">'.get_bloginfo('name').'</a></b>';
   		 		?>
          	</p>

          	<div class="welcome-panel-column-container">
				<div class="welcome-panel-column">
					<?php if ( current_user_can( 'customize' ) ) : ?>
						<h3><?php _e( 'Get Started' ); ?></h3>
						<a class="button button-primary button-hero load-customize hide-if-no-customize" href="<?php echo wp_customize_url(); ?>"><?php _e( 'Customize Your Site' ); ?></a>
					<?php endif; ?>
						<a class="button button-primary button-hero hide-if-customize" href="<?php echo admin_url( 'themes.php' ); ?>"><?php _e( 'Customize Your Site' ); ?></a>
					<?php if ( current_user_can( 'install_themes' ) || ( current_user_can( 'switch_themes' ) && count( wp_get_themes( array( 'allowed' => true ) ) ) > 1 ) ) : ?>
					<?php $themes_link = current_user_can( 'customize' ) ? add_query_arg( 'autofocus[panel]', 'themes', admin_url( 'customize.php' ) ) : admin_url( 'themes.php' ); ?>
					<?php endif; ?>
					<p class="hide-if-no-customize">
						<?php
						/* URL to go to youtube channel */
						printf( __( 'hoặc <a href="https://www.youtube.com/playlist?list=PLevztXLp-PPNm60B2surocH5WClIbDPkv" target="blank">đi đến kênh youtube hướng dẫn của chúng tôi</a>' ), $themes_link );
						?>
					</p>
				</div>
			</div>
			<div class="welcome-panel-column">
				<h3><?php printf( 'Truy cập nhanh' ); ?></h3>
				<ul>
				<?php if ( 'page' === get_option( 'show_on_front' ) && ! get_option( 'page_for_posts' ) ) : ?>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-edit-page">' . __( 'Edit your front page' ) . '</a>', get_edit_post_link( get_option( 'page_on_front' ) ) ); ?></li>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
				<?php elseif ( 'page' === get_option( 'show_on_front' ) ) : ?>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-menus">' . __( 'Manage menus' ) . '</a>', admin_url( 'nav-menus.php' ) ); ?></li>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Add a blog post' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
				<?php else : ?>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Write your first blog post' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add an About page' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-setup-home">' . __( 'Set up your homepage' ) . '</a>', current_user_can( 'customize' ) ? add_query_arg( 'autofocus[section]', 'static_front_page', admin_url( 'customize.php' ) ) : admin_url( 'options-reading.php' ) ); ?></li>
				<?php endif; ?>
					<li><?php printf( '<a href="%s" class="welcome-icon welcome-view-site">' . __( 'View your site' ) . '</a>', home_url( '/' ) ); ?></li>
				</ul>
		</div>
		<div class="welcome-panel-column welcome-panel-last">
			<h3><?php printf( 'Liên hệ ECOMCX' ); ?></h3>
			<ul>
			<li><?php printf( '<b><a class="welcome-company-name">CÔNG TY CỔ PHẦN GIẢI PHÁP ECOMCX</a></b>' )?></li>
			<li><?php printf( '<b><a class="ecx-icon"><i class="fas fa-location-arrow"></i></a> Địa chỉ:</b> Vạn Phúc, Thanh Trì, Hà Nội'); ?></li>
			<li><?php printf( '<b><a class="ecx-icon"><i class="fas fa-headphones"></i></a> CSKH 24/7:</b> <a href="tel:0931268639">0931 2686 39</a>' );?></li>
			<li><?php printf( '<b><a class="ecx-icon"><i class="fab fa-chrome"></i></a> Trang chủ:</b> <a href="https://ecomweb.net" target="blank">https://ecomweb.net </a> - <a href="https://ecomcx.com" target="blank">https://ecomcx.com</a>' ); ?></li>
			<li class="ecx-icon-info"><?php printf('<a href="https://facebook.com/ecomcx.offical" target="blank"><i class="fab fa-facebook"></i></a>');?>
				<?php printf('<a href="https://m.me/ecomcx.offical" target="blank"><i class="fab fa-facebook-messenger"></i></a>');?>
				<?php printf('<a href="https://www.instagram.com/phitran1508/" target="blank"><i class="fab fa-instagram"></i></a>');?>
				<?php printf('<a href="https://www.youtube.com/playlist?list=PLevztXLp-PPNm60B2surocH5WClIbDPkv" target="blank"><i class="fab fa-youtube"></i></a>');?>
			</li>
			</ul>
		</div>
    	</div>
    <?php
	}

	add_action( 'welcome_panel', 'ecx_welcome_pannel' );
