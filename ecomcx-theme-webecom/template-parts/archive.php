<?php
/**
 * The template for displaying archive pages.
 *
 * @package ECOMCX Theme
 */
?>
<div class="main-index">

	<!-- Begin Banner -->

        <section id="banner" class="archive section-banner position-relative d-flex align-items-center mb-50">

            <div class="container flex-wrap d-flex justify-content-between align-items-center">

                <div class="col-md-6 col-12 px-0 pe-lg-3 banner-wrap-info">

                	<?php 

                    if(is_category())

                    {

                        single_cat_title( '<h1 class="ecx-category-title text-uppercase">', '</h1>','' );

                    }

                    else

                    {

                        the_archive_title( '<h1 class="ecx-category-title text-uppercase">', '</h1>','' ); 

                    }

			        ?>

                </div>
                <div class="col-md-6 col-12 px-0 ps-lg-3 banner-wrap-info text-end">
                    <?php 
                        if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); 
                    ?>
                </div>

            </div>

        </section>

    <!-- End of Banner -->



    <!-- Begin content archive -->

        <section id="archive" class="section-archive position-relative">

            <div class="container d-flex flex-wrap">

            	<div class="col-md-8 col-12 pe-md-3 content-archive">

            		<div class="wrap-content-post flex-wrap justify-content-between d-flex">

            		    <?php if(have_posts()):

                            while ( have_posts() ) : the_post(); ?>

                            <article class="ecx-post ecx-flex-item">

                                <a href="<?php the_permalink() ?>">

                                    <div class="ecx-post-thumbnail">

                                        <?php the_post_thumbnail('large','') ?>

                                    </div>

                                </a>

                                <div class="ecx-post-info">

                                    <h2 class="ecx-post-title">

                                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>

                                    </h2>

                                    <div class="ecx-post-meta-data">
                                        <div class="ecx-post-meta">

                                            <span class="post-meta author d-inline-block">Bởi:&nbsp;
                                                <?php the_author(); ?>
                                            </span>

                                            <span class="post-meta archive d-inline-block">
                                                <?php the_category(', '); ?>
                                            </span>
 
                                            <span class="post-meta ecx-post-date d-inline-block">
                                                <?php echo get_the_date(); ?>
                                            </span>

                                        </div>

                                        <div class="ecx-post-description mt-3">

                                            <?php the_excerpt(); ?>

                                        </div>

                                        <a class="ecx-post-readmore" href="<?php the_permalink() ?>">Xem thêm

                                            <i class="bi bi-arrow-right-short"></i>

                                        </a>

                                    </div>

                                </div>

                            </article>

                        <?php endwhile; 

                        else:

                            echo '<h3 class="empty_post">Chuyên mục chưa có bài viết, vui lòng quay lại sau.</h3>';

                        endif; ?>

                    </div>

                        <?php global $wp_query;

                           $max_page=$wp_query->max_num_pages;

                           if($max_page>1) 

                            {   ?>

                                <div class="ecx-pagination">

                                    <?php   

                                            $big = 999999999;

                                            echo paginate_links( array(

                                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),

                                                'format' => '?paged=%#%',

                                                'prev_text'    => __('<i class="bi bi-chevron-compact-left"></i>'),

                                                'next_text'    => __('<i class="bi bi-chevron-compact-right"></i>'),

                                                'current' => max( 1, get_query_var('paged') ),

                                                'total' => $max_page,

                                                'type'         => 'list',

                                                'end_size'     => 3,

                                                'mid_size'     => 1,

                                            ) );

                                    ?>

                                </div>

                    <?php   }   ?>

                </div>

                <div class="col-md-4 col-12 ps-lg-3 sidebar">

                    <?php get_template_part( 'template-parts/section-chung/sidebar' ); ?>

                </div>

            </div>

        </section>

    <!-- End content archive -->



    <!-- Begin call in action -->

        <?php get_template_part( 'template-parts/section-chung/call_in_action' ); ?>

    <!-- End call in action -->

</div>