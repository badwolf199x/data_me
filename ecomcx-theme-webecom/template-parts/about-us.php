<?php
/**
 * The template for displaying about us pages.
 *
 * Template Name: About us
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); 
$id_setting_r=2134; 
$data=get_fields($id_setting_r);
$section_banner=$data['section_banner'];
$section_quote=$data['section_quote'];
$section_statistics=$data['section_statistics'];
$section_mision=$data['section_mision'];
$section_vision=$data['section_vision'];
$section_core_values=$data['section_core_values'];
?>

<div class="main-index">
    
<!-- Begin Banner -->
    <section id="header-page" class="contact section-banner position-relative d-flex align-items-center" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">

        <div class="container flex-wrap justify-content-between d-flex">
            <div class="section-title d-flex align-content-start flex-wrap justify-content-center header-corner-line-wrap mx-auto my-auto">
                <div class="header-corner-line-wrap-before"></div>
                <h1 class="page-heading text-center "><?= $section_banner['page_title']; ?></h1>
                <h5 class="page-sub-heading text-center mx-auto">
                    <?= $section_banner['sub_heading']; ?>
                </h5>
                <div class="header-corner-line-wrap-after"></div>
            </div>
        </div>
    </section>
<!-- End of Banner -->

<!-- Begin quote -->
    <section id="quote" class="section-quote position-relative">
        <div class="quote-overlay">
        </div> 
        <div class="container">
            <div class="quote-heading-wrap">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-quote" viewBox="0 0 16 16">
                              <path d="M12 12a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1h-1.388c0-.351.021-.703.062-1.054.062-.372.166-.703.31-.992.145-.29.331-.517.559-.683.227-.186.516-.279.868-.279V3c-.579 0-1.085.124-1.52.372a3.322 3.322 0 0 0-1.085.992 4.92 4.92 0 0 0-.62 1.458A7.712 7.712 0 0 0 9 7.558V11a1 1 0 0 0 1 1h2Zm-6 0a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1H4.612c0-.351.021-.703.062-1.054.062-.372.166-.703.31-.992.145-.29.331-.517.559-.683.227-.186.516-.279.868-.279V3c-.579 0-1.085.124-1.52.372a3.322 3.322 0 0 0-1.085.992 4.92 4.92 0 0 0-.62 1.458A7.712 7.712 0 0 0 3 7.558V11a1 1 0 0 0 1 1h2Z"/>
                </svg><br>
                <h2 class="quote-heading text-justify"><?= $section_quote['quote_text'] ?></h2>
            </div>
            <div class="quote-content-wrap d-flex flex-wrap">
                <div class="col-lg-6 col-12 px-0 pe-lg-3 wrap-img-quote">
                    <?= wp_get_attachment_image($section_quote['quote_image'],'large','',array('class'=>'img-quote')); ?>
                </div>
                <div class="col-lg-6 col-12 px-0 ps-lg-3 quote-content-item-wrap">
                    <div class="heading-wrap">
                        <h2 class="title text-left">
                            <?= $section_quote['title'] ?>
                        </h2>
                    </div>
                    <div class="quote-item-wrap">
                        <?php for($i=1;$i<=$section_quote['quote_item_count'];$i++) : ?>
                            <div class="quote-item d-flex">
                                <div class="wrap-circle">
                                    <i class="bi bi-circle-fill">
                                    <i class="bi bi-circle-fill"></i></i>
                                </div>
                                <span class="title text-justify">
                                    <?= $section_quote['quote_item_'.$i]['quote_text']; ?>
                                </span>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- End of quote -->

<!-- Begin Statistics -->
    <section id="statistics-2" class="section-statistics-2 position-relative" style="background-image:url('<?= $section_statistics['statistics_background_img'] ?>')">
        <div class="container">
            <div class="heading-wrap">
                        <h2 class="title text-center text-capitalize">
                            <?= $section_statistics['label_title'] ?>
                        </h2>
                    </div>
            <div class="d-flex align-items-top statistics-content-wrap flex-wrap">
                <?php for($i=1;$i<=4;$i++) : ?>
                    <div class="text-center col-md-3 col-6 statistics-item-wrap">
                        <div class="ecx-number-counter">
                            <span class="counter-value"><?= $section_statistics['counter_'.$i]['counter_value']; ?></span>
                            <span class="counter-value-sub"><?= $section_statistics['counter_'.$i]['counter_value_sub']; ?></span>
                        </div>
                        <div class="ecx-counter-title">
                            <?= $section_statistics['counter_'.$i]['counter_title']; ?>    
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </section>
<!-- End of Statistics -->

<!-- Begin mision -->
    <section id="mision" class="section-mision position-relative">
        <div class="mision-overlay">
        </div> 
        <div class="container">
            <div class="mision-content-wrap d-flex flex-wrap">
                <div class="col-lg-6 col-12 px-0 pe-lg-3 mision-content-item-wrap">
                    <div class="heading-wrap line-after">
                        <h4 class="sub-title text-left">
                            <?= $section_mision['sub_title'] ?>
                        </h4>
                        <h2 class="title text-left text-capitalize">
                            <?= $section_mision['title'] ?>
                        </h2>
                    </div>
                    <div class="mision-item-wrap">
                        <?php for($i=1;$i<=$section_mision['mision_item_count'];$i++) : ?>
                            <div class="quote-item d-flex">
                                <div class="wrap-circle">
                                    <i class="bi bi-circle-fill">
                                    <i class="bi bi-circle-fill"></i></i>
                                </div>
                                <span class="title text-justify">
                                    <?= $section_mision['mision_item_'.$i]['mision_text']; ?>
                                </span>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
                <div class="col-lg-6 col-12 px-0 ps-lg-3 wrap-img-mision">
                    <?= wp_get_attachment_image($section_mision['mision_image'],'large','',array('class'=>'img-mision')); ?>
                </div>
            </div>
        </div>
    </section>
<!-- End of mision -->

<!-- Begin vision -->
    <section id="vision" class="section-vision position-relative">
        <div class="vision-overlay">
        </div> 
        <div class="container">
            <div class="vision-content-wrap d-flex flex-wrap">
                <div class="col-lg-6 col-12 px-0 pe-lg-3  wrap-img-vision">
                    <?= wp_get_attachment_image($section_vision['vision_image'],'large','',array('class'=>'img-vision')); ?>
                </div>
                <div class="col-lg-6 col-12 px-0 ps-lg-3  vision-content-item-wrap">
                    <div class="heading-wrap line-after">
                        <h4 class="sub-title text-left">
                            <?= $section_vision['sub_title'] ?>
                        </h4>
                        <h2 class="title text-left text-uppercase">
                            <?= $section_vision['title'] ?>
                        </h2>
                    </div>
                    <div class="sub-heading-wrap">
                        <span class="sub-heading">
                            <?= $section_vision['sub_heading'] ?>
                        </span>
                    </div>
                    <div class="vision-item-wrap">
                        <?php for($i=1;$i<=$section_vision['vision_item_count'];$i++) : ?>
                            <div class="quote-item d-flex">
                                <div class="wrap-circle">
                                    <i class="bi bi-circle-fill">
                                    <i class="bi bi-circle-fill"></i></i>
                                </div>
                                <span class="title text-justify">
                                    <?= $section_vision['vision_item_'.$i]['vision_text']; ?>
                                </span>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- End of vision -->

<!-- Begin core values -->
    <section id="core-values" class="section-core-values position-relative">
        <div class="core-values-overlay">
        </div> 
        <div class="container">
            <div class="core-values-content-wrap d-flex flex-wrap">
                <div class="col-lg-6 col-12 px-0 pe-lg-3 core-values-content-item-wrap">
                    <div class="heading-wrap line-after">
                        <h4 class="sub-title text-left">
                            <?= $section_core_values['sub_title'] ?>
                        </h4>
                        <h2 class="title text-left text-capitalize">
                            <?= $section_core_values['title'] ?>
                        </h2>
                    </div>
                    <div class="core-values-swiper-slider-wrap">
                        <div class="swiper">
                            <div class="swiper-button-prev"><i class="bi bi-chevron-compact-left"></i></div>
                            <div class="swiper-wrapper">
                                <?php for($i=1;$i<=$section_core_values['core_values_slider_count'];$i++) : ?>
                                    <div class="swiper-slide">
                                        <div class="span-number text-center">
                                            <span>0<?= $i; ?></span>
                                        </div>
                                        <h3 class="title-slider text-center">
                                            <?= $section_core_values['core_values_slider_'.$i]['slider_title'] ?>
                                        </h3>
                                        <div class="content-slider">
                                            <?php for($j=1;$j<=$section_core_values['core_values_slider_'.$i]['line_count'];$j++) : ?>
                                                <span class="slider-line d-flex">
                                                    <i class="bi bi-check-lg"></i>
                                                    <p>
                                                        <?= $section_core_values['core_values_slider_'.$i]['line_'.$j] ?>
                                                    </p>
                                                </span>
                                            <?php endfor; ?>
                                        </div>
                                    </div>
                                <?php endfor; ?>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-next"><i class="bi bi-chevron-compact-right"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 px-0 ps-lg-3 wrap-img-core-values">
                    <?= wp_get_attachment_image($section_core_values['core_values_image'],'large','',array('class'=>'img-core-values')); ?>
                </div>
            </div>
        </div>
    </section>
<!-- End of core values -->

<!-- Begin Customer -->
    <?php get_template_part( 'template-parts/section-chung/section_customer' ); ?>
<!--End of Customer -->

</div>

<?php get_footer(); ?>