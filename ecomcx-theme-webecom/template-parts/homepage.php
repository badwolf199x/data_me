<?php

/**

 * The template for displaying homepage pages.

 *

 * Template Name: Trang chủ

 * 

 * @package ECOMCX Theme

 */

?>



<?php get_header();

//setting riêng

if(function_exists('get_field'))
{
$id_setting_r=82; 
$data=get_fields($id_setting_r);
$section_slider=$data['section_slider'];
$section_about=$data['section_about'];
$section_skill=$data['section_skill'];
$section_portfolio=$data['section_portfolio'];
$section_service=$data['section_service'];
$section_partner=$data['section_partner'];
$section_blog=$data['section_blog'];

$id_setting_public=2140;
$data_public=get_fields($id_setting_public);
$section_package=$data_public['section_package'];

}

$args_portfolio = array(
            'post_type'      => 'portfolio',
            'post_status'    => 'publish',
            'posts_per_page' => '6',
            'post__not_in'   => array( $post->ID ),
            'orderby'        =>'meta_value date',
            'order'        =>'DESC',
            'meta_key'=>'_is_featured_project',
        );
$portfolios=get_posts($args_portfolio);

if(get_option( 'sticky_posts' )!=null)
{
$args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 3,
            'orderby'        =>'date',
            'order'        =>'DESC',
            'post__in'            => get_option( 'sticky_posts' ),
            'ignore_sticky_posts' => 1,
        );
$blog_featured_sticky=get_posts($args_blog_sticky);
if(count($blog_featured_sticky)<3)
{
    $args_blog = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 3-count($blog_featured_sticky),
            'orderby'        =>'date',
            'order'        =>'DESC',
            'post__not_in'            => get_option( 'sticky_posts' ),
            'ignore_sticky_posts' => 1,
        );
$blog_featured=get_posts($args_blog);
}
$blog_featured_sticky=array_merge($blog_featured_sticky,$blog_featured);
}
elseif(get_option( 'sticky_posts' )==null)
{
    $args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 3,
            'orderby'        =>'date',
            'order'        =>'DESC',
        );
    $blog_featured_sticky=get_posts($args_blog_sticky);
}

$count_text=array('Zero','One','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten');

?>



<div class="main-index">


<!-- Begin Home Slider -->
    <section class="home-slider flex-wrap align-items-end d-flex " style="background-image:url('')">
        <div id="home-slider" class="home-slider swiper">
            <div class="swiper-wrapper">
                <?php for($i=1;$i<=$section_slider['slider_count'];$i++) : $slider=$section_slider['slider_'.$i] ?>
                <div class="swiper-slide">
                    <?= wp_get_attachment_image($slider['slider_image'],'full','',array('class'=>'slider-background-img')); ?>
                    <div class="slider-info-wrap py-4">
                        <div class="container ms-50">
                            <div class="title-wrap">
                                <h1 class="title slider-title"><?= $slider['slider_title'] ?></h1>
                            </div>
                            <a class="ecx-btn-more ecx-circle-3 mt-4 ms-3" href="<?= $slider['slider_link'] ?>"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
            <div class="background-info"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>
<!-- End of Home Slider -->

<!-- Begin Home About -->
    <section id="home-about" class="home-about d-flex  flex-wrap">
            <div class="section-img-wrap col-12 col-md-6">
                <?= wp_get_attachment_image($section_about['section_image'],'large','',array('class'=>'section-img')); ?>
            </div>
            <div class="content-wrap col-12 col-md-6 px-50 py-50 d-flex flex-wrap align-content-center" style="background-image: url('<?= $section_about['section_background'] ?>')">
                <h2 class="title section-title text-uppercase mb-4">
                    <?= $section_about['section_title'] ?>
                </h2>
                <div class="section-content text-justify">
                    <?= $section_about['section_content'] ?>
                </div>
                <a class="ecx-btn-more ecx-circle-3 mt-lg-5 mt-4 ms-auto" href="<?= $section_about['section_link'] ?>"><i class="bi bi-plus"></i></a>
            </div>
    </section>
<!-- End of Home About -->

<!--Begin Home Skill -->
    <section id="home-skill" class="home-skill d-flex flex-wrap">
        <div class="content-wrap col-12 col-md-6 px-50 py-50 d-flex flex-wrap align-content-center" style="background-image: url('<?= $section_skill['section_background'] ?>')">
            <h2 class="title section-title text-uppercase mb-4">
                <?= $section_skill['section_title'] ?>
            </h2>
            <div class="section-content skill-wrap">
                <?php $duration=2000; $delay=0; 
                for($i=1;$i<=$section_skill['counter_progress_count'];$i++) : $progress=$section_skill['progress_'.$i] ;?>
                <div class="skill d-flex flex-wrap justify-content-between" delay="<?= $delay ?>" duration="<?= $duration ?>">
                    <div class="skill-title text-uppercase mt-4">
                        <?= $progress['progress_title'] ?>
                    </div>
                    <div class="skill-percent mt-4">
                        <?= $progress['progress_percent'] ?>%
                    </div>
                    <div class="skill-line-wrap mt-4">
                        <div class="skill-line" style="transform:scale(0,1)"></div>
                    </div>
                </div>
                <?php $duration+=500; $delay+=300; endfor; ?>
            </div>
        </div>
        <div class="section-img-wrap col-12 col-md-6">
            <?= wp_get_attachment_image($section_skill['section_image'],'large','',array('class'=>'section-img')); ?>
        </div>
    </section>
<!--End of Home Skill -->

<!--Begin Home Package -->
    <section id="package" class="package d-flex">
        <div class="content-wrap d-flex flex-wrap align-content-center" >
            <div class="section-title py-50 px-50 d-flex align-content-start flex-wrap" style="background-image: url('<?= $section_package['section_background'] ?>')">
                <h3 class="sub-title text-center mb-3">
                    <?= $section_package['section_sub_title'] ?>
                </h3>
                <h2 class="title text-uppercase text-center mb-4">
                    <?= $section_package['section_title'] ?>
                </h2>
                <div class="overlay" style="background-color:rgba(0,0,0,0.6)">
                </div>
            </div>
            <div class="section-content d-flex flex-wrap justify-content-between pb-50 container">
                <?php for($i=1;$i<=3;$i++) : $package=$section_package['package_'.$i] ?>
                <div class="package-item-table py-50 px-lg-4 px-3">
                    <h4 class="title package-item-title text-uppercase text-center mb-lg-5 mb-3">
                        <?= $package['package_title'] ?>
                    </h4>
                    <div class="package-content text-center">
                        <?= $package['package_content'] ?>
                    </div>
                    <a class="ecx-btn-more ecx-circle-3 mt-4 mx-auto" href="<?= $package['package_link'] ?>"><i class="bi bi-plus"></i></a>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </section>
<!--End of Home Package -->

<!-- Begin Home Portfolio -->
    <section id="home-portfolio" class="home-portfolio d-flex">
        <div class="content-wrap d-flex flex-wrap align-content-center container pb-50" >
            <div class="section-title d-flex align-content-start flex-wrap corner-line-wrap mx-auto">
                <h2 class="title text-uppercase text-center mb-3">
                    <?= $section_portfolio['section_title'] ?>
                </h2>
                <h5 class="sub-title text-center mx-auto">
                    <?= $section_portfolio['section_sub_title'] ?>
                </h5>
            </div>
            <div class="section-content d-flex flex-wrap justify-content-between">
                <?php $temp_post = $GLOBALS['post'];
                foreach ( $portfolios as $portfolio ) {
                    setup_postdata( $portfolio );
                    $GLOBALS['post'] = $portfolio; ?>
                    <div class="ecx-portfolio-item">
                        <a href="<?php echo esc_url( get_the_permalink() ); ?>" rel="bookmark" class="ecx-portfolio-item__media">
                            <?php echo wp_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'medium' ) ?>     
                        </a>
                        <div class="ecx-portfolio-item-info px-4 d-flex flex-wrap align-items-stretch">
                            <h4 class="title ecx-portfolio-title text-center"><?php echo get_the_title(); ?></h4>
                            <a class="ecx-btn-more ecx-circle-3 mt-4 mx-auto" href="<?php echo esc_url( get_the_permalink() ); ?>" rel="bookmark">
                                <i class="bi bi-plus"></i>
                            </a>
                        </div>
                    </div>
                <?php }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>
            </div>
            <a href="/du-an/" class="ecx-button call-in-action-btn mx-auto">Xem thêm</a>
        </div>
    </section>
<!-- End of Home Portfolio -->

<!-- Begin Home Service -->
    <section id="home-service" class="home-service d-flex">
        <div class="content-wrap d-flex flex-wrap align-content-center pb-50" >
            <?php for($i=1;$i<=3;$i++) : $accordion=$section_service['accordion_'.$i]?>
            <div class="accordion-item">
                <button class="accordion-button <?= ($i==1) ? '' : 'collapsed'?>" type="button" data-bs-toggle="collapse" data-bs-target="<?= '#flush-collapse'.$count_text[$i]; ?>" aria-expanded="true" aria-controls="<?= 'flush-collapse'.$count_text[$i]; ?>" href="<?= '#flush-collapse'.$count_text[$i]; ?>">
                    <h3 class="accordion-header text-uppercase py-3 container"  id="<?= 'accordion-heading'.$count_text[$i]; ?>">
                        <?= $accordion['accordion_header'] ?>
                    </h3>
                </button>
                <div id="<?= 'flush-collapse'.$count_text[$i]; ?>" class="accordion-collapse collapse <?= ($i==1) ? 'show' : ''?>" aria-labelledby="<?= 'accordion-heading'.$count_text[$i]; ?>" data-bs-parent="#home-service">
                    <div class="accordion-body container d-flex flex-wrap">
                        <div class="accordion-content-wrap col-12 col-md-6 d-flex flex-wrap align-content-center" style="background-image: url('')">
                            <h3 class="title accordion-title text-uppercase mb-4">
                                <?= $accordion['accordion_body']['accordion_title'] ?>
                            </h3>
                            <div class="accordion-content text-justify">
                                <?= $accordion['accordion_body']['accordion_content'] ?>
                            </div>
                            <a class="ecx-btn-more ecx-circle-3 mt-lg-5 mt-4 ms-auto" href="<?= $accordion['accordion_body']['accordion_link'] ?>"><i class="bi bi-plus"></i></a>
                        </div> 
                        <div class="accordion-img-wrap col-12 col-md-6">
                           <?= wp_get_attachment_image($accordion['accordion_body']['accordion_image'],'large','',array('class'=>'accordion-img')); ?>
                        </div>                                   
                    </div>
                </div>
            </div>
            <?php endfor ?>
        </div>
    </section>
<!-- End of Home Service -->

<!-- Begin Home Partner -->
    <section id="home-partner" class="home-partner d-flex">
        <div class="content-wrap d-flex flex-wrap align-content-center container pb-50" >
            <div class="section-title d-flex align-content-start flex-wrap corner-line-wrap mx-auto">
                <h2 class="title text-uppercase text-center mb-3">
                    <?= $section_partner['section_title'] ?>
                </h2>
                <h5 class="sub-title text-center mx-auto">
                    <?= $section_partner['section_sub_title'] ?>
                </h5>
            </div>
            <div class="section-content d-flex flex-wrap justify-content-between">
                <div id="partner-slider" class="home-partner-slider swiper">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img class="slider-partner-img" src="/wp-content/uploads/2022/03/partner-img-logo.png">
                        </div>
                        <div class="swiper-slide">
                            <img class="slider-partner-img" src="/wp-content/uploads/2022/03/partner-img-logo_hb.png">
                        </div>
                        <div class="swiper-slide">
                            <img class="slider-partner-img" src="/wp-content/uploads/2022/03/partner-img-logo_hd.png">
                        </div>
                        <div class="swiper-slide">
                            <img class="slider-partner-img" src="/wp-content/uploads/2022/03/partner-img-logo_hf.png">
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
<!-- End of Home Partner -->

<!-- Begin Home Blog -->
    <section id="home-blog" class="home-blog d-flex">
        <div class="content-wrap d-flex flex-wrap align-content-center container" >
            <div class="section-title d-flex align-content-start flex-wrap corner-line-wrap mx-auto">
                <h2 class="title text-uppercase text-center mb-3">
                    <?= $section_blog['section_title'] ?>
                </h2>
                <h5 class="sub-title text-center mx-auto">
                    <?= $section_blog['section_sub_title'] ?>
                </h5>
            </div>
            <div class="section-content d-flex flex-wrap justify-content-between">
                <?php $temp_post = $GLOBALS['post'];
                foreach ( $blog_featured_sticky as $blog ) {
                    setup_postdata( $blog );
                    $GLOBALS['post'] = $blog; ?>
                    <article class="ecx-post ecx-grid-item">
                        <a href="<?php the_permalink() ?>">
                            <div class="ecx-post-thumbnail">
                                <?php the_post_thumbnail() ?>
                            </div>
                        </a>
                        <div class="ecx-post-info">
                            <h3 class="ecx-post-title mb-3">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h3>
                            <div class="ecx-post-meta-data">
                                <div class="ecx-post-date mb-3">
                                    <i class="bi bi-clock"></i>&nbsp;<?php echo get_the_date(); ?>
                                </div>
                                <div class="ecx-post-excerpt">
                                    <?php the_excerpt('(more…)'); ?>
                                </div>
                                <a class="ecx-btn-more ecx-circle-3 mt-4 ms-auto" href="<?php the_permalink() ?>"><i class="bi bi-plus"></i></a>
                            </div>
                        </div>
                    </article>
                <?php }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>
            </div>
        </div>
    </section>
<!-- End of Home Blog -->


</div>



<?php get_footer(); ?>