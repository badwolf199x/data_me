<?php
/**
 * The template for displaying header.
 *
 * @package ECOMCX
 */
$site_name = get_bloginfo( 'name' );
$header_nav_menu = wp_nav_menu( [

	'theme_location' => 'menu-1',

	'fallback_cb' => false,

	'echo' => false,

] );
$year = date_i18n ('Y');
$content=get_option ('content_sidebar');
$numb=get_option ('hotline_sidebar');
$email=get_option ('email_sidebar');
?>

<header class="site-header">
    <div class="main-header header-transparent">
        <div class="container">
            <div class="header-middle d-flex align-items-center">
                <div class="header-left-logo">
                    <div class="site-logo">
                        <?php the_custom_logo(); ?>
                    </div>
                </div>   
                <div class="header-main-menu px-4">
                    <div class="main-header-menu">
                        <nav class="site-main-menu">
                            <?php echo $header_nav_menu; ?>
                        </nav>
                    </div>
                </div>   
                <div class="header-right-button">
                    <div class="header-wrap-icon">
                        <a class="search_modal_button header-button"><i class="bi bi-search"></i></a>
                        <div id="header-sidebar-bt" class="ecx-hamburger-menu" aria-hidden="true">
                            <span class="bar"></span>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
	    <div class="wrap-button-search">
		    <form role="search" method="get" id="searchform" class="searchform" action="<?= esc_url( home_url( '/' ) ) ?>">
			    <input type="text" placeholder="Nhập từ khóa..." value="" name="s" id="s" autocomplete="off"/>
                <input type="submit" id="searchsubmit" value="" />
			    <div id="search-result"></div>
            </form>
	    </div>
    </div>
</header>



<?php if(get_option ('enable_sidebar')==1): ?>

<!-- Site Sidebar -->



<div id="site-sidebar-modal" class="site-sidebar style--sidebar">

    

    <div id="sidebar-modal" class="sidebar-container">

        <div class="sidebar-container-last">

            

            <div class="sidebar-top text-center">

                    <?php the_custom_logo(); ?>



                    <button id="site-close-handle" class="site-close-handle" aria-label="Đóng" title="Đóng">

                        <span class="site-close-btn" aria-hidden="true">

                            <i class="bi bi-x"></i>

                        </span>

                    </button>   

                                     

            </div>



            <?= $content ?>



            <div class="sidebar-contact-area">

                <div class="contact-info-content text-center">

                    <h3>

                        <a href="tel:<?= str_replace(' ', '', $numb) ?>"><i class="bi bi-telephone"></i><?= $numb ?></a>

                    </h3>

                    <h3>

                        <a href="mailto:<?= $email ?>"><i class="bi bi-envelope"></i><?= $email ?></a>

                    </h3>

                    <?php get_template_part( 'template-parts/section-chung/social_link' ); ?>

                </div>

            </div>



            <div class="sidebar-copyright text-center">

                <p>© <?php echo $year; ?> <a href="<?php echo site_url() ?>">ecomweb.net</a></p>

                <p>Một sản phẩm của <a href="https://ecomcx.com">ECOMCX</a></p>

            </div>



        </div>

    </div>



</div>



<!-- End of Site Sidebar -->

<?php endif; ?>



<!-- Site Sidebar NAV Mobile -->



<div id="site-sidebar-nav-mb" class="site-sidebar style--sidebar">



    <div id="sidebar-modal" class="sidebar-container">

        <div class="sidebar-container-last">



            <div class="sidebar-top text-center">

                    <?php the_custom_logo(); ?>

                    <button id="site-close-handle" class="site-close-handle" aria-label="Đóng" title="Đóng">

                        <span class="site-close-btn" aria-hidden="true">

                            <i class="bi bi-x"></i>

                        </span>

                    </button>

            </div>



            <div class="main-navbar">

                <nav class="primary-menu">

                    <?php echo $header_nav_menu; ?>

                </nav>

            </div>

            

            <?= $content ?>



            <div class="sidebar-contact-area">

                <div class="contact-info-content text-center">

                    <h3>

                        <a href="tel:<?= str_replace(' ', '', $numb) ?>"><i class="bi bi-telephone"></i><?= $numb ?></a>

                    </h3>

                    <h3>

                        <a href="mailto:<?= $email ?>"><i class="bi bi-envelope"></i><?= $email ?></a>

                    </h3>

                    <?php get_template_part( 'template-parts/section-chung/social_link' ); ?>

                </div>

            </div>



            <div class="sidebar-copyright text-center">

                <p>© <?php echo $year; ?> <a href="<?php echo site_url() ?>">ecomweb.net</a></p>

                <p>Một sản phẩm của <a href="https://ecomcx.com">ECOMCX</a></p>

            </div>



        </div>

    </div>

</div>



<!-- End of Site Sidebar NAV Mobile -->



<div id="site-overlay" class="site-overlay"></div>

