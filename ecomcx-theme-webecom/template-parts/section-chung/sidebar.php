<?php

/**

 * The template for displaying siderbar.

 *

 * @package ECOMCX Theme

 */

?>

<div class="siderbar-section register flex-wrap justify-content-between d-flex align-content-around" style="background-image:url('/wp-content/uploads/2022/03/skill-col-img.png'); ">

	<div class="background-overlay">

    </div> 

	<h3 class="title form-title text-center">

		Tăng trưởng doanh thu và mở rộng kênh bán hàng với giải pháp 

	</h3>

	<div class="siderbar-section-content">

        <?php echo do_shortcode('[contact-form-7 id="2180" title="Sidebar form"]'); ?>

    </div>

</div>

<div class="siderbar-section menu-blog">

	<h4 class="title sidebar-title">

		Blog ECOMCX

	</h4>

	<div class="siderbar-section-content">

		<?= wp_nav_menu( [

			'theme_location' => 'menu-3',

	        'fallback_cb' => false,

	        'echo' => false,] ); ?>

	</div>

</div>

<div class="siderbar-section recent-post">

	<h4 class="title sidebar-title">

		Bài viết gần đây

	</h4>

	<div class="siderbar-section-content">

		<ul>

		<?php   $args_recent = array(

                                'posts_per_page' => 5,

                                'ignore_sticky_posts'=>true,

                        );

                $recent_query = new WP_Query( $args_recent ); 

                while ($recent_query-> have_posts()) : $recent_query -> the_post(); ?>  

                	<li>

                		<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>

                	</li>

        <?php   endwhile; wp_reset_postdata();?>

        </ul>

	</div>

</div>

<div class="siderbar-section follow-us">

	<h4 class="title sidebar-title">

		Follow us

	</h4>

	<div class="siderbar-section-content">

        <div class="fb-page" data-href="https://www.facebook.com/ecomwebthietkewebsite/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ecomcx.official/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ecomcx.official/">Ecomcx - Giá trị thương hiệu Việt</a></blockquote></div>

	</div>

	<div class="follow-icon">

        <?php get_template_part( 'template-parts/section-chung/social_link' ); ?>

	</div>

</div>

