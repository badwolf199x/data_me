<?php

/**

 * The template for displaying section customer.

 *

 * @package ECOMCX Theme

 */

if(function_exists('get_field'))

{

    $section_customer=get_field('section_customer',2140);

}

?>

    <section class="customer">

        

        <div class="row">

            

            <div class="col-md-6 col-12 px-0 pe-md-3 ">

                <div class="customer-list">



                     <div class="customer-title-wrap px-50">

                        <h2 class="ecx-h2-title"><?= $section_customer['title']; ?></h2>

                        <h4 class="sub-title"><?= $section_customer['sub_title']; ?></h4>

                    </div>



                    <div class="customer-item-wrap d-flex justify-content-center">

                        <?php for($i=1;$i<=$section_customer['customer_item_count'];$i++) : ?>
                            
                            <?php if($section_customer['customer_item_'.$i]['image_item']!='') : ?>
                            <div class="customer-item">
                                <a href="<?= $section_customer['customer_item_'.$i]['item_link']; ?>" target="_blank">
                                     <?= wp_get_attachment_image($section_customer['customer_item_'.$i]['image_item'],'large','',array('class'=>'img-customer mt-4 ')); ?>
                                </a>
                            </div>
                            <?php endif; ?>
                            

                        <?php endfor; ?>

                    </div> 



                    <div class="wrap-button d-flex mt-3">

                        <a href="<?= $section_customer['button_link']; ?>" class="mx-auto me-md-0 ecx-button call-in-action-btn"><?= $section_customer['button_text']; ?></a>          

                    </div>  

                     

                </div>

            </div>



            <div class="col-md-6 col-12 px-0 ps-md-3">

                <div class="customer-quote" style="background-image:url('<?= $section_customer['customer_quote_background'] ?>')">

                    <div class="customer-quote-overlay">

                        <div class="customer-quote-wrap">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-quote" viewBox="0 0 16 16">

                              <path d="M12 12a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1h-1.388c0-.351.021-.703.062-1.054.062-.372.166-.703.31-.992.145-.29.331-.517.559-.683.227-.186.516-.279.868-.279V3c-.579 0-1.085.124-1.52.372a3.322 3.322 0 0 0-1.085.992 4.92 4.92 0 0 0-.62 1.458A7.712 7.712 0 0 0 9 7.558V11a1 1 0 0 0 1 1h2Zm-6 0a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1H4.612c0-.351.021-.703.062-1.054.062-.372.166-.703.31-.992.145-.29.331-.517.559-.683.227-.186.516-.279.868-.279V3c-.579 0-1.085.124-1.52.372a3.322 3.322 0 0 0-1.085.992 4.92 4.92 0 0 0-.62 1.458A7.712 7.712 0 0 0 3 7.558V11a1 1 0 0 0 1 1h2Z"/>

                          </svg>

                            <p class="customer-quote-text"><?= $section_customer['customer_quote_text']; ?></p>



                            <div class="customer-info-wrap">

                                <div class="title text-capitalize"><?= $section_customer['customer_info']['customer_name'] ?></div>

                                <div class="sub-title"><?= $section_customer['customer_info']['customer_sub_name'] ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



        </div>



    </section>