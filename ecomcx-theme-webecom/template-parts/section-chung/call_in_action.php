<?php

/**

 * The template for displaying call in action.

 *

 * @package ECOMCX Theme

 */

if(function_exists('get_field'))

{

    $section_call_in_action=get_field('section_call_in_action',2140);

}

?>

<section id="call-in-action" class="section-call-in-action position-relative d-flex justify-content-center" style="background-image:url('<?= $section_call_in_action['background_img']?>')">

    <div class="call-in-action-overlay" style="background-color:rgba(0,0,0,0.6)">
    </div> 

    <div class="container d-flex align-items-center justify-content-center py-50">

    	<div class="call-in-action-title-wrap text-center">

    		<h4 class="sub-title text-center">

                <?= $section_call_in_action['sub_title'] ?>

            </h4>

            <h2 class="title text-center text-uppercase mb-3">

                <?= $section_call_in_action['title'] ?>

            </h2>

            <div class="wrap-button text-center">

                <a class="ecx-button dark call-in-action-btn showpopup">TƯ VẤN MIỄN PHÍ</a>

            </div>     

        </div>

    </div>

</section>