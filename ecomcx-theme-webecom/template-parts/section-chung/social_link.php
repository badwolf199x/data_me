<?php
/**
 * The template for displaying social link.
 *
 * @package ECOMCX Theme
 */
?>
<ul class="social">
    <?php for($i=1;$i<=5;$i++) : 
        $icon_link=get_option('icon_link_'.$i);
        $icon_main=get_option('icon_main_'.$i);
        $icon_graycase=get_option('icon_graycase_'.$i); ?>
		<li>
            <a href="<?= $icon_link ?>" target="_blank">
                <img src="<?= $icon_main ?>" alt="icon logo facebook" title="social facebook">
                <img src="<?= $icon_graycase ?>" alt="icon logo facebook graycase" title="social facebook">
            </a>
        </li>
    <?php endfor; ?>   
</ul>
