<?php

/**

 * The template for displaying single pages.

 *

 * @package ECOMCX Theme

 */

?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="main-index">

	<!-- Begin Banner -->

        <section id="banner" class="single section-banner position-relative d-flex align-items-end">

            <div class="banner-overlay">

            </div> 

            <div class="banner-background">

            	<img src="<?= get_the_post_thumbnail_url(get_the_id(),'full'); ?>" alt="Single Img banner" class="img-banner">

            </div>

            <div class="container flex-wrap d-flex">

                <div class="col-12 banner-wrap-info">

                	<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); 

                    the_title('<h1 class="ecx-post-title text-uppercase">', '</h1>'); 

			        ?>

			        <div class="ecx-post-meta">

			        	<span class="post-meta fst-italic date"><i class="bi bi-clock"></i>&nbsp;

			        		<b class="label">Ngày đăng: </b>

			        		<?= get_the_date(); ?>

			        	</span>

			        	<span class="post-meta fst-italic viewed"><i class="bi bi-eye"></i>&nbsp;

			        		<?= get_post_meta( get_the_id(), 'views', true ) ?>lượt xem

			        	</span>

			        	<span class="post-meta fst-italic author"><i class="bi bi-person-circle"></i>&nbsp;

			        		<b class="label">Đăng bởi: </b>

			        		<?php the_author(); ?>

			        	</span>

			        </div>

                </div>

            </div>

        </section>

    <!-- End of Banner -->



    <!-- Begin content single -->

        <section id="single" class="section-single position-relative">

            <div class="container d-flex flex-wrap">

            	<div class="col-md-8 col-12 pe-md-3 content-single">

            		<div class="post-content"> 

            			<div class="wrap-toc" id="home-overview-accordion"> 

            			<h4 class="accordion-header" id="accordion-headingOne">

                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne" href="#flush-collapseOne"></button>

                        </h4>

                        <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="accordion-headingOne" data-bs-parent="#home-overview-accordion" style="">

                            <div class="accordion-body">

                            </div>                              

                        </div>

                        </div>

            		    <?php the_content(); ?>

            		</div>

                    <div class="post-tags"> 

                    	<?php the_tags( '<div class="tag-links"><i class="bi bi-tags-fill"></i><span class="tags-label">Tags: </span>', ' , ', '</div>' ); ?>

                    </div>

                    <div class="like-share-button d-flex justify-content-end flex-wrap"> 

                    	<div class="zalo-share-button" data-href="" data-oaid="896146097299045189" data-layout="1" data-color="blue" data-customize="false"></div>

                    	<div class="zalo-follow-only-button" data-oaid="896146097299045189"></div>

                    	<div class="fb-like" data-href="https://www.facebook.com/ecomcx.official/" data-width="95px" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div>

                    	<div class="fb-share-button d-flex" data-href="https://www.facebook.com/ecomcx.official/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2Fecomcx.official%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>

                    </div>

                    <?php if(is_single()) : ?>
                    <div class="post-navigation d-flex">
                        <?php if(get_previous_post_link()!="") : ?>
                        <div class="post-prev d-flex align-items-center"><i class="bi bi-chevron-compact-left"></i>
                            <?php previous_post_link( '%link', '<span class="navigation-post-title">%title</span>', true ); ?>
                        </div> 
                        <?php endif; ?>
                        <?php if(get_next_post_link()!="") : ?>
                        <div class="post-next d-flex align-items-center">
                            <?php next_post_link( '%link', '<span class="navigation-post-title">%title</span>', true ); ?>
                        <i class="bi bi-chevron-compact-right"></i>
                        </div> 
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>

                    <div class="post-comments"> 

                    	<?php comments_template(); ?>

                    </div>

                </div>

                <div class="col-md-4 col-12 ps-lg-3 sidebar">

                    <?php get_template_part( 'template-parts/section-chung/sidebar' ); ?>

                </div>

            </div>

        </section>

    <!-- End content single -->



<?php   $args_related = array(

                                'posts_per_page' => 3,

                                'ignore_sticky_posts'=>true,

                                'category__in' => wp_get_post_categories(),

                                'exclude' => array( get_the_id() ),

                            );
        $related_post=get_posts($args_related);

                if(count($related_post)>0) : ?> 

    <!-- Begin related post -->

        <section id="related-post" class="section-related-post position-relative">

            <div class="container">

            	<div class="heading-wrap line-after">

                    <h2 class="title text-left text-uppercase">

                        Bài viết liên quan

                    </h2>

                </div>

                <div class="related-post-content-wrap d-flex justify-content-between flex-wrap">

                	<?php $temp_post = $GLOBALS['post'];
                foreach ( $related_post as $blog ) {
                    setup_postdata( $blog );
                    $GLOBALS['post'] = $blog; ?>
                    <article class="ecx-post ecx-grid-item">
                        <a href="<?php the_permalink() ?>">
                            <div class="ecx-post-thumbnail">
                                <?php the_post_thumbnail() ?>
                            </div>
                        </a>
                        <div class="ecx-post-info">
                            <h3 class="ecx-post-title mb-3">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h3>
                            <div class="ecx-post-meta-data">
                                <div class="ecx-post-date mb-3">
                                    <i class="bi bi-clock"></i>&nbsp;<?php echo get_the_date(); ?>
                                </div>
                                <div class="ecx-post-excerpt">
                                    <?php the_excerpt('(more…)'); ?>
                                </div>
                                <a class="ecx-btn-more ecx-circle-3 mt-4 ms-auto" href="<?php the_permalink() ?>"><i class="bi bi-plus"></i></a>
                            </div>
                        </div>
                    </article>
                <?php }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>

                </div>

            </div>

        </section>

    <!-- End of related post -->

<?php endif; ?>



    <!-- Begin call in action -->

        <?php get_template_part( 'template-parts/section-chung/call_in_action' ); ?>

    <!-- End of subcribe -->

</div>



	<?php

endwhile;



