<?php

/**

 * The template for displaying contact pages.

 *

 * Template Name: Contact us

 * 

 * @package ECOMCX Theme

 */

?>

<?php get_header(); 

$id_setting_r=244; 

$data=get_fields($id_setting_r);

$section_banner=$data['section_banner'];

$section_header=$data['section_header'];

$section_content=$data['section_content'];

?>
<div class="main-index">
<!-- Begin Banner -->

    <section id="header-page" class="contact section-banner position-relative d-flex align-items-center" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">

        <div class="container flex-wrap justify-content-between d-flex">
            <div class="section-title d-flex align-content-start flex-wrap justify-content-center header-corner-line-wrap mx-auto my-auto">
                <div class="header-corner-line-wrap-before"></div>
                <h1 class="page-heading text-center "><?= $section_banner['page_title']; ?></h1>
                <h5 class="page-sub-heading text-center mx-auto">
                    <?= $section_banner['sub_heading']; ?>
                </h5>
                <div class="header-corner-line-wrap-after"></div>
            </div>
        </div>

    </section>

<!-- End of Banner -->


<!-- Begin content contact -->

    <section id="content-page" class="section-content-contact position-relative">

    	<div class="title-contact line-after">

        	<h2 class="title text-center text-uppercase">

        		<?= $section_header ?>

        	</h2>

        </div>

        <div class="ecomcx-maps has-animation" animation-name="fadeInRight" animation-duration="1s" animation-delay="0"  animation-iteration-count="1" animation-fill-mode="forwards">

            <?= $section_content['map_iframe']; ?>

        </div>

        <div class="container flex-wrap align-items-center d-flex has-animation" animation-name="fadeInLeft" animation-duration="1s" animation-delay="0"  animation-iteration-count="1" animation-fill-mode="forwards">

                <div class="col-md-5 col-12 content-contact">

                	<h4 class="title_bf text-left">

			            <?= $section_content['sub_heading']; ?>

		            </h4>

		            <h3 class="title_bg text-uppercase text-center">

			            <?= $section_content['title']; ?>

		            </h3>

		            <div class="contact-info">

		            	<?= $section_content['contact_content']; ?>

		            </div>

                	<div class="contact-form">

		            	<?= do_shortcode('[contact-form-7 id="2136" title="Form liên hệ"]'); ?>

		            </div>

                </div>

        </div>

    </section>

<!-- End content contact -->



<!-- Begin call in action -->

    <?php get_template_part( 'template-parts/section-chung/call_in_action' ); ?>

<!-- End call in action -->

</div>
<?php get_footer(); ?>