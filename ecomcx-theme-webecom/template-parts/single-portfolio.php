<?php

/**

 * The template for displaying single portfolio pages.

 *

 * @package ECOMCX Theme

 */

?>

<?php while ( have_posts() ) : the_post(); 
$project_images = Ecx_Portfolio_Helper::get_portfolio_images_ids();

?>

<div class="main-index">

	<!-- Begin Banner -->

        <section id="banner" class="single portfolio section-banner position-relative d-flex align-items-center">

            <div class="container flex-wrap d-flex justify-content-between align-items-center">

                <div class="col-md-6 col-12 px-0 pe-lg-3 banner-wrap-info">

                    <h1 class="ecx-post-title text-uppercase">
                        Dự án
                    </h1>
    
                </div>
                <div class="col-md-6 col-12 px-0 ps-lg-3 banner-wrap-info text-end">
                    <?php 
                        if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); 
                    ?>
                </div>

            </div>

        </section>

    <!-- End of Banner -->



    <!-- Begin content single -->

        <section id="single" class="section-single portfolio position-relative">

            <div class="container d-flex flex-wrap">

            	<div class="col-12 content-single">
                    <div class="post-gallery portfolio">
                        <?= do_shortcode('[portfolio_gallery class="gallery_portfolio"]'); ?>
                    </div>
                    <div class="post-content">
                        <div class="grids">
                            <div class="project-content grid s8">
                                <?php the_title('<h2 class="ecx-post-title mb-4">', '</h2>'); ?>
                                <div class="content-portfolio mb-50">
                                    <?php echo get_the_content(); ?>
                                </div>
                                <div class="like-share-button d-flex justify-content-end flex-wrap"> 
                                    <div class="zalo-share-button" data-href="" data-oaid="896146097299045189" data-layout="1" data-color="blue" data-customize="false"></div>
                                    <div class="zalo-follow-only-button" data-oaid="896146097299045189"></div>
                                    <div class="fb-like" data-href="https://www.facebook.com/ecomcx.official/" data-width="95px" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div>
                                    <div class="fb-share-button d-flex" data-href="https://www.facebook.com/ecomcx.official/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2Fecomcx.official%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                </div>
                            </div>
                            <div class="project-meta grid s4">
                                <?php
                                    $template = ECX_PORTFOLIO_TEMPLATES . '/portfolio-meta.php';
                                load_template( $template, false );
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php if(is_singular('portfolio')) : ?>
                    <div class="post-navigation d-flex justify-content-between mb-5">
                        <div class="post-prev d-flex align-items-center">
                        <?php if(get_previous_post_link('%link', '<span class="navigation-post-title">%title</span>', false,'','portfolio_cat')!="") : ?>
                            <?php previous_post_link( '%link', '<i class="bi bi-chevron-compact-left"></i><span class="navigation-post-title">%title</span>', false,'','portfolio_cat' ); ?>
                        <?php endif; ?>
                        </div> 
                        <div class="site-logo d-flex justify-content-center align-items-center">
                            <?php the_custom_logo(); ?>
                        </div>
                        <div class="post-next d-flex align-items-center justify-content-end">
                        <?php if(get_next_post_link('%link', '<span class="navigation-post-title">%title</span>', false,'','portfolio_cat')!="") : ?>
                            <?php next_post_link( '%link', '<span class="navigation-post-title">%title</span><i class="bi bi-chevron-compact-right"></i>', false,'','portfolio_cat' ); ?>
                        <?php endif; ?>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>

            </div>

        </section>

    <!-- End content single -->

</div>



	<?php

endwhile;



