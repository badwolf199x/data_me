<?php
/**

 * The template for displaying portfolio page.

 *

 * Template Name: Portfolio page

 * 

 * @package ECOMCX Theme

 */
wp_enqueue_script( 'isotope' );
wp_enqueue_script( 'ecx-portfolio' );

get_header();

$option          = Ecx_Portfolio_Helper::get_options();
$portfolios_args = array(
            'page' =>1,
        );
$portfolios = Ecx_Portfolio_Helper::get_portfolios( $portfolios_args );
$portfolios_args_all = array(
            'per_page'=>-1,
            'page' =>1,
        );
$portfolios_all = Ecx_Portfolio_Helper::get_portfolios( $portfolios_args_all );
$categories = Ecx_Portfolio_Helper::get_categories_from_portfolios( $portfolios_all );

$all_button_text = esc_html( $option['all_categories_text'] );

$theme       = in_array( $option['portfolio_theme'], array( 'one', 'two' ) ) ? $option['portfolio_theme'] : 'one';
$items_class = 'grids portfolio-items';
$items_class .= ' fp-theme-' . $theme;

?>
<div class="main-index">

    <!-- Begin Banner -->

        <section id="banner" class="archive portfolio section-banner position-relative d-flex align-items-center">

            <div class="container flex-wrap d-flex justify-content-between align-items-center">

                <div class="col-md-6 col-12 px-0 pe-lg-3 banner-wrap-info">

                    <?php the_title('<h1 class="ecx-post-title text-uppercase">', '</h1>'); ?>
    
                </div>
                <div class="col-md-6 col-12 px-0 ps-lg-3 banner-wrap-info text-end">
                    <?php 
                        if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); 
                    ?>
                </div>

            </div>

        </section>

    <!-- End of Banner -->



    <!-- Begin content archive -->

        <section id="archive" class="portfolio section-archive position-relative">

            <div class="container d-flex flex-wrap">

                <div class="col-12  content-archive portfolio">

                    <div id="ecx-portfolio" class="ecx-portfolio">
                        <?php if ( $categories && count( $categories ) > 1 ) { ?>
                            <div class="ecx-portfolio__terms is-justify-center flex-wrap">
                                <button class="button is-active" data-filter="*"><?php echo $all_button_text; ?></button>
                                <?php foreach ( $categories as $category ) { ?>
                                    <button class="button" data-filter=".<?php echo esc_attr( $category->slug ); ?>">
                                        <?php echo esc_html( $category->name ); ?>
                                            
                                        </button>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div id="portfolio-items" class="<?php echo $items_class; ?>">
                                <?php
                                $temp_post = $GLOBALS['post'];
                                foreach ( $portfolios as $portfolio ) {
                                    setup_postdata( $portfolio );
                                    $GLOBALS['post'] = $portfolio;
                                    do_action( 'ecx_portfolio_loop_post', $portfolio );
                                }
                                wp_reset_postdata();
                                $GLOBALS['post'] = $temp_post;
                                ?>
                            </div>
                    </div>

                </div>
                <?php if(count($portfolios)==intval( $option['posts_per_page'] )) : ?>
                    <div class="col-12  btn-show-more-portfolio-wrap d-flex">
                        <a class="ecx-button call-in-action-btn mx-auto mt-5 show-more-portfolio">Tải thêm</a>
                    </div>
                <?php endif; ?>
            </div>

        </section>

    <!-- End content archive -->

</div>

<?php get_footer(); ?>