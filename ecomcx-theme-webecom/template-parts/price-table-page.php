<?php 

/**

 * The template for displaying price table pages.

 *

 * Template Name: Price table page

 * 

 * @package ECOMCX Theme

 */

?>

<?php get_header(); 
if(function_exists('get_field'))
{
$id_setting_r=2195; 
$data=get_fields($id_setting_r);
$section_banner=$data['section_banner'];
$section_process=$data['section_process'];
$section_call_in_action=$data['section_call_in_action_form'];

$id_setting_public=2140;
$data_public=get_fields($id_setting_public);
$section_package=$data_public['section_package'];

}
$count_text=array('Zero','One','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten');

?>

<div class="main-index">
<!-- Begin Banner -->

    <section id="header-page" class="price-service section-banner position-relative d-flex align-items-center" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">

        <div class="container flex-wrap justify-content-between d-flex">
            <div class="section-title d-flex align-content-start flex-wrap justify-content-center header-corner-line-wrap mx-auto my-auto">
                <div class="header-corner-line-wrap-before"></div>
                <h1 class="page-heading text-center "><?= $section_banner['page_title']; ?></h1>
                <h5 class="page-sub-heading text-center mx-auto">
                    <?= $section_banner['sub_heading']; ?>
                </h5>
                <div class="header-corner-line-wrap-after"></div>
            </div>
        </div>

    </section>

<!-- End of Banner -->

<!--Begin Package -->
    <section id="package" class="package d-flex">
        <div class="content-wrap d-flex flex-wrap align-content-center" >
            <div class="section-title py-50 px-50 d-flex align-content-start flex-wrap" style="background-image: url('<?= $section_package['section_background'] ?>')">
                <h3 class="sub-title text-center mb-3">
                    <?= $section_package['section_sub_title'] ?>
                </h3>
                <h2 class="title text-uppercase text-center mb-4">
                    <?= $section_package['section_title'] ?>
                </h2>
                <div class="overlay" style="background-color:rgba(0,0,0,0.6)">
                </div>
            </div>
            <div class="section-content d-flex flex-wrap justify-content-between pb-50 container">
                <?php for($i=1;$i<=3;$i++) : $package=$section_package['package_'.$i] ?>
                <div class="package-item-table py-50 px-lg-4 px-3">
                    <h4 class="title package-item-title text-uppercase text-center mb-lg-5 mb-3">
                        <?= $package['package_title'] ?>
                    </h4>
                    <div class="package-content text-center">
                        <?= $package['package_content'] ?>
                    </div>
                    <a class="ecx-btn-more mt-4 mx-auto text-uppercase ecx-button call-in-action-btn showpopup">TƯ VẤN MIỄN PHÍ</a>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </section>
<!--End of Package -->

<!-- Begin Process -->
    <section id="process" class="process d-flex pb-50">
        <div class="content-wrap d-flex flex-wrap align-content-center container" >
            <div class="section-title d-flex align-content-start flex-wrap corner-line-wrap mx-auto">
                <h2 class="title text-uppercase text-center mb-3">
                    <?= $section_process['section_title'] ?>
                </h2>
                <h5 class="sub-title text-center mx-auto">
                    <?= $section_process['section_sub_title'] ?>
                </h5>
            </div>
            <div id="content-process"class="section-content">
                <div class="tab-header mb-4">
                    <ul class="nav nav-pills nav-fill d-flex flex-wrap justify-content-center">
                    <?php for($i=1;$i<=3;$i++) : $tab=$section_process['section_tab_'.$i];?>
                        <li class="nav-item">
                            <button class="nav-link <?= ($i==1) ? 'active' : '' ?>" id="<?= sanitize_title($tab['tab_name'])?>-tab" data-bs-toggle="tab" data-bs-target="#<?= sanitize_title($tab['tab_name'])?>" type="button" role="tab" aria-controls="<?= sanitize_title($tab['tab_name'])?>" aria-selected="true">
                                <h5 class="tab-label text-uppercase mb-0"><?= $tab['tab_name'] ?></h5>
                            </button>
                        </li>
                    <?php endfor; ?>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <?php for($i=1;$i<=3;$i++) : $tab=$section_process['section_tab_'.$i];?>
                        <div class="tab-pane fade <?= ($i==1) ? 'show active' : '' ?>" id="<?= sanitize_title($tab['tab_name'])?>" role="tabpanel" aria-labelledby="<?= sanitize_title($tab['tab_name'])?>">
                            <?php for($j=1;$j<=$tab['process_count'];$j++) : $process=$tab['process_'.$j]; ?>
                                <div class="process_wrap d-flex flex-wrap justify-content-between <?= ($j%2==1) ? 'flex-lg-row-reverse' : '' ?> mb-3">
                                    <div class="process_img_wrap col-lg-6 col-12 px-0 ps-lg-3 text-center">
                                        <?= wp_get_attachment_image($process['process_image'],'large','',array('class'=>'process-img')); ?>
                                    </div>
                                    <div class="process_content_wrap col-lg-6 col-12 px-0 pe-lg-3">
                                       <h4 class="process_label">Bước<?= ' '.$j?></h4>
                                        <h5 class="process_title text-uppercase"><?= $process['process_title'] ?></h5>
                                        <div class="process_content"><?= $process['process_content'] ?></div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </section>
<!-- End of Process -->

<!-- Begin Call in action form -->
    <section id="call-in-action-form" class="section-call-in-action-form position-relative d-flex justify-content-center" style="background-image:url('<?= $section_call_in_action['background_img']?>')">

    <div class="call-in-action-overlay" style="background-color:rgba(0,0,0,0.6)">
    </div> 

    <div class="container d-flex align-items-center justify-content-center py-50">

        <div class="call-in-action-title-wrap text-center">

            <h4 class="sub-title text-center">

                <?= $section_call_in_action['sub_title'] ?>

            </h4>

            <h2 class="title text-center text-uppercase mb-3">

                <?= $section_call_in_action['title'] ?>

            </h2>

            <div class="wrap-button text-center">

                <?php echo do_shortcode('[contact-form-7 id="2142" title="Form subcribe"]'); ?>

            </div>     

        </div>

    </div>

</section>
<!-- End of Call in action form  -->

</div>
<?php get_footer(); ?>