<?php

if ( ! class_exists( 'Ecx_Portfolio' ) ) {
	/**
	 * Main Ecx_Portfolio Class.
	 *
	 * @class Ecx_Portfolio
	 */
	final class Ecx_Portfolio {

		/**
		 * Plugin unique slug
		 *
		 * @var string
		 */
		private $plugin_name = 'ecx-portfolio';

		/**
		 * Holds various class instances
		 *
		 * @var array
		 */
		private $container = array();

		/**
		 * Current version number
		 *
		 * @var string
		 */
		private $version = '1.5.0';

		/**
		 * Instance of this class
		 *
		 * @var self
		 */
		protected static $instance;

		/**
		 * Ensures only one instance of this class is loaded or can be loaded.
		 *
		 * @return Ecx_Portfolio
		 * @since 1.2.3
		 */
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();

				// Define plugin constants
				self::$instance->define_constants();

				// Includes plugin files
				self::$instance->include_files();

				// initialize plugin classes
				self::$instance->init_classes();

				register_activation_hook( __FILE__, array( self::$instance, 'activation' ) );
				register_deactivation_hook( __FILE__, array( self::$instance, 'deactivation' ) );

				add_action( 'after_setup_theme', array( self::$instance, 'add_image_size' ) );

				add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ),
					array( self::$instance, 'action_links' ) );

				do_action( 'ecx_portfolio_init' );
			}

			return self::$instance;
		}

		/**
		 * Magic getter to bypass referencing plugin.
		 *
		 * @param string $property
		 *
		 * @return mixed
		 */
		public function __get( $property ) {
			if ( array_key_exists( $property, $this->container ) ) {
				return $this->container[ $property ];
			}

			return $this->{$property};
		}

		/**
		 * Define plugin constants
		 */
		public function define_constants() {
			define( 'ECX_PORTFOLIO_VERSION', $this->version );
			define( 'ECX_PORTFOLIO_FILE', __FILE__ );
			define( 'ECX_PORTFOLIO_PATH', dirname( ECX_PORTFOLIO_FILE ) );
			define( 'ECX_PORTFOLIO_INCLUDES', ECX_PORTFOLIO_PATH . '/includes' );
			define( 'ECX_PORTFOLIO_TEMPLATES', ECX_PORTFOLIO_PATH . '/templates' );
			define( 'ECX_PORTFOLIO_URL', get_stylesheet_directory_uri() . '/ecx-portfolio' );
			define( 'ECX_PORTFOLIO_ASSETS', ECX_PORTFOLIO_URL . '/assets' );
		}

		/**
		 * Define constant if not already set.
		 *
		 * @param string $name
		 * @param string|bool $value
		 */
		private function define( $name, $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value );
			}
		}

		/**
		 * To be run when the plugin is activated
		 * @return void
		 */
		public function activation() {
			do_action( 'ecx_portfolio_activation' );
			flush_rewrite_rules();
		}

		/**
		 * To be run when the plugin is deactivated
		 * @return void
		 */
		public function deactivation() {
			do_action( 'ecx_portfolio_deactivation' );
			flush_rewrite_rules();
		}

		/**
		 * Add custom image size for portfolio
		 */
		public function add_image_size() {
			add_image_size( 'ecx-portfolio', 370, 370, true );
		}

		/**
		 * Includes files
		 */
		private function include_files() {
			spl_autoload_register( function ( $class ) {

				// If class already exists, not need to include it
				if ( class_exists( $class ) || false === strpos( $class, 'Ecx_Portfolio_' ) ) {
					return;
				}

				// Include our classes
				$file_name = 'class-' . strtolower( str_replace( '_', '-', $class ) ) . '.php';

				$class_path = ECX_PORTFOLIO_INCLUDES . '/' . $file_name;
				if ( file_exists( $class_path ) ) {
					require_once $class_path;
				}
			} );
		}

		/**
		 * Include admin and front facing files
		 * @return void
		 */
		private function init_classes() {
			$this->container['admin']   = Ecx_Portfolio_Admin::init();
			$this->container['scripts'] = Ecx_Portfolio_Scripts::init();

			if ( $this->is_request( 'admin' ) ) {
				$this->container['setting'] = Ecx_Portfolio_Setting::init();
				$this->container['metabox'] = Ecx_Portfolio_Metabox::init();
			}

			if ( $this->is_request( 'frontend' ) ) {
				$this->container['portfolio'] = Ecx_Portfolio_Single_Post::init();
				$this->container['shortcode'] = Ecx_Portfolio_Shortcode::init();
				$this->container['shapla']    = Ecx_Portfolio_Shapla_Theme::init();
				$this->container['rest']      = Ecx_Portfolio_REST_Controller::init();
			}

			add_action( 'widgets_init', array( 'Ecx_Portfolio_Widget', 'register' ) );
		}

		/**
		 * Add custom links on plugins page.
		 *
		 * @param mixed $links
		 *
		 * @return array
		 */
		public function action_links( $links ) {
			$plugin_links = array(
				'<a href="' . admin_url( 'edit.php?post_type=portfolio&page=fp-settings' ) . '">' . __( 'Settings',
					'ecx-portfolio' ) . '</a>'
			);

			return array_merge( $plugin_links, $links );
		}


		/**
		 * What type of request is this?
		 *
		 * @param string $type admin, ajax, cron or frontend.
		 *
		 * @return bool
		 */
		private function is_request( $type ) {
			switch ( $type ) {
				case 'admin':
					return is_admin();
				case 'ajax':
					return defined( 'DOING_AJAX' );
				case 'cron':
					return defined( 'DOING_CRON' );
				case 'frontend':
					return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
			}

			return false;
		}
	}
}
if(!function_exists('portfolio_show_more'))
{
    function portfolio_show_more()
    {
        ob_start();
        $paged =  (isset($_POST['paged']))?esc_attr($_POST['paged']) : '';
        $option          = Ecx_Portfolio_Helper::get_options();
        $portfolios_args = array(
            'page' =>$paged,
        );
        $portfolios = Ecx_Portfolio_Helper::get_portfolios( $portfolios_args );
        $temp_post = $GLOBALS['post'];
        foreach ( $portfolios as $portfolio ) 
        {
            setup_postdata( $portfolio );
            $GLOBALS['post'] = $portfolio;
            do_action( 'ecx_portfolio_loop_post', $portfolio );
        }
        wp_reset_postdata();
        $GLOBALS['post'] = $temp_post;
        $result['html'] = ob_get_clean();
        $result['count'] = count($portfolios);
        wp_send_json_success($result); 
        die();
    }
    add_action( 'wp_ajax_portfolio_show_more', 'portfolio_show_more' );
    add_action( 'wp_ajax_nopriv_portfolio_show_more', 'portfolio_show_more' );
}
if (!function_exists('portfolio_gallery')) {

	add_shortcode('portfolio_gallery', 'portfolio_gallery' );


function portfolio_gallery($args,$content) 

{



if(!isset($args['class'])){$class="gallery_p";} else {$class=$args['class'];}



$project_images = Ecx_Portfolio_Helper::get_portfolio_images_ids();

$option          = Ecx_Portfolio_Helper::get_options();

$image_size     = $option['project_image_size'];

function light_box_gallery($project_images)
{ ?>
	<div class="portfolio-gallery swiper swiper-container-initialized swiper-container-horizontal">
		<div tabindex="0" role="button" aria-label="Đóng (Esc)" class="dialog-close-button dialog-lightbox-close-button"><i class="bi bi-x-circle-fill"></i></div>
		<div class="swiper-button-prev"><i class="eicon-chevron-left"></i></div>
		<div class="swiper-wrapper" >
			<?php foreach ( $project_images as $image_id ): ?>
				<div class="swiper-slide ecx-lightbox-item" >
					<div class="swiper-zoom-container">
					<img class="ecx-lightbox-image elementor-lightbox-prevent-close swiper-lazy swiper-lazy-loaded" src="<?= wp_get_attachment_image_url( $image_id, 'full' )?>">
					</div>
				</div>
		    <?php  endforeach; ?>
		</div>
		<div class="swiper-button-next"><i class="eicon-chevron-right"></i></div>
	</div>
<?php }

add_action('wp_footer',function() use ( $project_images ) { 
               light_box_gallery( $project_images ); });

?>



	<ul class="wp-block-gallery columns-3 is-cropped">



		<?php $i=0; foreach ( $project_images as $image_id ): ?>



			<li class="blocks-gallery-item">



				<figure>



					<a index="<?= $i ?>" >



						<?php echo wp_get_attachment_image( $image_id, $image_size ); ?>



					</a>



				</figure>



			</li>



		<?php $i++;endforeach; ?>



	</ul>


<script type="text/javascript">
	jQuery(function($)
	{
	    $('.blocks-gallery-item a').click(function(e)
		{
			$('.dialog-lightbox-close-button').click(function()
				{
					$('.portfolio-gallery.swiper').removeClass('active');
				});
		    $('.portfolio-gallery.swiper').addClass('active');
			var index=$(this).attr('index');
   		    var swiper_portfolio_gallery = new Swiper('.portfolio-gallery.swiper', 
   		    {
   			slidesPerView: 1,
   			spaceBetween: 0,
   			direction:	'horizontal',
   			initialSlide: index,
   			PreventInteractionOnTransition: true,
   			navigation: {
   				nextEl: '.swiper-button-next',
   				prevEl: '.swiper-button-prev',
   			},
   			zoom: {
   				containerClass: 'swiper-zoom-container',
   				maxRatio:	1.5,
   				minRatio:	1,
   				toggle: true,	
   				zoomedSlideClass: 'swiper-slide-zoomed'
   			},
   			keyboard: {
   				enabled: true,
   				onlyInViewport: false,
   			}
   		    });
   	    })
    })
</script>



		<?php 



}

}


/* End of Portfolio Shortcode */
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
Ecx_Portfolio::instance();
