<?php

if ( ! defined( 'ABSPATH' ) ) {
	die; // If this file is called directly, abort.
}

if ( ! class_exists( 'Ecx_Portfolio_Setting' ) ) {
	class Ecx_Portfolio_Setting {

		/**
		 * Instance of current class
		 *
		 * @var self
		 */
		private static $instance;

		/**
		 * @return Ecx_Portfolio_Setting
		 */
		public static function init() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();

				add_action( 'init', array( self::$instance, 'settings' ) );
			}

			return self::$instance;
		}

		/**
		 * Plugin setting fields
		 *
		 * @throws Exception
		 */
		public function settings() {
			$settings = new Ecx_Portfolio_Setting_API();
			$settings->add_menu( array(
				'page_title'  => __( 'Cấu hình dự án', 'ecx-portfolio' ),
				'menu_title'  => __( 'Cấu hình', 'ecx-portfolio' ),
				'about_text'  => __( 'Lưu ý: Thận trọng khi thay đổi các thông số!', 'ecx-portfolio' ),
				'menu_slug'   => 'fp-settings',
				'parent_slug' => 'edit.php?post_type=portfolio',
				'option_name' => 'ecx_portfolio',
			) );

			// Add settings page tab
			$settings->add_tab( array(
				'id'    => 'general',
				'title' => __( 'Cấu hình chung', 'ecx-portfolio' ),
			) );
			
			$settings->add_tab( array(
				'id'    => 'single-portfolio-settings',
				'title' => __( 'Cấu hình trang', 'ecx-portfolio' ),
			) );

			$settings->add_field( array(
				'id'      => 'columns',
				'type'    => 'select',
				'std'     => 'l4',
				'name'    => __( 'Cột', 'ecx-portfolio' ),
				'desc'    => __( 'The number of items you want to see on the Large Desktop Layout.',
					'ecx-portfolio' ),
				'options' => array(
					'l12' => __( '1 Column', 'ecx-portfolio' ),
					'l6'  => __( '2 Columns', 'ecx-portfolio' ),
					'l4'  => __( '3 Columns', 'ecx-portfolio' ),
					'l3'  => __( '4 Columns', 'ecx-portfolio' ),
					'l2'  => __( '6 Columns', 'ecx-portfolio' ),
				),
				'tab'     => 'responsive-settings',
			) );
			$settings->add_field( array(
				'id'      => 'columns_desktop',
				'type'    => 'select',
				'std'     => 'm4',
				'name'    => __( 'Columns:Desktop', 'ecx-portfolio' ),
				'desc'    => __( 'The number of items you want to see on the Desktop Layout (Screens size from 993 pixels DP to 1199 pixels DP)',
					'ecx-portfolio' ),
				'options' => array(
					'm12' => __( '1 Column', 'ecx-portfolio' ),
					'm6'  => __( '2 Columns', 'ecx-portfolio' ),
					'm4'  => __( '3 Columns', 'ecx-portfolio' ),
					'm3'  => __( '4 Columns', 'ecx-portfolio' ),
				),
				'tab'     => 'responsive-settings',
			) );
			$settings->add_field( array(
				'id'      => 'columns_tablet',
				'type'    => 'select',
				'std'     => 's6',
				'name'    => __( 'Columns:Tablet', 'ecx-portfolio' ),
				'desc'    => __( 'The number of items you want to see on the Tablet Layout (Screens size from 601 pixels DP to 992 pixels DP)',
					'ecx-portfolio' ),
				'options' => array(
					's12' => __( '1 Column', 'ecx-portfolio' ),
					's6'  => __( '2 Columns', 'ecx-portfolio' ),
					's4'  => __( '3 Columns', 'ecx-portfolio' ),
					's3'  => __( '4 Columns', 'ecx-portfolio' ),
				),
				'tab'     => 'responsive-settings',
			) );
			$settings->add_field( array(
				'id'      => 'columns_phone',
				'type'    => 'select',
				'std'     => 'xs12',
				'name'    => __( 'Columns:Phone', 'ecx-portfolio' ),
				'desc'    => __( 'The number of items you want to see on the Mobile Layout (Screens size from 320 pixels DP to 600 pixels DP)',
					'ecx-portfolio' ),
				'options' => array(
					'xs12' => __( '1 Column', 'ecx-portfolio' ),
					'xs6'  => __( '2 Columns', 'ecx-portfolio' ),
					'xs4'  => __( '3 Columns', 'ecx-portfolio' ),
					'xs3'  => __( '4 Columns', 'ecx-portfolio' ),
				),
				'tab'     => 'responsive-settings',
			) );
			$settings->add_field( array(
				'id'      => 'portfolio_theme',
				'type'    => 'select',
				'std'     => 'two',
				'name'    => __( 'Chủ đề', 'ecx-portfolio' ),
				'desc'    => __( 'Chọn chủ đề cho dự án của bạn.', 'ecx-portfolio' ),
				'options' => array(
					'one' => __( 'Chủ đề 1 - Layout cơ bản', 'ecx-portfolio' ),
					'two' => __( 'Chủ đề 2 - Layout cơ bản', 'ecx-portfolio' ),
				),
			) );
			$settings->add_field( array(
				'id'   => 'image_size',
				'type' => 'image_sizes',
				'std'  => 'ecx-portfolio',
				'name' => __( 'Kích thước ảnh', 'ecx-portfolio' ),
				'desc' => __( 'Thiết lập kích thước ảnh dự án.', 'ecx-portfolio' ),
			) );
			$settings->add_field( array(
				'id'   => 'button_color',
				'type' => 'color',
				'std'  => '#4cc1be',
				'name' => __( 'Màu button', 'ecx-portfolio' ),
				'desc' => __( 'Chọn màu sắc cho nút Chi tiết.',
					'ecx-portfolio' ),
			) );
			$settings->add_field( array(
				'id'      => 'order',
				'type'    => 'select',
				'std'     => 'DESC',
				'name'    => __( 'Sắp xếp', 'ecx-portfolio' ),
				'desc'    => __( 'Choose portfolio ascending or descending order.', 'ecx-portfolio' ),
				'options' => array(
					'ASC'  => __( 'Tăng dần', 'ecx-portfolio' ),
					'DESC' => __( 'Giảm dần', 'ecx-portfolio' ),
				),
			) );
			$settings->add_field( array(
				'id'      => 'orderby',
				'type'    => 'select',
				'std'     => 'ID',
				'name'    => __( 'Sắp xếp dự án theo', 'ecx-portfolio' ),
				'desc'    => __( 'Lựa chọn sắp xếp theo tham số.', 'ecx-portfolio' ),
				'options' => array(
					'ID'       => __( 'ID', 'ecx-portfolio' ),
					'title'    => __( 'Tên', 'ecx-portfolio' ),
					'date'     => __( 'Ngày tạo', 'ecx-portfolio' ),
					'modified' => __( 'Ngày sửa', 'ecx-portfolio' ),
					'rand'     => __( 'Ngẫu nhiên', 'ecx-portfolio' ),
				),
			) );
			$settings->add_field( array(
				'id'   => 'posts_per_page',
				'type' => 'number',
				'std'  => - 1,
				'name' => __( '# số dự án hiển thị', 'ecx-portfolio' ),
				'desc' => __( 'Số dự án hiển thị trên 1 trang. "-1" để hiển thị tất cả.',
					'ecx-portfolio' ),
			) );
			$settings->add_field( array(
				'id'   => 'all_categories_text',
				'type' => 'text',
				'std'  => __( 'Tất cả', 'ecx-portfolio' ),
				'name' => __( 'Nội dung', 'ecx-portfolio' ),
				'desc' => __( 'Nội dung nút tất cả danh mục' ),
			) );
			$settings->add_field( array(
				'id'   => 'details_button_text',
				'type' => 'text',
				'std'  => __( 'Xem thêm', 'ecx-portfolio' ),
				'name' => __( 'Nút chi tiết', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung nút chi tiết' ),
			) );
			$settings->add_field( array(
				'id'   => 'portfolio_slug',
				'type' => 'text',
				'std'  => 'portfolio',
				'name' => __( 'Đường dẫn', 'ecx-portfolio' ),
				'desc' => __( 'Đường dẫn dành riêng cho dự án. Lưu ý: Không được trùng với các trang hoặc chuyên mục khác.', 'ecx-portfolio' ),
			) );
			$settings->add_field( array(
				'id'   => 'category_slug',
				'type' => 'text',
				'std'  => 'portfolio-category',
				'name' => __( 'Đường dẫn danh mục dự án', 'ecx-portfolio' ),
				'desc' => __( 'Đường dẫn dành riêng cho danh mục dự án. Lưu ý: Không được trùng với các trang hoặc chuyên mục khác.', 'ecx-portfolio' ),
			) );
			$settings->add_field( array(
				'id'   => 'skill_slug',
				'type' => 'text',
				'std'  => 'portfolio-skill',
				'name' => __( 'Đường dẫn hạng mục dự án', 'ecx-portfolio' ),
				'desc' => __( 'Đường dẫn dành riêng cho hạng mục dự án. Lưu ý: Không được trùng với các trang hoặc chuyên mục khác.', 'ecx-portfolio' ),
			) );

			// Single Portfolio Settings
			$settings->add_field( array(
				'id'   => 'project_image_size',
				'type' => 'image_sizes',
				'std'  => 'full',
				'name' => __( 'Kích thước ảnh dự án', 'ecx-portfolio' ),
				'desc' => __( 'Chọn kích thước ảnh mong muốn.', 'ecx-portfolio' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'show_related_projects',
				'type' => 'checkbox',
				'std'  => 1,
				'name' => __( 'Hiển thị dự án liên quan', 'ecx-portfolio' ),
				'desc' => __( 'Bật để hiển thị các dự án liên quan.', 'ecx-portfolio' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'related_projects_number',
				'type' => 'number',
				'std'  => 3,
				'name' => __( 'Số lượng' ),
				'desc' => __( 'Số lượng dự án liên quan bạn muốn hiển thị trên 1 trang.',
					'ecx-portfolio' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_description_text',
				'type' => 'text',
				'std'  => __( 'Mô tả dự án', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề mô tả', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề mô tả dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_details_text',
				'type' => 'text',
				'std'  => __( 'Chi tiết dự án', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề chi tiết dự án', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề chi tiết dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_skills_text',
				'type' => 'text',
				'std'  => __( 'Hạng mục dự án:', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề hạng mục', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề hạng mục dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_categories_text',
				'type' => 'text',
				'std'  => __( 'Danh mục:', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề danh mục', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề danh mục dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_url_text',
				'type' => 'text',
				'std'  => __( 'Diện tích:', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề diện tích', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề diện tích dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_date_text',
				'type' => 'text',
				'std'  => __( 'Ngày hoàn thiện:', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề ngày hoàn thiện', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề ngày hoàn thiện dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'project_client_text',
				'type' => 'text',
				'std'  => __( 'Khách hàng:', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề khách hàng', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề khách hàng (chủ đầu tư) dự án' ),
				'tab'  => 'single-portfolio-settings',
			) );
			$settings->add_field( array(
				'id'   => 'related_projects_text',
				'type' => 'text',
				'std'  => __( 'Dự án liên quan', 'ecx-portfolio' ),
				'name' => __( 'Tiêu đề dự án liên quan', 'ecx-portfolio' ),
				'desc' => __( 'Nhập nội dung tùy chỉnh cho tiêu đề dự án liên quan.', 'ecx-portfolio' ),
				'tab'  => 'single-portfolio-settings',
			) );
		}
	}
}
