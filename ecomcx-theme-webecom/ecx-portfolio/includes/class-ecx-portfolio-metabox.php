<?php

if ( ! defined( 'ABSPATH' ) ) {
	die; // If this file is called directly, abort.
}

if ( ! class_exists( 'Ecx_Portfolio_Metabox' ) ) {

	class Ecx_Portfolio_Metabox {

		/**
		 * Instance of current class
		 *
		 * @var self
		 */
		private static $instance;

		/**
		 * @return Ecx_Portfolio_Metabox
		 */
		public static function init() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();

				add_action( 'add_meta_boxes', array( self::$instance, 'add_meta_box' ) );
				add_action( 'save_post', array( self::$instance, 'save_meta_boxes' ) );
			}

			return self::$instance;
		}

		/**
		 * Save custom meta box
		 *
		 * @param int $post_id The post ID
		 */
		public function save_meta_boxes( $post_id ) {
			if ( ! isset( $_POST['_fp_nonce'] ) ) {
				return;
			}

			if ( ! wp_verify_nonce( $_POST['_fp_nonce'], 'ecx_portfolio_nonce' ) ) {
				return;
			}

			// Check if user has permissions to save data.
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}

			// Check if not an autosave.
			if ( wp_is_post_autosave( $post_id ) ) {
				return;
			}

			// Check if not a revision.
			if ( wp_is_post_revision( $post_id ) ) {
				return;
			}

			if ( ! isset( $_POST['ecx_portfolio_meta'] ) ) {
				return;
			}

			foreach ( $_POST['ecx_portfolio_meta'] as $key => $val ) {
				update_post_meta( $post_id, $key, stripslashes( htmlspecialchars( $val ) ) );
			}
		}

		/**
		 * Adds the meta box container.
		 */
		public function add_meta_box() {
			$args     = apply_filters( 'ecx_portfolio_meta_box', array(
				'id'          => 'ecx-portfolio-metabox',
				'title'       => __( 'Thông tin dự án', 'ecx-portfolio' ),
				'description' => __( 'Vui lòng nhập thông tin chi tiết của dự án.', 'ecx-portfolio' ),
				'screen'      => 'portfolio',
				'context'     => 'normal',
				'priority'    => 'high',
				'fields'      => apply_filters( 'ecx_portfolio_meta_box_fields', array(
					'_project_images'      => array(
						'name' => __( 'Hình ảnh dự án', 'ecx-portfolio' ),
						'desc' => __( 'Chọn hình ảnh dự án từ thư viện ảnh hoặc tải hình mới lên.', 'ecx-portfolio' ),
						'id'   => '_project_images',
						'type' => 'images',
						'std'  => '',
					),
					'_client_name'         => array(
						'name' => __( 'Tên khách hàng', 'ecx-portfolio' ),
						'desc' => __( 'Nhập tên chủ đầu tư dự án', 'ecx-portfolio' ),
						'id'   => '_client_name',
						'type' => 'text',
						'std'  => ''
					),
					'_project_add'         => array(
						'name' => __( 'Địa chỉ', 'ecx-portfolio' ),
						'desc' => __( 'Địa chỉ dự án', 'ecx-portfolio' ),
						'id'   => '_project_add',
						'type' => 'text',
						'std'  => ''
					),
					'_project_date'        => array(
						'name' => __( 'Ngày hoàn thiện dự án', 'ecx-portfolio' ),
						'desc' => __( 'Chọn ngày hoàn thiện dự án.', 'ecx-portfolio' ),
						'id'   => '_project_date',
						'type' => 'date',
						'std'  => '',
					),
					'_project_url'         => array(
						'name' => __( 'Diện tích', 'ecx-portfolio' ),
						'desc' => __( 'Diện tích tổng thể của dự án', 'ecx-portfolio' ),
						'id'   => '_project_url',
						'type' => 'text',
						'std'  => ''
					),
					'_is_featured_project' => array(
						'name' => __( 'Dự án nổi bật', 'ecx-portfolio' ),
						'desc' => __( 'Chọn mục này nếu đây là dự án nổi bật.', 'ecx-portfolio' ),
						'id'   => '_is_featured_project',
						'type' => 'checkbox',
						'std'  => 'no'
					),
				) ),
			) );
			$meta_box = new Ecx_Portfolio_MetaBox_API();
			$meta_box->add( $args );
		}
	}
}
