<?php

if ( ! defined( 'ABSPATH' ) ) {
	die; // If this file is called directly, abort.
}

if ( ! class_exists( 'Ecx_Portfolio_Admin' ) ) {

	class Ecx_Portfolio_Admin {

		/**
		 * Instance of current class
		 *
		 * @var self
		 */
		protected static $instance;

		/**
		 * Portfolio post type slug
		 *
		 * @var string
		 */
		private $portfolio_slug = 'portfolio';

		/**
		 * Portfolio taxonomy slug
		 *
		 * @var string
		 */
		private $category_slug = 'portfolio-category';

		/**
		 * Portfolio skill slug
		 *
		 * @var string
		 */
		private $skill_slug = 'portfolio-skill';

		/**
		 * Ensures only one instance of this class is loaded or can be loaded.
		 *
		 * @return self
		 */
		public static function init() {

			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
				self::$instance->initiate_hooks();
			}

			return self::$instance;
		}

		/**
		 * Initiate hooks
		 */
		private function initiate_hooks() {
			$option = get_option( 'ecx_portfolio' );

			// Set portfolio slug from plugin option
			if ( ! empty( $option['portfolio_slug'] ) ) {
				$this->portfolio_slug = $option['portfolio_slug'];
			}

			// Set portfolio category slug from plugin option
			if ( ! empty( $option['category_slug'] ) ) {
				$this->category_slug = $option['category_slug'];
			}

			// Set portfolio skill slug from plugin option
			if ( ! empty( $option['skill_slug'] ) ) {
				$this->skill_slug = $option['skill_slug'];
			}

			add_action( 'ecx_portfolio_activation', array( $this, 'post_type' ) );
			add_action( 'ecx_portfolio_activation', array( $this, 'taxonomy' ) );
			add_action( 'init', array( $this, 'post_type' ) );
			add_action( 'init', array( $this, 'taxonomy' ) );
		}

		/**
		 * Create portfolio post type
		 *
		 * @return void
		 */
		public function post_type() {
			$labels = apply_filters( 'ecx_portfolio_labels', array(
				'name'               => __( 'Dự án', 'ecx-portfolio' ),
				'singular_name'      => __( 'Dự án', 'ecx-portfolio' ),
				'menu_name'          => __( 'Dự án', 'ecx-portfolio' ),
				'parent_item_colon'  => __( 'Danh mục cha:', 'ecx-portfolio' ),
				'all_items'          => __( 'Tất cả dự án', 'ecx-portfolio' ),
				'view_item'          => __( 'Xem', 'ecx-portfolio' ),
				'add_new_item'       => __( 'Thêm mới dự án', 'ecx-portfolio' ),
				'add_new'            => __( 'Thêm mới', 'ecx-portfolio' ),
				'edit_item'          => __( 'Sửa dự án', 'ecx-portfolio' ),
				'update_item'        => __( 'Cập nhật', 'ecx-portfolio' ),
				'search_items'       => __( 'Tìm kiếm dự án', 'ecx-portfolio' ),
				'not_found'          => __( 'Không tìm thấy', 'ecx-portfolio' ),
				'not_found_in_trash' => __( 'Thùng rác trống', 'ecx-portfolio' ),
			) );

			$supports = array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'revisions' );

			$args = apply_filters( 'ecx_portfolio_post_type_args', array(
				'label'               => __( 'Dự án', 'ecx-portfolio' ),
				'description'         => __( 'A WordPress ecx portfolio to display portfolio images or gallery to your site.',
					'ecx-portfolio' ),
				'labels'              => $labels,
				'supports'            => apply_filters( 'ecx_portfolio_supports', $supports ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_rest'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 5,
				'menu_icon'           => 'dashicons-portfolio',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'rewrite'             => array( 'slug' => $this->portfolio_slug, 'with_front' => false, ),
				'capability_type'     => 'page',
			) );
			register_post_type( Ecx_Portfolio_Helper::POST_TYPE, $args );
		}

		/**
		 * Create two taxonomies "portfolio_cat" and "portfolio_skill"
		 * for the post type "portfolio"
		 *
		 * @return void
		 */
		public function taxonomy() {
			$category_args = apply_filters( 'ecx_portfolio_category_args', array(
				'label'             => __( 'Danh mục', 'ecx-portfolio' ),
				'singular_label'    => __( 'Danh mục', 'ecx-portfolio' ),
				'hierarchical'      => true,
				'public'            => true,
				'show_ui'           => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'show_in_rest'      => true,
				'args'              => array( 'orderby' => 'term_order' ),
				'query_var'         => true,
				'rewrite'           => array( 'slug' => $this->category_slug, 'hierarchical' => true ),
			) );
			register_taxonomy( Ecx_Portfolio_Helper::CATEGORY, Ecx_Portfolio_Helper::POST_TYPE, $category_args );

			$skill_args = apply_filters( 'ecx_portfolio_skill_args', array(
				'label'             => __( 'Hạng mục', 'ecx-portfolio' ),
				'singular_label'    => __( 'Hạng mục', 'ecx-portfolio' ),
				'hierarchical'      => true,
				'public'            => true,
				'show_ui'           => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'show_in_rest'      => true,
				'args'              => array( 'orderby' => 'term_order' ),
				'query_var'         => true,
				'rewrite'           => array( 'slug' => $this->skill_slug, 'hierarchical' => true ),
			) );
			register_taxonomy( Ecx_Portfolio_Helper::SKILL, Ecx_Portfolio_Helper::POST_TYPE, $skill_args );
		}
	}
}
