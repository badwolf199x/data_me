<?php

if ( ! defined( 'WPINC' ) ) {
	die; // If this file is called directly, abort.
}
if ( ! Ecx_Portfolio_Helper::has_portfolio_images() ) {
	return;
}

$project_images = Ecx_Portfolio_Helper::get_portfolio_images_ids();
$option         = Ecx_Portfolio_Helper::get_options();
$image_size     = $option['project_image_size'];
?>
<div class="swiper gallery-portfolio">
    <div class="swiper-button-prev"></div>
    <div class="swiper-wrapper">
		<?php foreach ( $project_images as $image_id ): ?>
            <div class="swiper-slide">
                <div class="img img-1">
					<?php echo wp_get_attachment_image( $image_id, $image_size ); ?>
                </div>
            </div>
		<?php endforeach; ?>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-pagination"></div>
    <div class="swiper-scrollbar"></div>
</div>
