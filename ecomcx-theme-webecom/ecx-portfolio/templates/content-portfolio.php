<?php
/**
 * Template part for displaying portfolio content
 *
 * @package Ecx_Portfolio
 * @since 1.3.3
 */

if ( ! defined( 'WPINC' ) ) {
	die; // If this file is called directly, abort.
}

$options     = Ecx_Portfolio_Helper::get_options();
//$button_text = esc_html( $options['details_button_text'] );

$item_class   = array( 'ecx-portfolio-item', 'portfolio-item', 'grid' );
$item_class[] = esc_attr( $options['columns_phone'] );
$item_class[] = esc_attr( $options['columns_tablet'] );
$item_class[] = esc_attr( $options['columns_desktop'] );
$item_class[] = esc_attr( $options['columns'] );

$categories_slug = array();
$categories      = get_the_terms( get_the_ID(), 'portfolio_cat' );
if ( $categories && ! is_wp_error( $categories ) ) {
	$categories_slug = wp_list_pluck( $categories, 'slug' );
	$item_class      = array_merge( $item_class, $categories_slug );
}

/*$image_size        = esc_attr( $options['image_size'] );
$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );*/
?>
<div id="id-<?php echo get_the_ID(); ?>" class="<?php echo implode( ' ', $item_class ) ?> ecx-portfolio-item">
    <div class="ecx-portfolio-item-content-wrap">
        <a href="<?php echo esc_url( get_the_permalink() ); ?>" rel="bookmark" class="ecx-portfolio-item__media">
            <?php echo wp_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'medium' ) ?>     
        </a>
        <div class="ecx-portfolio-item-info px-4 d-flex flex-wrap align-items-stretch">
            <h4 class="title ecx-portfolio-title text-center"><?php echo get_the_title(); ?></h4>
            <a class="ecx-btn-more ecx-circle-3 mt-4 mx-auto" href="<?php echo esc_url( get_the_permalink() ); ?>" rel="bookmark">
                <i class="bi bi-plus"></i>
            </a>
        </div>
    </div>
</div>
