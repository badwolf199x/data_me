<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>

	<section class="related products">

		<div class="section-header">
            <div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
            	<a class="header-link text-center">
                    <h2 class="section-label text-uppercase" style="color: #6b4620">
                        Sản phẩm cùng loại
                    </h2>
                    <div class="lead text-center">
                        <img src="/wp-content/uploads/2022/03/icon-header.png">
                    </div>
                </a>
            </div>
        </div>
		
		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

					<?php
					$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found
					?>
                    <div class="product-related-wrap col-12 col-sm-6 col-md-4 col-lg-3">
					    <?php wc_get_template_part( 'content', 'product' ); ?>
                    </div>
			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>
	<?php
endif;

wp_reset_postdata();
