<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
/*huy style woo*/
add_filter( 'woocommerce_enqueue_styles', 'ecx_dequeue_styles_woo' );
function ecx_dequeue_styles_woo( $enqueue_styles ) {
unset( $enqueue_styles['woocommerce-general'] ); // Remove woocommerce.css
unset( $enqueue_styles['woocommerce-layout'] ); // Remove woocommerce-layout.css
unset( $enqueue_styles['woocommerce-smallscreen'] ); // Remove the woocommerce-smallscreen.css
return $enqueue_styles;
}
get_header();


?>

<div id="page-content-wrap" class="container d-block position-relative d-lg-flex align-items-stretch justify-content-between flex-wrap"> <?php

if (function_exists('rank_math_the_breadcrumbs'))  rank_math_the_breadcrumbs(); 

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' ); ?>

<!-- Begin content page -->
<section id="content-page">
<div class="woocommerce-products-header d-flex flex-wrap align-items-center justify-content-between ">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; 
	woocommerce_result_count(); ?>
</div>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end(); ?>
    </section>
    <!-- End of content page --> <?php
	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */ ?>
	<!-- Begin pagination -->
    <section id="category-pagination"> <?php
	    do_action( 'woocommerce_after_shop_loop' ); ?>
	</section>
    <!-- end of pagination --> <?php
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
} 
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' ); ?>
</div> 

<?php 

get_footer();
