<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package ECOMCX Theme
 */
$args_products_featured = array( 
    'post_type' => 'product',
    'post_status'   => 'publish',
    'posts_per_page' => 30, 
    'orderby'        =>'meta_value_num date',
    'order'        =>'DESC',
    'meta_key' => '_featured_product',
    'meta_query' => array(                  
        'relation' => 'AND',
        array(
            'key' => '_price',
            'value' => 0,
            'compare' => '>'),
        array(
            'key' => '_stock_status',
            'value' => 'instock',
            'compare' => '=')
    ),
);
$products_featured = new WP_Query( $args_products_featured );
?>
<div class="main-index">
<div class="container">
<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
</div>
<!-- Begin content 404 page -->
<section id="page-404" class="section-content-page-404 position-relative container pb-5">
	<div class="content-wrap">
        <div class="title-brand-page line-after col-12">
            <h1 class="ecx-post-title text-capitalize text-center mb-4" style="font-size: 64px; line-height: 64px;">
            	404!
            </h1>
            <div class="sub-title-404 text-center pb-4">Rất tiếc, nội dung bạn vừa tìm kiếm hiện không có. Bạn có thể thử bằng cách tìm kiếm từ bên dưới</div>
        </div>
        <div class="col-12 form-search-404 d-flex flex-wrap justify-content-center pb-5">
            <?= get_product_search_form();  ?>
        </div>
        <div class="footer-wrap-404 d-flex flex-wrap align-items-stretch pt-5">
            <div class="col-12 col-lg-8 product-404 " id="slider-featured">
            	<div class="section-header">
            		<div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
            	        <a class="header-link text-center">
                            <h2 class="section-label text-uppercase" style="color: #6b4620">
                                Sản phẩm nổi bật
                            </h2>
                            <div class="lead text-center">
                                <img src="/wp-content/uploads/2022/03/icon-header.png">
                            </div>
                        </a>
                    </div>
                </div>
        	    <div class="swiper ecx-product-loop">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-wrapper">
                        <?php   if ( $products_featured->have_posts() ) :  $c=1;
                            while ( $products_featured->have_posts() ) :
                                $products_featured->the_post(); ?>
                                <?php if($c==1 || $c%2==1) : ?>
                                    <div class="swiper-slide"> 
                                <?php endif; ?>
                                    <?php wc_get_template_part( 'content', 'product' ); ?>
                                <?php if($c==2 || $c%2==0) : ?>
                                    </div> 
                                <?php endif; ?>
                            <?php $c++; endwhile; 
                            do_action( 'woocommerce_after_shop_loop' ); 
                        else :
                                do_action( 'woocommerce_no_products_found' );
                        endif; 
                        wp_reset_postdata(); ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <div class="col-12 col-lg-4 categories-product-404 ps-0 ps-lg-5" style="background-color: #fff;z-index: 2">
            	<div class="section-header">
            		<div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
            	        <a class="header-link text-center">
                            <h2 class="section-label text-uppercase" style="color: #6b4620">
                                Product Categories
                            </h2>
                            <div class="lead text-center">
                                <img src="/wp-content/uploads/2022/03/icon-header.png">
                            </div>
                        </a>
                    </div>
                </div>
            	<ul class="siderbar-section-content">
            		<li class="all-cat active">
            			<div class="wrap-button-label d-flex">
            				<h4>
            					Danh mục sản phẩm
            				</h4>
            				<span class="child-indicator"><i class="bi bi-chevron-right"></i></span>
            			</div>
            			<ul class="product-categories">
            				<?php show_list_category(0,''); ?>
            			</ul>
            		</li>
            	</ul>
            </div>
        </div>
    </div>
</section>
<!-- End content 404 page -->
</div>
