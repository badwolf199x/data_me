<?php
/**
 * The template for displaying archive pages.
 *
 * @package ECOMCX Theme
 */
?>
<?php $args_categories = array(
        'parent'    => 0,
        'hide_empty'=> 0,
        'exclude_tree' => 16,
        'taxonomy'  => 'product_cat',
        'orderby' => 'menu_order', 
        'order' => 'ASC',
    );
$categories=get_categories( $args_categories ); 
    if(get_option( 'sticky_posts' )!=null)
    {
        $args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'orderby'        =>'date',
            'order'        =>'DESC',
            'post__in'            => get_option( 'sticky_posts' ),
            'ignore_sticky_posts' => 1,
        );
        $blog_featured_sticky=get_posts($args_blog_sticky);
        if(count($blog_featured_sticky)<6)
        {
            $args_blog = array(
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => 6-count($blog_featured_sticky),
                'orderby'        =>'date',
                'order'        =>'DESC',
                'post__not_in'            => get_option( 'sticky_posts' ),
                'ignore_sticky_posts' => 1,
            );
            $blog_featured=get_posts($args_blog);
        }
        $blog_featured_sticky=array_merge($blog_featured_sticky,$blog_featured);
    }
    elseif(get_option( 'sticky_posts' )==null)
    {
        $args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'orderby'        =>'date',
            'order'        =>'DESC',
        );
        $blog_featured_sticky=get_posts($args_blog_sticky);
    }

?>
<div class="main-index">
    <!-- Begin banner -->
    <section id="banner-blog">
        <div class="container flex-wrap align-items-stretch d-flex justify-content-between pb-50">
            <div class="megamenu-left col-3">
                <ul class='megamenu-wrap'>
                <?php foreach($categories as $key => $category) : 
                    echo mega_menu_taxonomy($category->term_id,'product_cat',$category->name,1,true);
                endforeach; ?>
                </ul>
            </div>
            <div class="banner-middle col-9">
                <div class="banner-middle-top ecx-blog-slider">
                    <div class="swiper">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-wrapper">
                            <?php $temp_post = $GLOBALS['post'];
                        foreach ( $blog_featured_sticky as $blog ) {
                        setup_postdata( $blog );
                        $GLOBALS['post'] = $blog; $category=wp_get_post_terms( $blog->ID, 'category' );?>
                        <article class="swiper-slide post">
                            <div class="wrap-tag">
                                <a href="<?= $category[0]->slug ?>">
                                    <?= $category[0]->name ?>
                                </a>
                            </div>
                            <a href="<?php the_permalink() ?>">
                                <div class="ecx-post-thumbnail">
                                    <?php the_post_thumbnail() ?>
                                </div>
                            </a>
                            <div class="ecx-post-info">
                                <h3 class="ecx-post-title mb-3">
                                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                </h3>
                                <div class="ecx-post-meta-data">
                                    <div class="ecx-post-excerpt">
                                        <?php the_excerpt('(more…)'); ?>
                                    </div>
                                </div>
                            </div>
                        </article>
                <?php }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- End of banner -->
    <!-- Begin content archive -->
        <section id="archive" class="section-archive position-relative">
            <div class="container d-flex flex-wrap">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            	<div class="col-12 col-lg-8 content-archive">
            		<div class="wrap-content-post flex-wrap justify-content-between d-flex">
            		    <?php if(have_posts()):
                            while ( have_posts() ) : the_post(); ?>
                            <article class="ecx-post ecx-flex-item">
                                <a href="<?php the_permalink() ?>">
                                    <div class="ecx-post-thumbnail">
                                        <?php the_post_thumbnail() ?>
                                    </div>
                                </a>
                                <div class="ecx-post-info">
                                    <h3 class="ecx-post-title">
                                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <div class="ecx-post-meta-data d-flex flex-wrap">
                                        <div class="ecx-post-date">
                                            <i class="bi bi-clock"></i>&nbsp;<?php echo get_the_date(); ?>
                                        </div>
                                        <div class="ecx-post-view">
                                            <i class="bi bi-eye"></i>&nbsp;<?php the_views(); ?>
                                        </div>
                                    </div>
                                    <div class="ecx-post-description">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </article>
                        <?php endwhile; 
                        else:
                            if(!is_search()):
                            echo '<h3 class="empty_post">Chuyên mục chưa có bài viết, vui lòng quay lại sau.</h3>';
                            else :
                            echo '<h3 class="empty_post">Không tìm thấy nội dung chứa từ khóa.</h3>';
                            endif;
                        endif; ?>
                    </div>
                        <?php global $wp_query;
                           $max_page=ceil(($wp_query->found_posts-$wp_query->offset)/get_query_var('posts_per_page'));
                           if($max_page>1) 
                            {   ?>
                                <div class="ecx-pagination">
                                    <?php   
                                            $big = 999999999;
                                            echo paginate_links( array(
                                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                                'format' => '?paged=%#%',
                                                'prev_text'    => '←',
                                                'next_text'    => '→',
                                                'current' => max( 1, get_query_var('paged') ),
                                                'total' => $max_page,
                                                'type'         => 'list',
                                                'end_size'     => 3,
                                                'mid_size'     => 1,
                                            ) );
                                    ?>
                                </div>
                    <?php   }   ?>
                </div>
                <div class="col-12 col-lg-4 sidebar">
                    <?php get_template_part( 'template-parts/section-chung/sidebar' ); ?>
                </div>
            </div>
        </section>
    <!-- End content archive -->
</div>