<?php
/**
 * The template for displaying siderbar shop.
 *
 * @package ECOMCX Theme
 */
//filter price
    $min_price=(isset($_GET['min_price'])) ? $_GET['min_price'] : '';   
    $min_price_filter_text="min_price=";
    $max_price=(isset($_GET['max_price'])) ? $_GET['max_price'] : '';   
    $max_price_filter_text="max_price=";
    $url_new=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $class_chosen="woocommerce-widget-layered-nav-list__item--chosen chosen";
    if( !parse_url($url_new, PHP_URL_QUERY) )
    { 
        $url_new.='?';
        $filter_0_99000=$url_new.$min_price_filter_text.'0&'.$max_price_filter_text.'99000';
        $filter_100000_299000=$url_new.$min_price_filter_text.'100000&'.$max_price_filter_text.'299000';
        $filter_300000_599000=$url_new.$min_price_filter_text.'300000&'.$max_price_filter_text.'599000';
        $filter_600000_999000=$url_new.$min_price_filter_text.'600000&'.$max_price_filter_text.'999000';
        $filter_1000000=$url_new.$min_price_filter_text.'1000000';
    }
    else
    {
        if($min_price==""&&$max_price=="")
        {
            $url_new.='&';
            $filter_0_99000=$url_new.$min_price_filter_text.'0&'.$max_price_filter_text.'99000';
            $filter_100000_299000=$url_new.$min_price_filter_text.'100000&'.$max_price_filter_text.'299000';
            $filter_300000_599000=$url_new.$min_price_filter_text.'300000&'.$max_price_filter_text.'599000';
            $filter_600000_999000=$url_new.$min_price_filter_text.'600000&'.$max_price_filter_text.'999000';
            $filter_1000000=$url_new.$min_price_filter_text.'1000000';
        }
        elseif($min_price==0 && $max_price==99000)
        { 
        $filter_0_99000=preg_replace( '/&|\?min_price=0&max_price=99000/', '', $url_new);
        $filter_100000_299000=preg_replace( '/min_price=0&max_price=99000/',$min_price_filter_text.'100000&'.$max_price_filter_text.'299000' , $url_new);
        $filter_300000_599000=preg_replace( '/min_price=0&max_price=99000/',$min_price_filter_text.'300000&'.$max_price_filter_text.'599000' , $url_new);
        $filter_600000_999000=preg_replace( '/min_price=0&max_price=99000/',$min_price_filter_text.'600000&'.$max_price_filter_text.'999000' , $url_new);
        $filter_1000000=preg_replace( '/min_price=0&max_price=99000/',$min_price_filter_text.'1000000' , $url_new);
        }
        elseif($min_price==100000 && $max_price==299000)
        { 
        $filter_0_99000=preg_replace( '/min_price=100000&max_price=299000/',$min_price_filter_text.'0&'.$max_price_filter_text.'99000' , $url_new);
        $filter_100000_299000=preg_replace( '/&|\?min_price=100000&max_price=299000/', '', $url_new);
        $filter_300000_599000=preg_replace( '/min_price=100000&max_price=299000/',$min_price_filter_text.'300000&'.$max_price_filter_text.'599000' , $url_new);
        $filter_600000_999000=preg_replace( '/min_price=100000&max_price=299000/',$min_price_filter_text.'600000&'.$max_price_filter_text.'999000' , $url_new);
        $filter_1000000=preg_replace( '/min_price=100000&max_price=299000/',$min_price_filter_text.'1000000' , $url_new);
        }
        elseif($min_price==300000 && $max_price==599000)
        { 
        $filter_0_99000=preg_replace( '/min_price=300000&max_price=599000/',$min_price_filter_text.'0&'.$max_price_filter_text.'99000' , $url_new);
        $filter_100000_299000=preg_replace( '/min_price=300000&max_price=599000/',$min_price_filter_text.'100000&'.$max_price_filter_text.'299000' , $url_new);
        $filter_300000_599000=preg_replace( '/&|\?min_price=300000&max_price=599000/', '', $url_new);
        $filter_600000_999000=preg_replace( '/min_price=300000&max_price=599000/',$min_price_filter_text.'600000&'.$max_price_filter_text.'999000', $url_new);
        $filter_1000000=preg_replace( '/min_price=300000&max_price=599000/',$min_price_filter_text.'1000000', $url_new);
        }
        elseif($min_price==600000 && $max_price==999000)
        { 
        $filter_0_99000=preg_replace( '/min_price=600000&max_price=999000/',$min_price_filter_text.'0&'.$max_price_filter_text.'99000', $url_new);
        $filter_100000_299000=preg_replace( '/min_price=600000&max_price=999000/',$min_price_filter_text.'100000&'.$max_price_filter_text.'299000', $url_new);
        $filter_300000_599000=preg_replace( '/min_price=600000&max_price=999000/',$min_price_filter_text.'300000&'.$max_price_filter_text.'599000', $url_new);
        $filter_600000_999000=preg_replace( '/&|\?min_price=600000&max_price=999000/', '', $url_new);
        $filter_1000000=preg_replace( '/min_price=600000&max_price=999000/',$min_price_filter_text.'1000000', $url_new);
        }
        elseif($min_price==1000000 && $max_price=='')
        { 
        $filter_0_99000=preg_replace( '/min_price=1000000/',$min_price_filter_text.'0&'.$max_price_filter_text.'99000', $url_new);
        $filter_100000_299000=preg_replace( '/min_price=1000000/',$min_price_filter_text.'100000&'.$max_price_filter_text.'299000', $url_new);
        $filter_300000_599000=preg_replace( '/min_price=1000000/',$min_price_filter_text.'300000&'.$max_price_filter_text.'599000', $url_new);
        $filter_600000_999000=preg_replace( '/min_price=1000000/',$min_price_filter_text.'600000&'.$max_price_filter_text.'999000', $url_new);
        $filter_1000000=preg_replace( '/&|\?min_price=1000000/', '', $url_new);
        }
    }
//filter brand
    $thuong_hieu_terms=get_terms(['taxonomy' => 'pa_thuong-hieu','hide_empty' => false,]);
    foreach ($thuong_hieu_terms as $key => $term) 
    {
        $key_first[]=ucfirst(substr($term->name, 0, 1));
    }
    $key_first=array_unique($key_first);
//list category
$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
$term_name = $queried_object->name;
$term_count = $queried_object->count;
$args_cat_current = array(
    'hierarchical' => 1,
    'show_option_none' => '',
    'hide_empty' => 0,
    'parent' => $term_id,
    'exclude_tree' =>'',
    'taxonomy' => 'product_cat'
    );
$subcats_current = get_categories($args_cat_current); 
?>
<div class="siderbar-section price-filter ">
    <h3 class="title">
        Lọc theo khoảng giá
    </h3>
    <div class="siderbar-section-content">
        <ul class="woocommerce-widget-layered-nav-list">
            <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?=($min_price==0 && $max_price==99000) ? $class_chosen : '' ?>">
                <a rel="nofollow" href="<?=$filter_0_99000?>">Dưới 100.000 VND</a>
            </li>
            <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?=($min_price==100000 && $max_price==299000) ? $class_chosen : '' ?>">
                <a rel="nofollow" href="<?=$filter_100000_299000?>">100.000 — 299.000 VND</a>
            </li>
            <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?=($min_price==300000 && $max_price==599000) ? $class_chosen : '' ?>">
                <a rel="nofollow" href="<?=$filter_300000_599000?>">300.000 — 599.000 VND</a>
            </li>
            <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?=($min_price==600000 && $max_price==999000) ? $class_chosen : '' ?>">
                <a rel="nofollow" href="<?=$filter_600000_999000?>">600.000 — 999.000 VND</a>
            </li>
            <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?=($min_price==1000000 && $max_price=='') ? $class_chosen : '' ?>">
                <a rel="nofollow" href="<?=$filter_1000000?>">Trên 1.000.000 VND</a>
            </li>
        </ul> 
    </div>       
</div>
<div class="siderbar-section brand-filter">
    <h3 class="title">
        Tìm Kiếm Theo Thương Hiệu
    </h3>
    <div class="siderbar-section-content" data-filter="ALL">
        <div class="wrap-button-filter">
            <div class="brand-filter-all">
                <h5 class="button-filter text-uppercase" data-key="ALL">
                    all
                </h5>
            </div>
            <div class="brand-filter-alphabet">
                <?php foreach ($key_first as $key => $value) : ?>
                    <h5 class="button-filter text-uppercase" data-key="<?=$value?>">
                        <?=$value?>
                    </h5>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="content-filter">
            <?php foreach ($thuong_hieu_terms as $key => $term) : ?>
                <div class="barnd-filter-item" data-key="<?= ucfirst(substr($term->name, 0, 1)) ?>">
                    <a href="/thuong-hieu/<?= $term->slug ?>">
                        <?= $term->name ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="siderbar-section recent-post">
	<ul class="siderbar-section-content">
        <li class="all-cat<?= ((!is_product_category() && !is_shop()) || is_shop()) ? ' active' : '' ?>">
            <div class="wrap-button-label d-flex">
                <h4>
                    Browse Categories
                </h4>
                <?= ((!is_product_category() && !is_shop()) || is_shop()) ? '' : '<span class="child-indicator"><i class="bi bi-chevron-right"></i></span>' ?>
            </div>
            <ul class="product-categories">
                <?php show_list_category(0,$term_id); ?>
            </ul>
        </li>
        <?php if(is_product_category() && !is_shop()) : ?>
        <li class="current-cat">
            <div class="wrap-button-label d-flex">
                <?= (count($subcats_current)==0) ? '<span class="no-child"></span>' : '<span class="child-indicator"><i class="bi bi-chevron-right"></i></span>' ?>
                <h4>
                    <?= $term_name ?>
                </h4>
                <span class="count">(<?=$term_count?>)</span>
            </div>
            <?php  if(count($subcats_current)>0) : ?>
                <ul class="children"> 
                    <?php show_list_category($term_id,'') ?>
                </ul>
            <?php endif; ?>
        </li>
        <?php endif; ?>
    </ul>
</div>
<?php if(isset($_SESSION["viewed"]) && $_SESSION["viewed"]) : ?>
<div class="siderbar-section follow-us">
    <h3 class="title">
        Sản phẩm đã xem
    </h3>
    <div class="siderbar-section-content type-product">
	        <?php show_viewed_product() ; ?>
    </div>
</div>
<?php endif; ?>
