<?php 
/**
 * The template for displaying social link.
 *
 * @package ECOMCX Theme
 */
$id_setting_r=156; 
$data=get_fields($id_setting_r);
?>
<ul class="socialList">
		<li>
			<a href="<?= $data['facebook_link'] ?>" target="_blank">
				<img src="/wp-content/uploads/icon_svg/icon-facebook-main.svg" alt="icon logo facebook" title="social facebook">
			</a>
		</li>
		<li>
			<a href="<?= $data['instagram_link'] ?>" target="_blank">
				<img src="/wp-content/uploads/icon_svg/icon-instagram-main.svg" alt="icon logo instagram" title="social instagram">
			</a>
		</li>
		<li>
			<a href="<?= $data['zalo_link'] ?>" target="_blank">
				<img src="/wp-content/uploads/icon_svg/icon-zalo-main.svg" alt="icon logo zalo" title="social zalo">
			</a>
		</li>
    	<li>
    		<a href="<?= $data['lazada_link'] ?>" target="_blank">
    			<img src="/wp-content/uploads/icon_svg/lazada.svg" alt="icon logo lazada" title="social lazada">
    		</a>
    	</li>
    	<li>
    		<a href="<?= $data['shopee_link'] ?>" target="_blank">
    			<img src="/wp-content/uploads/icon_svg/shopee.svg" alt="icon logo shopee" title="social shopee">
    		</a>
    	</li>
</ul>