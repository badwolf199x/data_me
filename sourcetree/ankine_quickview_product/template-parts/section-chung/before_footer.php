<?php
/**
 * The template for displaying section before footer.
 *
 * @package ECOMCX Theme
 */
?>
<?php 
$args_stores = array(
            'post_type'      => 'local-store',
            'post_status'    => 'publish',
            'nopaging'       => true,
            'orderby'        =>'date',
            'order'          =>'DESC',
        );
    $stores=get_posts($args_stores);
?>
<section id="before-footer" class="before-footer">
    <div class="container  pt-4 pb-50" >
        <div class="section-header">
            <div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
                <a class="header-link text-center" href="/cua-hang/">
                    <h2 class="section-label text-uppercase">
                        DANH SÁCH CỬA HÀNG
                    </h2>
                    <div class="lead text-center">
                        <img src="/wp-content/uploads/2022/03/icon-header.png">
                    </div>
                 </a>
            </div>
        </div>
        <div class="ecx-store-slider">
            <div class="swiper ecx-store-loop">
                <div class="swiper-button-prev"></div>
                <div class="swiper-wrapper">
                    <?php $temp_post = $GLOBALS['post'];
                        foreach ( $stores as $store ) {
                        setup_postdata( $store );
                        $GLOBALS['post'] = $store; 
                        $data=get_post_meta( $store->ID, 'dvls_data', true );
                        $phone_1=$data['phone1'];
                        $phone_2=$data['phone2'];
                        $hotline_1=$data['hotline1'];
                        $hotline_2=$data['hotline2'];
                        $email=$data['email'];
                        $open_time=$data['open']; ?>
                        <article class="swiper-slide store text-center">
                            <a href="/he-thong-cua-hang/">
                                <div class="ecx-store-thumbnail mb-3">
                                    <?php the_post_thumbnail() ?>
                                </div>
                            </a>
                            <div class="ecx-store-info">
                                <div class="ecx-store-meta-data">
                                    <div class="ecx-store-item title mb-2">
                                        <?php the_title() ?>
                                    </div>
                                    <?php foreach($data as $key => $value) : 
                                        if($key=='phone1' || $key=='phone2' || $key=='hotline1' || $key=='hotline2' || $key=='email' || $key=='open') :?>
                                        <div class="ecx-store-item mb-2">
                                            <?php if($key=='phone1' || $key=='phone2' || $key=='hotline1' || $key=='hotline2' || $key=='email') :?>
                                                <a href="<?= ($key=='email') ? 'mailto:' : 'tel:' ?><?= str_replace(' ', '', $value) ?>">
                                            <?php endif; ?>
                                                    <?= $value ?>
                                            <?php if($key=='phone1' || $key=='phone2' || $key=='hotline1' || $key=='hotline2' || $key=='email') :?>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; endforeach; ?>
                                </div>
                            </div>
                        </article>
                <?php  }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>
