<?php
/**
 * The template for displaying single pages.
 *
 * @package ECOMCX Theme
 */
?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="main-index">
	<!-- Begin Banner -->
        <section id="banner" class="single section-banner position-relative d-flex align-items-end">
            <div class="banner-overlay">
            </div> 
            <div class="banner-background">
                <?php if(get_post_thumbnail_id()!='') : ?>
            	    <img src="<?= get_the_post_thumbnail_url(get_the_id(),'full'); ?>" alt="Single Img banner" class="img-banner">
                <?php endif; ?>
            </div>
            <div class="container flex-wrap d-flex">
                <div class="col-md-12 col-sm-12 col-xs-12 banner-wrap-info">
                	<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); 
                    the_title('<h1 class="ecx-post-title text-capitalize">', '</h1>'); 
			        ?>
                    <?php if(is_single()) : ?>
			        <div class="ecx-post-meta">
			        	<span class="post-meta fst-italic date"><i class="bi bi-clock"></i>&nbsp;
			        		<b class="label">Ngày đăng: </b>
			        		<?= get_the_date(); ?>
			        	</span>
			        	<span class="post-meta fst-italic viewed"><i class="bi bi-eye"></i>&nbsp;
			        		<?= get_post_meta( get_the_id(), 'views', true ) ?>lượt xem
			        	</span>
			        	<span class="post-meta fst-italic author"><i class="bi bi-person-circle"></i>&nbsp;
			        		<b class="label">Đăng bởi: </b>
			        		<?php the_author(); ?>
			        	</span>
			        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <!-- End of Banner -->

    <!-- Begin content single -->
        <section id="single" class="section-single position-relative">
            <div class="container d-flex flex-wrap">
            	<div class="col-12 col-lg-8 content-single">
            		<div class="post-content"> 
                        <?php if(is_single()) : ?>
            			<div class="wrap-toc" id="toc-accordion"> 
            			<h4 class="accordion-header" id="accordion-headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne" href="#flush-collapseOne"></button>
                        </h4>
                        <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="accordion-headingOne" data-bs-parent="#toc-accordion" style="">
                            <div class="accordion-body">
                            </div>                              
                        </div>
                        </div>
                        <?php endif; ?>
            		    <?php the_content(); ?>
            		</div>
                    <div class="post-tags"> 
                    	<?php the_tags( '<div class="tag-links"><i class="bi bi-tags-fill"></i><span class="tags-label">Tags: </span>', ' , ', '</div>' ); ?>
                    </div>
                    <!--<div class="like-share-button d-flex justify-content-end flex-wrap"> 
                    	<div class="zalo-share-button" data-href="" data-oaid="896146097299045189" data-layout="1" data-color="blue" data-customize="false"></div>
                    	<div class="zalo-follow-only-button" data-oaid="896146097299045189"></div>
                    	<div class="fb-like" data-href="https://www.facebook.com/ecomcx.official/" data-width="95px" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div>
                    	<div class="fb-share-button d-flex" data-href="https://www.facebook.com/ecomcx.official/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2Fecomcx.official%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                    </div>!-->
                    <?php if(is_single()) : ?>
                    <div class="post-navigation d-flex">
                        <?php if(get_previous_post_link()!="") : ?>
                        <div class="post-prev d-flex align-items-center"><i class="bi bi-chevron-compact-left"></i>
                        	<?php previous_post_link( '%link', '<span class="navigation-label text-uppercase">BÀI VIẾT TRƯỚC</span><span class="navigation-post-title">%title</span>', true ); ?>
                        </div> 
                        <?php endif; ?>
                        <?php if(get_next_post_link()!="") : ?>
                        <div class="post-next d-flex align-items-center">
                        	<?php next_post_link( '%link', '<span class="navigation-label text-uppercase">BÀI VIẾT TIẾP</span><span class="navigation-post-title">%title</span>', true ); ?>
                        <i class="bi bi-chevron-compact-right"></i>
                        </div> 
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                    <div class="post-comments"> 
                    	<?php comments_template(); ?>
                    </div>
                </div>
                <div class="col-12 col-lg-4 sidebar">
                    <?php get_template_part( 'template-parts/section-chung/sidebar' ); ?>
                </div>
            </div>
        </section>
    <!-- End content single -->

<?php   
if(is_single()) :
    $args_related = array(
                                'posts_per_page' => 4,
                                'ignore_sticky_posts'=>true,
                                'category__in' => wp_get_post_categories(),
                                'exclude' => array( get_the_id() ),
                            );
                $related_query = new WP_Query( $args_related ); 
                if($related_query->have_posts()) : ?> 
    <!-- Begin related post -->
        <section id="related-post" class="section-related-post position-relative">
            <div class="container">
            	<div class="heading-wrap line-after">
                    <h2 class="title text-left text-uppercase">
                        Bài viết liên quan
                    </h2>
                </div>
                <div class="related-post-content-wrap d-flex justify-content-between flex-wrap">
                	<?php while ($related_query-> have_posts()) : $related_query -> the_post(); ?>
                	<article class="ecx-post ecx-grid-item">
                		<a href="<?php the_permalink() ?>">
                            <div class="ecx-post-thumbnail">
                                <?php the_post_thumbnail() ?>
                            </div>
                        </a>
                        <div class="ecx-post-info">
                        	<h3 class="ecx-post-title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h3>
                            <div class="ecx-post-meta-data">
                                <div class="ecx-post-date">
                                    <i class="bi bi-clock"></i>&nbsp;<?php echo get_the_date(); ?>
                                </div>
                                <a class="ecx-post-readmore" href="<?php the_permalink() ?>">Xem thêm
                                    <i class="bi bi-arrow-right-short"></i>
                                </a>
                            </div>
                        </div>
                    </article>
                <?php   endwhile; wp_reset_postdata();?>
                </div>
            </div>
        </section>
    <!-- End of related post -->
<?php endif; endif; ?>

</div>

	<?php
endwhile;

