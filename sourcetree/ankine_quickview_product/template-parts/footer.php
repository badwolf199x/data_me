<?php
/**
 * The template for displaying footer.
 *
 * @package ECOMCX Theme
 */

$footer_menu = wp_nav_menu( [
    'theme_location' => 'menu-2',
    'fallback_cb' => false,
    'echo' => false,
] );
$footer_logo = wp_get_attachment_image_src(get_theme_mod('custom_logo'),'full')[0];
$id_setting_r=156; 
$data=get_fields($id_setting_r);

?>
<!-- Begin before footer -->
        <?php get_template_part( 'template-parts/section-chung/before_footer' ); ?>
<!-- End of before footer  -->
<footer id="site-footer" class="footer" role="contentinfo" style="background-image: url('<?= $data['footer_bacground'] ?>');">
    <div class="site-footer container d-block d-lg-flex">
        <div class=" col-12 col-lg-3 left-footer-wrap mb-4 pb-2 mb-lg-0 pb-lg-0">
            <div class="footer-site-logo mb-3">
                <a href="<?php echo site_url() ?>">
                    <img src="<?= $footer_logo ?>" alt="<?= $site_name ?>">
                </a>
            </div>
            <a href="http://online.gov.vn/Home/WebDetails/71198" target="_blank" class="bo-cong-thuong-wrap">
                <img src="/wp-content/uploads/2022/03/logo-thong-bao-bo-cong-thuong.webp" alt="Logo Đã thông báo Bộ Công Thương" width="140" height="auto" class="bo-cong-thuong mb-3">
            </a>
            <div class="footer-follow">
                <?php get_template_part( 'template-parts/section-chung/social-link' ); ?>
            </div>
        </div>
        <div class="col-12 col-lg-6 main-footer-wrap px-0 pe-lg-4 pe-xl-5">
            <?= $data['contact_info'] ?>
        </div> 
        <div class="col-12 col-lg-3 right-footer-wrap ps-xl-4 d-none d-lg-block">
            <h4 class="footer-title text-uppercase d-none d-md-block">
                Hỗ trợ khách hàng
            </h4>
            <?= $footer_menu?>
        </div>           
    </div>
</footer>

