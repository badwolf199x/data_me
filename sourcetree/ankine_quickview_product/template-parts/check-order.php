<?php
/**
 * The template for displaying check order pages.
 *
 * Template Name: Check order page
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); 
if(isset($_GET["id_order"]))
{
    $id_order=$_GET["id_order"];
}
?>
<div class="main-index">
<div class="container">
<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
</div>
<!-- Begin content check order page -->
<section id="check-order" class="section-content-check-order-page position-relative container d-flex flex-wrap pb-5">
        <div class="title-brand-page line-after col-12">
            <?php the_title('<h1 class="ecx-post-title text-capitalize text-center">', '</h1>'); ?>
        </div>
        <div class="col-12 content-check-order-page d-flex flex-wrap justify-content-center">
            <form  method="get" class="order-search ">
                <input type="search" id="order-search-field-0" class="search-field" placeholder="Nhập mã đơn hàng" value="" name="id_order">
                <button type="submit" value="Tìm kiếm">Tìm kiếm</button>
            </form>
        </div>
</section>
<?php if($id_order!='' && $id_order!=null) : 

$order = wc_get_order( $id_order );
$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
    wc_get_template(
        'order/order-downloads.php',
        array(
            'downloads'  => $downloads,
            'show_title' => true,
        )
    );
}
?>
<section class="woocommerce woocommerce-order-detail pb-4">
    <div class="container">

    <p class="order-info">
    <?php
    echo wp_kses_post(
        apply_filters(
            'woocommerce_order_tracking_status',
            sprintf(
                /* translators: 1: order number 2: order date 3: order status */
                __( 'Đơn hàng: %1$s Đã được đặt vào: %2$s .Trạng thái đơn hàng: %3$s.', 'woocommerce' ),
                '<mark class="order-number">' . $order->get_order_number() . '</mark>',
                '<mark class="order-date">' . wc_format_datetime( $order->get_date_created() ) . '</mark>',
                '<mark class="order-status">' . wc_get_order_status_name( $order->get_status() ) . '</mark>'
            )
        )
    ); ?>
    </p>
    <?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

    <h2 class="woocommerce-order-details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>

    <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

        <thead>
            <tr>
                <th class="woocommerce-table__product-name product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
                <th class="woocommerce-table__product-table product-total"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
            </tr>
        </thead>

        <tbody>
            <?php
            do_action( 'woocommerce_order_details_before_order_table_items', $order );

            foreach ( $order_items as $item_id => $item ) {
                $product = $item->get_product();

                wc_get_template(
                    'order/order-details-item.php',
                    array(
                        'order'              => $order,
                        'item_id'            => $item_id,
                        'item'               => $item,
                        'show_purchase_note' => $show_purchase_note,
                        'purchase_note'      => $product ? $product->get_purchase_note() : '',
                        'product'            => $product,
                    )
                );
            }

            do_action( 'woocommerce_order_details_after_order_table_items', $order );
            ?>
        </tbody>

        <tfoot>
            <?php
            foreach ( $order->get_order_item_totals() as $key => $total ) {
                ?>
                    <tr>
                        <th scope="row"><?php echo esc_html( $total['label'] ); ?></th>
                        <td><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>
                    </tr>
                    <?php
            }
            ?>
            <?php if ( $order->get_customer_note() ) : ?>
                <tr>
                    <th><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
                    <td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
                </tr>
            <?php endif; ?>
        </tfoot>
    </table>

    <?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
    </div>
</section>
<?php endif; ?>
<!-- End content check order page -->
</div>
<?php get_footer(); ?>