<?php
/**
 * The template for displaying header.
 *
 * @package ECOMCX
 */

$header_logo = wp_get_attachment_image_src(get_theme_mod('custom_logo'),'full')[0];

$site_name = get_bloginfo( 'name' );

$header_nav_menu = wp_nav_menu( [
	'theme_location' => 'menu-1',
	'fallback_cb' => false,
	'echo' => false,
] );

if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ))
{
    $cart=WC()->cart;
    $cart_count = $cart->cart_contents_count; 
    $cart_url = wc_get_cart_url(); 
}
if(function_exists('get_fields'))
{
$id_setting_r=625; 
$data=get_fields($id_setting_r);
}
$args_categories = array(
        'parent'    => 0,
        'hide_empty'=> 0,
        'exclude_tree' => 16,
        'taxonomy'  => 'product_cat',
        'orderby' => 'menu_order', 
        'order' => 'ASC',
    );
$categories=get_categories( $args_categories );
function show_list_category_nav($parent)
        {
        $args_cat = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent,
        'taxonomy' => 'product_cat'
        );
        $subcats = get_categories($args_cat); 
        foreach ($subcats as $sc): $args_cat['parent']=$sc->term_id; $count_childs=count(get_categories($args_cat)); ?>
            <li class="cat-item cat-item-<?= $sc->term_id ?> <?= ($count_childs==0) ? '' : 'menu-item-has-children' ?>">
                <div class="wrap-li d-flex justify-content-between">
                    <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
                        <?= $sc->name ?>
                    </a>
                    <?php  if($count_childs>0) : ?>
                        <i class="bi bi-chevron-right"></i>
                    <?php endif; ?>
                </div>
                <?php  if($count_childs>0) : ?>
                    <ul class="sub-menu subnav-children"> 
                        <?php show_list_category_nav($sc->term_id) ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach;
        }
?>
<header class="site-header">
    <div class="header-top d-lg-block d-none">
            <div class="container wrap-content d-flex justify-content-between align-items-center tex-uppercase">
                <div class="wrap-hotline col-lg-3">
                    <div class="wrap-icon d-inline-flex justify-content-center align-items-center"><i class="bi bi-telephone-fill"></i></div>
                    <a class="hotline" href="tel:<?= $data['hotline']?>"><?= $data['hotline']?></a>
                </div>
                <div class="wrap-text-run col-12 col-lg-6 overflow-hidden">
                    <div class="text-run run_text"><?= $data['text_run']?></div>
                </div>
                <div class="wrap-hotline col-lg-3"></div>
            </div>
    </div>
    <div class="header-middle">
        <div class="container d-flex justify-content-between align-items-center">
            <div class="header-middle-item wrap-icon-sidebar d-lg-none d-md-block col-3">
                <i class="bi bi-list"></i>
            </div>
            <div class="wrap-site-logo d-flex justify-content-start align-items-center col-6 col-lg-3">
                <div class="site-logo">
                    <a href="<?php echo site_url() ?>">
                        <img src="<?php echo $header_logo ?>" alt="<?php echo $site_name ?>">
                    </a>
                </div>
            </div>
            <div class="search-product d-lg-flex justify-content-center d-none col-6">
                    <?php 
                    if(in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ))
                    { 
                        echo get_product_search_form(); 
                    } 
                    ?>
            </div>
            <div class="header-middle-item wrap-icon d-flex justify-content-end align-items-start col-3">
                <div class="d-lg-none icon-search">
                    <i class="bi bi-search"></i>
                </div>
                <div class="user-account text-center">
                    <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>" class="d-flex justify-content-center align-items-center mx-auto">
                        <i class="bi bi-person-fill"></i>
                    </a>
                    <?php if(!is_user_logged_in()): ?>
                        <span class="d-none d-lg-block">Đăng nhập/Đăng ký</span>
                    <?php else : ?>
                        <span class="d-none d-lg-block"><?= wp_get_current_user()->user_nicename ?></span>
                    <?php endif; ?>
                </div>
                <ul id="mini-cart">
                    <li>
                        <a class="menu-item cart-contents d-flex justify-content-center align-items-center mx-auto" href="<?php echo $cart_url; ?>" title="giỏ hàng" >
                        <i class="bi bi-cart-check"></i>
                            <span class="cart-contents-count d-flex justify-content-center align-items-center"><?php echo $cart_count; ?></span>
                        </a>
                        <?php if(get_option ('mini_cart')==1) : ?>
                        <span id="drop-cart" class="header-quickcart"> 
                            <?php  get_template_part( 'template-parts/mini-cart' ); ?> 
                        </span>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>    
        </div>
    </div>
    <div class="header-bottom d-lg-block  d-none">
        <div class="container d-flex justify-content-between align-items-center">
            <div class="mega-menu-wrap col-3">
                <div class="main-nav col-12 d-flex justify-content-between align-items-center">
                        Danh mục sản phẩm <i class="fa fa-bars"></i>
                </div>
                <ul class="megamenu-wrap">
                    <?php foreach($categories as $key => $category) : 
                       echo mega_menu_taxonomy($category->term_id,'product_cat',$category->name,1,true);
                    endforeach; ?>
                </ul>
            </div>
            <div class="main-menu col-9">
                <?= $header_nav_menu ?>
            </div>
        </div>
    </div>
</header>

<!-- Site Sidebar Menu-->
<div  class="site-sidebar menu-custom">
    <div  class="sidebar-container">
        <div class="sidebar-logo text-center">
                <?php the_custom_logo(); ?>             
        </div>
        <div class="sidebar-menu">
            <?= $header_nav_menu ?>
        </div>
        <div class="sidebar-menu">
            <div class="wrap-content-nav-drop">
                <ul class="menu">
                    <?php show_list_category_nav(0); ?>
        	   </ul>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Site Sidebar Menu-->
