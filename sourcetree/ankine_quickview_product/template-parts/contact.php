<?php
/**
 * The template for displaying contact pages.
 *
 * Template Name: Contact us
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); 
$id_setting_r=159; 
$data=get_fields($id_setting_r);
$map_iframe=$data['map_iframe'];
$contact_content=$data['contact_content'];
$left_col_title=$data['left_col_title'];
$right_col_title=$data['right_col_title'];
$short_code_form=$data['short_code_form'];
$args_stores = array(
            'post_type'      => 'local-store',
            'post_status'    => 'publish',
            'nopaging'       => true,
            'orderby'        =>'date',
            'order'          =>'DESC',
        );
    $stores=get_posts($args_stores);
?>
<div class="main-index">
<div class="container">
<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
</div>
<!-- Begin content contact -->
    <section id="contact" class="section-content-contact position-relative container d-flex flex-wrap">
        <div class=" flex-wrap align-items-center col-md-6 col-12 content-contact">
            <h2 class="ecx-contact-title text-capitalize mb-4">
                <?= $left_col_title ?>
            </h2>
		    <div class="contact-info ms-md-3 ms-lg-5">
		        <?= $contact_content; ?>
		    </div>
            <div class="contact-form ms-md-3 ms-lg-5">
		        <?= do_shortcode($short_code_form); ?>
		    </div>
        </div>
        <div class="col-md-6 col-12 ecomcx-local-store-wrap px-4 py-4 px-lg-5 py-lg-5">
            <div class="ecomcx-local-store px-3 px-lg-5">
                <h1 class="ecx-local-store-title mb-4 pt-3">
                    <?= $right_col_title ?>
                </h1>
                <?php $temp_post = $GLOBALS['post'];
                        foreach ( $stores as $store ) {
                        setup_postdata( $store );
                        $GLOBALS['post'] = $store; 
                        $data=get_post_meta( $store->ID, 'dvls_data', true );
                        $address=$data['address']; ?>
                        <div class="store mb-3">
                            <div class="ecx-store-info">
                                <div class="ecx-store-meta-data">
                                    <div class="ecx-store-item title">
                                        <?= $address ?>
                                    </div>
                                </div>
                            </div>
                            <a href="/he-thong-cua-hang/" class="button view-more-store">
                                Xem bản đồ
                            </a>
                        </div>
                <?php  }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>
            </div>
        </div>
    </section>
<!-- End content contact -->
</div>
<?php get_footer(); ?>