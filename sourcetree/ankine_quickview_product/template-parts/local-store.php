<?php
/**
 * The template for displaying local store pages.
 *
 * Template Name: Hệ thống cửa hàng
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); ?>
<div class="main-index">
	<div class="container pt-50">
		<?= do_shortcode('[devvn_local_stores]'); ?>
	</div>
</div>
<?php get_footer(); ?>