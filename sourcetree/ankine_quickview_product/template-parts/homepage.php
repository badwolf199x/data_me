<?php
/**
 * The template for displaying homepage pages.
 *
 * Template Name: Trang chủ
 * 
 * @package ECOMCX Theme
 */
/*huy style woo*/
add_filter( 'woocommerce_enqueue_styles', 'ecx_dequeue_styles_woo' );
function ecx_dequeue_styles_woo( $enqueue_styles ) {
unset( $enqueue_styles['woocommerce-general'] ); // Remove woocommerce.css
unset( $enqueue_styles['woocommerce-layout'] ); // Remove woocommerce-layout.css
unset( $enqueue_styles['woocommerce-smallscreen'] ); // Remove the woocommerce-smallscreen.css
return $enqueue_styles;
}?>

<?php get_header();
//setting riêng
$id_setting_r=157; 
$data=get_fields($id_setting_r);
//data banner
$section_banner=$data['section_banner'];
$banner_middle_top=$section_banner['banner_slider'];
$args_categories = array(
        'parent'    => 0,
        'hide_empty'=> 0,
        'exclude_tree' => 16,
        'taxonomy'  => 'product_cat',
        'orderby' => 'menu_order', 
        'order' => 'ASC',
    );
$categories=get_categories( $args_categories );
//data show product featured
$section_featured_product=$data['section_show_product_featured'];
$args_products_featured = array( 
    'post_type' => 'product',
    'post_status'   => 'publish',
    'posts_per_page' => 30, 
    'orderby'        =>'meta_value_num date',
    'order'        =>'DESC',
    'meta_key' => '_featured_product',
    'meta_query' => array(                  
        'relation' => 'AND',
        array(
            'key' => '_price',
            'value' => 0,
            'compare' => '>'),
        array(
            'key' => '_stock_status',
            'value' => 'instock',
            'compare' => '=')
    ),
);
$products_featured = new WP_Query( $args_products_featured );
//data show product
$Section_Show_Product=$data['Section Show Product'];
for($i=1;$i<=$Section_Show_Product['section_show_product_count'];$i++)
{
    if($Section_Show_Product['section_show_product_'.$i]!='')
    {
        $section_show_product[$i]=$Section_Show_Product['section_show_product_'.$i];
    }
}
$count_section_show_product=$Section_Show_Product['section_show_product_count'];
//data home blog
$section_home_blog=$data['section_show_blog'];
if($section_home_blog['section_blog_enable_sticky_post']==true)
{
    if(get_option( 'sticky_posts' )!=null)
    {
        $args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'orderby'        =>'date',
            'order'        =>'DESC',
            'post__in'            => get_option( 'sticky_posts' ),
            'ignore_sticky_posts' => 1,
        );
        $blog_featured_sticky=get_posts($args_blog_sticky);
        if(count($blog_featured_sticky)<6)
        {
            $args_blog = array(
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => 6-count($blog_featured_sticky),
                'orderby'        =>'date',
                'order'        =>'DESC',
                'post__not_in'            => get_option( 'sticky_posts' ),
                'ignore_sticky_posts' => 1,
            );
            $blog_featured=get_posts($args_blog);
        }
        $blog_featured_sticky=array_merge($blog_featured_sticky,$blog_featured);
    }
    elseif(get_option( 'sticky_posts' )==null)
    {
        $args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'orderby'        =>'date',
            'order'        =>'DESC',
        );
        $blog_featured_sticky=get_posts($args_blog_sticky);
    }
}
else
{
    $args_blog_sticky = array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'orderby'        =>'date',
            'order'        =>'DESC',
        );
    $blog_featured_sticky=get_posts($args_blog_sticky);
}
?>
<div class="main-index">

<!-- Begin Home banner -->
    <section id="banner-home">
        <div class="container flex-wrap align-items-stretch d-flex justify-content-between">
            <div class="megamenu-left col-3">
                <ul class='megamenu-wrap'>
                <?php foreach($categories as $key => $category) : 
                    echo mega_menu_taxonomy($category->term_id,'product_cat',$category->name,1,true);
                endforeach; ?>
                </ul>
            </div>
            <div class="banner-middle col-9">
                <div class="banner-middle-top">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <?php for ($i=1; $i <= $banner_middle_top['item_count']; $i++) : ?>
                                <div class="swiper-slide">
                                    <?php if($banner_middle_top['bottom_'.$i]['url_link']!=null && $banner_middle_top['bottom_'.$i]['url_link']!=''): ?>
                                        <a href="<?= $banner_middle_top['bottom_'.$i]['url_link']?>">
                                    <?php endif; ?>
                                    <?= wp_get_attachment_image($banner_middle_top['bottom_'.$i]['select_image'],'large',false,['class'=>'banner-img','alt'=>'Banner Image'.$i])?> 
                                    <?php if($banner_middle_top['bottom_'.$i]['url_link']!=null && $banner_middle_top['bottom_'.$i]['url_link']!=''): ?>
                                        </a>
                                    <?php endif; ?>    
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- End of Home banner -->

<!-- Begin Home slider featured -->
    <section id="slider-featured" style="background-color:<?= $section_featured_product['background_color'] ?>">
        <div class="container  pt-4 pb-50" >
            <div class="section-wrap">
            <div class="section-header">
                <div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
                    <a class="header-link text-center" href="/cua-hang/">
                        <h2 class="section-label text-uppercase">
                            <?= $section_featured_product['section_label'] ?>
                        </h2>
                        <div class="lead text-center">
                            <img src="/wp-content/uploads/2022/03/icon-header.png">
                        </div>
                    </a>
                </div>
            </div>
            <div class="ecx-product-featured-wrap justify-content-between d-flex flex-wrap align-items-stretch">
                <?php if($section_featured_product['enable_banner']==true) : ?>
                    <div class="ecx-product-show-banner justify-content-between d-flex flex-wrap d-lg-block" > <?php 
                        for($j=1;$j<=$section_featured_product['banner_count'];$j++) : ?>
                            <div class="product-show-banner-<?=$j?>-wrap" style="<?=($section_featured_product['banner_count']==1) ? 'width:100%': ''; ?>">
                                <?php if($section_featured_product['url_link_'.$j]!=null && $section_featured_product['url_link_'.$j]!=''): ?>
                                    <a href="<?= $section_featured_product['url_link_'.$j]?>">
                                <?php endif; ?>
                                <?= wp_get_attachment_image($section_featured_product['select_banner_'.$j]['id'],'large',false,['class'=>'banner-img','alt'=>'Banner Image Product Show '.$i.'-'.$j])?>
                                <?php if($section_featured_product['url_link_'.$j]!=null && $section_featured_product['url_link_'.$j]!=''): ?>
                                    </a>
                                <?php endif; ?>
                            </div> <?php
                        endfor; ?>
                    </div> <?php 
                endif ?>
                <div class="swiper ecx-product-loop">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-wrapper">
                        <?php   if ( $products_featured->have_posts() ) :  $c=1;
                            while ( $products_featured->have_posts() ) :
                                $products_featured->the_post(); ?>
                                <?php if($c==1 || $c%2==1) : ?>
                                    <div class="swiper-slide"> 
                                <?php endif; ?>
                                    <?php wc_get_template_part( 'content', 'product' ); ?>
                                <?php if($c==2 || $c%2==0) : ?>
                                    </div> 
                                <?php endif; ?>
                            <?php $c++; endwhile; 
                            do_action( 'woocommerce_after_shop_loop' ); 
                        else :
                                do_action( 'woocommerce_no_products_found' );
                        endif; 
                        wp_reset_postdata(); ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            </div>
        </div>
    </section>
<!-- End of Home slider featured -->

<?php for($i=1;$i<=$count_section_show_product;$i++) : 
      $data_section=$section_show_product[$i];                         
      if(($data_section['select_product']!='' && $data_section['select_type']=='custom') || ($data_section['select_category']!='' && $data_section['select_type']=='category') || $data_section['select_type']=='new') : 
      if($data_section['section_enable']==true) : 
      if($data_section['section_style']=='type_1') :
        $color_bg='#fff';
        $color_label='#6b4620';
        $src_img_label='/wp-content/uploads/2022/03/icon-header.png';
      elseif($data_section['section_style']=='type_2'): 
        $color_bg='#f8f8f8';
        $color_label='#6b4620';
        $src_img_label='/wp-content/uploads/2022/03/icon-header.png';
      else :
        $color_bg='#6b4620';
        $color_label='#fff';
        $src_img_label='/wp-content/uploads/2022/03/icon-header-1.png';
      endif;?>
<!-- Begin show product -->
    <section id="show-product-<?= $i ?>" class="show-product <?= $data_section['section_style'] ?>" style="background-color:<?= $color_bg ?>">
        <div class="container pt-4 pb-50" >
            <div class="section-header">
                <div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
                    <a class="header-link text-center" <?= ($data_section['section_link']!='' && $data_section['section_link']!=null) ? 'href="'.$data_section['section_link'].'"' : '' ?> >
                        <h2 class="section-label text-uppercase" style="color: <?= $color_label ?>">
                            <?= $data_section['section_label'] ?>
                        </h2>
                        <div class="lead text-center">
                            <img src="<?= $src_img_label ?>">
                        </div>
                    </a>
                </div>
            </div>
            <div class="ecx-product-show-wrap-<?=$i?>"> 
                <div class="ecx-product-show">
                    <div class="ecx-product-loop swiper">   <?php 
                        if($data_section['select_type']=='custom') :  
                                $product_ids=($data_section['select_product']);
                                $args_products_custom = array(
                                    'posts_per_page' => 20,
                                    'post_status'   => 'publish',
                                    'post_type' => 'product',
                                    'post__in' => $product_ids, 
                                    'orderby'        =>'meta_value_num date',
                                    'order'        =>'DESC',
                                    'meta_key'=>'_featured_product',
                                    'meta_query' => array(                  
                                        'relation' => 'AND',
                                        array(
                                            'key' => '_price',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_stock_status',
                                            'value' => 'instock',
                                            'compare' => '=')
                                    ),
                                );
                                $products_show = new WP_Query( $args_products_custom);
                        elseif($data_section['select_type']=='category') :
                                $category_id=$data_section['select_category'];
                                $args_products_category = array( 
                                    'posts_per_page' => 20,
                                    'post_status'   => 'publish',
                                    'post_type' => 'product', 
                                    'orderby'        =>'meta_value_num date',
                                    'order'        =>'DESC',
                                    'tax_query' => array(                     
                                        'relation' => 'OR',                     
                                        array(
                                            'taxonomy' => 'product_cat',                
                                            'field' => 'id',                    
                                            'terms' => $category_id,    
                                            'include_children' => true,           
                                            'operator' => 'IN'                    
                                        ),
                                    ),
                                    'meta_key'=>'_featured_product',
                                    'meta_query' => array(                  
                                        'relation' => 'AND',
                                        array(
                                            'key' => '_price',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_stock_status',
                                            'value' => 'instock',
                                            'compare' => '=')
                                    ),
                                );
                                $products_show = new WP_Query( $args_products_category);
                        elseif($data_section['select_type']=='new') :
                                $args_products_new = array( 
                                    'posts_per_page' => $data_section['select_count_product_new'],
                                    
                                    'post_type' => 'product', 
                                    'post_status'   => 'publish',
                                    'orderby'        =>'date',
                                    'order'        =>'DESC',
                                    'meta_query' => array(                  
                                        'relation' => 'AND',
                                        array(
                                            'key' => '_price',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_stock_status',
                                            'value' => 'instock',
                                            'compare' => '=')
                                    ),
                                );
                                $products_show = new WP_Query( $args_products_new);
                        endif; ?>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-wrapper">
                        <?php if ( $products_show->have_posts() ) :
                            while ( $products_show->have_posts() ) :
                                $products_show->the_post(); ?>
                                <div class="swiper-slide">
                                    <?php wc_get_template_part( 'content', 'product' ); ?>
                                </div>
                            <?php endwhile; 
                        else :
                                do_action( 'woocommerce_no_products_found' );
                        endif; 
                        woocommerce_reset_loop();
                        wp_reset_query();
                        wp_reset_postdata(); ?>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- End of show product -->

<?php $data_banner=$data['section_show_banner']['section_show_banner_'.$i];
if($data_banner['enable_banner']==true) : ?>
<!-- Begin show banner -->
    <section id="show-banner-<?= $i ?>" class="show-banner">
        <div class="container d-flex flex-wrap justify-content-between" >
            <?php for($im=1;$im<=$data_banner['banner_count'];$im++) : ?>
                <?php if($data_banner['banner_'.$im]!='' && $data_banner['banner_'.$im]!=null) :
                    $class_img="banner-img ";
                    $class="";
                    if($data_banner['banner_count']==1):
                        $class.="col-12 ";
                    else :
                        $class.="col-12 col-lg-6 ";
                        if($im==1):
                            $class.="pe-0 pe-lg-1 ";
                        else :
                            $class.="pe-0 ps-lg-1 pt-4 pt-lg-0";
                        endif;
                    endif;
                    if($data_banner['url_link_'.$im]!=null && $data_banner['url_link_'.$im]!=''): ?>
                        <a href="<?= $data_banner['url_link_'.$im] ?>" class="<?= $class ?>">
                    <?php endif; 
                    if($data_banner['url_link_'.$im]==null || $data_banner['url_link_'.$im]==''): 
                        $class_img.=$class;
                    endif; 
                    echo wp_get_attachment_image($data_banner['banner_'.$im]['id'],'large',false,['class'=>$class_img,'alt'=>'Banner Image'.$i]);
                    if($data_banner['url_link_'.$im]!=null && $data_banner['url_link_'.$im]!=''): ?>
                        </a>
                    <?php endif; 
                endif;
            endfor; ?>
            <img src="/wp-content/uploads/2022/03/2ca9efc877218f7fd630.jpg" class='col-12'>
        </div>
    </section>
<!-- End of show banner -->
<?php endif; endif; endif; endfor; ?>

<?php if($section_home_blog['section_blog_enable']==true) : ?>
<!-- Begin Home blog -->
    <section id="home-blog">
        <div class="container  pt-4 pb-50" >
            <div class="section-header">
                <div class="section-label-wrap d-flex flex-wrap align-items-start justify-content-center"> 
                    <a class="header-link text-center" href="/xu-huong-lam-dep/">
                        <h2 class="section-label text-uppercase">
                            <?= $section_home_blog['section_blog_label'] ?>
                        </h2>
                        <div class="lead text-center">
                            <img src="/wp-content/uploads/2022/03/icon-header.png">
                        </div>
                    </a>
                </div>
            </div>
            <div class="ecx-blog-slider">
                <div class="swiper ecx-blog-loop">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-wrapper">
                        <?php $temp_post = $GLOBALS['post'];
                        foreach ( $blog_featured_sticky as $blog ) {
                        setup_postdata( $blog );
                        $GLOBALS['post'] = $blog; $category=wp_get_post_terms( $blog->ID, 'category' );?>
                        <article class="swiper-slide post">
                            <div class="wrap-tag">
                                <a href="<?= $category[0]->slug ?>">
                                    <?= $category[0]->name ?>
                                </a>
                            </div>
                            <a href="<?php the_permalink() ?>">
                                <div class="ecx-post-thumbnail">
                                    <?php the_post_thumbnail() ?>
                                </div>
                            </a>
                            <div class="ecx-post-info">
                                <h3 class="ecx-post-title mb-3">
                                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                </h3>
                                <div class="ecx-post-meta-data">
                                    <div class="ecx-post-excerpt">
                                        <?php the_excerpt('(more…)'); ?>
                                    </div>
                                </div>
                            </div>
                        </article>
                <?php }
                wp_reset_postdata();
                $GLOBALS['post'] = $temp_post;
                ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
<!-- End of Home blog-->
<?php endif; ?>

</div>

<?php get_footer(); ?>