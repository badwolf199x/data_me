jQuery(function($){
//Add element to menu
$(document).ready(function()
{
	//button show more wooo
  if($('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').height()>0)
	  {
		  $('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').each(function()
		  {
			  if($(this).height()>400)
			  {
			  $(this).append('<p class="show-more text-center readmore"><a class="btn-custom">Đọc thêm</a></p>');
			  $(this).css('max-height','400px');
			  $('.btn-custom').click(function(){
				  if($(this).text()=='Đọc thêm')
				  {
					  $(this).parents('.show-more').removeClass('readmore');
					  $(this).parents('.show-more').addClass('hidden');
					  $(this).text('Thu gọn');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('max-height','unset');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('padding-bottom','126px');
					  show++;
				  }
				  else
				  {
					  $(this).parents('.show-more').removeClass('hidden');$(this).parents('.show-more').addClass('readmore');
					  $(this).text('Đọc thêm');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('max-height','400px');
					  $(this).parents('.woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel').css('padding-bottom','');
					  show--;
				  }
			  });
			  }
		  });
	  }
    //scroll top button
	$('#up-scroll').click(function () {
	        $("html, body").animate({
	            	scrollTop: 0
	        }, 600);
	        return false;
	});
	//changer brand
	if($('select#cat').length>0)
	{
		$('select#cat').change(function()
		{
			var val=$(this).val();
			if(val!=='0')
			{
				$('.col-brand').each(function()
				{
					if($(this).attr('id')!=val.substr(0, 1).toUpperCase())
					{
						if($(this).hasClass('d-flex'))
						{
						    $(this).removeClass('d-flex');
						    $(this).addClass('d-none');
						}
					}
					else
					{
						if($(this).hasClass('d-none'))
						{
						    $(this).removeClass('d-none');
						    $(this).addClass('d-flex');
						}
						$(this).find('.content-col-brand').each(function()
						{
							if($(this).attr('id')!=val)
						    {
						    	$(this).css('display','none');
						    }
						    else
						    {
						    	$(this).css('display','block');
						    }
						});
					}
				});
			}
			else
			{
				$('.col-brand').each(function()
				{
					$(this).removeClass('d-none');
					$(this).removeClass('d-flex');
					$(this).addClass('d-flex');
					$(this).find('.content-col-brand').each(function()
						    	{
						    		$(this).css('display','block');
						    	});
				});
			}
		});
	}
	//sticky header
	function sticky()
	{   var header = $(".site-header");
        var scroll = $(window).scrollTop();
            if (scroll > 210) {
                header.addClass("sticky");
            } else {
                header.removeClass("sticky");
            }  
	}
	if($('body.page-id-611').length==0)
	{
	    sticky();
	    //scroll on all
	    $(window).scroll(function()
	    {
		    sticky();
        });
	}
	if ( $(window).width() <768 ){
			$('#footer-menu-accordion .accordion-collapse').each(function()
			{
				$(this).removeClass('show');
			});
		}
    if ( $(window).width() >991 ){
        $( ".menu-item-has-children > a" ).append(' <span class="icon-nav icon-nav--down"><i class="bi bi-chevron-down"></i></span> ');
        $(".menu-item-has-children > ul").wrap(' <div class="dropdown-wrapper dropdown-width--auto"><div class="dropdown-content"><div class="dropdown-list"></div></div></div> ');
     
    }
	else
	{
		$('header.site-header').append('<div class="drop_nav_categories"></div>');
		//droplist category header
		$( ".sidebar-menu ul li.cat-item .wrap-li" ).click(function()
		{
			if($(this).closest('li').hasClass('active'))
			{
				$(this).closest('li').removeClass('active');
			}
			else
			{
				$(this).closest('li').addClass('active');
			}
		});
		$( ".menu-item-has-children > ul" ).addClass(' subnav-children ');
        $(".sidebar-menu ul:not(.subnav-children) > .menu-item-has-children > a").removeAttr("href");
        $( ".sidebar-menu ul:not(.subnav-children) > .menu-item-has-children > a" ).append('<i class="bi bi-chevron-right"></i>');
		$( ".sidebar-menu ul li.menu-item" ).click(function()
		{
			if($(this).hasClass('active'))
			{
				$(this).removeClass('active');
			}
			else
			{
				$(this).parents('.sidebar-menu').find('li.menu-item-has-children').each(function()
			    {
				    $(this).removeClass('active');
			    });
				$(this).addClass('active');
			}
			
		});
		$( ".icon-search i" ).click(function(){
			if($(this).hasClass('bi-search'))
		    {
				$(this).removeClass('bi-search');
				$(this).addClass('bi-x-lg');
				$('.search-product').removeClass('d-none');
				$('.site-header').addClass('search');
			}
		    else
			{
				$(this).removeClass('bi-x-lg');
				$(this).addClass('bi-search');
				$('.search-product').addClass('d-none');
				$('.site-header').removeClass('search');
			}
		   });
		$( '.wrap-icon-sidebar' ).on( "click", function(e) {
			$('.site-sidebar.menu-custom').addClass('active');
			$('.site-sidebar.menu-custom').click(function (e){
				let sidebar = $('.sidebar-container');
				if (!sidebar.is(e.target) && sidebar.has(e.target).length === 0) 
				{
					$( '.site-sidebar.menu-custom' ).removeClass( "active" );
                }
			})
		});
		$( '.filter-button' ).on( "click", function(e) {
			$('.ecx-sidebar-widget').addClass('active');
			$('.ecx-sidebar-widget').click(function (e){
				let sidebar = $('.sidebar-shop-wrap');
				if (!sidebar.is(e.target) && sidebar.has(e.target).length === 0) 
				{
					$( '.ecx-sidebar-widget' ).removeClass( "active" );
                }
			})
		});
		//out click hidden mobile
		$(document).click(function (e){
			let icon = $('.icon-search');
			if (!icon.is(e.target) && icon.has(e.target).length === 0) {
				let drop = $('.search-product');
				if (!drop.is(e.target) && drop.has(e.target).length === 0) {
				    if($(".icon-search i").hasClass('bi-x-lg'))
		            {
		    		    $(".icon-search i").removeClass('bi-x-lg');
				        $(".icon-search i").addClass('bi-search');
			    	    $('.search-product').addClass('d-md-none');
			    	    $('.site-header').removeClass('search');
			        }
			    }
			}
		});
	}
//table of content single
if($('.single.single-post').width()>0)
{
		var title=$('div#toc_container p.toc_title').text();
		var content=$('div#toc_container ul.toc_list');
	if(title!="")
		{
		$('.wrap-toc h4#accordion-headingOne button').text(title);
		$('.wrap-toc div#flush-collapseOne .accordion-body').append(content);
		$('div#toc_container').remove();
		}
    else
		{
			$('#toc-accordion').remove();
		}
}
//swiper product img
if($('body.single.single-product').width()>0)
{
	$('ul.flex-direction-nav').prepend($('ol.flex-control-nav.flex-control-thumbs'));
	$('ul.flex-direction-nav').addClass('swiper');
	$('ol.flex-control-nav.flex-control-thumbs').addClass('swiper-wrapper');
	$('ol.flex-control-nav.flex-control-thumbs li').each(function()
	{
		$(this).addClass('swiper-slide');
	})
	var swiper_product_img = new Swiper('ul.flex-direction-nav', {
  slidesPerView: 3,
  spaceBetween: 6,
	breakpoints: {
	1480: {
      slidesPerView: 6,
      spaceBetween: 6
    },
    992: {
      slidesPerView: 5,
      spaceBetween: 6
    },
	768: {
      slidesPerView: 4,
      spaceBetween: 6
    },
	500: {
      slidesPerView: 5,
      spaceBetween: 6
    },
  },
  loop:false,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: 'li.flex-nav-next',
    prevEl: 'li.flex-nav-prev',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  },
   });
}
//filter brand & drop product category
if($('body.archive.woocommerce').width()>0)
{
	$('.brand-filter .button-filter').click(function(){
		$(this).parents('.siderbar-section-content').attr('data-filter',$(this).attr('data-key'));
	});
	$(".siderbar-section-content .child-indicator").click(function()
	{
		if($(this).closest('li').hasClass('active'))
		{
			$(this).closest('li').removeClass('active');
			$(this).find('i').removeClass('bi-chevron-up');
	    }
		else
		{
			$(this).closest('li').addClass('active');
			$(this).find('i').addClass('bi-chevron-up');
		}
	});
}
if($('body.archive:not(.woocommerce)').width()>0)
{
	$(".siderbar-section-content .child-indicator").click(function()
	{
		if($(this).closest('li').hasClass('active'))
		{
			$(this).closest('li').removeClass('active');
			$(this).find('i').removeClass('bi-chevron-up');
	    }
		else
		{
			$(this).closest('li').addClass('active');
			$(this).find('i').addClass('bi-chevron-up');
		}
	});
}

//count down date time
if($('.home').width()>0)
{  
//swiper banner
var swiper_banner = new Swiper('.banner-middle-top .swiper', {
  slidesPerView: 1,
  loop:true,
  spaceBetween: 0,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  },
  autoplay: {
   delay: 6000,
   disableOnInteraction	:false,
   pauseOnMouseEnter: true,
 }
   });
//swiper slider product
$('.show-product').each(function()
	{
		count_show_product=$(this).attr('id');
		new Swiper('#'+count_show_product+' .swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    slidesPerGroup: 1,
    breakpoints: {
    992: {
      slidesPerView: 5,
      spaceBetween: 10,
      slidesPerGroup: 5,
    },
	768: {
      slidesPerView: 3,
      spaceBetween: 10,
      slidesPerGroup: 3,
    },
	500: {
      slidesPerView: 2,
      spaceBetween: 10,
      slidesPerGroup: 2,
    },
  },
    loop: true,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable:true,
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  },
  autoplay: {
   delay: 5000,
   disableOnInteraction	:false,
   pauseOnMouseEnter: true,
 }
});	

	});
}
if($('#slider-featured .swiper').length>0)
{
//swiper slider featured product
var slider_sale = new Swiper('#slider-featured .swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    slidesPerGroup: 1,
    breakpoints: {
	768: {
      slidesPerView: 3,
      spaceBetween: 10,
      slidesPerGroup: 3,
    },
	500: {
      slidesPerView: 2,
      spaceBetween: 10,
      slidesPerGroup: 2,
    },
  },
  loop:true,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable:true,
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  },
  autoplay: {
   delay: 5000,
   disableOnInteraction	:false,
   pauseOnMouseEnter: true,
 }
});
}
if($('.ecx-blog-slider').width()>0)
{
	var ecx_blog_slider=new Swiper('.ecx-blog-slider .swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    slidesPerGroup: 1,
    breakpoints: {
    992: {
      slidesPerView: 3,
      spaceBetween: 20,
      slidesPerGroup: 3,
    },
	600: {
      slidesPerView: 2,
      spaceBetween: 20,
      slidesPerGroup: 2,
        },
    },
    loop: true,
    direction:    'horizontal',
    initialSlide: 0,
    PreventInteractionOnTransition: true,
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },
    pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable:true,
    },
    keyboard: {
    enabled: true,
    onlyInViewport: false,
    },
    autoplay: {
    delay: 5000,
    disableOnInteraction	:false,
    pauseOnMouseEnter: true,
    }
    });	
  }
//tore slider
if($('.before-footer').width()>0)
{
  var before_footer=new Swiper('.before-footer .swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    slidesPerGroup: 1,
    breakpoints: {
    992: {
      slidesPerView: 5,
      spaceBetween: 20,
      slidesPerGroup: 5,
    },
	768: {
      slidesPerView: 3,
      spaceBetween: 20,
      slidesPerGroup: 3,
    },
	500: {
      slidesPerView: 2,
      spaceBetween: 20,
      slidesPerGroup: 2,
    },
  },
    loop: true,
    direction:    'horizontal',
    initialSlide: 0,
    PreventInteractionOnTransition: true,
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },
    pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable:true,
    },
    keyboard: {
    enabled: true,
    onlyInViewport: false,
    },
    autoplay: {
    delay: 5000,
    disableOnInteraction	:false,
    pauseOnMouseEnter: true,
    }
    });	
}
});
})

