<?php

    // add extra metabox tab to woocommerce
    if( !function_exists('ecx_add_wc_extra_metabox_tab')){
        function ecx_add_wc_extra_metabox_tab($tabs){
            $ecx_tab = array(
                'label'    => 'Nhãn dán sản phẩm',
                'target'   => 'ecx_product_data',
                'class'    => '',
                'priority' => 80,
            );
            $tabs['ecx_product_badge'] = $ecx_tab;
            return $tabs;
        }
        add_filter( 'woocommerce_product_data_tabs', 'ecx_add_wc_extra_metabox_tab' );
    }

    // add metabox to general tab
    if( !function_exists('ecx_add_metabox_to_general_tab')){
        function ecx_add_metabox_to_general_tab(){
            echo '<div id="ecx_product_data" class="panel woocommerce_options_panel hidden">';
                woocommerce_wp_text_input( array(
                    'id'          => '_saleflash_text',
                    'label'       => 'Nhãn dán sản phẩm',
                    'placeholder' => 'onsale',
                    'description' => 'Nhập nhãn dán',
                ) );
            echo '</div>';
        }
        add_action( 'woocommerce_product_data_panels', 'ecx_add_metabox_to_general_tab' );
    }
    // Update data
    if( !function_exists('ecx_save_metabox_of_general_tab') ){
        function ecx_save_metabox_of_general_tab( $post_id ){
            $saleflash_text = wp_kses_post( stripslashes( $_POST['_saleflash_text'] ) );
            update_post_meta( $post_id, '_saleflash_text', $saleflash_text);
        }
        add_action( 'woocommerce_process_product_meta', 'ecx_save_metabox_of_general_tab');
    }

?>