<?php
//viewed cache
if( !function_exists('viewedProduct'))
{
        function viewedProduct(){
        session_start();
        if(!isset($_SESSION["viewed"])){
            $_SESSION["viewed"] = array();
        }
        if(is_singular('product')){
            $_SESSION["viewed"][get_the_ID()] = get_the_ID();
        }
    }
    add_action('wp', 'viewedProduct');
}
//viewed short_code
if( !function_exists('show_viewed_product'))
{
    function show_viewed_product()
    {  
        if(isset($_SESSION["viewed"]) && $_SESSION["viewed"]) :
            $viewed_products=$_SESSION["viewed"];
            foreach($viewed_products as $product_id) :
            $product=wc_get_product( $product_id ); ?>
            <li class="item d-flex align-items-stretch">
                <a class="item-thumbnail" href="<?php get_permalink($product->get_id()); ?>">
                    <?= wp_get_attachment_image($product->get_image_id(),'thumbnail'); ?>
                </a>
                <div class="item-info d-flex flex-wrap">
                <a class="item-name" href="<?php get_permalink($product->get_id()); ?>">
                    <?= $product->get_name(); ?>
                </a>
                <?php if ( $product->get_price_html()!="" ) : ?>
                    <span class="item-price"><?= $product->get_price_html() ?></span>
                <?php endif; ?>
                </div>
            </li> <?php
            endforeach;
        endif;
    }
}
