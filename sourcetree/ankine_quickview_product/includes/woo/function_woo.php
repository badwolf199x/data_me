<?php 

add_action('woocommerce_after_add_to_cart_button','ecx_quickbuy_after_addtocart_button');

function ecx_quickbuy_after_addtocart_button(){

    global $product;

    ?>

    <style>

        .ecx-quickbuy button.single_add_to_cart_button.loading:after {

            display: none;

        }

        .ecx-quickbuy button.single_add_to_cart_button.button.alt.loading {

            color: #fff;

            pointer-events: none !important;

        }

        .ecx-quickbuy button.buy_now_button {

            position: relative;

            color: rgba(255,255,255,0.05);

            background-color: green!important;

        

        }

        .ecx-quickbuy button.buy_now_button:after {

            animation: spin 500ms infinite linear;

            border: 2px solid #fff;

            border-radius: 32px;

            border-right-color: transparent !important;

            border-top-color: transparent !important;

            content: "";

            display: block;

            height: 16px;

            top: 50%;

            margin-top: -8px;

            left: 50%;

            margin-left: -8px;

            position: absolute;

            width: 16px;

        }

    </style>

    <button type="button" class="button buy_now_button">Mua ngay</button>

    <input type="hidden" name="is_buy_now" class="is_buy_now" value="0" autocomplete="off"/>

     <?php dynamic_sidebar('block-action-down-cart'); ?>

    <script>

        jQuery(document).ready(function(){

            jQuery('body').on('click', '.buy_now_button', function(){

                var thisParent = jQuery(this).parents('form.cart');

                if(jQuery('.single_add_to_cart_button', thisParent).hasClass('disabled')) return false;

                thisParent.addClass('ecx-quickbuy');

                jQuery('.is_buy_now', thisParent).val('1');

                jQuery('.single_add_to_cart_button', thisParent).trigger('click');

            });

        });

    </script>

    <?php

}

add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');

function redirect_to_checkout($redirect_url) {

    if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {

        $redirect_url = wc_get_checkout_url(); //or wc_get_cart_url()

    }

    return $redirect_url;

}

if( !function_exists('show_list_category'))
{
    function show_list_category($parent,$current_term_id)
    {
        $args_cat = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent,
        'exclude_tree' =>$current_term_id,
        'taxonomy' => 'product_cat'
        );
        $subcats = get_categories($args_cat); 
        foreach ($subcats as $sc): $args_cat['parent']=$sc->term_id; $count_childs=count(get_categories($args_cat)); ?>
            <li class="cat-item cat-item-<?= $sc->term_id ?>">
                <?= ($count_childs==0) ? '<span class="no-child"></span>' : '<span class="child-indicator"><i class="bi bi-chevron-right"></i></span>' ?>
                <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
                    <?= $sc->name ?>
                    <span class="count">(<?=$sc->count?>)</span>
                </a>
                <?php  if($count_childs>0) : ?>
                    <ul class="children"> 
                        <?php show_list_category($sc->term_id,$current_term_id) ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach;
    }
}