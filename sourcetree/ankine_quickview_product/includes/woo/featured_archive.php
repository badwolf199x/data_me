<?php
//featured archive
function featured_archive($post_id, $post, $update) 
{
    if($post->post_type=='product')
    {
        $featureds=wc_get_featured_product_ids();
        if(in_array($post_id,$featureds))
        {
            update_post_meta( $post_id, '_featured_product', 2 );
        }
        else
        {
            update_post_meta( $post_id, '_featured_product', 1 );
        }
    }
}
add_action( 'wp_insert_post', 'featured_archive', 10, 3 );

add_filter('woocommerce_get_catalog_ordering_args', 'custom_catalog_ordering_args');
//Function to handle choices
function custom_catalog_ordering_args($args) {
    $orderby_value = isset( $_GET['orderby'] ) ? $_GET['orderby'] : '';
    // Changed the $_SESSION to $_GET
    if ($orderby_value == "featured_cate" || $orderby_value=='') {
        $args['meta_key'] = '_featured_product';
        $args['orderby'] = 'meta_value_num date';
        $args['order'] = "DESC";
    } 
    return $args;
}

add_filter( 'woocommerce_catalog_orderby', 'custom_catalog_orderby' );
function custom_catalog_orderby( $orderby ) {
    return array(
        'featured_cate'    => __('Thứ tự nổi bật', 'woocommerce'),
    ) + $orderby ;
    return $orderby ;
}

apply_filters( 'woocommerce_default_catalog_orderby', 'custom_default_catalog_orderby' );
function custom_default_catalog_orderby( $default_orderby ) {
    return 'featured_cate';
}

add_filter('admin_head','featured_product');
function featured_product()
{
    if(isset($_GET['post_type']))
	{
if($_GET['post_type'] =='product')
{ ?>
 <script type="text/javascript">
 	jQuery(function($){
 	    $(document).ready(function(){
 	        $('td.column-featured a').click(function(){
 	            var id_send=$(this).parent('td.column-featured').parent('tr.type-product').attr('id');
 	            id_send = id_send.replace(/post-/g, '');
 	           function box()
 	           {
 	            $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "ajax_featured", 
                        id_send: id_send,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) 
                    {
                        if(response.success)
                        {

                        }
                        else 
                        {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
 	           }
                 setTimeout( box , 300 );
 	        });
 	    })
 	})
 </script>
<?php } 
	}
}
function ajax_featured()
{
    ob_start();
    $post_id =  (isset($_POST['id_send']))?esc_attr($_POST['id_send']) : '';
    $featureds=wc_get_featured_product_ids();
        if(in_array($post_id,$featureds))
        {
            update_post_meta( $post_id, '_featured_product', 2 );
        }
        else
        {
            update_post_meta( $post_id, '_featured_product', 1 );
        }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_ajax_featured', 'ajax_featured' );
add_action( 'wp_ajax_nopriv_ajax_featured', 'ajax_featured' );

