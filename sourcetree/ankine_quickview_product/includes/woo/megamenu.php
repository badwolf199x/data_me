<?php 
function mega_menu_page($id,$label,$loop)
{
    $parent=$id;
    $label_first=$label;
    $loop=$loop;
    $args_page = array(
        'nopaging' => true,
        'order'          => 'ASC',
        'orderby' => 'date', 
        'post_parent'    => $parent,
        'post_type'      => 'page'
    );
    $out='';
    $childs=get_children( $args_page,ARRAY_A );
    if($loop!=1)
    {
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_the_permalink($parent).'">'.get_the_title($parent).'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
                $out.= mega_menu_page($child['ID'],'',$loop);
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_the_permalink($parent).'">'.get_the_title($parent).'</a>';
            $out.='</li>';
        }
    }
    else
    {
        $loop++;
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_the_permalink($parent).'">'.$label_first.'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
                $out.= mega_menu_page($child['ID'],'',$loop);
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_the_permalink($parent).'">'.$label_first.'</a>';
            $out.='</li>';
        }
    }
    return $out;
}
function mega_menu_taxonomy($id,$tax,$label,$loop,$show_img)
{
    $show_img=$show_img;
    $parent=$id;
    $taxonomy=$tax;
    $label_first=$label;
    $loop=$loop;
    $args_tax = array(
        'parent'    => $parent,
        'exclude_tree' => 16,
        'hide_empty'=> 0,
        'taxonomy'  => $taxonomy,
        'orderby' => 'menu_order', 
        'order' => 'ASC',
    );
    $out='';
    $childs=get_categories( $args_tax );
    $icon=get_term_meta( $parent, 'icon_mega_menu', true );
    if($show_img==true) :
        $image_cat=get_term_meta( $parent, 'banner_image', true );
    endif;
    if($loop!=1)
    {
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.get_term_by( 'id', $parent, $taxonomy )->name.'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
               $out.= mega_menu_taxonomy($child->term_id,$taxonomy,'',$loop,false);
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.get_term_by( 'id', $parent, $taxonomy )->name.'</a>';
            $out.='</li>';
        }
    }
    else
    {
        $loop++;
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.wp_get_attachment_image($icon,'thumbnail',true,array('class'=>'icon-product-cat')).$label_first.'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
               $out.= mega_menu_taxonomy($child->term_id,$taxonomy,'',$loop,false);
            }
            if($image_cat!='' && $image_cat!=null)
            {
                $out.='<div class="product-category__thumbnail">'.wp_get_attachment_image($image_cat,'medium',flase,array('class'=>'image-product-cat')).'</div>';   
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.wp_get_attachment_image($icon,'thumbnail',true,array('class'=>'icon-product-cat')).$label_first.'</a>';
            $out.='</li>';
        }
    }
    return $out;
}