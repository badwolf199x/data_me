<?php 
if ( ! function_exists( 'autocomplete_search' ) ) {
function autocomplete_search() { 
    global $wpdb;
    $response=array();
	$text =  (isset($_POST['text']))?esc_attr($_POST['text']) : '';
	
	$query = "SELECT ID  FROM  `" . $wpdb->prefix . "posts` where (`post_title` like '%{$text}%' or `post_content` like '%{$text}%')  and `post_type` = 'post' and `post_status` = 'publish'	UNION SELECT p.ID
FROM " . $wpdb->prefix . "posts p
JOIN " . $wpdb->prefix . "term_relationships cr
    on (p.`id`=cr.`object_id`)
JOIN " . $wpdb->prefix . "term_taxonomy ct
    on (ct.`term_taxonomy_id`=cr.`term_taxonomy_id`
    and ct.`taxonomy`='category')
JOIN " . $wpdb->prefix . "terms c on
    (ct.`term_id`=c.`term_id`)
JOIN " . $wpdb->prefix . "term_relationships tr
    on (p.`id`=tr.`object_id`)
JOIN " . $wpdb->prefix . "term_taxonomy tt
    on (tt.`term_taxonomy_id`=tr.`term_taxonomy_id`
    and tt.`taxonomy`='post_tag')
JOIN " . $wpdb->prefix . "terms t
    on (tt.`term_id`=t.`term_id`)
where t.`name` like '%{$text}%'  and p.`post_type` = 'post' and p.`post_status` = 'publish'  UNION SELECT ID  FROM  `" . $wpdb->prefix . "posts` where (`post_title` like '%{$text}%' or `post_content` like '%{$text}%')  and `post_type` = 'page' and `post_status` = 'publish' limit 0,8";
    $posts = $wpdb->get_results($query);
	
	$search_title="Kết quả tìm kiếm nổi bật";
    $html='<ul class="suggest"><li><div class="viewed">'.$search_title.'</div></li>';
    if ($posts):
	   foreach ($posts as $post):
	      $post_thumbnail_id = get_post_thumbnail_id($post->ID);
	      if(!$post_thumbnail_id):$post_thumbnail_id=195;
	      endif;
          $src_thumbnail =wp_get_attachment_image_src($post_thumbnail_id ,'full');
          $html .= '<li><a href="'.get_permalink($post->ID).'" ><div class="avata"><img width="50" height="50" src="'.$src_thumbnail[0].'" alt="'.get_the_title($post->ID).'"></div><div class="info"><span title="'.get_the_title($post->ID).'">'.get_the_title($post->ID).'</span></div></a></li>';
       endforeach;
    $html .= '</ul>';
	else :
	$html .= '<li>Không tìm thấy nội dung liên quan</li>';
	$html .= '</ul>';
    endif;
	
    $response['html'] =$html;
    wp_send_json_success($response);
  }
add_action( 'wp_ajax_autocomplete_search', 'autocomplete_search' );
add_action( 'wp_ajax_nopriv_autocomplete_search', 'autocomplete_search' );
}
?>