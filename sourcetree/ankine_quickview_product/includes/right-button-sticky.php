<?php
if ( ! function_exists( 'up_scroll' ) ) {
function up_scroll()
	{ 
	    ?>
		    <div id="right-sticky">
		    	<?php get_template_part( 'template-parts/section-chung/social-link' ); ?>
		        <div id='up-scroll'><i class="bi bi-caret-up-fill"></i></div>
	        </div>
	    <?php 
    }
add_action('wp_footer', 'up_scroll'); 
}