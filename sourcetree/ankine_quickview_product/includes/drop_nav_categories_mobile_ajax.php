<?php 
if( !function_exists('drop_nav_categories'))
{
	function drop_nav_categories()
    {
    	function show_list_category_nav($parent)
        {
        $args_cat = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent,
        'taxonomy' => 'product_cat'
        );
        $subcats = get_categories($args_cat); 
        foreach ($subcats as $sc): $args_cat['parent']=$sc->term_id; $count_childs=count(get_categories($args_cat)); ?>
            <li class="cat-item cat-item-<?= $sc->term_id ?> <?= ($count_childs==0) ? '' : 'menu-item-has-children' ?>">
            	<div class="wrap-li d-flex justify-content-between">
                    <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
                        <?= $sc->name ?>
                    </a>
                    <?php  if($count_childs>0) : ?>
                        <i class="bi bi-chevron-right"></i>
                    <?php endif; ?>
                </div>
                <?php  if($count_childs>0) : ?>
                    <ul class="sub-menu subnav-children"> 
                        <?php show_list_category_nav($sc->term_id) ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach;
        }
    	ob_start(); ?>
    	<div class="drop_nav_categories__button text-uppercase container"><i class="bi bi-list"></i>DANH MỤC SẢN PHẨM</div>
    	<div class="site-sidebar categories">
    		<div class="sidebar-container">
    			<div class="sidebar-logo text-center">
                    <?php the_custom_logo(); ?>             
                </div>
                <div class="sidebar-menu">
                	<div class="wrap-content-nav-drop">
            	        <ul class="menu">
        		            <?php show_list_category_nav(0); ?>
        	            </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php $result = ob_get_clean();
        wp_send_json_success($result); 
        die();
    }
add_action( 'wp_ajax_drop_nav_categories', 'drop_nav_categories' );
add_action( 'wp_ajax_nopriv_drop_nav_categories', 'drop_nav_categories' );
}