<?php
/**
* ECOMCX Admin file
*
* by Phi Tran
*/

/*Update CSS within in Admin*/
    function ecx_login_style() {
        wp_register_style('ecomcx-login-style', get_stylesheet_directory_uri() .'/ecomcx-admin/inc/templates/ecx-login-page.css');
        wp_enqueue_style('ecomcx-login-style');
    }
    add_action('login_enqueue_scripts', 'ecx_login_style');
/*Thay đổi logo trang đăng nhập và đổi màu nền*/
    function ecx_custom_logo() {
        echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri() .'/ecomcx-admin/inc/templates/ecx-login-page.css'.'" type="text/css" media="all">';
    }
    
    add_action('login_enqueue_scripts', 'ecx_custom_logo');
    
/*Thay đổi đường dẫn logo admin về ECOMCX */
    function wpc_url_login(){
        return "https://ecomweb.net/"; // duong dan vao website cua ban
    }
    
    add_filter('login_headerurl', 'wpc_url_login');

/* Tự động chuyển đến một trang khác sau khi login */
    function my_login_redirect( $redirect_to, $request, $user ) {
        //is there a user to check?
        global $user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
                //check for admins
                if ( in_array( 'administrator', $user->roles ) ) {
                        // redirect them to the default place
                        return admin_url();
                } elseif ( in_array( 'editor', $user->roles ) ) {
                        // redirect them to the default place
                        return admin_url();
                } elseif ( in_array( 'shop_manager', $user->roles ) ) {
                        // redirect them to the default place
                        return admin_url();
                }else {
                        return home_url();
                }
                } else {
                return $redirect_to;
        }
    }
    add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );


