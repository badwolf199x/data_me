<?php
class Custom_Widgets_Setup {

    function __construct() {
        add_action('elementor/init', array($this, 'custom_initiate_elementor'));
        add_action('elementor/widgets/widgets_registered', array($this, 'custom_widget_register'));
    }

    public function custom_widget_register() {
        require_once('widgets/home-slider.php');
        require_once('widgets/home-news.php');
        require_once('widgets/home-participants.php');
        require_once('widgets/home-sponsor.php');
        require_once('widgets/home-slider-signup.php');
        require_once('widgets/home-features.php');
        require_once('widgets/home-schedule.php');
        require_once('widgets/home-hurry-up.php');
        require_once('widgets/home-photo.php');
        require_once('widgets/home-partners.php');
        require_once('widgets/home-video.php');
    }

    public function custom_initiate_elementor() {
        Elementor\Plugin::instance()->elements_manager->add_category('custom-widgets', array( 'title' => __('Custom Widgets', 'bachh')), 1);
    }

}

new Custom_Widgets_Setup();


/*hung function*/

//buton singup menu
function singup_link()
{
if(get_the_ID()==2453 || (get_post_type()=='post' && pll_current_language()=='en'))
{ ?>
<script type="text/javascript">
    jQuery(function($){
        $('.sign-up.menu-item a').attr('href','<?=get_home_url()?>/contact/#signup');
    })
</script>
<?php }
elseif(get_the_ID()==2981 || (get_post_type()=='post' && pll_current_language()=='vi'))
{ ?>
<script type="text/javascript">
    jQuery(function($){
        $('.sign-up.menu-item a').attr('href','<?=get_home_url()?>/lien-he//#signup');
    })
</script>
<?php }
}
add_action('wp_head','singup_link');

//tin tuc carousel
 if ( ! function_exists( 'tin_tuc_new_carousel' ) ) {
function tin_tuc_new_carousel($args,$content)
{
    $args = array(
'post_type'=>'post',
'post_status'=>'publish',
'posts_per_page' => 9,
'post__not_in' => array(1),
'orderby' => 'publish_date',
'order' => 'DESC');
$post_query = new WP_Query( $args );
$var_i=1;
?>
<div class="wrap_ajax_projects swiper-container-project swiper-container desktop">
    <div class="slick-prev desktop"><i class="fas fa-chevron-left"></i></div>
    <div class="ajax_projects swiper-wrapper">
        <?php   while ( $post_query->have_posts() ) { 

                        $post_query->the_post();

                            $post_id=get_the_ID();

                                 if($var_i==1)
                                 {
                                    echo '<div class="wrap-grid swiper-slide">';
                                 }
                                 elseif($var_i%3==1)
                                {
                                    echo '</div><div class="wrap-grid swiper-slide">';
                                }
                            ?>
                        <a class="wrap-post swiper-slide-item" href="<?=get_permalink()?>">
                                    <div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'post-thumbnail' ) ?>"/>
                                    </div>
                            <div class="elementor-post__text">
                            <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
                            </h3>
                            </div>
                        </a> <?php      $var_i++;    } wp_reset_query();
                                    echo '</div>';
                     ?>               
    </div>
    <div class="slick-next desktop"><i class="fas fa-chevron-right"></i></div>
</div>
<div class="swiper-pagination desktop"></div>
<div class="wrap_ajax_projects swiper-container-project swiper-container tablet">
    <div class="slick-prev tablet"><i class="fas fa-chevron-left"></i></div>
    <div class="ajax_projects swiper-wrapper">
        <?php   $args['posts_per_page']=6; $post_query = new WP_Query( $args ); $var_i=1; while ( $post_query->have_posts() ) { 

                        $post_query->the_post();

                            $post_id=get_the_ID();

                                 if($var_i==1)
                                 {
                                    echo '<div class="wrap-grid swiper-slide">';
                                 }
                                 elseif($var_i%2==1)
                                {
                                    echo '</div><div class="wrap-grid swiper-slide">';
                                }
                            ?>
                        <a class="wrap-post swiper-slide-item" href="<?=get_permalink()?>">
                                    <div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'post-thumbnail' ) ?>"/>
                                    </div>
                            <div class="elementor-post__text">
                            <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
                            </h3>
                            </div>
                        </a> <?php      $var_i++;  } wp_reset_query();
                                    echo '</div>';
                     ?>               
    </div>
    <div class="slick-next tablet"><i class="fas fa-chevron-right"></i></div>
</div>
<div class="swiper-pagination tablet"></div>
<div class="wrap_ajax_projects swiper-container-project swiper-container mobile">
    <div class="slick-prev mobile"><i class="fas fa-chevron-left"></i></div>
    <div class="ajax_projects swiper-wrapper">
        <?php    while ( $post_query->have_posts() ) { 

                        $post_query->the_post();

                            $post_id=get_the_ID(); ?>

                            <div class="wrap-grid swiper-slide">';
                            <a class="wrap-post swiper-slide-item" href="<?=get_permalink()?>">
                                    <div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'post-thumbnail' ) ?>"/>
                                    </div>
                            <div class="elementor-post__text">
                            <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
                            </h3>
                            </div>
                        </a></div> <?php      $var_i++;   } wp_reset_query();
                     ?>               
    </div>
    <div class="slick-next mobile"><i class="fas fa-chevron-right"></i></div>
</div>
<div class="swiper-pagination mobile"></div> 
<?php 
}
add_shortcode( 'tin_tuc_new_carousel', 'tin_tuc_new_carousel' );
}
//js tin tuc carousel
function js_tintuc()
{ 
if(get_the_ID()==2453 || get_the_ID()==2981) 
    { ?>
<script type="text/javascript">
    jQuery(function($){
  var swiper = new Swiper('.swiper-container.desktop', {
 slidesPerView: 1,
  spaceBetween: 0,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.slick-next.desktop',
    prevEl: '.slick-prev.desktop',
  },
  pagination: {
    el: '.swiper-pagination.desktop',
    type: 'bullets',
    clickable: 'true',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });

var swiper = new Swiper('.swiper-container.tablet', {
 slidesPerView: 1,
  spaceBetween: 0,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.slick-next.tablet',
    prevEl: '.slick-prev.tablet',
  },
  pagination: {
    el: '.swiper-pagination.tablet',
    type: 'bullets',
    clickable: 'true',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });

var swiper = new Swiper('.swiper-container.mobile', {
 slidesPerView: 1,
  spaceBetween: 0,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.slick-next.mobile',
    prevEl: '.slick-prev.mobile',
  },
  pagination: {
    el: '.swiper-pagination.mobile',
    type: 'bullets',
    clickable: 'true',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });

    })
    </script>
<?php }
}
add_action('wp_head','js_tintuc');

//gallery
 if ( ! function_exists( 'media_gallery_page' ) ) {
function media_gallery_page($args,$content)
{
if($_GET["page_num"]>0){ $page=$_GET["page_num"]; }else{ $page=1; }
$post_content= get_post(2835);
$content = $post_content->post_content;
wp_reset_query();
preg_match('/ids=".*"/i', $content, $text);
$id_img=str_replace('ids="','',$text[0]);
$id_img=str_replace('"','',$id_img);
$id_imgs=explode(",",$id_img);
$count=ceil(count($id_imgs)/10)+1;
?>
<div class="wrap-gallery">
<div class="image-page"> <?php
for($i=$page*10-10;$i<$page*10;$i++)
{ 
    if(wp_get_attachment_image_url( $id_imgs[$i] )!="") : ?>
<div data-id="<?=$i?>" class="wrap-img <?= ($i==0 || $i==9) ? 'wrap-img-570-360' : 'wrap-img-270-165' ?>">
    <div class="img <?= ($i==0 || $i==9) ? 'img-570-360' : 'img-270-165' ?>" link-data="<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'full') : 'unset'?>"  style="background-image:url(<?= (wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail')!="" && ($i==0 || $i==9)) ? wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail') : ''?><?= (wp_get_attachment_image_url( $id_imgs[$i], 'medium')!="" && $i!=0 && $i!=9) ? wp_get_attachment_image_url( $id_imgs[$i], 'medium') : ''?>  )"></div>
    <div class="<?= ($i==0 || $i==9) ? 'overlay-570-360' : 'overlay-270-165' ?>"></div>
</div> <?php endif;
} ?>
</div> 
<div id="wrap-gallery-page"> <?php
    if( $page!=1) : ?> <a class="truoc-do">←</a> <?php endif; 
   for($i=1;$i<$count;$i++)
   {
    if( ($i==1 && $page!=1) || ($i==$count-1&&$page!=$count-1) || $i==$page-1 || $i==$page+1 ) : ?><a class="page-num"><?=$i?></a> <?php
    elseif($i==$page) : ?> <span class="current-num"><?= $page ?></span> <?php
    else : if($center<2 && ($i==$page-2 || $i==$count-2)) : ?> <span class="center-num">...</span> <?php $center++; endif ; endif;
   }
    if( $page!=$count-1) : ?> <a class="xem-tiep">→</a> <?php endif; ?>
</div>
</div>
<div class="wrap-gallery mobile">
<div class="image-page"> <?php
$count_mobile=ceil(count($id_imgs)/5)+1;
for($i=$page*5-5;$i<$page*5;$i++)
{ 
    if(wp_get_attachment_image_url( $id_imgs[$i] )!="") : ?>
<div data-id="<?=$i?>" class="wrap-img <?= ($i==0 || $i==9) ? 'wrap-img-570-360' : 'wrap-img-270-165' ?>">
    <div class="img <?= ($i==0 || $i==9) ? 'img-570-360' : 'img-270-165' ?>" link-data="<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'full') : 'unset'?>"  style="background-image:url(<?= (wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail')!="" && ($i==0 || $i==9)) ? wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail') : ''?><?= (wp_get_attachment_image_url( $id_imgs[$i], 'medium')!="" && $i!=0 && $i!=9) ? wp_get_attachment_image_url( $id_imgs[$i], 'medium') : ''?>  )"></div>
    <div class="<?= ($i==0 || $i==9) ? 'overlay-570-360' : 'overlay-270-165' ?>"></div>
</div> <?php endif;
} ?>
</div> 
<div id="wrap-gallery-page"> <?php
    if( $page!=1) : ?> <a class="truoc-do">←</a> <?php endif; 
   for($i=1;$i<$count_mobile;$i++)
   {
    if( ($i==1 && $page!=1) || ($i==$count_mobile-1&&$page!=$count_mobile-1) || $i==$page-1 || $i==$page+1 ) : ?><a class="page-num"><?=$i?></a> <?php
    elseif($i==$page) : ?> <span class="current-num"><?= $page ?></span> <?php
    else : if($center<2 && ($i==$page-2 || $i==$count_mobile-2)) : ?> <span class="center-num">...</span> <?php $center++; endif ; endif;
   }
    if( $page!=$count_mobile-1) : ?> <a class="xem-tiep">→</a> <?php endif; ?>
</div>
</div>
<div class="wrap-zoom-img">
<div class="zoom-prev"><i class="fas fa-chevron-left"></i></div>
<?php for($i=0;$i<count($id_imgs);$i++)
{ ?>
  <div class="img-zoom" data-id="<?=$i?>">
  <img src="<?= wp_get_attachment_image_url( $id_imgs[$i], 'full') ?>"><h2 class="caption-zoom"><?= (wp_get_attachment_caption($id_imgs[$i])!="" && pll_current_language()=='vi') ? wp_get_attachment_caption($id_imgs[$i]) : get_post($id_imgs[$i])->post_content ?></h2>
  </div>
<?php } ?>
<div class="zoom-next"><i class="fas fa-chevron-right"></i></div>
</div>
<script type="text/javascript">
jQuery(function($){
    var page=1;
    var count='<?= $count; ?>';
    var id_show=0;
    function action()
    {
    $('.wrap-img').click(function(){
        var data_id=$(this).attr('data-id');
        $('.wrap-zoom-img').addClass('show');
        $('.img-zoom[data-id='+data_id+']').addClass('show');
        id_show=data_id;
      //
      $('.wrap-zoom-img').on('click', function (e) {
        if ($(e.target).closest(".img-zoom img").length === 0 && $(e.target).closest(".img-zoom h2").length === 0 && $(e.target).closest(".zoom-next").length === 0 && $(e.target).closest(".zoom-prev").length === 0 && $('.wrap-zoom-img').hasClass('show')) 
        {
          $('.wrap-zoom-img').removeClass('show');
          $('.img-zoom').removeClass('show');
        }
        });
    });
    //
    $('.zoom-next').click(function(){
        id_show++;
        if(id_show>='<?=count($id_imgs)?>'){id_show=0;}
        $('.img-zoom').removeClass('show');
        $('.img-zoom[data-id='+id_show+']').addClass('show');
    });
    $('.zoom-prev').click(function(){
        id_show--;
        if(id_show<0){id_show='<?=count($id_imgs)-1?>';}
        $('.img-zoom').removeClass('show');
        $('.img-zoom[data-id='+id_show+']').addClass('show');
    });
    $('a.xem-tiep').click(function(){
        page++;if(page>count){page=count}; load_gallery(page);
    });
    $('a.truoc-do').click(function(){
        page--;if(page<1){page=1}; load_gallery(page);
    });
    $('a.page-num').click(function(){
        page=$(this).text(); load_gallery(page);
    });
    if($('.image-page > div').length<4){ $('.image-page').css('display','flex'); }
    else { $('.image-page').css('display','grid'); }
    }
     function load_gallery(n)
    {    
    $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "load_gallery", 
                        page: page,
                        window: $(window).width(),
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                    {
                        $('.wrap-gallery').html(response.data);
                        action();
                    }
                    else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
    }
    action();
})
</script>
<?php
}
add_shortcode( 'media_gallery_page', 'media_gallery_page' );
function load_gallery()
{
ob_start();
$page =  (isset($_POST['page']))?esc_attr($_POST['page']) : '1';
$window = (isset($_POST['window']))?esc_attr($_POST['window']) : '768';
$post_content= get_post(2835);
$content = $post_content->post_content;
wp_reset_query();
preg_match('/ids=".*"/i', $content, $text);
$id_img=str_replace('ids="','',$text[0]);
$id_img=str_replace('"','',$id_img);
$id_imgs=explode(",",$id_img);
if($window>767)
{
$count=ceil(count($id_imgs)/10)+1;
?>
<div class="image-page"> <?php
for($i=$page*10-10;$i<$page*10;$i++)
{ 
    if(wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail')!="") : ?>
<div data-id="<?=$i?>" class="wrap-img <?= ($i==$page*10-10 || $i==$page*10-1) ? 'wrap-img-570-360' : 'wrap-img-270-165' ?>">
    <div class="img <?= ($i==$page*10-10 || $i==$page*10-1) ? 'img-570-360' : 'img-270-165' ?>" link-data="<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'full') : 'unset'?>" caption-data="<?= (wp_get_attachment_caption($id_imgs[$i])!="" && pll_current_language()=='vi') ? wp_get_attachment_caption($id_imgs[$i]) : get_post($id_imgs[$i])->post_content ?>" style="background-image:url(<?= (wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail')!="" && ($i==$page*10-10 || $i==$page*10-1)) ? wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail') : ''?><?= (wp_get_attachment_image_url( $id_imgs[$i], 'medium')!="" && $i!=$page*10-10 && $i!=$page*10-1) ? wp_get_attachment_image_url( $id_imgs[$i], 'medium') : ''?>  )"></div>
    <div class="<?= ($i==$page*10-10 || $i==$page*10-1) ? 'overlay-570-360' : 'overlay-270-165' ?>"></div>
</div> <?php endif;
} ?>
</div> 
<div id="wrap-gallery-page"> <?php
    if( $page!=1) : ?> <a class="truoc-do">←</a> <?php endif; 
   for($i=1;$i<$count;$i++)
   {
    if( ($i==1 && $page!=1) || ($i==$count-1&&$page!=$count-1) || $i==$page-1 || $i==$page+1 ) : ?><a class="page-num"><?=$i?></a> <?php
    elseif($i==$page) : ?> <span class="current-num"><?= $page ?></span> <?php
    else : if($center<2 && ($i==$page-2 || $i==$count-2)) : ?> <span class="center-num">...</span> <?php $center++; endif ; endif;
   }
    if( $page!=$count-1) : ?> <a class="xem-tiep">→</a> <?php endif; ?>
</div> <?php 
 }
 else
 { ?>
<div class="image-page"> <?php
$count_mobile=ceil(count($id_imgs)/5)+1;
for($i=$page*5-5;$i<$page*5;$i++)
{ 
    if(wp_get_attachment_image_url( $id_imgs[$i] )!="") : ?>
<div data-id="<?=$i?>" class="wrap-img <?= ($i==$page*5-5) ? 'wrap-img-570-360' : 'wrap-img-270-165' ?>">
    <div class="img <?= ($i==$page*5-5) ? 'img-570-360' : 'img-270-165' ?>" link-data="<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'full') : 'unset'?>" caption-data="<?= (wp_get_attachment_caption($id_imgs[$i])!="" && pll_current_language()=='vi') ? wp_get_attachment_caption($id_imgs[$i]) : get_post($id_imgs[$i])->post_content ?>" style="background-image:url(<?= (wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail')!="" && ($i==$page*5-5)) ? wp_get_attachment_image_url( $id_imgs[$i], 'post-thumbnail') : ''?><?= (wp_get_attachment_image_url( $id_imgs[$i], 'medium')!="" && $i!=$page*5-5) ? wp_get_attachment_image_url( $id_imgs[$i], 'medium') : ''?>  )"></div>
    <div class="<?= ($i==$page*5-5) ? 'overlay-570-360' : 'overlay-270-165' ?>"></div>
</div> <?php endif;
} ?>
</div> 
<div id="wrap-gallery-page"> <?php
    if( $page!=1) : ?> <a class="truoc-do">←</a> <?php endif; 
   for($i=1;$i<$count_mobile;$i++)
   {
    if( ($i==1 && $page!=1) || ($i==$count_mobile-1&&$page!=$count_mobile-1) || $i==$page-1 || $i==$page+1 ) : ?><a class="page-num"><?=$i?></a> <?php
    elseif($i==$page) : ?> <span class="current-num"><?= $page ?></span> <?php
    else : if($center<2 && ($i==$page-2 || $i==$count_mobile-2)) : ?> <span class="center-num">...</span> <?php $center++; endif ; endif;
   }
    if( $page!=$count_mobile-1) : ?> <a class="xem-tiep">→</a> <?php endif; ?>
</div>
<?php  }
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_load_gallery', 'load_gallery' );
add_action( 'wp_ajax_nopriv_load_gallery', 'load_gallery' );
}

if ( ! function_exists( 'exhibz_get_breadcrumbs_shortcode' ) ) {
function exhibz_get_breadcrumbs_shortcode($args,$content)
{
 echo exhibz_get_breadcrumbs();
}
add_shortcode( 'exhibz_get_breadcrumbs_shortcode', 'exhibz_get_breadcrumbs_shortcode' );
}

//google analytics
function GA()
{
  echo' <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131409006-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag("js", new Date());

  gtag("config", "UA-131409006-3");
</script>';
}
add_action('wp_head','GA');
