<div class="schedule-item">
    <div class="left">
        <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/time.svg" alt=""><?php the_field('time'); ?></p>
        <?php require ('get_speaker.php'); ?>
    </div>

    <div class="center-box">
        <h3><?php the_title(); ?></h3>
        <?php require ('get_avatar.php'); ?>
        <?php if(!empty(get_field('des'))) : ?>
            <p><?php the_field('des'); ?></p>
        <?php endif; ?>
    </div>

    <div class="show-desc">
        <?php if(!empty(get_field('des'))) : ?>
            <button>
                <i class="fal fa-plus"></i>
            </button>
        <?php endif; ?>
    </div>
</div>