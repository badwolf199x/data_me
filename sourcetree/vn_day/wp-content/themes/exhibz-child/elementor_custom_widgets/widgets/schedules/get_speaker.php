<?php
    $speakerData = get_field('speaker');
    if($speakerData) : 
?>
<div class="speakers">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/by.svg" alt="">
    <ul>
        <?php
        $i = 0;
        foreach ($speakerData as $speaker) :
            $i++;
            $speakerId = $speaker->ID;
            $speakerName = get_the_title($speakerId);
            $speakerUrl = get_the_permalink($speakerId);
            ?>
            <li>
                <a href="##"><?php echo $speakerName; ?></a><?php if($i != count($speakerData)) echo ", "; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>