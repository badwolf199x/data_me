<?php 
    $speakerData = get_field('speaker');
    if($speakerData) :
?>
<ul>
    <?php
        foreach ($speakerData as $speaker) :
            $speakerId = $speaker->ID;
            $speakerLink = get_the_permalink($speakerId);
            $data = get_post_meta($speakerId);
            if(!empty($data['fw_options'])) :
                $data = unserialize($data['fw_options'][0]);
                $dataImage = $data['exhibs_photo'];
                $imageUrl = $dataImage['url'];
                ?>
                <li>
                    <!--<a class="image" href="<?php echo $speakerLink; ?>">-->
                        <img src="<?php echo $imageUrl; ?>" alt="">
                    <!--</a>-->
                </li>
            <?php
            endif;
        endforeach; 
    ?>
</ul>
<?php endif; ?>