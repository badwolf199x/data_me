<?php
$args = array(
    'post_type' => 'schedule',
    'showposts' => 99,
    'orderby' => 'date',
    'order'   => 'ASC',
    'tax_query' => array (
        array(
            'taxonomy' => 'schedule_category',
            'field' => 'slug',
            'terms' => $scheCat4Slug,
        )
    )
);
$my_query = new \WP_Query( $args );
?>
<?php if ( $my_query->have_posts() ) : ?>
    <?php
    while ( $my_query->have_posts() ) : $my_query->the_post();
        require ('template.php');
    endwhile;
endif;
wp_reset_query();
?>