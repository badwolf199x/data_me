<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Partners extends Widget_Base {

    public function get_name() {
        return 'home-partners';
    }

    public function get_title() {
        return __( 'Home Partners', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Partners', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_logo', [
                'label' => __( 'Choose Logo', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );
        $repeater->add_control(
            'item_url', [
                'label' => __( 'Url', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
    
        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ item_url }}}',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-partners">
                <h2 class="section-title"><?php echo $settings['title']; ?></h2>
                <div class="partners-list">
                    <div class="owl-carousel owl-theme">
                        <?php  
                            if ( $settings['list'] ) :
                                foreach ( $settings['list'] as $item ) : ?>
                                <div class="item">
                                    <a href="<?php echo $item['item_url']; ?>" title="">
                                        <img src="<?php echo $item['item_logo']['url']; ?>" alt="" loading="lazy">
                                    </a>
                                </div>
                            <?php endforeach;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Partners() );