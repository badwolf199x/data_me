<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_News extends Widget_Base {

    public function get_name() {
        return 'home-news';
    }

    public function get_title() {
        return __( 'Home News', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home News', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-consulting">
                <h2 class="section-title"><?php echo $settings['title']; ?></h2>

                <div class="main">
                    <div class="consulting-list">
                        <div class="owl-wrapper">
                            <div class="owl-carousel owl-theme">
                                <?php
                                    $args = array(
                                        'post_type' => 'post',
                                        'showposts' => 4
                                    ); 
                                    $my_query = new \WP_Query( $args ); 
                                ?>
                                <?php if ( $my_query->have_posts() ) : ?>
                                    <?php 
                                        while ( $my_query->have_posts() ) : $my_query->the_post(); 
                                            get_template_part( 'template-parts/content', 'item' );
                                        endwhile;
                                    endif; 
                                    wp_reset_query(); 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_News() );