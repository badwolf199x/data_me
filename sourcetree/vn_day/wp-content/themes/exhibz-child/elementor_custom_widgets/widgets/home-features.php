<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Features extends Widget_Base {

    public function get_name() {
        return 'home-features';
    }

    public function get_title() {
        return __( 'Home Features', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Features', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_image', [
                'label' => __( 'Choose Image', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'item_title', [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_desc', [
                'label' => __( 'Description', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );


        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-features">
                <h2 class="section-title"><?php echo $settings['title']; ?></h2>

                <div class="owl-carousel owl-theme">
                    <?php  
                        if ( $settings['list'] ) :
                            foreach ( $settings['list'] as $item ) : ?>
                            <div class="item">
                                <div class="image">
                                    <img src="<?php echo $item['item_image']['url']; ?>" alt="" loading="lazy">
                                </div>
                                <div class="text-box">
                                    <h3><?php echo $item['item_title']; ?></h3>
                                    <p><?php echo $item['item_desc']; ?></p>
                                </div>
                            </div>
                        <?php endforeach;
                        endif;
                    ?>
                </div>

            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Features() );