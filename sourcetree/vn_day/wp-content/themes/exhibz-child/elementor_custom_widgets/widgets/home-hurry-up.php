<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Hurry_Up extends Widget_Base {

    public function get_name() {
        return 'home-hurry-up';
    }

    public function get_title() {
        return __( 'Home Hurry Up', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
            'addon_text_display',
            [
                'label' => __( 'Home Hurry Up', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // title
        $this->add_control(
            'form',
            [
                'label' => __( 'Form', 'bachh' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_number', [
                'label' => __( 'Number', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_desc', [
                'label' => __( 'Description', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ item_number }}}',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
        <div class="home-hurry-up">
            <div class="left">
                <h3><?php echo $settings['title']; ?></h3>
                <div class="numbers-list">
                    <?php
                    if ( $settings['list'] ) :
                        $i = 0;
                        foreach ( $settings['list'] as $item ) : $i++; 
                            if($i === 1) {
                                $id = 'd';
                            } elseif ($i === 2) {
                                $id = 'h';
                            } elseif ($i === 3) {
                                $id = 'm';
                            } else {
                                $id = 's';
                            }
                            ?>
                            <div class="item">
                                <h4 id="timer-<?php echo $id; ?>"><?php echo $item['item_number']; ?></h4>
                                <p><?php echo $item['item_desc']; ?></p>
                            </div>
                        <?php endforeach;
                    endif;
                    ?>
                </div>
            </div>
            <div class="right">
                <div class="contact-form">
                    <?php
                    $shortcode = $this->get_settings( 'form' );
                    $shortcode = do_shortcode( shortcode_unautop( $shortcode ) );
                    echo $shortcode;
                    ?>
                </div>

            </div>
        </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Hurry_Up() );