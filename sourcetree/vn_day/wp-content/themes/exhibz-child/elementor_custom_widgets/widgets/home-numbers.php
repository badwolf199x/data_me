<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Numbers extends Widget_Base {

    public function get_name() {
        return 'home-numbers';
    }

    public function get_title() {
        return __( 'Home Numbers', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Numbers', 'bachh' ),
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_number', [
                'label' => __( 'Number', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_desc', [
                'label' => __( 'Description', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ item_number }}}',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-numbers">
                <div class="numbers-list">
                    <?php
                        if ( $settings['list'] ) :
                            foreach ( $settings['list'] as $item ) : ?>
                            <div class="item">
                                <h4><?php echo $item['item_number']; ?></h4>
                                <p><?php echo $item['item_desc']; ?></p>
                            </div>
                        <?php endforeach;
                        endif;
                    ?>
                </div>
            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Numbers() );