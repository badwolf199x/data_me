<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Slider extends Widget_Base {

    public function get_name() {
        return 'home-slider';
    }

    public function get_title() {
        return __( 'Home Slider', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Slider', 'bachh' ),
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_image', [
                'label' => __( 'Choose Image', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-slider">
                <div class="owl-carousel owl-theme">
                    <?php  
                        if ( $settings['list'] ) :
                            foreach ( $settings['list'] as $item ) : ?>
                            <div class="item">
                                <div class="image">
                                    <img src="<?php echo $item['item_image']['url']; ?>" alt="" loading="lazy">
                                </div>
                            </div>
                        <?php endforeach;
                        endif;
                    ?>
                </div>

            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Slider() );