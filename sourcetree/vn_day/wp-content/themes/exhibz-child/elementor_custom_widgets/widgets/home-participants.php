<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Participants extends Widget_Base {

    public function get_name() {
        return 'home-participants';
    }

    public function get_title() {
        return __( 'Home Participants', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Participants', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // button
        $this->add_control(
            'button_text',
            [
                'label' => __( 'Button Text', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'button_link',
            [
                'label' => __( 'Button Link', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_image', [
                'label' => __( 'Choose Image', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );
        $repeater->add_control(
            'item_title', [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_desc', [
                'label' => __( 'Description', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_desc_2', [
                'label' => __( 'Description 2', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ item_title }}}',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-why participants">
                <h2 class="section-title"><?php echo $settings['title']; ?></h2>

                <div class="why-list">
                    <?php  
                        if ( $settings['list'] ) :
                            foreach ( $settings['list'] as $item ) : ?>
                            <div class="item">
                                <div class="image">
                                    <figure>
                                        <?php
                                            $image_id = $item['item_image']['id'];
                                            if(!wp_is_mobile()) {
                                                $image_url = wp_get_attachment_image_url($image_id, 'large');
                                            } else {
                                                $image_url = wp_get_attachment_image_url($image_id, 'medium');
                                            }

                                        ?>
                                        <img src="<?php echo $image_url; ?>" alt="" loading="lazy">
                                    </figure>
                                    <div class="desc-2">
                                        <?php echo $item['item_desc_2']; ?>
                                    </div>
                                </div>

                                <div class="text-box">
                                    <h3><?php echo $item['item_title']; ?></h3>
                                    <p><?php echo $item['item_desc']; ?></p>
                                </div>

                            </div>
                        <?php endforeach;
                        endif;
                    ?>
                </div>

                <?php if(!empty($settings['button_link'])) : ?>
                    <div class="actions">
                        <a href="<?php echo $settings['button_link']; ?>" class="see-more"><?php echo $settings['button_text']; ?></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Participants() );