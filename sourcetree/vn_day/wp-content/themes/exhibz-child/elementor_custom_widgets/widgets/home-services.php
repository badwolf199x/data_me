<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Services extends Widget_Base {

    public function get_name() {
        return 'home-services';
    }

    public function get_title() {
        return __( 'Home Services', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Services', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_icon', [
                'label' => __( 'Choose Icon', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );
        $repeater->add_control(
            'item_title', [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_desc', [
                'label' => __( 'Description', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_url', [
                'label' => __( 'URL', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'item_image', [
                'label' => __( 'Choose Image', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ item_title }}}',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div id="home-services" class="home-services">
                <div class="left">
                    <h2 class="section-title"><?php echo $settings['title']; ?></h2>
                    <?php if(wp_is_mobile()) : ?>
                        <div class="right for-mobile">
                            <div class="box-image">
                                <?php  
                                    if ( $settings['list'] ) :
                                        $i = 0;
                                        foreach ( $settings['list'] as $item ) : $i++; ?>
                                        <div class="item <?php if($i == 1) echo 'active'; ?>">
                                            <div class="image">
                                                <?php  
                                                    $image_id = $item['item_image']['id'];
                                                    if(!my_wp_is_mobile()) {
                                                        $image_url = wp_get_attachment_image_url($image_id, 'large');
                                                    } else {
                                                        $image_url = wp_get_attachment_image_url($image_id, 'medium');
                                                    }
                                                    
                                                ?>
                                                <img src="<?php echo $image_url; ?>" alt="" loading="lazy">
                                            </div>
                                        </div>
                                    <?php endforeach;
                                    endif;
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="services-list">
                        <?php  
                            if ( $settings['list'] ) :
                                $i = 0;
                                foreach ( $settings['list'] as $item ) : $i++; ?>
                                <div class="item <?php if($i == 1) echo 'active'; ?>">
                                    <div class="icon">
                                        <img src="<?php echo $item['item_icon']['url']; ?>" alt="" loading="lazy">
                                    </div>
                                    <div class="content">
                                        <h3><?php echo $item['item_title']; ?></h3>
                                        <div class="display">
                                            <p><?php echo $item['item_desc']; ?></p>
                                            <a href="<?php echo $item['item_url']; ?>" title=""><?php _e('View Details', 'bachh'); ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/arrow-green.svg" alt="" class="arrow-green"></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;
                            endif;
                        ?>
                    </div>
                </div>
 
                <div class="right for-tablet">
                    <div class="box-image">
                        <?php  
                            if ( $settings['list'] ) :
                                $i = 0;
                                foreach ( $settings['list'] as $item ) : $i++; ?>
                                <div class="item <?php if($i == 1) echo 'active'; ?>">
                                    <div class="image">
                                        <?php  
                                            $image_id = $item['item_image']['id'];
                                            if(!my_wp_is_mobile()) {
                                                $image_url = wp_get_attachment_image_url($image_id, 'large');
                                            } else {
                                                $image_url = wp_get_attachment_image_url($image_id, 'medium');
                                            }
                                            
                                        ?>
                                        <img src="<?php echo $image_url; ?>" alt="" loading="lazy">
                                    </div>
                                </div>
                            <?php endforeach;
                            endif;
                        ?>
                    </div>  

                    <?php if(!wp_is_mobile()) : ?>
                        <div class="scroll-content">
                            <div id="box1"></div>
                            <div id="box2"></div>
                            <div id="box3"></div>
                            <div id="box4"></div>
                        </div>
                    <?php endif; ?>
                </div>   
          
            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Services() );