<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Photo extends Widget_Base {

    public function get_name() {
        return 'home-organizer';
    }

    public function get_title() {
        return __( 'Home Photo', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Photo', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // button
        $this->add_control(
            'button_text',
            [
                'label' => __( 'Button Text', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'button_link',
            [
                'label' => __( 'Button Link', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_logo', [
                'label' => __( 'Choose Image', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'item_text', [
                'label' => __( 'Text', 'bachh' ),
                'type' => \Elementor\Controls_Manager::TEXT,
            ]
        );
    
        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-photo">
                <h2 class="section-title"><?php echo $settings['title']; ?></h2>
                <div class="photo-list">
                    <?php
                        $i = 0;
                        if ( $settings['list'] ) :
                            foreach ( $settings['list'] as $item ) : $i++; ?>

                                <a href="<?php echo $item['item_logo']['url']; ?>" class="item-<?php echo $i; ?>" data-toggle="lightbox" data-gallery="example-gallery" data-footer="<?php echo $item['item_text']; ?>">
                                    <img src="<?php echo $item['item_logo']['url']; ?>" alt="" loading="lazy" class="img-fluid">
                                </a>

                        <?php endforeach;
                        endif;
                    ?>
                </div>
                <?php if(!empty($settings['button_link'])) : ?>
                    <div class="actions">
                        <a href="<?php echo $settings['button_link']; ?>" class="see-more"><?php echo $settings['button_text']; ?></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Photo() );