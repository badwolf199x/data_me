<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Video extends Widget_Base {

    public function get_name() {
        return 'home-video';
    }

    public function get_title() {
        return __( 'Home Video', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Video', 'bachh' ),
            ]
        );

        // image
        $this->add_control(
            'image', [
                'label' => __( 'Choose Image', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );

        // video
        $this->add_control(
            'video', [
                'label' => __( 'Video', 'bachh' ),
                'type' => \Elementor\Controls_Manager::CODE,
            ]
        );
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-video">
                <!-- Button trigger modal -->
                <button type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter">
                    <img src="<?php echo $settings['image']['url']; ?>" alt="">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/play_video.png" alt="" class="play-video">
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="main-video">
                                    <?php echo $settings['video']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Video() );