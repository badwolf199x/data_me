<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Schedule extends Widget_Base {

    public function get_name() {
        return 'home-schedule';
    }

    public function get_title() {
        return __( 'Home Schedule', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Schedule', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // Category Select
        $cats = get_terms( 'schedule_category', array(
            'hide_empty' => false,
        ) );
        $catArr = [];
        foreach ($cats as $item) {
            $catId = $item->term_id;
            $catName = $item->name;
            $catArr[$catId] = $catName;
        }

        $this->add_control(
            'cat_1',
            [
                'label' => __( 'Category 1', 'bachh' ),
                'type' => Controls_Manager::SELECT,
                'default' => $cats[0]->term_id,
                'options' => $catArr,
            ]
        );

        $this->add_control(
            'cat_2',
            [
                'label' => __( 'Category 2', 'bachh' ),
                'type' => Controls_Manager::SELECT,
                'default' => $cats[0]->term_id,
                'options' => $catArr,
            ]
        );

        $this->add_control(
            'cat_3',
            [
                'label' => __( 'Category 3', 'bachh' ),
                'type' => Controls_Manager::SELECT,
                'default' => $cats[0]->term_id,
                'options' => $catArr,
            ]
        );

        $this->add_control(
            'cat_4',
            [
                'label' => __( 'Category 4', 'bachh' ),
                'type' => Controls_Manager::SELECT,
                'default' => $cats[0]->term_id,
                'options' => $catArr,
            ]
        );

        $this->add_control(
            'desc',
            [
                'label' => __( 'Desc', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div id="home-schedule">
                <h2 class="section-title"><?php echo $settings['title']; ?></h2>
                <p class="description"><?php echo $settings['desc']; ?></p>

                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <?php
                                        $scheCat1 = get_term( $settings['cat_1'] , 'schedule_category' );
                                        $scheCat1Slug = $scheCat1->slug;
                                        echo $scheCat1->name;
                                    ?>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <?php require_once ('schedules/one.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <?php
                                        $scheCat2 = get_term( $settings['cat_2'] , 'schedule_category' );
                                        $scheCat2Slug = $scheCat2->slug;
                                        echo $scheCat2->name;
                                    ?>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <?php require_once ('schedules/two.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <?php
                                        $scheCat3 = get_term( $settings['cat_3'] , 'schedule_category' );
                                        $scheCat3Slug = $scheCat3->slug;
                                        echo $scheCat3->name;
                                    ?>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <?php require_once ('schedules/three.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <?php
                                        $scheCat4 = get_term( $settings['cat_4'] , 'schedule_category' );
                                        $scheCat4Slug = $scheCat4->slug;
                                        echo $scheCat4->name;
                                    ?>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                <?php require_once ('schedules/four.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Schedule() );