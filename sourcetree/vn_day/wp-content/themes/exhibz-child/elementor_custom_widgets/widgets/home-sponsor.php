<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit;

class Home_Sponsor extends Widget_Base {

    public function get_name() {
        return 'home-sponsor';
    }

    public function get_title() {
        return __( 'Home Sponsor', 'bachh' );
    }

    public function get_icon() {
        return 'eicon-featured-image';
    }

    public function get_categories() {
        return [ 'custom-widgets' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
        'addon_text_display',
            [
                'label' => __( 'Home Sponsor', 'bachh' ),
            ]
        );

        // title
        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // button
        $this->add_control(
            'button_text',
            [
                'label' => __( 'Button Text', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'button_link',
            [
                'label' => __( 'Button Link', 'bachh' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        // repeater
        $repeater = new Repeater();
        $repeater->add_control(
            'item_logo', [
                'label' => __( 'Choose Logo', 'bachh' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
            ]
        );
        $repeater->add_control(
            'item_url', [
                'label' => __( 'Url', 'bachh' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
    
        $this->add_control(
            'list',
            [
                'label' => __( 'List', 'bachh' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ item_url }}}',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
            <div class="home-sponsor <?php if(!empty($settings['button_link'])) echo 'has-link'; ?>">
                <h3 class="sponsor-title"><?php echo $settings['title']; ?></h3>
                <div class="sponsor-list">

                        <?php  
                            if ( $settings['list'] ) :
                                foreach ( $settings['list'] as $item ) : ?>
                                <div class="item">
                                    <a href="<?php echo $item['item_url']; ?>" title="">
                                        <img src="<?php echo $item['item_logo']['url']; ?>" alt="" loading="lazy">
                                    </a>
                                </div>
                            <?php endforeach;
                            endif;
                        ?>

                </div>
            </div>
            <?php if(!empty($settings['button_link'])) : ?>
                <div class="actions">
                    <a href="<?php echo $settings['button_link']; ?>" class="see-more"><?php echo $settings['button_text']; ?></a>
                </div>
            <?php endif; ?>
        <?php
    }

    protected function _content_template() {

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new Home_Sponsor() );