<?php
    function schedule_post_type(){
        /*
         * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
         */
        $label = array(
            'name' => 'Schedule', //Tên post type dạng số nhiều
            'singular_name' => 'Schedule' //Tên post type dạng số ít
        );
        /*
         * Biến $args là những tham số quan trọng trong Post Type
         */
        $args = array(
            'labels' => $label, //Gọi các label trong biến $label ở trên
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'author',
                'thumbnail',
                'comments',
                'trackbacks',
                'revisions',
                'custom-fields'
            ), //Các tính năng được hỗ trợ trong post type
            'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
            'public' => true, //Kích hoạt post type
            'show_ui' => true, //Hiển thị khung quản trị như Post/Page
            'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
            'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
            'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
            'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
            'menu_icon' => 'dashicons-cart', //Đường dẫn tới icon sẽ hiển thị
            'can_export' => true, //Có thể export nội dung bằng Tools -> Export
            'has_archive' => true, //Cho phép lưu trữ (month, date, year)
            'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
            'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
            'capability_type' => 'post' //
        );
        register_post_type('schedule', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
    }
    add_action('init', 'schedule_post_type');

    // Register Custom Taxonomy
    function schedule_category_func() {
        $labels = array(
            'name'                       => _x( 'Danh mục', 'Taxonomy General Name', 'devvn' ),
            'singular_name'              => _x( 'Danh mục', 'Taxonomy Singular Name', 'devvn' ),
            'menu_name'                  => __( 'Danh mục', 'devvn' ),
            'all_items'                  => __( 'All Items', 'devvn' ),
            'parent_item'                => __( 'Parent Item', 'devvn' ),
            'parent_item_colon'          => __( 'Parent Item:', 'devvn' ),
            'new_item_name'              => __( 'New Item Name', 'devvn' ),
            'add_new_item'               => __( 'Add New Item', 'devvn' ),
            'edit_item'                  => __( 'Edit Item', 'devvn' ),
            'update_item'                => __( 'Update Item', 'devvn' ),
            'view_item'                  => __( 'View Item', 'devvn' ),
            'separate_items_with_commas' => __( 'Separate items with commas', 'devvn' ),
            'add_or_remove_items'        => __( 'Add or remove items', 'devvn' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'devvn' ),
            'popular_items'              => __( 'Popular Items', 'devvn' ),
            'search_items'               => __( 'Search Items', 'devvn' ),
            'not_found'                  => __( 'Not Found', 'devvn' ),
            'no_terms'                   => __( 'No items', 'devvn' ),
            'items_list'                 => __( 'Items list', 'devvn' ),
            'items_list_navigation'      => __( 'Items list navigation', 'devvn' ),
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );
        register_taxonomy( 'schedule_category', array( 'schedule' ), $args );
    }
    add_action( 'init', 'schedule_category_func', 0 );