<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bachh
 */

?>

<div class="item-post single">
    <div class="image">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php  
                if(!wp_is_mobile()) {
                    the_post_thumbnail('medium');
                } else {
                    the_post_thumbnail('thumbnail');
                }
            ?>
        </a>
    </div>
    <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
    <p><?php echo wp_trim_words( get_the_excerpt(), 55, "..."); ?></p>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="read-more">
        <?php  
            if(get_locale() == "vi") {
                echo "Xem chi tiết";
            } else {
                echo "View detail";
            }
        ?>
        <img src="<?php bloginfo('template_url'); ?>/assets/imgs/arrow-green.svg" alt="" class="arrow-green">
    </a>
</div>
