<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bachh
 */

?>

<div class="item-post">
    <div class="image">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php  
                if(!wp_is_mobile()) {
                    the_post_thumbnail('medium');
                } else {
                    the_post_thumbnail('medium');
                }
            ?>
        </a>
    </div>
    <div class="text-box">
        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
        <p><?php echo wp_trim_words( get_the_excerpt(), 15, "..."); ?></p>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="read-more"><?= (pll_current_language()=='vi') ? 'Xem thêm' : 'Read more' ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/arrow-red.svg" alt="" class="arrow-green"></a>
    </div>

</div>
