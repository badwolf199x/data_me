<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bachh
 */

global $redux_bachh;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
	<?php if(pll_current_language()=='vi')
    {
	echo '<title>Ngày Việt Nam ở nước ngoài</title>';
    }
	else 
	{
	echo '<title>Vietnam Days Abroad</title>';
	}
	?>
	
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="icon" href="<?php echo $redux_bachh['header-favicon']['url']; ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header class="web-header">
		<div class="wrapper">
			<div class="main-header">
				<!-- logo -->
				<div class="logo">
					<a href="<?php echo get_home_url(); ?>" title="">
						<img src="<?php echo $redux_bachh['header-logo']['url']; ?>" alt="" class="logo-1">
					</a>
				</div>
				<!-- menu -->
				<div class="main-menu">
                    <div class="close-menu header-for-mobile">
                        <span><i class="fal fa-arrow-from-right"></i></span>
                    </div>

					<?php 
						wp_nav_menu( array(
					        'menu'              => 'main-menu',  
					        'theme_location'    => 'main-menu',
					        'depth'             => 2,
					        'container'         => '',
					        'container_class'   => '',
					        'container_id'      => '',
					        'menu_class'        => '',
							'menu_id'        	=> '',
						));
					?>

                    <div class="select-language header-for-desktop">
                        <ul><?php pll_the_languages(array('show_flags'=>1,'show_names'=>0)); ?></ul>
                    </div>
				</div>

                <div class="open-menu header-for-mobile">
                    <div class="select-language">
                        <ul><?php pll_the_languages(array('show_flags'=>1,'show_names'=>0)); ?></ul>
                    </div>
                    <i class="fal fa-bars"></i>
                </div>
				
			</div>
		</div>
	</header>