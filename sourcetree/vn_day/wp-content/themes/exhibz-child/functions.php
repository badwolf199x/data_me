<?php

if ( !defined( 'WP_DEBUG' ) ) {
    die( 'Direct access forbidden.' );
}

add_action( 'wp_enqueue_scripts', 'exhibz_child_enqueue_styles', 99 );
function exhibz_child_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_stylesheet_directory_uri() . '/style.css' );
}

// add redux theme option
if( !class_exists( 'ReduxFramewrk' ) ) {
    require_once( dirname( __FILE__ ) . '/bachh_options/framework.php' );
}
if( !isset( $redux_demo ) ) {
    require_once( dirname( __FILE__ ) . '/bachh_options/sample/barebones-config.php');
}

// classic editor
add_filter('use_block_editor_for_post', '__return_false');
add_filter( 'use_widgets_block_editor', '__return_false' );

// custom post type
require_once(dirname(__FILE__) . '/inc/posttype-schedule.php');

// custom widget
require_once(dirname( __FILE__ ) . '/elementor_custom_widgets/custom_widgets.php');

// widget
function bachh_widgets_init() {
    
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 1', 'bachh' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here.', 'bachh' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

}
add_action( 'widgets_init', 'bachh_widgets_init' );

// assets
function bachh_assets()
{
    //css
    wp_enqueue_style('child-font-awesome-5', get_stylesheet_directory_uri() . '/assets/fontawesome-5/css/all.min.css');
    wp_enqueue_style('child-lightbox', get_stylesheet_directory_uri() . '/assets/css/ekko-lightbox.css', 'all');  
    wp_enqueue_style('child-bachh', get_stylesheet_uri());

    //js   
    wp_enqueue_script('child-lightbox', get_stylesheet_directory_uri() . '/assets/js/ekko-lightbox.min.js', array(), '20190519', true); 
    wp_enqueue_script('child-bachh', get_stylesheet_directory_uri() . '/assets/js/script.js', array(), '20190519', true);

    if(is_front_page()) :
        wp_enqueue_script('child-counter', get_stylesheet_directory_uri() . '/assets/js/script-counter.js', array(), '20190519', true);
    endif;
}   

add_action('wp_enqueue_scripts', 'bachh_assets');

