<?php
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    $opt_name = "redux_bachh";

    $theme = wp_get_theme(); 

    $args = array(
        'opt_name'             => $opt_name,
        'display_name'         => 'WeDo',
        'display_version'      => $theme->get( 'Version' ),
        'menu_type'            => 'menu',
        'allow_sub_menu'       => true,
        'menu_title'           => __( 'Other Options', 'redux-framework-demo' ),
        'page_title'           => __( 'Other Options', 'redux-framework-demo' ),
        'google_api_key'       => '',
        'google_update_weekly' => false,
        'async_typography'     => true,
        'admin_bar'            => true,
        'admin_bar_icon'       => 'dashicons-portfolio',
        'admin_bar_priority'   => 50,
        'global_variable'      => '',
        'dev_mode'             => false,
        'update_notice'        => true,
        'customizer'           => true,
        'page_priority'        => null,
        'page_parent'          => 'themes.php',
        'page_permissions'     => 'manage_options',
        'menu_icon'            => '',
        'last_tab'             => '',
        'page_icon'            => 'icon-themes',
        'page_slug'            => '_options',
        'save_defaults'        => true,
        'default_show'         => false,
        'default_mark'         => '',
        'show_import_export'   => false,
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        'output_tag'           => true,
        'database'             => '',
        'use_cdn'              => true,
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    Redux::setArgs( $opt_name, $args );

    // -> START Header
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header', 'redux-framework-demo' ),
        'id'     => 'header-website',
        'icon'   => 'el el-home',
        'fields' => array(
            array(
                'id'       => 'header-favicon',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Favicon', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'header-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Logo', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'header-logo-2',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Logo 2', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
        )
    ));

    // -> START Footer
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Footer', 'redux-framework-demo' ),
        'id'     => 'footer-website',
        'icon'   => 'el el-home',
        'fields' => array(
            array(
                'id'       => 'footer-bg-image',
                'type'     => 'text',
                'url'      => true,
                'title'    => __( 'Footer BG Image', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'facebook',
                'type'     => 'text',
                'title'    => __( 'Facebook', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'youtube',
                'type'     => 'text',
                'title'    => __( 'Youtube', 'redux-framework-demo' ),
            ),
        )
    ));