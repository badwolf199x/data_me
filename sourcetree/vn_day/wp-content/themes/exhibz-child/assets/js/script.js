jQuery(document).ready(function() {
    /*jQuery('.home-video .play-video').on('click', function(ev) {
        jQuery("#main-video")[0].src += "&autoplay=1";
        ev.preventDefault();
    });*/

    // lightbox
    jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        jQuery(this).ekkoLightbox();
    });

    // home slider
    jQuery('.home-slider .owl-carousel').owlCarousel({
        loop:false,
        lazyLoad: true,
        nav:false,
        dots:true,
        autoplay:false,
        autoplayHoverPause: false,
        autoplayTimeout: 2000,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1,
            },
            1000:{
                items:1,
            }
        }
    });

    // home features
    jQuery('.home-features .owl-carousel').owlCarousel({
        loop:true,
        lazyLoad: true,
        nav:true,
        navText: ["<i class=\"fal fa-arrow-left\"></i>","<i class=\"fal fa-arrow-right\"></i>"],
        dots:false,
        autoplay:true,
        autoplayHoverPause: false,
        autoplayTimeout: 3000,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1,
            },
            1000:{
                items:1,
            }
        }
    });

    // home slider-signup
    jQuery('.home-slider-signup .owl-carousel').owlCarousel({
        loop:false,
        lazyLoad: true,
        nav:false,
        dots:false,
        autoplay:false,
        autoplayHoverPause: false,
        autoplayTimeout: 2000,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1,
            },
            1000:{
                items:1,
            }
        }
    });

    // home partners
	jQuery('.home-partners .owl-carousel').owlCarousel({
        loop:false,
        lazyLoad: true,
        nav:false,
        dots:true,
        margin:50,
        autoplay:true,
        autoplayHoverPause: false,
        autoplayTimeout: 2000,
        responsive:{
            0:{
                items:2
            },
            480:{
                items:3,
            },
            1000:{
                items:3,
            }
        }
    });

    // home consulting
    jQuery('.consulting-list .owl-carousel').owlCarousel({
        loop:false,
        nav:false,
        dots:true,
        responsive:{
            0:{
                items:1,
                margin:20,
                loop:true,
            },
            480:{
                items:2,
                margin:20,
                loop:false,
            },
            1000:{
                items:3,
                margin:20,
                loop:false,
                pullDrag:false
            }
        }
    });

    // fix width
    // function owlWrapperWidth( selector ) {
    //     jQuery(selector).each(function(){
    //         jQuery(this).find('.owl-carousel').outerWidth( jQuery(this).closest( selector ).innerWidth() );
    //     });
    // }
    //
    // // trigger on start and resize
    // owlWrapperWidth( '.owl-wrapper' );
    // jQuery( window ).resize(function() {
    //     owlWrapperWidth( jQuery('.owl-wrapper') );
    // });

    jQuery(function(){
        for(var i = 0; i < 99; i++){
            menuCategori(i);
        }
        function menuCategori(i){
            jQuery(".schedule-item .show-desc button").eq(i).click(function(){
                jQuery(".schedule-item .center-box>p").eq(i).slideToggle();
            });
        }
    })

    // Count Down
    var countDownDate = new Date("Oct 9, 2021 15:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      document.getElementById("timer-d").innerHTML = days;
      document.getElementById("timer-h").innerHTML = hours;
      document.getElementById("timer-m").innerHTML = minutes;
      document.getElementById("timer-s").innerHTML = seconds;

      // If the count down is finished, write some text
      if (distance <= 0) {
        clearInterval(x);
        document.getElementById("timer-d").innerHTML = '00';
        document.getElementById("timer-h").innerHTML = '00';
        document.getElementById("timer-m").innerHTML = '00';
        document.getElementById("timer-s").innerHTML = '00';
      }
    }, 1000);

    // MOBILE
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        // mobile menu 
        jQuery(".open-menu>i").click(function() {
            jQuery("header .main-menu").addClass("active");
        });

        jQuery(".close-menu").click(function() {
            jQuery("header .main-menu").removeClass("active");
        });

        jQuery(function(){
            for(var i = 0; i < 6; i++){
                menuCategori(i); 
            }
            function menuCategori(i){
                jQuery(".main-menu > ul > li.menu-item-has-children").eq(i).click(function(){
                    jQuery(".main-menu > ul > li > ul.sub-menu").eq(i).slideToggle();
                }); 
            }
        });
    }
});

// ELEMENTOR LIVE PREVIEW
jQuery( window ).on( 'elementor/frontend/init', function() {
    elementorFrontend.hooks.addAction( 'frontend/element_ready/home-consulting.default', function(){
        // home consulting
        jQuery('.consulting-list .owl-carousel').owlCarousel({
            loop:true,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1.1,
                    margin:20,
                    loop:true,
                },
                480:{
                    items:2,
                    margin:20,
                    loop:false,
                },
                1000:{
                    items:3,
                    margin:30,
                    loop:false,
                    pullDrag:false
                }
            }
        });
    });
});