<?php
/*
 Template Name: Home Main
 */

get_header();
?>

	<div class="page-one">
		
		<main>
			<?php while ( have_posts() ) : the_post();

				the_content();

			endwhile;
			?>
		</main>
		
	</div>

<?php
get_footer();
