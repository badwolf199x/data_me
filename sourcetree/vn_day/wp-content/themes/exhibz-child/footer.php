<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bachh
 */

global $redux_bachh;
?>

	<footer class="web-footer">
        <div class="wrapper">
            <div class="top">
                <div class="logo">
                    <a href="<?php echo get_home_url(); ?>" title="">
                        <img src="<?php echo $redux_bachh['header-logo']['url']; ?>" alt="" class="logo-1">
                    </a>
                </div>
                <div class="content">
                    <?php dynamic_sidebar('footer-1'); ?>
                </div>
            </div>
            <div class="bottom">
                <ul>
                    <li><a href="<?php echo $redux_bachh['facebook']; ?>"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="<?php echo $redux_bachh['youtube']; ?>"><i class="fab fa-youtube"></i></a></li>
                </ul>
                <div class="copy-right">© 2021- All rights reserved. Designed by Creativa JSC</div>
            </div>
        </div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>