var ShoppingCart = {
    guid: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    },
    urlCheckout: '.izzipayment.com',
    totalCount: 0,
    data: null,
    totalShoppingCartItem: 'totalShoppingCartItem',
    init: function () {
        ShoppingCart.data = this.queryData();
        if (ShoppingCart.data != undefined) {
            localStorage.setItem(Configuration.shoppingCartId, ShoppingCart.data.id);
            $.each(ShoppingCart.data.shoppingCartItems, function (i, val) {
                ShoppingCart.totalCount += val.quantity;
            });
            $(".totalShoppingCartItem").html(ShoppingCart.totalCount);
            if (ShoppingCart.data.customerId != localStorage.getItem(Configuration.userId)) {
                this.updateUser();
            }
        }

        this.initEvent();
    },
    initEvent: function () {
        $(".payCart").on("click",
            function () {
                var data = $(this).data();
                //alert(data.id);
                ShoppingCart.payCart(data.id);
            });



        $(".quantityTarget").on('change', function () {
            var data = $(this).data();
            //alert(data.id);
            if ($(this).val() < 0) {
                $(this).val(Math.abs($(this).val()));
            }
            if ($(this).val() == 0) {
                $(this).val(1);
            }
            ShoppingCart.updateItemCart(data.id, $(this).val());


            var tempTotal = data.price * parseInt($(this).val());
            $("#tr_item" + data.id).find('.total').each(function (i, val) {
                $(this).html(tempTotal.formatMoney(0, 3));
                $(this).attr('data-total', tempTotal);
            });

            var grandTotal = 0;
            $(".shoppingCart").find('.total').each(function (i, val) {
                grandTotal += parseInt($(this).attr('data-total'));
            });
            $(".shoppingCart").find('.grandTotal').html(grandTotal.formatMoney(0, 3));
        });

        $('.laboPlus').on('click', function () {
            if ($(this).prev().val() < 20) {
                $(this).prev().val(+$(this).prev().val() + 1);

                var data = $(this).prev().data();

                ShoppingCart.updateItemCart(data.id, $(this).prev().val());

                var tempTotal = data.price * parseInt($(this).prev().val());
                $("#tr_item" + data.id).find('.total').each(function (i, val) {
                    $(this).html(tempTotal.formatMoney(0, 3));
                    $(this).attr('data-total', tempTotal);
                });
                var grandTotal = 0;
                $(".shoppingCart").find('.total').each(function (i, val) {
                    grandTotal += parseInt($(this).attr('data-total'));
                });
                $(".shoppingCart").find('.grandTotal').html(grandTotal.formatMoney(0, 3));
            }

        });
        $('.laboMinus').on('click', function () {
            if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);

                var data = $(this).next().data();

                ShoppingCart.updateItemCart(data.id, $(this).next().val());

                var tempTotal = data.price * parseInt($(this).next().val());
                $("#tr_item" + data.id).find('.total').each(function (i, val) {
                    $(this).html(tempTotal.formatMoney(0, 3));
                    $(this).attr('data-total', tempTotal);
                });
                var grandTotal = 0;
                $(".shoppingCart").find('.total').each(function (i, val) {
                    grandTotal += parseInt($(this).attr('data-total'));
                });
                $(".shoppingCart").find('.grandTotal').html(grandTotal.formatMoney(0, 3));

            }
        });

        $(".createOrder").on("click",
            function () {
                var data = $(this).data();
                var inputId = "#qty-" + data.id;
                var inputQuantity = $(inputId).length ? $(inputId).val() : 1;
                //alert(data.id);
                if (inputQuantity > 0) {
                    ShoppingCart.createOrder(data.id, inputQuantity);
                } else {
                    ShoppingCart.createOrder(data.id, 1);
                }
            });

        $(".createBookingRoomOrder").on("click",
            function () {
                var data = $(this).data();
                //alert(data.id);
                ShoppingCart.createBookingRoomOrder(data.id);
            });

        $(".addCart").on("click",
            function () {
                var data = $(this).data();
                var inputId = "#qty-" + data.id;
                var inputQuantity = $(inputId).length ? $(inputId).val() : 1;
                // alert(inputId);
                // alert(inputQuantity);
                // alert(data.id);
                if (inputQuantity > 0) {
                    ShoppingCart.addItemCart(data.id, inputQuantity);
                    $('.toastCart').toast('hide');
                    $('.toastCart').toast({ delay: 2000 });
                    $('.toastCart').toast('show');
                } else {
                    alert('Vui lòng chọn số lượng');
                }

            });
        $('.backEmptyCart').on('click', function (e) {
            e.preventDefault();
            window.history.back();
        });
        $(".removeItemCart").on("click",
            function () {
                $(this).addClass("linkDisabled");
                var data = $(this).data();
                //alert(data.id);
                ShoppingCart.deleteItemCart(data.id);
            });
    },
    createBookingRoomOrder: function (productId) {
        var startDate = Merchant.getParameterByName("startDate");
        if (startDate != null) {
            startDate = startDate.split("/")[2] + "/" + startDate.split("/")[1] + "/" + startDate.split("/")[0]
        }
        var start = startDate + " " + Merchant.getParameterByName("startTime");
        var end = startDate + "  " + Merchant.getParameterByName("endTime");

        var id = Merchant.guid();
        var orderId = Merchant.guid();

        var createdBy = localStorage.getItem(Configuration.userId);
        var data = {
            Id: id,
            MerchantId: localStorage.getItem(Configuration.merchantId),
            ProductId: productId,
            OrderId: orderId,
            CustomerId: localStorage.getItem(Configuration.userId),

            CreatedDate: Merchant.getCurrentDateTime(),
            CreatedBy: createdBy,//localStorage.getItem(Configuration.userId)
            Name: "",
            Description: "",
            StartDateTime: start,
            EndDateTime: end,
            NotificationEmails: [],
        };
        var dataCommand = {
            CommandName: "CreateOrderBookingRoomSale",
            Domain: "Sale",
            Content: JSON.stringify(data),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: id,
            //ReCaptcha: reCaptcha,
            timeOutSecond: 30,
        }
        var tokenParam = '';
        if (Merchant.getCookie(Configuration.token) != '') {
            tokenParam += '&tokenId=' + Merchant.getCookie(Configuration.token);
        }
        $.ajax({
            //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
            url: Merchant.apiUrl + "/Command/SendSync",
            type: 'post',
            contentType: 'application/json',
            processData: false,
            dataType: 'json',
            data: JSON.stringify(dataCommand),
            // async:false
            success: function (res) {
                if (res.Success) {

                    var temp = 'http://' + merchantCode + ShoppingCart.urlCheckout + '?id=' +
                        orderId +
                        '&urlReturn=https://' +
                        encodeURI(window.location.hostname)
                        + tokenParam;
                    console.log(temp);
                    window.location.href = temp;

                }
                else {
                    alert(res.Message);
                }

            },
            error: function (err) {
                alert(err.responseText);
            },
        });


    },
    createOrder: function (productId, quantity) {
        var id = Merchant.guid();
        var items = [{
            Id: Merchant.guid(),
            TargetId: productId,
            quantity: quantity
        }]
        var createdBy = localStorage.getItem(Configuration.userId)
        if (Merchant.getCookie(window.Configuration.refId) != "") {
            createdBy = Merchant.getCookie(window.Configuration.refId);
        }
        var data = {
            Id: id,
            MerchantId: localStorage.getItem(Configuration.merchantId),
            TargetId: localStorage.getItem(Configuration.merchantId),
            CustomerId: localStorage.getItem(Configuration.userId),
            OrderItems: items,
            CreatedDate: Merchant.getCurrentDateTime(),
            CreatedBy: createdBy//localStorage.getItem(Configuration.userId)
        };
        var dataCommand = {
            CommandName: "CreateOrder",
            Domain: "CustomerRelationshipManagement",
            Content: JSON.stringify(data),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: id,
            //ReCaptcha: reCaptcha,
            timeOutSecond: 30,
        }
        var tokenParam = '';
        if (Merchant.getCookie(Configuration.token) != '') {
            tokenParam += '&tokenId=' + Merchant.getCookie(Configuration.token);
        }
        $.ajax({
            //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
            url: Merchant.apiUrl + "/Command/SendSync",
            type: 'post',
            contentType: 'application/json',
            processData: false,
            dataType: 'json',
            data: JSON.stringify(dataCommand),
            // async:false
            success: function (res) {
                if (res.Success) {

                    window.location.href = 'http://' + merchantCode + ShoppingCart.urlCheckout + '?id=' +
                        id +
                        '&urlReturn=https://' +
                        encodeURI(window.location.hostname)
                        + tokenParam;

                }
                else {
                    alert(res.Message);
                }

            },
            error: function (err) {
                alert(err.responseText);
            },
        });


    },
    queryData: function () {
        var returnData = [];
        var searchObject = {
            "_source": {

            },
            query: {
                query: {
                    "bool": {
                        "must": [
                            {
                                "match": { "inventoryId": localStorage.getItem(Configuration.merchantId) }
                            },
                            {
                                "match": { "customerId": localStorage.getItem(Configuration.userId) }
                            }
                        ]
                    }

                }
            },
            sort: []
        };

        var xhr = new XMLHttpRequest();
        //Starts the variable xhr as the variable for the request
        xhr.open("POST", "https://es.foodizzi.com/shoppingcarts/shoppingcart/_search?&size=1", false);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader('Authorization', 'Basic ' + btoa('amara:dSPKMcdQkG5X97b'));
        //Runs method 'open' which defines the request

        //Sends the request
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var temp = JSON.parse(xhr.responseText);
                    totalQuery = temp.hits.total;
                    $.each(temp.hits.hits, function (i, val) {
                        returnData.push(val._source);
                    });

                    // if (handleData != undefined) {
                    //     handleData(returnData);
                    // }

                } else {
                    console.error(xhr.statusText);
                }
            }
        };
        xhr.onerror = function (e) {
            console.error(xhr.statusText);
        };
        xhr.send(JSON.stringify(searchObject));
        return returnData[0];
    },
    updateUser: function () {
        var data = {
            Id: localStorage.getItem(Configuration.shoppingCartId),
            CustomerId: localStorage.getItem(Configuration.userId),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: localStorage.getItem(Configuration.userId)
        };

        var dataCommand = {
            CommandName: "UpdateShoppingCartCustomerId",
            Domain: "ShoppingCart",
            Content: JSON.stringify(data),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: localStorage.getItem(Configuration.userId),
            //ReCaptcha: reCaptcha,
            timeOutSecond: 30,
        }

        $.ajax({
            //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
            url: Merchant.apiUrl + "/Command/SendSync",
            type: 'post',
            contentType: 'application/json',
            processData: false,
            dataType: 'json',
            data: JSON.stringify(dataCommand),
            // async:false
            success: function (res) {
                if (res.Success) {
                }
                else {
                    //alert(res.Message);
                }

            },
            error: function (err) {
                //alert(err.responseText);
            },
        });
    },
    payCart: function (id) {
        var orderId = Merchant.guid();
        var createdBy = localStorage.getItem(Configuration.userId)
        if (Merchant.getCookie(window.Configuration.refId) != "") {
            createdBy = Merchant.getCookie(window.Configuration.refId);
        }
        var data = {
            Id: id,
            OrderId: orderId,
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: createdBy// localStorage.getItem(Configuration.userId)
        };

        var dataCommand = {
            CommandName: "CheckoutShoppingCart",
            Domain: "Sale",
            Content: JSON.stringify(data),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: id,
            //ReCaptcha: reCaptcha,
            timeOutSecond: 30,
        }
        var tokenParam = '';
        if (Merchant.getCookie(Configuration.token) != '') {
            tokenParam += '&tokenId=' + Merchant.getCookie(Configuration.token);
        }
        $.ajax({
            //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
            url: Merchant.apiUrl + "/Command/SendSync",
            type: 'post',
            contentType: 'application/json',
            processData: false,
            dataType: 'json',
            data: JSON.stringify(dataCommand),
            // async:false
            success: function (res) {
                if (res.Success) {
                    localStorage.removeItem(Configuration.shoppingCartId);
                    window.location.href = 'http://' + merchantCode + ShoppingCart.urlCheckout + '?id=' +
                        orderId +
                        '&urlReturn=https://' +
                        encodeURI(window.location.hostname)
                        + tokenParam;

                }
                else {
                    alert(res.Message);
                }

            },
            error: function (err) {
                alert(err.responseText);
            },
        });
    },
    deleteItemCart: function (id) {
        var data = {
            Id: localStorage.getItem(Configuration.shoppingCartId),
            InventoryId: localStorage.getItem(Configuration.merchantId),
            ShoppingCartItemId: id,
            CustomerId: localStorage.getItem(Configuration.userId),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: localStorage.getItem(Configuration.userId),
            SubscribeId: localStorage.getItem(Configuration.shoppingCartId) + id
        };
        var dataCommand = {
            CommandName: "DeleteShoppingCartItem",
            Domain: "ShoppingCart",
            Content: JSON.stringify(data),
            ModifiedDate: Merchant.getCurrentDateTime(),
            ModifiedBy: id,
            //ReCaptcha: reCaptcha,
            timeOutSecond: 30,
        }

        $.ajax({
            //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
            url: Merchant.apiUrl + "/Command/SendSync",
            type: 'post',
            contentType: 'application/json',
            processData: false,
            dataType: 'json',
            data: JSON.stringify(dataCommand),
            // async:false
            success: function (res) {
                if (res.Success) {

                    $("#tr_item" + id).remove();

                }
                else {
                    alert(res.Message);
                }

            },
            error: function (err) {
                alert(err.responseText);
            },
        });
    },
    updateItemCart: function (itemId, quantity) {
        if (localStorage.getItem(Configuration.shoppingCartId) == undefined) {
            alert("Chưa có giỏ hàng");
        } else {
            var id = localStorage.getItem(Configuration.shoppingCartId);
            var items = {
                Id: itemId,
                Quantity: quantity
            };
            var data = {
                Id: id,
                ShoppingCartItem: items,
                ModifiedDate: Merchant.getCurrentDateTime(),
                ModifiedBy: localStorage.getItem(Configuration.userId)
            };
            var dataCommand = {
                CommandName: "UpdateShoppingCartItem",
                Domain: "ShoppingCart",
                Content: JSON.stringify(data),
                ModifiedDate: Merchant.getCurrentDateTime(),
                ModifiedBy: id,
                //ReCaptcha: reCaptcha,
                timeOutSecond: 30,
            }

            $.ajax({
                //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
                url: Merchant.apiUrl + "/Command/SendSync",
                type: 'post',
                contentType: 'application/json',
                processData: false,
                dataType: 'json',
                data: JSON.stringify(dataCommand),
                // async:false
                success: function (res) {
                    if (res.Success) {

                        //alert("Cho vào giỏ thành công");

                    }
                    else {
                        alert(res.Message);
                    }

                },
                error: function (err) {
                    alert(err.responseText);
                },
            });
        }
    },
    addItemCart: function (productId, quantity) {
        if (localStorage.getItem(Configuration.shoppingCartId) == undefined) {

            var id = Merchant.guid();
            localStorage.setItem(Configuration.shoppingCartId, id);
            var items = [{
                Id: Merchant.guid(),
                TargetId: productId,
                Quantity: quantity
            }]
            var data = {
                Id: id,
                MerchantId: localStorage.getItem(Configuration.merchantId),
                TargetId: localStorage.getItem(Configuration.merchantId),
                InventoryId: localStorage.getItem(Configuration.merchantId),
                ShoppingCartItems: items,
                CustomerId: localStorage.getItem(Configuration.userId),
                CreatedDate: Merchant.getCurrentDateTime(),
                CreatedBy: localStorage.getItem(Configuration.userId)
            };
            var dataCommand = {
                CommandName: "CreateShoppingCart",
                Domain: "ShoppingCart",
                Content: JSON.stringify(data),
                ModifiedDate: Merchant.getCurrentDateTime(),
                ModifiedBy: id,
                //ReCaptcha: reCaptcha,
                timeOutSecond: 30,
            }

            $.ajax({
                //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
                url: Merchant.apiUrl + "/Command/SendSync",
                type: 'post',
                contentType: 'application/json',
                processData: false,
                dataType: 'json',
                data: JSON.stringify(dataCommand),
                // async:false
                success: function (res) {
                    if (res.Success) {
                        //alert("Cho vào giỏ thành công");

                        ShoppingCart.totalCount += parseInt(quantity, 10);

                        $(".totalShoppingCartItem").html(ShoppingCart.totalCount);
                    }
                    else {
                        localStorage.removeItem(Configuration.shoppingCartId)
                    }

                },
                error: function (err) {
                    alert(err.responseText);
                },
            });
        }
        else {
            var id = localStorage.getItem(Configuration.shoppingCartId);
            var items = {
                Id: Merchant.guid(),
                TargetId: productId,
                quantity: quantity
            };
            var data = {
                Id: id,

                ShoppingCartItem: items,
                ModifiedDate: Merchant.getCurrentDateTime(),
                ModifiedBy: localStorage.getItem(Configuration.userId)
            };
            var dataCommand = {
                CommandName: "AddItemToShoppingCart",
                Domain: "ShoppingCart",
                Content: JSON.stringify(data),
                ModifiedDate: Merchant.getCurrentDateTime(),
                ModifiedBy: id,
                //ReCaptcha: reCaptcha,
                timeOutSecond: 30,
            }

            $.ajax({
                //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
                url: Merchant.apiUrl + "/Command/SendSync",
                type: 'post',
                contentType: 'application/json',
                processData: false,
                dataType: 'json',
                data: JSON.stringify(dataCommand),
                // async:false
                success: function (res) {
                    if (res.Success) {

                        ShoppingCart.totalCount += parseInt(items.quantity, 10);
                        $(".totalShoppingCartItem").html(ShoppingCart.totalCount);
                        //alert("Cho vào giỏ thành công");

                    }
                    else {
                        alert(res.Message);
                    }

                },
                error: function (err) {
                    alert(err.responseText);
                },
            });
        }
        // public class CreateShoppingCart : Command
        // {
        //     public readonly Guid MerchantId;
        //     public readonly Guid InventoryId;
        //     public readonly Guid TargetId;
        //     public readonly Guid CustomerId;
        //     public readonly List<ShoppingCartItemEntity> ShoppingCartItems;
        //     public readonly DateTime CreatedDate;
        //     public readonly Guid CreatedBy;

        //     public CreateShoppingCart(Guid id, Guid merchantId, Guid targetId, Guid customerId, Guid inventoryId, DateTime createdDate, Guid createdBy, List<ShoppingCartItemEntity> shoppingCartItems) : base(id)
        //     {
        //         CustomerId = customerId;
        //         InventoryId = inventoryId;
        //         CreatedDate = createdDate;
        //         CreatedBy = createdBy;
        //         ShoppingCartItems = shoppingCartItems;
        //         MerchantId = merchantId;
        //         TargetId = targetId;
        //     }
        // }

        // public class ShoppingCartItemEntity
        // {
        //     public Guid Id { get; set; }
        //     public string TargetId { get; set; }
        //     public int Quantity { get; set; }
        // }


    }
}