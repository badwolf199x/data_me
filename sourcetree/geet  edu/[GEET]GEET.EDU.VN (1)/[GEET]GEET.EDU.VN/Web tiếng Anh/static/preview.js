Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "," : d,
        t = t == undefined ? "." : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : " Đ");
};
var Common = {
    staticUrlCurrent: 0,
    activePage: 1,
    totalItemsCount: 0,

    getCookie: function (cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },

    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    findVariable(myRegex, myString) {
        var match = myRegex.exec(myString);
        var variables = [];
        while (match != null) {
            variables.push(match[0])
            match = myRegex.exec(myString);
        }
        return variables;
    },

    replaceSection: function (section, items) {

        var build = '';
        $.each(items, function (i, item) {
            var tempItemBuild = section.outerHTML;
            var subSections = $(section).find('[repeat]');
            if (subSections.length > 0) {
                $.each(subSections, function (i, subSection) {
                    tempItemBuild = tempItemBuild.replace(subSection.outerHTML, Common.replaceSection(subSection, item[$(subSection).attr('repeat')]));
                });
            }
            var tempVariables = Common.findVariable(/{{\s*[^}]*}}/g, section.outerHTML);
            $.each(tempVariables, function (index, variable) {
                tempItemBuild = Common.replaceVariable(item, variable, tempItemBuild);
            });
            build += tempItemBuild;
        });
        return build;
    },
    replaceVariable: function (item, variable, build) {
        var tempArray = variable.slice(2, variable.length - 2).split('|');
        if (tempArray.length === 0) return 'invalid variable!';

        //Lấy dữ liệu theo tên.
        //ví dụ: item["name"] là object, item["images"] là array
        var nameVariable = tempArray[0];
        var tempData = item[nameVariable];
        console.log(tempData);
        //Lấy function filter dạng mảng.
        var arrayFunction = tempArray.slice(1);
        if (arrayFunction.length > 0) {
            $.each(arrayFunction, function (i, val) {
                var functionName = val.split(':')[0];
                switch (functionName.toLowerCase()) {
                    case 'substring':
                        var etc = '';
                        if (val.split(':')[2] != undefined) etc = val.split(':')[2];
                        if (val.split(':')[1] != undefined) tempData = Common.subStringWithLastSpace(tempData, parseInt(val.split(':')[1]), etc);
                        break;
                    case 'rewrite':
                        tempData = Common.rewriteUrl(tempData);
                        break;
                    case 'uppercase':
                        tempData = tempData.toUpperCase();
                        break;
                    case 'lowercase':
                        tempData = tempData.toLowerCase();
                        break;
                    case 'formatdatetime':
                        if (val.split(':')[1] != undefined) {
                            tempData = Common.formatDateTime(tempData, val.split(':')[1]);
                        } else {
                            tempData = Common.formatDateTime(tempData);
                        }
                        break;
                    case 'formatmoney':
                        tempData = tempData.formatMoney(0, 3);
                        break;
                    case 'getobjectbyindex':

                        if (val.split(':')[1] != undefined && Array.isArray(tempData) && tempData.length >= parseInt(val.split(':')[1])) {
                            tempData = tempData[parseInt(val.split(':')[1])]
                        } else {
                            tempData = null;
                        }
                        break;
                    case 'getthumbnailbyindex':
                        if (nameVariable != 'images') break;
                        console.log(tempData);
                        var tempFilter = tempData.filter(e => e.isFeatured == true);
                        if (tempFilter.length > 0) {
                            if (val.split(':')[1] != undefined && tempFilter.length >= parseInt(val.split(':')[1])) {
                                tempData = tempFilter[parseInt(val.split(':')[1])]
                            } else {
                                tempData = tempFilter[0]
                            }
                        } else {
                            tempData = null;
                        }
                        break;
                    case 'getpath':

                        if (nameVariable != 'images') break;

                        if (Array.isArray(tempData) || tempData == null) {
                            tempData = Configuration.imageDefault;
                        }
                        else {
                            Common.staticUrlCurrent++;
                            if (Common.staticUrlCurrent >= Configuration.staticUrl.length) {
                                Common.staticUrlCurrent = 0;
                            }

                            tempData = Configuration.staticUrl[Common.staticUrlCurrent] + tempData['path'];
                            if (val.split(':')[1] != undefined) tempData += val.split(':')[1];
                        }
                        break;


                    default:
                        break;
                }
            });
        }
        return build.replace(variable, tempData);
    },

    change_alias(alias) {
        if (alias == null) {
            return '';
        }
        var str = alias;
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        return str;
    },
    rewriteUrl(string) {
        return Common.change_alias(string).replace(/\s/g, '-');
    },

    subStringWithLastSpace: function (str, length, etc) {
        if (str === undefined) return '';
        if (str.length < length) {
            return str;
        }
        if (etc == undefined) {
            etc = '';
        }
        str = str || "";
        var temp = str.substring(0, length);
        temp = temp.substring(0, temp.lastIndexOf(" "));
        return temp + etc;

    },

    formatDateTime(date, format) {
        var monthNames = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ];
        var d = new Date(date),
            month = "" + (d.getMonth() + 1),
            day = "" + d.getDate(),
            year = d.getFullYear();
        var hour = d.getHours();
        var min = d.getMinutes();
        var sec = d.getSeconds();

        if (month.length < 2) month = "0" + month;
        if (day.length < 2) day = "0" + day;
        if (format == undefined) {
            return [month, day, year].join("-") + " " + hour + ":" + min + ":" + sec;
        } else {
            switch (format) {
                case "yyyy-mm-dd":
                    return [year, month, day].join("-");

                case "dd-mm-yyyy":
                    return [day, month, year].join("-");

                case "mm-dd-yyyy":
                    return [month, day, year].join("-");

                case "MM":
                    return month;
                case "MMM":
                    return monthNames[d.getMonth()];
                case "DD":
                    return day;
                case "YYYY":
                    return year;
                case "mm":
                    return min;
                case "ss":
                    return sec;
                case "hh":
                    return hour < 10 ? "0" + hour.toString() : hour;
                case "hh-mm":
                    var tempHour = hour < 10 ? "0" + hour.toString() : hour;
                    var tempMin = min < 10 ? "0" + min.toString() : min;
                    return tempHour + ":" + tempMin;
                case "hh-mm-ss":
                    var tempHour = hour < 10 ? "0" + hour.toString() : hour;
                    var tempMin = min < 10 ? "0" + min.toString() : min;
                    var tempSec = sec < 10 ? "0" + sec.toString() : sec;
                    return tempHour + ":" + tempMin + ":" + tempSec;
                default:
                    return (
                        [day, month, year].join("-") + " " + hour + ":" + min + ":" + sec
                    );
            }
        }
    },

    getImageThumbnail(images, resizeMode) {
        resizeMode = resizeMode || '';
        var path = Configuration.imageDefault + resizeMode;

        if (images != undefined && images.length > 0) {
            $.each(images, function (i, val) {
                if (val.isFeatured == true) {
                    path = Configuration.imageRoot + val.path + resizeMode;
                }
            });
        }
        return path;
    },

};
let htmlIframe;
if (window.addEventListener) {
    window.addEventListener("message", function (evt) {

        switch (evt.data.action) {
            case "cancelPreView":
                cancelPreView(evt);
                break;
            case "sendDataPreview":
                addPreview(evt);
                break;
            case "resetPreview":
                resetPreview();
                break;
            case "clickScroll":
                clickScroll(evt);
                break;
            case "clickScrollBottom":
                clickScrollBottom(evt);
                break;
            case "bindingDataIframe":
                bindingDataIframe(evt);
                break;
            case "sendDataInput":
                sendDataInput(evt);
                break;
            case "sendDataRepeat":
                sendDataRepeat(evt);
                break;
            case "sendDataRepeatMenu":
                sendDataRepeatMenu(evt);
                break;
            case "deleteRepeat":
                deleteRepeat(evt);
                break;
            case "replaceNewSection":
                replaceNewSection(evt);
                break;
            case "sendLanguageId":
                sendLanguageId(evt);
                break;
            default:
                break;
        }
    }, false);

}
else {
    window.attachEvent("onmessage", displayMessage);
}

function sendLanguageId(evt) {
    console.log(evt)
    Common.setCookie(Configuration.currentLanguage, evt.data.languageId);
    window.location.reload();

}
function cancelPreView(evt) {
    let html = htmlIframe;
    $(`#${evt.data.id}`).replaceWith(evt.data.html);
    if (evt.data.name.toLowerCase() === "header") {
        $(`header`).replaceWith($(html));
        $.each($(`header`).find("[repeat]"), function (i, val) {
            $(this).hide();
        })
    }
    else if (evt.data.name.toLowerCase() === "footer") {
        $(`header`).replaceWith($(html));
        $.each($(`header`).find("[repeat]"), function (i, val) {
            $(this).hide();
        })
    }
    else {
        $(`#${evt.data.id}`).replaceWith($(html));
        $.each($(`#${evt.data.id}`).find("[repeat]"), function (i, val) {
            $(this).hide();
        })
    }
    if (evt.data.initScript !== undefined) {
        $("body").append(`<script src="${evt.data.initScript}"/>`);
    }
}
function sendDataRepeatMenu(evt) {
    console.log(evt)
    var html = evt.data.html;
    let $html = $(html);
    // var tempHtml = $("#" + evt.data.id);
    // var $temp = $(tempHtml);

    $.each($html.find("[repeat]"), function (indexRepeat, value) {
        let str = $(this)[0].outerHTML;
        if (str.includes("param:type:256")) {
            var self = this;
            let strRepeat = $(this).attr('repeat');
            let data = evt.data.arrayData[indexRepeat];
            let strHtml = "";
            $.each(data, function (index, items) {
                let dom = $(self)[0].outerHTML;
                var tempVariables = findVariable(/{{\s*[^}]*}}/g, dom);
                $.each(tempVariables, function (i, variables) {
                    let tempName = variables.slice(2, variables.length - 2);
                    switch (tempName.split("|")[0]) {
                        case "name":
                            dom = Common.replaceVariable(items, variables, dom);
                            break;
                        case "subDescription":
                            dom = Common.replaceVariable(items, variables, dom);;
                            break;
                        case "images":
                            dom = Common.replaceVariable(items, variables, dom);
                            break;
                        case "createdDate":
                            dom = dom = Common.replaceVariable(items, variables, dom);
                            break
                        case "price":
                            dom = dom = Common.replaceVariable(items, variables, dom);
                            break;
                        default:
                            break;
                    }
                })
                strHtml += dom;
            })
            // console.log(strHtml)
            $(this).parent().prepend(strHtml)

            let parentDomRepeatIframe = $(`header`).find(`[repeat='${strRepeat}']`).parent();

            console.log(parentDomRepeatIframe)

            let parentDomRepeat = $html.find(this).parent();
            console.log(parentDomRepeat)
            $("header").find(parentDomRepeatIframe).replaceWith(parentDomRepeat);
            $(this).hide();
        }
    })
    if (evt.data.initScript !== undefined) {
        $("body").append(`<script src="${evt.data.initScript}"/>`);
    }
}
function sendDataRepeat(evt) {
    console.log(evt)
    var html = evt.data.html;
    let $html = $(html);
    // var tempHtml = $("#" + evt.data.id);
    // var $temp = $(tempHtml);

    $.each($html.find("[repeat]"), function (indexRepeat, value) {
        var self = this;
        let strRepeat = $(this).attr('repeat');
        let data = evt.data.arrayData[indexRepeat];
        console.log(data);
        let strHtml = "";
        $.each(data, function (index, items) {
            let dom = $(self)[0].outerHTML;
            var tempVariables = findVariable(/{{\s*[^}]*}}/g, dom);
            $.each(tempVariables, function (i, variables) {
                let tempName = variables.slice(2, variables.length - 2);
                console.log(tempName);
                switch (tempName.split("|")[0]) {
                    case "name":
                        dom = Common.replaceVariable(items, variables, dom);
                        break;
                    case "subDescription":
                        dom = Common.replaceVariable(items, variables, dom);;
                        break;
                    case "description":
                        dom = Common.replaceVariable(items, variables, dom);;
                        break;
                    case "images":
                        dom = Common.replaceVariable(items, variables, dom);
                        break;
                    case "createdDate":
                        dom = Common.replaceVariable(items, variables, dom);
                        break
                    case "price":
                        dom = Common.replaceVariable(items, variables, dom);
                        break;
                    default:
                        break;
                }
            })
            strHtml += dom;
        })
        $(this).parent().append(strHtml)

        let parentDomRepeatIframe = $(`#${evt.data.id}`).find(`[repeat='${strRepeat}']`).parent();
        console.log(parentDomRepeatIframe)
        let parentDomRepeat = $html.find(this).parent();

        $("#" + evt.data.id).find(parentDomRepeatIframe).replaceWith(parentDomRepeat);
        $(this).hide();
    })
    if (evt.data.initScript !== undefined) {
        $("body").append(`<script src="${evt.data.initScript}"/>`);
    }
    // clickScroll(evt)
}

function addPreview(evt) {
    if ($('#root').find('footer').length > 0) {
        $('#root').find('footer').before("<div id='previewSection'></div>")
    } else {
        $('#root').append("<div id='previewSection'></div>")
    }

    $("#previewSection").html(evt.data.defaultHtml);
    message = evt.data.text;
    clickScroll(evt);
    if (evt.data.initScript !== undefined) {
        $("body").append(`<script src="${evt.data.initScript}"/>`)
    }
}
function resetPreview() {
    $('#previewSection').remove();
}
function bindingDataIframe(evt) {
    message = evt.data.text;
    clickScroll(evt);
    if ($(`${evt.data.name}`).length > 0 && $(`${evt.data.name}`).attr("id") === undefined) {
        $(`${evt.data.name}`).attr("id", evt.data.id)
    }
    let indexBinding = parseInt(evt.data.nameInput.slice(-1)) - 1;
    if (isNaN(indexBinding)) {
        indexBinding = 0;
    }

    switch (evt.data.dataBinding) {
        case "binding-bgpath":
            if ($(`#${evt.data.id}`).find(`[${evt.data.dataBinding}]`).length > 0) {
                $(`#${evt.data.id}`).find(`[${evt.data.dataBinding}]`).css({ 'background-image': `url('${message}')` })
            }
            else {
                $(`#${evt.data.id}`).css({ 'background-image': `url('${message}')` });
            }
            break;
        case "binding-widthBg":
            try {
                let height = "";
                if ($(`#${evt.data.id}`).find(`[binding-bgPath]`).length > 0) {
                    let currentPath = $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");

                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("height=")) {
                                height = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&width=${message}&height=${height}`;
                    $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
                else {
                    let currentPath = $(`#${evt.data.id}`).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("height=")) {
                                height = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&width=${message}&height=${height}`;
                    $(`#${evt.data.id}`).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
            } catch (err) {
                console.log("Có lỗi xảy ra khi chỉnh width")
            }
            break;
        case "binding-heightBg":
            try {
                let width = "";
                if ($(`#${evt.data.id}`).find(`[binding-bgPath]`).length > 0) {
                    let currentPath = $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");

                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("width=")) {
                                width = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&width=${message}&width=${width}`;
                    $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
                else {
                    let currentPath = $(`#${evt.data.id}`).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("width=")) {
                                width = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&height=${message}&width=${width}`;
                    $(`#${evt.data.id}`).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
            } catch (err) {
                console.log("Có lỗi xảy ra khi chỉnh width")
            }
            break;

        case "binding-widthImg":
            try {
                let height = "";
                let currentPath = $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src');
                if (currentPath.split("?").length > 1) {
                    let strCrop = currentPath.split("?")[1];
                    $.each(strCrop.split("&"), function (i, val) {
                        if (val.includes("height=")) {
                            height = val.split("=")[1];
                        }
                    })
                }
                $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src', currentPath.split("?")[0] + `?mode=crop&width=${message}&height=${height}`);
            }
            catch (err) {
                console.log("Có lỗi xảy ra khi chỉnh width! ")
            }
            break;

        case "binding-heightImg":
            try {
                let currentPath = $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src');
                let width = "";
                if (currentPath.split("?").length > 1) {
                    let strCrop = currentPath.split("?")[1];
                    $.each(strCrop.split("&"), function (i, val) {
                        if (val.includes("width=")) {
                            width = val.split("=")[1];
                        }
                    })
                }
                $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src', currentPath.split("?")[0] + `?mode=crop&width=${width}&height=${message}`);
            }
            catch (err) {
                console.log("Có lỗi xảy ra khi điều chỉnh height!")
            }
            break;
        case "binding-imgpath":

            if ($(`#${evt.data.id}`).find(`[binding-imgpath]`).length > 0) {
                $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src', message);
            }
            else {
                $(`#${evt.data.id}`).attr('src', message);
            }
            break;
        case "binding-bgcolor":
            if ($(`#${evt.data.id}`).find(`[binding-bgcolor]`).length > 0) {
                $($(`#${evt.data.id}`).find(`[binding-bgcolor]`)[indexBinding]).css({ 'background-color': `${message}` })
            }
            else {
                $(`#${evt.data.id}`).css({ 'background-color': `${message}` })
            }
            break;
        case "binding-btnhref":
            break;
        default:
            if ($(`#${evt.data.id}`).find(`[${evt.data.dataBinding}]`).length > 1) {
                $($(`#${evt.data.id}`).find(`[${evt.data.dataBinding}]`)[indexBinding]).html(message);
            }
            else {
                $(`#${evt.data.id}`).find(`[${evt.data.dataBinding}]`).html(message);
            }
            break;
    }

}
function sendDataInput(evt) {
    console.log(evt);
    let message = evt.data.text;
    clickScroll(evt);
    if ($(`${evt.data.name}`).length > 0 && $(`${evt.data.name}`).attr("id") === undefined) {
        $(`${evt.data.name}`).attr("id", evt.data.id)
    }
    let indexBinding = parseInt(evt.data.nameInput.slice(-1));
    // if (isNaN(indexBinding)) {
    //     indexBinding = 0;
    // }

    let dataBinding = evt.data.dataBinding.slice(0, evt.data.dataBinding.length - 1);
    switch (evt.data.dataBinding) {
        case "binding-bgpath":
            if ($(`#${evt.data.id}`).find(`[${dataBinding}]`).length > 0) {
                $(`#${evt.data.id}`).find(`[${evt.data.dataBinding}]`).css({ 'background-image': `url('${message}')` })
            }
            else {
                $(`#${evt.data.id}`).css({ 'background-image': `url('${message}')` });
            }
            break;
        case "binding-widthBg":
            try {
                let height = "";
                if ($(`#${evt.data.id}`).find(`[binding-bgPath]`).length > 0) {
                    let currentPath = $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");

                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("height=")) {
                                height = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&width=${message}&height=${height}`;
                    $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
                else {
                    let currentPath = $(`#${evt.data.id}`).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("height=")) {
                                height = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&width=${message}&height=${height}`;
                    $(`#${evt.data.id}`).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
            } catch (err) {
                console.log("Có lỗi xảy ra khi chỉnh width")
            }
            break;
        case "binding-heightBg":
            try {
                let width = "";
                if ($(`#${evt.data.id}`).find(`[binding-bgPath]`).length > 0) {
                    let currentPath = $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");

                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("width=")) {
                                width = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&width=${message}&width=${width}`;
                    $($(`#${evt.data.id}`).find(`[binding-bgPath]`)[indexBinding]).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
                else {
                    let currentPath = $(`#${evt.data.id}`).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
                    if (currentPath.split("?").length > 1) {
                        let strCrop = currentPath.split("?")[1];
                        $.each(strCrop.split("&"), function (i, val) {
                            if (val.includes("width=")) {
                                width = val.split("=")[1];
                            }
                        })
                    }
                    let strCrop = `?mode=crop&height=${message}&width=${width}`;
                    $(`#${evt.data.id}`).css({ 'background-image': `url(${currentPath.split("?")[0] + strCrop}` });
                }
            } catch (err) {
                console.log("Có lỗi xảy ra khi chỉnh width")
            }
            break;

        case "binding-widthImg":
            try {
                let height = "";
                let currentPath = $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src');
                if (currentPath.split("?").length > 1) {
                    let strCrop = currentPath.split("?")[1];
                    $.each(strCrop.split("&"), function (i, val) {
                        if (val.includes("height=")) {
                            height = val.split("=")[1];
                        }
                    })
                }
                $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src', currentPath.split("?")[0] + `?mode=crop&width=${message}&height=${height}`);
            }
            catch (err) {
                console.log("Có lỗi xảy ra khi chỉnh width! ")
            }
            break;

        case "binding-heightImg":
            try {
                let currentPath = $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src');
                let width = "";
                if (currentPath.split("?").length > 1) {
                    let strCrop = currentPath.split("?")[1];
                    $.each(strCrop.split("&"), function (i, val) {
                        if (val.includes("width=")) {
                            width = val.split("=")[1];
                        }
                    })
                }
                $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src', currentPath.split("?")[0] + `?mode=crop&width=${width}&height=${message}`);
            }
            catch (err) {
                console.log("Có lỗi xảy ra khi điều chỉnh height!")
            }
            break;
        case "binding-imgpath":

            if ($(`#${evt.data.id}`).find(`[binding-imgpath]`).length > 0) {
                $($(`#${evt.data.id}`).find(`[binding-imgpath]`)[indexBinding]).attr('src', message);
            }
            else {
                $(`#${evt.data.id}`).attr('src', message);
            }
            break;
        case "binding-bgcolor":
            if ($(`#${evt.data.id}`).find(`[binding-bgcolor]`).length > 0) {
                $($(`#${evt.data.id}`).find(`[binding-bgcolor]`)[indexBinding]).css({ 'background-color': `${message}` })
            }
            else {
                $(`#${evt.data.id}`).css({ 'background-color': `${message}` })
            }
            break;
        case "binding-btnhref":
            break;
        default:
            console.log("hehe")
            console.log(dataBinding)
            if ($(`#${evt.data.id}`).find(`[${dataBinding}]`).length > 1) {
                console.log("object")
                $($(`#${evt.data.id}`).find(`[${dataBinding}]`)[indexBinding]).html(message);
            }
            else {
                $(`#${evt.data.id}`).find(`[${dataBinding}]`).html(message);
            }
            break;
    }
}
function clickScroll(evt) {
    location.href = "#" + evt.data.id;
}
function clickScrollBottom(evt) {
    clickScroll(evt);

}

function findVariable(myRegex, myString) {
    var match = myRegex.exec(myString);
    var variables = [];
    while (match != null) {
        variables.push(match[0])
        match = myRegex.exec(myString);
    }
    return variables;
}

function replaceSection(section, items) {

    var build = '';
    $.each(items, function (i, item) {

        var tempItemBuild = section.outerHTML;
        var subSections = $(section).find('[repeat]');
        if (subSections.length > 0) {
            $.each(subSections, function (i, subSection) {
                tempItemBuild = tempItemBuild.replace(subSection.outerHTML, replaceSection(subSection,
                    item[$(subSection).attr('repeat')]));
            });
        }

        var tempVariables = findVariable(/{{\s*[^}]*}}/g, section.outerHTML);
        $.each(tempVariables, function (index, variable) {
            tempItemBuild = replaceVariable(item, variable, tempItemBuild);
        });
        build += tempItemBuild;
    });
    return build;
};
function replaceVariable(item, variable, build) {
    var tempArray = variable.slice(2, variable.length - 2).split('|');
    // console.log(tempArray);
    if (tempArray.length === 0) return 'invalid variable!';

    //Lấy dữ liệu theo tên.
    //ví dụ: item["name"] là object, item["images"] là array
    var nameVariable = tempArray[0];
    var tempData = item[nameVariable];

    //Lấy function filter dạng mảng.
    var arrayFunction = tempArray.slice(1);
    if (arrayFunction.length > 0) {
        $.each(arrayFunction, function (i, val) {
            var functionName = val.split(':')[0];
            switch (functionName.toLowerCase()) {
                case 'substring':
                    var etc = '';
                    if (val.split(':')[2] != undefined) etc = val.split(':')[2];
                    if (val.split(':')[1] != undefined) tempData = Common.subStringWithLastSpace(tempData, parseInt(val.split(':')[1]),
                        etc);
                    break;
                case 'rewrite':
                    tempData = Common.rewriteUrl(tempData);
                    break;
                case 'uppercase':
                    tempData = tempData.toUpperCase();
                    break;
                case 'lowercase':
                    tempData = tempData.toLowerCase();
                    break;
                case 'formatdatetime':
                    if (val.split(':')[1] != undefined) {
                        tempData = Common.formatDateTime(tempData, val.split(':')[1]);
                    } else {
                        tempData = Common.formatDateTime(tempData);
                    }
                    break;
                case 'formatmoney':
                    tempData = tempData.formatMoney(0, 3);
                    break;
                case 'getobjectbyindex':

                    if (val.split(':')[1] != undefined && Array.isArray(tempData) && tempData.length >= parseInt(val.split(':')[1])) {
                        tempData = tempData[parseInt(val.split(':')[1])]
                    } else {
                        tempData = null;
                    }
                    break;
                case 'getthumbnailbyindex':
                    if (nameVariable != 'images') break;
                    var tempFilter = tempData.filter(e => e.isFeatured == true);
                    if (tempFilter.length > 0) {
                        if (val.split(':')[1] != undefined && tempFilter.length >= parseInt(val.split(':')[1])) {
                            tempData = tempFilter[parseInt(val.split(':')[1])]
                        } else {
                            tempData = tempFilter[0]
                        }
                    } else {
                        tempData = null;
                    }
                    break;
                case 'getpath':

                    if (nameVariable != 'images') break;

                    if (Array.isArray(tempData) || tempData == null) {
                        tempData = Configuration.imageDefault;
                    }
                    else {
                        tempData = Configuration.imageRoot + tempData['path'];
                    }
                    break;
                default:
                    break;
            }
        });
    }
    return build.replace(variable, tempData);
}
function replaceNewSection(evt) {
    console.log(evt)
    htmlIframe = $(`#${evt.data.id}`)[0].outerHTML;
    var html = evt.data.html;

    if (evt.data.type !== "reGetData") {
        if ($(`#${evt.data.id}`).find("[repeat]").length == 0) {
            $(document).ready(function () {
                if (evt.data.name.toLowerCase() === "header") {
                    $(`header`).replaceWith($(html));
                    $.each($(`header`).find("[repeat]"), function (i, val) {
                        $(this).hide();
                    })
                }
                else if (evt.data.name.toLowerCase() === "footer") {
                    $(`header`).replaceWith($(html));
                    $.each($(`header`).find("[repeat]"), function (i, val) {
                        $(this).hide();
                    })
                }
                else {
                    $(`#${evt.data.id}`).replaceWith($(html));
                    $.each($(`#${evt.data.id}`).find("[repeat]"), function (i, val) {
                        $(this).hide();
                    })
                }
            })


        }
    }
    else {
        $(`#${evt.data.id}`).replaceWith($(html));
        $.each($(`#${evt.data.id}`).find("[repeat]"), function (i, val) {
            $(this).hide();
        })
    }


    // else {
    //     $(`#${evt.data.id}`).replaceWith($(html));
    //     $.each($(`#${evt.data.id}`).find("[repeat]"), function (i, val) {
    //         $(this).hide();
    //     })
    // }

    if (evt.data.initScript !== undefined) {
        $("body").append(`<script src="${evt.data.initScript}"/>`); /// xem lại có thể sẽ khởi tạo hiệu ứng
        clickScroll(evt)
    }

}



function deleteRepeat(evt) {
    $('#' + evt.data.id).find("[repeat]").remove();
}
