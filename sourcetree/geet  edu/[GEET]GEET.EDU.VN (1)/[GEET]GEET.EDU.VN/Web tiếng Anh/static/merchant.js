Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN((c = Math.abs(c))) ? 2 : c,
        d = d == undefined ? "," : d,
        t = t == undefined ? "." : t,
        s = n < 0 ? "-" : "",
        i = parseInt((n = Math.abs(+n || 0).toFixed(c))) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return (
        s +
        (j ? i.substr(0, j) + t : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
        (c
            ? d +
              Math.abs(n - i)
                  .toFixed(c)
                  .slice(2)
            : " Đ")
    );
};

var Configuration = {
    userLanguage: "userLanguage",
    refId: "refId",
    urlGraphQl: "https://apicms.izzi.asia/graphql/",
    shoppingCartId: "shoppingCartId",
    repeatPattern: /<\s*repeat[^>]*>([\s\S]*?)<\s*\/\s*repeat>/g,
    repeatRegular: /<[^>]*repeat[^>]*>([\s\S]*?)<[^>]+>/g,
    categoryIndex: "categories",
    categoryType: "category",
    userId: "userId",
    productIndex: "products",
    productType: "product",
    guidEmpty: "00000000-0000-0000-0000-000000000000",
    //url
    imageRoot: "https://static.foodizzi.com/",
    staticUrl: [
        "https://static0.foodizzi.com/",
        "https://static1.foodizzi.com/",
        "https://static2.foodizzi.com/",
        "https://static3.foodizzi.com/",
        "https://static4.foodizzi.com/",
        "https://static5.foodizzi.com/",
    ],
    imageDefault: "https://static.foodizzi.com/images/no-image.png",
    apiUrl: "https://apiRestful.izzi.asia",
    //merchant
    merchantId: "merchantId",
    //language
    languageVi: "838aef56-78bb-11e6-b5a6-00155d582814",
    languageEn: "e3509252-c42d-4ae5-9779-d4805a687a8e",
    //token
    currentLanguage: "tokenLanguage",
    userId: "userId",
    merchantCodeEndUser: "merchantCodeEndUser",
    token: "izziToken",
    //domain
    domainCms: ".labo.vn",
    //http
    http: "http://",
    //https
    https: "https://",
    //item per page
    itemsPerPage: 9,
    totalAddressResort: 0,
    typeOnSubmit: {
        showThankYou: 1,
        openUrl: Math.pow(2, 5),
    },
    languages: [
        { id: "7c88bd96-6a9b-46c2-b57a-dc21ab3f87d0", code: "ja-JP", imagePath: "https://static.foodizzi.com/images/FlagIcons/JP.svg" },
        { id: "838aef56-78bb-11e6-b5a6-00155d582814", code: "vi-VN", imagePath: "https://static.foodizzi.com/images/FlagIcons/vn.svg" },
        { id: "9801ff94-a210-495d-8dff-70459d0aee49", code: "ko-KR", imagePath: "https://static.foodizzi.com/images/FlagIcons/kr.svg" },
        { id: "e3509252-c42d-4ae5-9779-d4805a687a8e", code: "en-US", imagePath: "https://static.foodizzi.com/images/FlagIcons/us.svg" },
        { id: "b26d20b5-f86b-4f89-8fc6-72e8b7ff8934", code: "km-KH", imagePath: "https://static.foodizzi.com/images/FlagIcons/kh.svg" },
    ],
};

var Merchant = {
    totalItemsCount: 0,
    page: 1,
    data: null,
    pageSize: 12,
    pageSizeArticles: 9,
    pageSizeProducts: 9,
    apiUrl: "https://apiRestful.izzi.asia",
    domain: window.location.hostname,
    getParameterByName: function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    updateQueryStringParameter(key, value,uri = window.location.href) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
          return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
          return uri + separator + key + "=" + value;
        }
      },
    showModalBs(objData) {
        let data = {
            title: objData.title,
            message: objData.message,
        };
        let text = `<div class="modal fade" id="modalCustom">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <div class="modal-header">
              <h4 class="modal-title">${data.title}</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <div class="modal-body">
                 ${data.message}
            </div>
      
            <div class="modal-footer">
              <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Đóng</button>
            </div>
      
          </div>
        </div>
      </div>`;
        if ($("#modalCustom").length) {
            $("#modalCustom").replaceWith(text);
        } else {
            $("body").append(text);
        }
        $("#modalCustom").modal("show");
    },
    formatDateTime(date, format) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var d = new Date(date),
            month = "" + (d.getMonth() + 1),
            day = "" + d.getDate(),
            year = d.getFullYear();
        var hour = d.getHours();
        var min = d.getMinutes();
        var sec = d.getSeconds();

        if (month.length < 2) month = "0" + month;
        if (day.length < 2) day = "0" + day;
        if (format == undefined) {
            return [month, day, year].join("-") + " " + hour + ":" + min + ":" + sec;
        } else {
            switch (format) {
                case "yyyy-mm-dd":
                    return [year, month, day].join("-");

                case "dd-mm-yyyy":
                    return [day, month, year].join("-");

                case "mm-dd-yyyy":
                    return [month, day, year].join("-");

                case "MM":
                    return month;
                case "MMM":
                    return monthNames[d.getMonth()];
                case "DD":
                    return day;
                case "YYYY":
                    return year;
                case "mm":
                    return min;
                case "ss":
                    return sec;
                case "hh":
                    return hour < 10 ? "0" + hour.toString() : hour;
                case "hh-mm":
                    var tempHour = hour < 10 ? "0" + hour.toString() : hour;
                    var tempMin = min < 10 ? "0" + min.toString() : min;
                    return tempHour + ":" + tempMin;
                case "hh-mm-ss":
                    var tempHour = hour < 10 ? "0" + hour.toString() : hour;
                    var tempMin = min < 10 ? "0" + min.toString() : min;
                    var tempSec = sec < 10 ? "0" + sec.toString() : sec;
                    return tempHour + ":" + tempMin + ":" + tempSec;
                default:
                    return [day, month, year].join("-") + " " + hour + ":" + min + ":" + sec;
            }
        }
    },
    getDataGraphQl(query) {
        var temp = null;
        $.ajax({
            url: Configuration.urlGraphQl,
            contentType: "application/json",
            type: "POST",
            async: false,
            data: JSON.stringify({ query: query }),
            success: function (result) {
                //console.log(result);
                temp = result;
            },
        });

        return temp;
    },

    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
        var expires = "expires=" + d.toUTCString();
        if (window.local) {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;";
        } else {
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;SameSite=None;Secure;";
        }
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    deleteCookie: function (cname) {
        document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    },
    loadLinkCss: function (arr) {
        arr.forEach((element) => {
            var link = document.createElement("link");
            link.rel = "stylesheet";
            link.type = "text/css";
            link.href = element;
            document.head.appendChild(link);
        });
    },
    loadLinkScript: function (array) {
        var done = false;
        var scr = document.createElement("script");

        scr.onload = handleLoad;
        scr.onreadystatechange = handleReadyStateChange;
        scr.onerror = handleError;
        scr.src = decodeURIComponent(array[0]);
        document.body.appendChild(scr);
        //console.log(array[0]);
        function handleLoad() {
            if (!done) {
                done = true;
                array.shift();
                if (array.length > 0) {
                    Merchant.loadLinkScript(array);
                }
            }
        }

        function handleReadyStateChange() {
            var state;

            if (!done) {
                state = scr.readyState;
                if (state === "complete") {
                    handleLoad();
                }
            }
        }
        function handleError() {
            if (!done) {
                done = true;

                return;
            }
        }
    },

    queryDataMerchant: function (domain) {
        var returnData = [];
        var searchObject = {
            _source: {},
            query: {
                match_phrase: {
                    "domains.name": {
                        query: domain,
                    },
                },
            },
            sort: [],
        };

        var xhr = new XMLHttpRequest();
        //Starts the variable xhr as the variable for the request
        xhr.open("POST", "https://es.foodizzi.com/merchants/merchant/_search?&size=100", false);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader("Authorization", "Basic " + btoa("amara:dSPKMcdQkG5X97b"));
        //Runs method 'open' which defines the request

        //Sends the request
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var temp = JSON.parse(xhr.responseText);
                    totalQuery = temp.hits.total;
                    $.each(temp.hits.hits, function (i, val) {
                        returnData.push(val._source);
                    });
                    //console.log(returnData);
                    // if (handleData != undefined) {
                    //     handleData(returnData);
                    // }
                } else {
                    console.error(xhr.statusText);
                }
            }
        };
        xhr.onerror = function (e) {
            console.error(xhr.statusText);
        };
        xhr.send(JSON.stringify(searchObject));
        var returnValue = null;
        $.each(returnData, function( index, value ) {
            if(value.domains.find(e=>e.name == domain)){
                returnValue =  value;
            }
          });
          return returnValue;
        //return returnData.find(e=> e.domains.name == domain);
    },

    authentication: function () {
        ////////////////////================ izzi cms==================

        if (
            localStorage.getItem("merchantId") === "46330eee-eaff-47e6-a4b8-53aa116b3eb7" ||
            localStorage.getItem("merchantId") === "5459bd9b-7440-ae4f-d86e-b81499cb128c"
        ) {
            $("#formRegisterCms input[name='merchantCode']").on("input", function () {
                $("#formRegisterCms #merchantText").css("display", "inline-block");
                let tempValue = $(this).val().toLowerCase().replace(/\./g, "-");
                $("#formRegisterCms #merchantText span").text(tempValue);
            });

            $("#formRegisterElearn input[name='merchantCode']").on("input", function () {
                $("#formRegisterElearn #merchantText").css("display", "inline-block");
                let tempValue = $(this).val().toLowerCase().replace(/\./g, "-");
                $("#formRegisterElearn #merchantText span").text(tempValue);
            });

            $("[href='#registerCms']").click(function () {
                $("#registerCms").modal("show");
            });

            $("[href='#registerElearn']").click(function () {
                $("#registerElearn").modal("show");
            });

            $("#registerCms").on("show.bs.modal", function (event) {
                $("#formRegisterCms")
                    .find("input")
                    .each(function () {
                        $(this).val("");
                    });
            });

            $("#formRegisterElearn").on("show.bs.modal", function (event) {
                $("#formRegisterElearn")
                    .find("input")
                    .each(function () {
                        $(this).val("");
                    });
            });

            $("#formLoginCms button[type='submit']").click(function () {
                let merchantCodeEndUser = $("#formLoginCms input[name='merchantCode']").val();
                //$("body").LoadingOverlay("show");
                let objData = {
                    UserName: $("#formLoginCms input[name='email']").val(),
                    Password: $("#formLoginCms input[name='password']").val(),
                    MerchantCode: merchantCodeEndUser,
                };

                $.ajax({
                    url: Configuration.apiUrl + "/Authentication/MerchantLogin",
                    type: "post",
                    contentType: "application/json",
                    processData: false,
                    dataType: "json",
                    data: JSON.stringify(objData),
                    success: function (res) {
                        if (res.Success) {
                            Merchant.setCookie(Configuration.merchantCodeEndUser, merchantCodeEndUser);
                            // alert("đăng ký thành công");
                            $("#loginCms").modal("hide");
                            $("body").LoadingOverlay("hide");

                            afterLogin(res);
                        } else {
                            $("#loginCms").modal("hide");
                            $("body").LoadingOverlay("hide");
                            Merchant.showModalBs({
                                title: "Thất bại",
                                message: "Đăng nhập thất bại",
                            });
                        }
                    },
                    error: function (err) {
                        $("#loginCms").modal("hide");
                        $("body").LoadingOverlay("hide");
                        Merchant.showModalBs({
                            title: "Thất bại",
                            message: "Đăng nhập thất bại",
                        });
                    },
                });
            });

            $("#formRegisterCms button[type='submit']").click(function () {
                var $button = $(this);
                //return;
                $("#formRegisterCms").validate({
                    rules: {
                        email: {
                            required: true,
                            email: true,
                        },
                        mobile: {
                            required: true,
                            digits: true,
                            minlength: 10,
                            maxlength: 10,
                        },
                        giftcode: {
                            required: true,
                        },
                        merchantCode: {
                            required: true,
                        },
                    },
                    messages: {
                        email: {
                            required: "Mời nhập email",
                            email: "Mời nhập đúng định dạng email",
                        },
                        mobile: {
                            required: "Xin mời nhập số điện thoại",
                            digits: "Mời nhập số",
                            minlength: "Mời nhập 10 số",
                            maxlength: "Mời nhập 10 só",
                        },
                        giftcode: {
                            required: "Mời nhập mã.",
                        },
                        merchantCode: {
                            required: "Tên website",
                        },
                    },
                });
                if (!$("#formRegisterCms").valid()) {
                    return;
                }
                let btnLabel = $button.html();
                $button.prop("disabled", true);
                $button.html(`<div class="labo-spinner-border labo-spinner-border-sm mr-1" role="status"></div> Đang xử lý...`);
                let tempId = Merchant.guid();
                let merchantCode = $("#formRegisterCms input[name='merchantCode']").val().toLowerCase().replace(/\./g, "-");
                //$("body").LoadingOverlay("show");
                $.ajax({
                    url: Configuration.apiUrl + `/Merchant/CheckCode?code=${merchantCode}`,
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    success: function (res) {
                        if (res.Success) {
                            requestRegisterCMS();
                        } else {
                            // $("body").LoadingOverlay("hide");
                            Merchant.showModalBs({
                                title: "Thất bại",
                                message: "Tên website đã tồn tại!",
                            });
                            $button.prop("disabled", false);
                            $button.html(btnLabel);
                        }
                    },
                    error: function (err) {
                        //  $("body").LoadingOverlay("hide");
                        Merchant.showModalBs({
                            title: "Thất bại",
                            message: "Có lỗi xảy ra",
                        });
                        $button.prop("disabled", false);
                        $button.html(btnLabel);
                    },
                });

                return false;

                function requestRegisterCMS() {
                    let objData = {
                        Id: tempId,
                        Email: $("#formRegisterCms input[name='email']").val(),
                        Mobile: $("#formRegisterCms input[name='mobile']").val(),
                        GiftCode: $("#formRegisterCms input[name='giftcode']").val(),
                        ProductId: "08507823-18a2-4d02-9b2b-7d9354b5b3a5",
                        MerchantId: localStorage.getItem(Configuration.merchantId),
                        MerchantCode: merchantCode,
                        ModifiedDate: Merchant.formatDateTime(new Date()),
                        ModifiedBy: tempId,
                    };
                    let dataSend = {
                        CommandName: "RegisterMerchantSale",
                        Domain: "Sale",
                        Content: JSON.stringify(objData),
                        ModifiedBy: tempId,
                        // ReCaptcha: reCaptcha,
                        ModifiedDate: Merchant.formatDateTime(new Date()),
                        TimeOutSecond: 60,
                    };
                    $.ajax({
                        // url: Configuration.apiUrl + "/Command/SendSyncReCaptcha",
                        url: Configuration.apiUrl + "/Command/SendSync",
                        type: "post",
                        contentType: "application/json",
                        processData: false,
                        dataType: "json",
                        data: JSON.stringify(dataSend),
                        success: function (res) {
                            if (res.Success) {
                                $("#registerCms").modal("hide");
                                //   $("body").LoadingOverlay("hide");
                                Merchant.showModalBs({
                                    title: "Thành Công",
                                    message: "Đăng ký thành công. Vui lòng kiểm tra thông tin tài khoản ở Email",
                                });
                                // window.open(
                                //     Configuration.http + merchantCode + Configuration.domainCms,
                                //     '_blank'
                                // );
                            } else {
                                $("#registerCms").modal("hide");
                                //  $("body").LoadingOverlay("hide");
                                Merchant.showModalBs({
                                    title: "Thất bại",
                                    message: res.Message,
                                });
                            }
                        },
                        error: function (err) {
                            $("#registerCms").modal("hide");
                            //   $("body").LoadingOverlay("hide");
                            Merchant.showModalBs({
                                title: "Thất bại",
                                message: "Đăng ký thất bại",
                            });
                        },
                        complete: function () {
                            $button.prop("disabled", false);
                            $button.html(btnLabel);
                        },
                    });
                }
            });

            $("#formRegisterElearn button[type='submit']").click(function () {
                var $button = $(this);
                //return;
                $("#formRegisterElearn").validate({
                    rules: {
                        email: {
                            required: true,
                            email: true,
                        },
                        mobile: {
                            required: true,
                            digits: true,
                            minlength: 10,
                            maxlength: 10,
                        },
                        name: {
                            required: true,
                        },
                        merchantCode: {
                            required: true,
                        },
                    },
                    messages: {
                        email: {
                            required: "Mời nhập email",
                            email: "Mời nhập đúng định dạng email",
                        },
                        mobile: {
                            required: "Xin mời nhập số điện thoại",
                            digits: "Mời nhập số",
                            minlength: "Mời nhập 10 số",
                            maxlength: "Mời nhập 10 só",
                        },
                        name: {
                            required: "Mời nhập mã.",
                        },
                        merchantCode: {
                            required: "Tên website",
                        },
                    },
                });
                if (!$("#formRegisterElearn").valid()) {
                    return;
                }
                let btnLabel = $button.html();
                $button.prop("disabled", true);
                $button.html(`<div class="labo-spinner-border labo-spinner-border-sm mr-1" role="status"></div> Đang xử lý...`);
                let tempId = Merchant.guid();
                let merchantCode = $("#formRegisterElearn input[name='merchantCode']").val().toLowerCase().replace(/\./g, "-");
                //$("body").LoadingOverlay("show");
                $.ajax({
                    url: Configuration.apiUrl + `/Merchant/CheckCode?code=${merchantCode}`,
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    success: function (res) {
                        if (res.Success) {
                            requestRegisterElearn();
                        } else {
                            // $("body").LoadingOverlay("hide");
                            Merchant.showModalBs({
                                title: "Thất bại",
                                message: "Tên website đã tồn tại!",
                            });
                            //alert("Tên website đã tồn tại!");
                            $button.prop("disabled", false);
                            $button.html(btnLabel);
                        }
                    },
                    error: function (err) {
                        //  $("body").LoadingOverlay("hide");
                        Merchant.showModalBs({
                            title: "Thất bại",
                            message: "Có lỗi xảy ra",
                        });
                        $button.prop("disabled", false);
                        $button.html(btnLabel);
                    },
                });

                return false;

                function requestRegisterElearn() {
                    let productId = $("#formRegisterElearn input[name='productId']").val();
                    let objData = {
                        Id: tempId,
                        Email: $("#formRegisterElearn input[name='email']").val(),
                        Mobile: $("#formRegisterElearn input[name='mobile']").val(),
                        Name: $("#formRegisterElearn input[name='name']").val(),
                        ProductId: productId,
                        MerchantId: localStorage.getItem(Configuration.merchantId),
                        MerchantCode: merchantCode,
                        ModifiedDate: Merchant.formatDateTime(new Date()),
                        ModifiedBy: tempId,
                    };
                    let dataSend = {
                        CommandName: "RegisterMerchantElearnSale",
                        Domain: "Sale",
                        Content: JSON.stringify(objData),
                        ModifiedBy: tempId,
                        // ReCaptcha: reCaptcha,
                        ModifiedDate: Merchant.formatDateTime(new Date()),
                        TimeOutSecond: 60,
                    };
                    $.ajax({
                        // url: Configuration.apiUrl + "/Command/SendSyncReCaptcha",
                        url: Configuration.apiUrl + "/Command/SendSync",
                        type: "post",
                        contentType: "application/json",
                        processData: false,
                        dataType: "json",
                        data: JSON.stringify(dataSend),
                        success: function (res) {
                            if (res.Success) {
                                $("#registerElearn").modal("hide");
                                var temp = res.Message.split("|");
                                //console.log(temp);
                                var urlRedirect =
                                    "http://izzilearn" +
                                    ShoppingCart.urlCheckout +
                                    "?id=" +
                                    temp[0] +
                                    "&urlReturn=https://" +
                                    encodeURI(window.location.hostname) +
                                    "&tokenId=" +
                                    temp[1];
                                //console.log(urlRedirect);
                                window.location.href = urlRedirect;
                                //'http://izzilearn' + ShoppingCart.urlCheckout + '?id=' +
                                //     temp[0] +
                                //     '&urlReturn=https://' +
                                //     encodeURI(window.location.hostname)
                                //     + '&tokenId=' + temp[1];
                            } else {
                                $("#registerElearn").modal("hide");
                                //  $("body").LoadingOverlay("hide");
                                Merchant.showModalBs({
                                    title: "Thất bại",
                                    message: res.Message,
                                });
                            }
                        },
                        error: function (err) {
                            $("#registerElearn").modal("hide");
                            //   $("body").LoadingOverlay("hide");
                            Merchant.showModalBs({
                                title: "Thất bại",
                                message: "Đăng ký thất bại",
                            });
                        },
                        complete: function () {
                            $button.prop("disabled", false);
                            $button.html(btnLabel);
                        },
                    });
                }
            });

            $("#cmsUser").click(function () {
                $("#cmsUser").attr("target", "_blank");
                $("#cmsUser").attr("href", `https://${Merchant.getCookie("merchantCodeEndUser")}.labo.vn`);
            });

            $.each($(".checkOutPrice"), function (i, val) {
                $(this).click(function () {
                    $("body").LoadingOverlay("show");
                    // let idProduct = $(this).attr("data-id");
                    let tempId = Merchant.guid();
                    // console.log(tempId)
                    // console.log(Configuration.userId);
                    // console.log(Merchant.getCookie(Configuration.userId))
                    let objData = {
                        Id: tempId,
                        MerchantId: Merchant.getCookie(Configuration.merchantId),
                        UserId: Merchant.getCookie(Configuration.userId) === "" ? Configuration.guidEmpty : Merchant.getCookie(Configuration.userId),
                        ProductId: $(this).attr("data-id"),
                        ModifiedDate: Merchant.formatDateTime(new Date()),
                    };
                    let dataSend = {
                        CommandName: "CreateOrderByIzziCms",
                        Domain: "Sale",
                        Content: JSON.stringify(objData),
                        ModifiedBy: Merchant.getCookie(Configuration.userId) === "" ? Configuration.guidEmpty : Merchant.getCookie(Configuration.userId),
                        ModifiedDate: Merchant.formatDateTime(new Date()),
                        TimeOutSecond: 10,
                    };
                    $.ajax({
                        url: Configuration.apiUrl + "/Command/SendSync",
                        type: "post",
                        contentType: "application/json",
                        processData: false,
                        dataType: "json",
                        data: JSON.stringify(dataSend),
                        success: function (res) {
                            if (res.Success) {
                                $("body").LoadingOverlay("hide");

                                let tempUrl =
                                    Configuration.https +
                                    `checkout.foodtalk.vn/Checkout/index?id=${tempId}` +
                                    `${Merchant.getCookie(Configuration.token) === "" ? "" : "&tokenId=" + Merchant.getCookie(Configuration.token)}&urlReturn=${
                                        window.location.href
                                    } `;
                                // alert(tempUrl)
                                window.location.href = tempUrl;
                                // console.log(tempUrl)
                            } else {
                                $("body").LoadingOverlay("hide");
                                Merchant.showModalBs({
                                    title: "Thất bại",
                                    message: "Thanh toán thất bại",
                                });
                            }
                        },
                        error: function (err) {
                            $("body").LoadingOverlay("hide");
                            Merchant.showModalBs({
                                title: "Thất bại",
                                message: "Thanh toán thất bại",
                            });
                        },
                    });
                });
            });
        }

        $("#profileUser").click(() => {
            $("#profileUser").attr("target", "_blank");
            if(window.merchantCode.toLowerCase() == "geetonline"){
                $("#profileUser").attr(
                    "href",
                    `https://learn.geetonline.vn?tokenId=${Merchant.getCookie(Configuration.token)}&returnUrl=${window.location.href}`
                );

            }
            else{
                $("#profileUser").attr(
                    "href",
                    `https://${window.merchantCode}.izzimember.com?tokenId=${Merchant.getCookie(Configuration.token)}&returnUrl=${window.location.href}`
                );
            }
            
        });

        $("#logoutFb").click(function () {
            $.ajax({
                url: Configuration.apiUrl + `/User/Logout?tokenId=${Merchant.getCookie(Configuration.token)}`,
                type: "GET",
                contentType: "application/json",
                processData: false,
                dataType: "json",
                // data: JSON.stringify({}),
                success: function (res) {
                    if (res.Success) {
                        Merchant.deleteCookie(Configuration.token);

                        Merchant.deleteCookie(Configuration.userId);
                        Merchant.deleteCookie(Configuration.merchantCodeEndUser);

                        localStorage.removeItem(Configuration.token);
                        localStorage.removeItem(Configuration.userId);
                        window.location.reload();
                    } else {
                        console.log(res);
                    }
                },
                error: function (res) {
                    console.log(res);
                },
            });
        });

        $("#btnFbLoginIn").click(() => {
            $(".popup-modal-dismiss").click();
            //   $("body").LoadingOverlay("show");
            FB.getLoginStatus(function (response) {
                // See the onlogin handler
                statusChangeCallback(response);
            });
        });

        $("#submitLogin").click(() => {
            $(".popup-modal-dismiss").click();
            //$("body").LoadingOverlay("show");
            let dataForm = {
                UserName: $("#formLogin input[name='name']").val().toLowerCase().trim(),
                Password: $("#formLogin input[name='password']").val().trim(),
                MerchantId: localStorage.getItem(Configuration.merchantId),
            };
            //console.log(dataForm)
            $.ajax({
                url: Configuration.apiUrl + "/User/Login",
                type: "post",
                contentType: "application/json",
                processData: false,
                dataType: "json",
                data: JSON.stringify(dataForm),
                success: function (res) {
                    if (res.Success) {
                        afterLogin(res);
                    } else {
                        //  $("body").LoadingOverlay("hide");
                        alert("Đăng nhập thất bại");
                    }
                },
                error: function (res) {
                    //  $("body").LoadingOverlay("hide");
                    alert("Đăng nhập thất bại");
                },
            });
        });

        $("#btnRegister #submitRegister").click(() => {
            $(".popup-modal-dismiss").click();
            //       $("body").LoadingOverlay("show");

            let dataForm = {
                Email: $("#formRegister input[name='email']").val(),
                Mobile: $("#formRegister input[name='mobile']").val(),
                MerchantId: Merchant.getCookie("merchantId"),
            };

            let dataCheckEmail = JSON.stringify({
                Field: "Email",
                Value: `${dataForm.Email}`,
                MerchantId: Merchant.getCookie("merchantId"),
            });
            let dataCheckMobile = JSON.stringify({
                Field: "Email",
                Value: `${dataForm.Email}`,
                MerchantId: Merchant.getCookie("merchantId"),
            });

            function sendCommandRegister() {
                $.ajax({
                    url: "https://api.foodtalk.vn" + "/User/Register",
                    type: "post",
                    contentType: "application/json",
                    processData: false,
                    dataType: "json",
                    data: JSON.stringify(dataForm),
                    success: function (res) {
                        if (res.Success) {
                            alert("Đăng ký thành công! Mật khẩu được gửi vào Email của bạn");
                            //   $("body").LoadingOverlay("hide");
                        } else {
                            $("body").loading("stop");
                            alert("Đăng ký thất bại");
                        }
                    },
                    error: function (err) {
                        alert("Đăng ký thất bại");
                        //     $("body").LoadingOverlay("hide");
                    },
                });
            }
            $.ajax({
                url: "https://api.foodtalk.vn" + "/User/CheckUserExist",
                type: "post",
                contentType: "application/json",
                data: dataCheckEmail,
                success: function (res) {
                    if (res.Success) {
                        alert("Email đã tồn tại");
                    } else {
                        $.ajax({
                            url: "https://api.foodtalk.vn" + "/User/CheckUserExist",
                            type: "post",
                            contentType: "application/json",
                            data: dataCheckMobile,
                            success: function (res) {
                                if (res.Success) {
                                    alert("Số điện thoại đã tồn tại");
                                } else {
                                    sendCommandRegister();
                                }
                            },
                        });
                    }
                },
            });
        });
        /**
            function user define
        **/
        function tryRequestLogin(oauthToken, provider) {
            $.ajax({
                url: Configuration.apiUrl + "/User/FacebookLogin",
                type: "post",
                dataType: "json",
                contentType: "application/json",
                success: function (response) {
                    if (response.Success) {
                        afterLogin(response);
                    } else {
                        //$("body").LoadingOverlay("hide");
                        alert("Không thể đăng nhập facebook");
                        // $('#siginModal').modal('hide');
                        // messageDialog.show("Thông báo", "Xảy ra lỗi");
                    }
                },
                error: function () {
                    alert("Có lỗi xảy ra");
                    //$("body").LoadingOverlay("hide");
                },
                data: JSON.stringify({
                    Token: oauthToken,
                    Provider: provider,
                    MerchantId: Merchant.getCookie("merchantId"),
                }),
            });
        }

        function afterLogin(response) {
            if (response.Success) {
                Merchant.setCookie(Configuration.token, response.Data.tokenId);
                localStorage.setItem(Configuration.token, response.Data.tokenId);
                location.reload();
                //getUser by tokien
                if (Merchant.getCookie(Configuration.token) != "") {
                    $.ajax({
                        url: Configuration.apiUrl + "/User/GetUserDataByToken?tokenId=" + Merchant.getCookie(Configuration.token),
                        method: "GET",
                        dataType: "json",
                        contentType: !1,
                        processData: !1,
                        async: false,
                        success: function (e) {
                            if (e.Success) {
                                // $('body').loading('stop');
                                //$("body").LoadingOverlay("hide");
                                let infoUser = JSON.parse(e.Message);
                                Merchant.setCookie(Configuration.userId, infoUser.Id);

                                localStorage.setItem(Configuration.userId, infoUser.Id);
                                ShoppingCart.updateUser();
                                //$('#btnLogin').attr('style', sectionLogin 'display:none !important');
                                $("#btnRegister, #btnLogin, .userNotLogin").attr("style", "display:none !important");
                                $("#stringNameUser, .stringNameUser").attr("style", "display:inline-block !important");
                                $(".userLogin").attr("style", "display:block !important");
                                $("#displayName, .displayName").text(infoUser.FullName);
                            } else {
                                // $("body").LoadingOverlay("hide");
                            }
                        },
                        error: function () {
                            // $("body").LoadingOverlay("hide");
                        },
                    });
                }
            } else {
                alert("Có lỗi xảy ra");
                // $("body").LoadingOverlay("hide");
            }
        }

        /**
            api facebook 
        **/

        function statusChangeCallback(response) {
            // Called with the results from FB.getLoginStatus().
            FB.login(
                function (response) {
                    if (response.status === "connected") {
                        tryRequestLogin(response.authResponse.accessToken, "facebook");
                    } else {
                        //$("body").LoadingOverlay("hide");
                        // closed window, not autherized with: not_authorized, unkown status will close without inform
                        alert("Không thể đăng nhập Facebook");
                    }
                },
                {
                    scope: "public_profile,email",
                }
            );
        }        
        
    },

    initCheckLogin: function () {
        if (window.userLogin) {
            $(".userNotLogin").remove();            
            $(".userLogin").addClass("d-block");
            $(".userLogin.inline").addClass("d-inline");
            $("#btnLogin, #btnRegister").attr("style", "display:none !important");            
            $("#stringNameUser, .stringNameUser").attr("style", "display:inline-block !important");            
            $("#displayName, .displayName").text(userName);
            $(".popup-modal-dismiss").click();
        } else {            
            $(".userNotLogin").addClass("d-block");
            $(".userNotLogin.inline").addClass("d-inline");
            $(".userLogin").attr("style", "display:none !important");     
            var hostLink = window.location.hostname;        
            if (hostLink.includes(".labo.io") != true) {
                $(".userLogin").remove();
            }
        }
    },

    initSubscribe: function () {
        $(".subscribeAuthor").click(function () {
            if (localStorage.getItem(Configuration.token)) {
                var authorId = $(this).data("id");
                var data = {
                    Id: localStorage.getItem(Configuration.userId),
                    AuthorId: authorId,
                };
                var dataCommand = {
                    CommandName: "SubcribeAuthorByUser",
                    Domain: "User",
                    Content: JSON.stringify(data),
                    ModifiedDate: Merchant.getCurrentDateTime(),
                    ModifiedBy: localStorage.getItem(Configuration.userId),
                    timeOutSecond: 30,
                };

                $.ajax({
                    url: Merchant.apiUrl + "/Command/SendSync",
                    type: "post",
                    contentType: "application/json",
                    processData: false,
                    dataType: "json",
                    data: JSON.stringify(dataCommand),
                    // async:false
                    success: function (res) {
                        if (res.Success) {
                            $("#modalNotify").modal("show");
                            $("#modalMessage").html("Chúc mừng bạn đã đăng ký thành công, vui lòng kiểm tra tin nhắn và email để nhận mã code");
                        } else {
                            $("#modalNotify").modal("show");
                            $("#modalMessage").html(res.Message);
                        }
                    },
                    error: function (err) {
                        alert(err.responseText);
                    },
                });
            } else {
                alert("Bạn cần đăng nhập!");
            }
        });

        $(".subscribeCategory").click(function () {
            if (localStorage.getItem(Configuration.token)) {
                var authorId = $(this).data("id");
                var data = {
                    Id: localStorage.getItem(Configuration.userId),
                    CategoryId: authorId,
                };
                var dataCommand = {
                    CommandName: "SubcribeCategoryArticleByUser",
                    Domain: "User",
                    Content: JSON.stringify(data),
                    ModifiedDate: Merchant.getCurrentDateTime(),
                    ModifiedBy: localStorage.getItem(Configuration.userId),
                    timeOutSecond: 30,
                };

                $.ajax({
                    url: Merchant.apiUrl + "/Command/SendSync",
                    type: "post",
                    contentType: "application/json",
                    processData: false,
                    dataType: "json",
                    data: JSON.stringify(dataCommand),
                    // async:false
                    success: function (res) {
                        if (res.Success) {
                            $("#modalNotify").modal("show");
                            $("#modalMessage").html("Chúc mừng bạn đã đăng ký thành công, vui lòng kiểm tra tin nhắn và email để nhận mã code");
                        } else {
                            $("#modalNotify").modal("show");
                            $("#modalMessage").html(res.Message);
                        }
                    },
                    error: function (err) {
                        alert(err.responseText);
                    },
                });
            } else {
                alert("Bạn cần đăng nhập!");
            }
        });
    },

    initThirdParty: function () {
        if (Merchant.data != undefined && Merchant.data != null) {
            if (Merchant.data.messenger != undefined && Merchant.data.messenger.trim() != "") {
                var messenger = JSON.parse(Merchant.data.messenger);
                var buildConfig = "";

                if (messenger.PageId != undefined && messenger.PageId != "") {
                    buildConfig += ' page_id="' + messenger.PageId + '"';
                }

                if (messenger.ThemeColor != undefined && messenger.ThemeColor != "") {
                    buildConfig += ' theme_color="' + messenger.ThemeColor + '"';
                }

                if (messenger.LoggedInGreeting != undefined && messenger.LoggedInGreeting != "") {
                    buildConfig += ' logged_in_greeting="' + messenger.LoggedInGreeting + '"';
                }
                if (messenger.LoggedOutGreeting != undefined && messenger.LoggedOutGreeting != "") {
                    buildConfig += ' logged_out_greeting="' + messenger.LoggedOutGreeting + '"';
                }

                var tempMessenger = '<div class="fb-customerchat" attribution=setup_tool ' + buildConfig + " ></div>";
                $("#fb-root").after(tempMessenger);
            }
            var tempAppId = "";
            if (Merchant.data.facebookApp != undefined && Merchant.data.facebookApp.trim() != "") {
                var temp = JSON.parse(Merchant.data.facebookApp);
                if (temp.AppId != undefined) {
                    tempAppId = temp.AppId;

                    //<meta property="fb:app_id" content="{YOUR_APP_ID}" />
                    var link = document.createElement("meta");
                    link.setAttribute("property", "fb:app_id");
                    link.content = tempAppId;
                    document.getElementsByTagName("head")[0].appendChild(link);
                }
            }
            window.fbAsyncInit = function () {
                FB.init({
                    appId: tempAppId,
                    cookie: true,
                    xfbml: true,
                    version: "v6.0",
                });
            };

            (function (d, s, id) {
                var js,
                    fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
                fjs.parentNode.insertBefore(js, fjs);
            })(document, "script", "facebook-jssdk");

            (function (d, s, id) {
                // Load the SDK asynchronously
                var js,
                    fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            })(document, "script", "facebook-jssdk");
            if (Merchant.data.googleAnalytics != undefined && Merchant.data.googleAnalytics.trim() != "") {
                var googleAnalytics = JSON.parse(Merchant.data.googleAnalytics);
                if (googleAnalytics.Key != undefined) {
                    var urlGA = "https://www.googletagmanager.com/gtag/js?id=" + googleAnalytics.Key;
                    var scr = document.createElement("script");
                    scr.src = urlGA;
                    document.body.appendChild(scr);

                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag("js", new Date());
                    gtag("config", googleAnalytics.Key);
                }
            }

            if (Merchant.data.googleManager != undefined && Merchant.data.googleManager.trim() != "") {
                var googleManager = JSON.parse(Merchant.data.googleManager);
                if (googleManager.Key != undefined && googleManager.Key.trim() != "") {
                    (function (w, d, s, l, i) {
                        w[l] = w[l] || [];
                        w[l].push({
                            "gtm.start": new Date().getTime(),
                            event: "gtm.js",
                        });
                        var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s),
                            dl = l != "dataLayer" ? "&l=" + l : "";
                        j.async = true;
                        j.src = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;
                        f.parentNode.insertBefore(j, f);
                    })(window, document, "script", "dataLayer", googleManager.Key);

                    var iframe = document.createElement("iframe");
                    iframe.setAttribute("src", "https://www.googletagmanager.com/ns.html?id=" + googleManager.Key);
                    iframe.setAttribute("style", "display:none;visibility:hidden");
                    document.getElementsByTagName("noscript")[0].appendChild(iframe);
                    // <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-53P26TW"
                    //     height="0" width="0" style="display:none;visibility:hidden"></iframe>
                }
            }

            //============meta tag=====
            if (Merchant.data.metaTag != undefined && Merchant.data.metaTag.trim() != "") {
                var metaTag = JSON.parse(Merchant.data.metaTag);

                if (metaTag.Title != undefined && metaTag.Title != "") {
                    document.title = metaTag.Title;
                }

                if (metaTag.Description != undefined && metaTag.Description != "") {
                    document.description = metaTag.Description;
                }

                if (metaTag.Keyword != undefined && metaTag.Keyword != "") {
                    document.keywords = metaTag.Keyword;
                }

                if (metaTag.FaviconUrl != undefined && metaTag.FaviconUrl != "") {
                    var link = document.querySelector("link[rel*='icon']") || document.createElement("link");
                    link.type = "image/x-icon";
                    link.rel = "shortcut icon";
                    link.href = metaTag.FaviconUrl;
                    document.getElementsByTagName("head")[0].appendChild(link);
                }
            }

            (function (window, factory) {
                var globalInstall = function () {
                    factory(window.lazySizes);
                    window.removeEventListener("lazyunveilread", globalInstall, true);
                };

                factory = factory.bind(null, window, window.document);

                if (typeof module == "object" && module.exports) {
                    factory(require("lazysizes"));
                } else if (window.lazySizes) {
                    globalInstall();
                } else {
                    window.addEventListener("lazyunveilread", globalInstall, true);
                }
            })(window, function (window, document, lazySizes) {
                /*jshint eqnull:true */
                "use strict";
                var bgLoad, regBgUrlEscape;
                var uniqueUrls = {};

                if (document.addEventListener) {
                    regBgUrlEscape = /\(|\)|\s|'/;

                    bgLoad = function (url, cb) {
                        var img = document.createElement("img");
                        img.onload = function () {
                            img.onload = null;
                            img.onerror = null;
                            img = null;
                            cb();
                        };
                        img.onerror = img.onload;

                        img.src = url;

                        if (img && img.complete && img.onload) {
                            img.onload();
                        }
                    };

                    addEventListener(
                        "lazybeforeunveil",
                        function (e) {
                            if (e.detail.instance != lazySizes) {
                                return;
                            }

                            var tmp, load, bg, poster;
                            if (!e.defaultPrevented) {
                                var target = e.target;

                                if (target.preload == "none") {
                                    target.preload = target.getAttribute("data-preload") || "auto";
                                }

                                if (target.getAttribute("data-autoplay") != null) {
                                    if (target.getAttribute("data-expand") && !target.autoplay) {
                                        try {
                                            target.play();
                                        } catch (er) {}
                                    } else {
                                        requestAnimationFrame(function () {
                                            target.setAttribute("data-expand", "-10");
                                            lazySizes.aC(target, lazySizes.cfg.lazyClass);
                                        });
                                    }
                                }

                                tmp = target.getAttribute("data-link");
                                if (tmp) {
                                    addStyleScript(tmp, true);
                                }

                                // handle data-script
                                tmp = target.getAttribute("data-script");
                                if (tmp) {
                                    addStyleScript(tmp);
                                }

                                // handle data-require
                                tmp = target.getAttribute("data-require");
                                if (tmp) {
                                    if (lazySizes.cfg.requireJs) {
                                        lazySizes.cfg.requireJs([tmp]);
                                    } else {
                                        addStyleScript(tmp);
                                    }
                                }

                                // handle data-bg
                                bg = target.getAttribute("data-bg");
                                if (bg) {
                                    e.detail.firesLoad = true;
                                    load = function () {
                                        target.style.backgroundImage = "url(" + (regBgUrlEscape.test(bg) ? JSON.stringify(bg) : bg) + ")";
                                        e.detail.firesLoad = false;
                                        lazySizes.fire(target, "_lazyloaded", {}, true, true);
                                    };

                                    bgLoad(bg, load);
                                }

                                // handle data-poster
                                poster = target.getAttribute("data-poster");
                                if (poster) {
                                    e.detail.firesLoad = true;
                                    load = function () {
                                        target.poster = poster;
                                        e.detail.firesLoad = false;
                                        lazySizes.fire(target, "_lazyloaded", {}, true, true);
                                    };

                                    bgLoad(poster, load);
                                }
                            }
                        },
                        false
                    );
                }

                function addStyleScript(src, style) {
                    if (uniqueUrls[src]) {
                        return;
                    }
                    var elem = document.createElement(style ? "link" : "script");
                    var insertElem = document.getElementsByTagName("script")[0];

                    if (style) {
                        elem.rel = "stylesheet";
                        elem.href = src;
                    } else {
                        elem.src = src;
                    }
                    uniqueUrls[src] = true;
                    uniqueUrls[elem.src || elem.href] = true;
                    insertElem.parentNode.insertBefore(elem, insertElem);
                }
            });
        }
    },
    guid: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
    },
    getCurrentDateTime: function () {
        var d = new Date(),
            month = "" + (d.getMonth() + 1),
            day = "" + d.getDate(),
            year = d.getFullYear();
        var hour = d.getHours();
        var min = d.getMinutes();
        var sec = d.getSeconds();

        if (month.length < 2) month = "0" + month;
        if (day.length < 2) day = "0" + day;
        return [year, month, day].join("-") + " " + hour + ":" + min + ":" + sec;
    },
    initEvent: function(){
        
        if($(".loadMore").length){
            $(".loadMore").click(function(){
                
                //var page = Merchant.getParameterByName("page");
                if(location.hash == null || location.hash == ''){
                    window.location.hash = "2";
                    //Merchant.updateQueryStringParameter("page", 2);
                }else{
                    //Merchant.updateQueryStringParameter("page", page++);
                    window.location.hash =  (parseInt(location.hash.replace("#","")) + 1) + "";
                }
                
            });
          
        }
        var nameSession = "attributeValueIds";
            var temp = sessionStorage.getItem(nameSession);
       $(".laboFilter").click(function(){
            if($(this).prop("checked") == true){
                if(temp != undefined){
                    sessionStorage.setItem(nameSession,temp + " " + $(this).attr("value"));
                }else{
                    sessionStorage.setItem(nameSession,$(this).attr("value"));
                }
            }
            else if($(this).prop("checked") == false){
                sessionStorage.setItem(nameSession,temp.replace($(this).attr("value"),""));
                
            }

            
            location.reload();
       });

       $(".laboSort").change(function(){
        sessionStorage.setItem("laboSort",$(".laboSort").attr("name") +":"+ $(".laboSort option:selected").val());                
        location.reload();       
       });

       if(temp != undefined){
            $(".laboFilter").each(function(i, val){
               if(temp.includes($(this).attr("value"))) {
                $(this).attr("checked","checked");
               }
            });         
        }
       
    },
    init: function () {
        //console.log(Merchant.data);
        this.authentication();
        this.initCheckLogin();
        this.initSubscribe();
        this.initThirdParty();

        
       if(sessionStorage.getItem("laboSort") != undefined){
            $(".laboSort").val(sessionStorage.getItem("laboSort").split(":")[1]);
        }

        if($(".laboSort").length){

           sessionStorage.setItem("laboSort",$(".laboSort").attr("name") +":"+ $(".laboSort option:selected" ).val());                
        }
        

        $(".btnCategory").click(function(){
            localStorage.setItem("productType", $(this).attr("param"));     
            $(this).addClass("active");     
            location.reload();       
        });
        
        $(".searchResults").html(Merchant.totalItemsCount);
        //languageCode
        
        $(".flag-avatar").attr("src", currentImageLanguage);

        $("a[data-language]").click(function () {
            let chooseLanguageCode = $(this).attr("data-languageCode");
            Merchant.setCookie(Configuration.userLanguage, chooseLanguageCode, 365);
            window.location.href = window.location.pathname.toLowerCase().replace(languageCode.toLowerCase(), chooseLanguageCode.toLowerCase());
        });

        if ($(".customForm").length) {
            $.each($(".customForm"), function (i, val) {

                // $.each($(this).find('input[type="tel"]'), function(i,val){
                //     $(this).attr('pattern',"[0-9]{10}");
                // });
                // $(this).validate({
                //     rules: {                       
                //         mobile: {
                //             required: true,
                //             digits: true,
                //             minlength: 0,
                //             maxlength: 10,
                //         },
                //         email: {
                //             required: true,
                //             email: true,
                //         }                   
                //     },
                //     messages: {
                      
                //         mobile: {
                //             required: "Xin mời nhập số điện thoại",
                //             digits: "Mời nhập số",
                //             minlength: "Mời nhập 10 số",
                //             maxlength: "Mời nhập 10 só",
                //         },
                //         email: {
                //             required: "Mời nhập email",
                //             email: "Mời nhập đúng định dạng email",
                //         },

                    
                //     },
                // });

                var selfForm = this;
                if ($(this).find("button").length) {
                    $(this)
                        .find("button")
                        .eq(0)
                        .click(function (e) {
                            if (!$(selfForm).valid()) {
                                
                                //alert("hello");
                                return;
                            }
                            e.preventDefault();

                            var $button = $(this);
                            let btnLabel = $button.html();
                            $button.prop("disabled", true);
                            $button.html(`<div class="labo-spinner-border labo-spinner-border-sm mr-1" role="status"></div> Đang xử lý...`);

                            // var $button = $(this);
                            let tempId = Merchant.guid();
                            let tempData = $(selfForm).serializeArray();
                            var dataObj = {};
                            $(tempData).each(function (index, obj) {
                                //console.log(obj);
                                var name = obj.name.replace("[]", "");
                                if (dataObj[name] == undefined) {
                                    dataObj[name] = obj.value;
                                } else {
                                    dataObj[name] += ", " + obj.value;
                                }
                            });
                            //console.log(dataObj);
                            //==============================================
                            let data = {
                                Id: $(selfForm).attr("id") || "",
                                Data: JSON.stringify(dataObj),
                                ModifiedBy: tempId,
                                ModifiedDate: Merchant.getCurrentDateTime(),
                            };
                            //console.log(data);
                            //return;
                            var dataCommand = {
                                CommandName: "SubmitForm",
                                Domain: "CustomerRelationshipManagement",
                                Content: JSON.stringify(data),
                                ModifiedDate: Merchant.getCurrentDateTime(),
                                ModifiedBy: tempId,
                                //ReCaptcha: reCaptcha,
                                timeOutSecond: 30,
                            };

                            var $returnMessage = $(selfForm).parent().find(".returnMessage");

                            $.ajax({
                                //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
                                url: Merchant.apiUrl + "/Command/SendSync",
                                type: "post",
                                contentType: "application/json",
                                processData: false,
                                dataType: "json",
                                data: JSON.stringify(dataCommand),
                                // async:false
                                success: function (res) {
                                    if (res.Success) {
                                        if (res.Message != "") {
                                            let resultDisplay = JSON.parse(res.Message);
                                            switch (resultDisplay.TypeOnSubmit) {
                                                case Configuration.typeOnSubmit.showThankYou:
                                                    if ($returnMessage.length) {
                                                        $returnMessage.html(resultDisplay.ValueOnSubmit);
                                                        setTimeout(function () {
                                                            $returnMessage.html("");
                                                        }, 5000);
                                                    } else {
                                                        alert(resultDisplay.ValueOnSubmit);
                                                    }
                                                    break;
                                                case Configuration.typeOnSubmit.openUrl:
                                                    window.open(resultDisplay.ValueOnSubmit);
                                                    break;
                                                default:
                                                    break;
                                            }

                                            setTimeout(function () {
                                                if ($(".mfp-close").length) {
                                                    $(".mfp-close").click();
                                                }
                                                if ($(".modal.show .close").length) {
                                                    $(".modal.show .close").click();
                                                }
                                            }, 5000);
                                        }
                                        $(selfForm).find("input[type=text],input[type=tel],textarea,input[type=number],input[type=email]").val("");
                                        $button.prop("disabled", false);
                                        $button.html(btnLabel);
                                    } else {
                                        if (res.Message != "") {
                                            if ($returnMessage.length) {
                                                $returnMessage.html(res.Message);
                                                setTimeout(function () {
                                                    $returnMessage.html("");
                                                }, 5000);
                                            } else {
                                                alert(res.Message);
                                            }
                                        } else {
                                            alert("thất bại");
                                        }
                                        $button.prop("disabled", false);
                                        $button.html(btnLabel);
                                    }
                                },
                                error: function (err) {
                                    alert(err.responseText);
                                    $button.prop("disabled", false);
                                    $button.html(btnLabel);
                                },
                                complete: function () {
                                    $button.prop("disabled", false);
                                    $button.html(btnLabel);
                                },
                            });
                        });
                }
            });
        }

        if ($(".formBooking").length) {
            // $('.bookingDate').datepicker({
            //     format: 'mm/dd/yyyy',
            //     startDate: '0d'
            // });Merchant.getCurrentDate()
            $(".formBooking").find(".bookingDate").attr("min", Merchant.getCurrentDateTime());
            $(".bookingDate").change(function () {
                var temp = Merchant.getDataGraphQl(
                    `{
                    availablebookings(param:
                    {   
                        from:"` +
                        $(this).val() +
                        `",               
                         merchantId: "` +
                        Merchant.getCookie("merchantId") +
                        `"})
                    {
            totalCount,
                items
            {
        
                id,
              fromTime,
              toTime,
              available           
                    }
        }
                }            
            
            `
                );
                var tempOption = "";
                $.each(temp.data.availablebookings.items, function (i, val) {
                    if (val.available) {
                        tempOption += '<option value="' + val.id + '">' + Merchant.formatDateTime(val.fromTime, "hh-mm") + "</option>";
                    } else {
                        tempOption += '<option value="' + val.id + '" disabled>' + Merchant.formatDateTime(val.fromTime, "hh-mm") + " <b>hết chỗ</b></option>";
                    }
                });
                $(".formBooking").find(".bookingId").html(tempOption);
            });

            $.each($(".formBooking"), function (i, val) {
                $(this)
                    .find("button")
                    .click(function (i, val) {
                        var $button = $(this);
                        var $form = $(this).parents("form:first");
                        $form.validate({
                            rules: {
                                name: {
                                    required: true,
                                    maxlength: 160,
                                },
                                mobile: {
                                    required: true,
                                    digits: true,
                                    minlength: 0,
                                    maxlength: 10,
                                },
                                email: {
                                    required: true,
                                    email: true,
                                },
                                note: {
                                    required: true,
                                },
                                bookingDate: {
                                    required: true,
                                },
                                bookingId: {
                                    required: true,
                                },
                            },
                            messages: {
                                name: {
                                    required: "Xin mời nhập tên",
                                    maxlength: "Hãy nhập tối đa 160 ký tự",
                                },
                                mobile: {
                                    required: "Xin mời nhập số điện thoại",
                                    digits: "Mời nhập số",
                                    minlength: "Mời nhập 10 số",
                                    maxlength: "Mời nhập 10 só",
                                },
                                email: {
                                    required: "Mời nhập email",
                                    email: "Mời nhập đúng định dạng email",
                                },

                                note: {
                                    required: "Mời nhập thông tin thêm.",
                                },
                            },
                        });
                        if (!$form.valid()) {
                            return;
                        }
                        let btnLabel = $button.html();
                        $button.attr("disabled", "disabled");
                        $button.html(`<div class="labo-spinner-border labo-spinner-border-sm mr-1" role="status"></div> Đang xử lý...`);
                        var id = Merchant.guid();
                        var tempRelation = [];
                        var data = {
                            Id: id,
                            MerchantId: Merchant.getCookie("merchantId"),
                            Name: $form.find('input[name="name"]').val(),
                            Email: $form.find('input[name="email"]').val(),
                            Mobile: $form.find('input[name="mobile"]').val(),
                            Message: $form.find('textarea[name="note"]').val(),
                            ModifiedDate: Merchant.getCurrentDateTime(),
                            BookingDate: $form.find('input[name="bookingDate"]').val(),
                            BookingId: $form.find('select[name="bookingId"]').val(),
                            ModifiedBy: id,
                        };

                        var dataCommand = {
                            CommandName: "CreateBookingTransaction",
                            Domain: "Booking",
                            Content: JSON.stringify(data),
                            ModifiedDate: Merchant.getCurrentDateTime(),
                            ModifiedBy: id,
                            //ReCaptcha: reCaptcha,
                            timeOutSecond: 30,
                        };

                        var $returnMessage = $form.find(".returnMessage");

                        $.ajax({
                            //url: Merchant.apiUrl + "/Command/SendSyncReCaptcha",
                            url: Merchant.apiUrl + "/Command/SendSync",
                            type: "post",
                            contentType: "application/json",
                            processData: false,
                            dataType: "json",
                            data: JSON.stringify(dataCommand),
                            // async:false
                            success: function (res) {
                                $button.removeAttr("disabled");
                                $button.html(btnLabel);
                                if (res.Success) {
                                    $form.find("input[type=text], input[type=email], input[type=phone], textarea").val("");

                                    if ($returnMessage.length) {
                                        $returnMessage.html("Đăng kí thành công");
                                        setTimeout(function () {
                                            $returnMessage.html("");
                                            $(".modal").modal("hide");
                                        }, 5000);
                                    } else {
                                        alert("Đăng kí thành công");
                                    }
                                } else {
                                    if ($returnMessage.length) {
                                        $returnMessage.html(res.Message);
                                        setTimeout(function () {
                                            $returnMessage.html("");
                                        }, 5000);
                                    } else {
                                        alert(res.Message);
                                    }
                                }
                            },
                            error: function (err) {
                                alert(err.responseText);
                            },
                        });
                    });
            });
        }
    },
};
