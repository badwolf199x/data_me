/*php html*/
 <script type="text/javascript">jQuery(function($){
                        function ajax_search_autocomplete(n){
                    var text=n;
                $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "search_autocomplete_text", 
                        text: text,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                        {
                        $('.search_autocomplete').html(response.data);
                        $('.autocomplete_text').click(function(){
                            $('input#search_keywords').val($(this).text());
                            $('.search_autocomplete').css('display','none');
                        });
                        }
                        else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
            }
              $("#search_keywords").on("change paste keyup", function() { $('.search_autocomplete').css('display','block'); ajax_search_autocomplete($(this).val()); });

}) 
                    </script>

/*ajax*/
if ( ! function_exists( 'search_autocomplete_text' ) ) {
function search_autocomplete_text() {
    ob_start();
    $text =  (isset($_POST['text']))?esc_attr($_POST['text']) : '';
    $autocomplete=array();
    foreach (get_object_taxonomies('job_listing') as $value => $taxonomy_text )
    { 
        $terms=get_terms(['taxonomy' => $taxonomy_text,'hide_empty' => false,]); foreach ( $terms as $terms_name) : $autocomplete[]=$terms_name->name;  endforeach;
    }
    foreach (get_posts(array('post_type' => 'job_listing','post_status'=>'publish','nopaging' => true,)) as $value => $post)
    {
        $autocomplete[] = strip_tags_content(get_the_title($post->ID));
    }
    
    if($text!="") :
    $args = array(
    'post_type' => 'job_listing',
    'post_status'=>'publish',
    'nopaging' => true,
    'order' => 'ASC',  
    'orderby' => 'meta_value',
);
$location_select=array();
$post_query = new WP_Query( $args );
if($post_query->have_posts()):
                            while ( $post_query->have_posts() ) { ?>
                        <?php
                        $post_query->the_post();

                            $post_id=get_the_ID();

                                foreach (get_post_meta( $post_id ) as $value => $meta_name) {

                                     $autocomplete[] = strip_tags_content($meta_name[0]);
                                 }                      
                            }
endif; wp_reset_query();
$autocomplete = array_unique($autocomplete);
asort($autocomplete);
                        $text_grep='/^I.*/i';
                        $text=str_replace("I",$text,$text_grep);
                        foreach (preg_grep($text, $autocomplete ) as $value => $autocomplete_text) {
                            if(strlen($autocomplete_text)<35){
                                  echo '<div class="autocomplete_text">'.$autocomplete_text.'</div>';}
                         }  
    endif;
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_search_autocomplete_text', 'search_autocomplete_text' );
add_action( 'wp_ajax_nopriv_search_autocomplete_text', 'search_autocomplete_text' );
}?>