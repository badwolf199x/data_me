/*mega menu*/
function echo_ul_1($a)
{
	$args_con = array(
    'parent'    => $a,
	'hide_empty'=> 0,
	'taxonomy'  => 'product_cat',
	'orderby' => 'menu_order', 
	'order' => 'ASC',
	'exclude' => 15,
);
	$categories = get_categories( $args_con );										  
	if(!empty($categories))
	   { ?>
	<?php foreach ( $categories as $category ) 
{
		$categories_child = get_categories(
array('parent' => $category->cat_ID,'hide_empty' => 0,'orderby' => 'menu_order', 'order' => 'ASC','taxonomy'  => 'product_cat',)); 
		
if(!empty($categories_child) && strpos($category->name,'-')>0)
    { ?>
	     <li class="line-1">
	<?php	foreach ( $categories_child as $categories_child_id )
		{ ?>
		    <div class="wrap-item">
			<div class="icon-cat" style="-webkit-mask-image:url('<?= get_field('icon_cho_danh_muc','term_'.$categories_child_id->cat_ID.'')?>');mask-image: url('<?= get_field('icon_cho_danh_muc','term_'.$categories_child_id->cat_ID.'')?>')"></div>
			<div class="label label-inline"><a href="<?= get_term_link($categories_child_id->slug, 'product_cat')?>"><?= $categories_child_id->name?></a></div>
			</div>
	<?php 
		} 
	$categories_sub = get_categories(
    array('parent' => $categories_child_id->cat_ID,'hide_empty' => 0,'orderby' => 'menu_order', 'order' => 'ASC','taxonomy'  => 'product_cat',)); 
    if(!empty($categories_sub)) { ?>
			 <i class="fas fa-angle-right"></i>
			 <div class='sub-ul'>
               <?php foreach ( $categories_child as $categories_child_id )
		{ ?><div class='sub-ul-li'>
			<div class="sub-label"><a href="<?= get_term_link($categories_child_id->slug, 'product_cat')?>"><?= $categories_child_id->name?></a></div>
			<?php echo_ul_2($categories_child_id->cat_ID); ?>
			</div>
  <?php } ?>
			 </div>
			               <?php } ?>
	     </li>
<?php } 
else {  ?>
		<li class="line-1">
		<div class="wrap-item">
	    <div class="icon-cat" style="-webkit-mask-image:url('<?= get_field('icon_cho_danh_muc','term_'.$category->cat_ID.'')?>');mask-image: url('<?= get_field('icon_cho_danh_muc','term_'.$category->cat_ID.'')?>')"></div>
	    <div class="label"><a href="<?= get_term_link($category->slug, 'product_cat')?>"><?= $category->name?></a></div>
		</div>
<?php   
	$categories_sub = get_categories(
    array('parent' => $category->cat_ID,'hide_empty' => 0,'orderby' => 'menu_order', 'order' => 'ASC','taxonomy'  => 'product_cat',)); 
    if(!empty($categories_sub)){?>
		<i class="fas fa-angle-right"></i>
		
		<div class='sub-ul'> 
		<div class='sub-ul-li'>
			<div class="sub-label"><a href="<?= get_term_link($category->slug, 'product_cat')?>"><?= $category->name?></a></div><?php
		echo_ul_2($category->cat_ID); ?>
		</div>
		</div>
<?php	}
      } 
} ?>	
<?php  }
}
function echo_ul_2($a)
{
	$args_con = array(
    'parent'    => $a,
	'hide_empty'=> 0,
	'taxonomy'  => 'product_cat'
);
	$categories = get_categories( $args_con );										  
	if(!empty($categories))
	 { ?>
		<div class="sub-content">
		<?php foreach ( $categories as $category ) {  ?>
			
				<div class="line"><i class="fas fa-caret-right"></i> <a href="<?= get_term_link($category->slug, 'product_cat')?>"><?= $category->name?></a></div>
				                        <?php } ?>
		</div>
<?php }
}
function mega_menu() {?>
<nav class="mega-menu top">
	<h3 class="mega-menu-title"><i aria-hidden="true" class="fas fa-bars"></i>Danh mục sản phẩm</h3>
		<ul class="mcd-menu">
<?php

		echo_ul_1(null); ?>		
			
		</ul>
	</nav>
<?php
}
function mega_menu_left() {?>
<nav class="mega-menu left">
		<ul class="mcd-menu">
<?php
		echo_ul_1(null); ?>		
			
		</ul>
	</nav>
<?php
}
add_shortcode( 'mega_menu_left', 'mega_menu_left' );
add_shortcode( 'megamenu', 'mega_menu' );