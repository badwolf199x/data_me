﻿add_filter('woocommerce_empty_price_html', 'custom_for_price');

function custom_for_price() {
    return "<a href='http://localhost/pt_pet_shop/lien-he/'; style='display: block; font-family: roboto ;font-size:14px;height:28; font-weight: 500; color:#ED880E; padding: 0px 0px 0px 0px;'>Liên hệ mua hàng</a>";
}



function ecx_wc_custom_get_price_html( $price, $product ) {
    if ( $product->get_price() == 0 ) {
        if ( $product->is_on_sale() && $product->get_regular_price() ) {
            $regular_price = wc_get_price_to_display( $product, array( 'qty' => 1, 'price' => $product->get_regular_price() ) );
 
            $price = wc_format_price_range( $regular_price, __( 'Free!', 'woocommerce' ) );
        } else {
            $price = '<span class="amount contact">' . __( 'Liên hệ', 'woocommerce' ) . '</span>';
        }
    }
    return $price;
}
add_filter( 'woocommerce_get_price_html', 'ecx_wc_custom_get_price_html', 10, 2 );