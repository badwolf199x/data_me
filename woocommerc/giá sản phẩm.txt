giá sp custom strip_tags($regular_price)); strip_tags($sale_price))

$getposts->the_post(); $regular=get_post_meta( get_the_ID(), '_regular_price', true )
number_format( $regular , $decimals = 0 , $dec_point = "," , $thousands_sep = "." )


$getposts->the_post(); $sale=get_post_meta( get_the_ID(), '_sale_price', true );
number_format( $sale , $decimals = 0 , $dec_point = "," , $thousands_sep = "." )


// All prices: multidimensional array with all variation prices (including active prices, regular prices and sale prices). 
$prices = $product->get_variation_prices();
$prices_for_display = $product->get_variation_prices( true ); // For display

// The min or max active price.
$min_price = $product->get_variation_price(); // Min active price
$min_price_for_display = $product->get_variation_price('min', true); // Min active price for display
$max_price = $product->get_variation_price('max'); // Max active price
$max_price_for_display = $product->get_variation_price('max', true); // Max active price for display

// The min or max regular price.
$min_price = $product->get_variation_regular_price(); // Min regular price
$min_price_for_display = $product->get_variation_regular_price('min', true); // Min regular price for display
$max_price = $product->get_variation_regular_price('max'); // Max regular price
$max_price_for_display = $product->get_variation_regular_price('max', true); // Max regular price for display

// The min or max sale price.
$min_price = $product->get_variation_sale_price(); // Min sale price
$min_price_for_display = $product->get_variation_sale_price('min', true); // Min sale price for display
$max_price = $product->get_variation_sale_price('max'); // Max sale price
$max_price_for_display = $product->get_variation_sale_price('max', true); // Max sale price for display