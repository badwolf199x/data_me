﻿
add_filter( 'gettext', function ( $strings ) {
$text = array(
'SHOPPING CART' => 'Giỏ hàng',
'CHECKOUT DETAILS' => 'Thanh toán',
'ORDER COMPLETE' => 'Hoàn tất',
	'Mua hàng' => 'Thêm vào giỏ hàng',
);
$strings = str_ireplace( array_keys( $text ), $text, $strings );
return $strings;
}, 20 );
