wc-processing -- đang xử lý -- Mới , Chờ xác nhận, Đã xác nhận, Đang xác nhận, Đổi kho hàng, Đang đóng gói, Đã đóng gói , Chờ thu gom, Đang chuyển, Đang hoàn
wc-completed  -- đã hoàn thành -- Thành công
wc-refunded   -- đã hoàn lại -- Đã hoàn
wc-failed     -- thất bại  -- Hết hàng, Thất bại
wc-cancelled  -- đã hủy    -- Khách hủy, Hệ thống hủy , HVC hủy
wc-on-hold    -- tạm giữ
wc-pending    -- chờ thanh toán