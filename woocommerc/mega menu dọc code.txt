﻿/*mega menu*/
add_shortcode( 'megamenu', 'mega_menu' );
function mega_menu() {?>
  <div class="container">
<nav>
		<ul class="mcd-menu">
			<li>
				<a href="<?=esc_url(get_home_url())?>" class="<?= get_permalink( get_queried_object_id() )==esc_url(get_home_url()) ? 'active' : ''?>">
					<i class="fa fa-home"></i>
					<strong>Trang chủ</strong>
				</a>
			</li>
			<li>
				<a href="<?= esc_url( get_page_link(1122))?>" class="<?= get_permalink( get_queried_object_id())==esc_url( get_page_link(1122)) ? 'active' : ''?>">
					<i class="fa fa-edit"></i>
					<strong>Giới thiệu</strong>
				</a>
			</li>
			<li>
				<a href="<?= esc_url( get_page_link(9))?>" class="<?= get_permalink( get_queried_object_id())==esc_url( get_page_link(9)) ? 'active' : ''?>">
					<i class="fa fa-gift"></i>
					<strong>Văn phòng phẩm</strong>
				</a>
<?php
$count_ul=0;

function echo_ul_1($a){
	$args_con = array(
    'parent'    => $a,
	'hide_empty'=> 0,
	'taxonomy'  => 'product_cat'
);
	$categories = get_categories( $args_con );										  
	if(count($categories)>0)
	{
		echo '<ul>';
		foreach ( $categories as $category ) { ?>
			<li><a href="<?= get_term_link($category->slug, 'product_cat')?>"><?= $category->name?></a>
	<?php echo_ul_2($category->cat_ID);	?>
	<?php }
		echo'</ul></li>';
	}
	else
	   {
		echo '</li>';
	};	
}
function echo_ul_2($a){
	$args_con = array(
    'parent'    => $a,
	'hide_empty'=> 0,
	'taxonomy'  => 'product_cat'
);
	$categories = get_categories( $args_con );										  
	if(count($categories)>0)
	{
		echo '<ul>';
		foreach ( $categories as $category ) { ?>
			<li><a href="<?= get_term_link($category->slug, 'product_cat')?>"><?= $category->name?></a>
	<?php echo_ul_1($category->cat_ID);	?>
	<?php }
		echo'</ul></li>';
	}
	else
	   {
		echo '</li>';
	};	
}

		echo_ul_1(null); ?>		
			</li>
			<li>
				<a href="<?= esc_url( get_category_link(117))?>" class="<?= get_term_link( get_queried_object_id())==esc_url( get_category_link(117)) ? 'active' : ''?>">
					<i class="fa fa-globe"></i>
					<strong>Tin tức mới</strong>
				</a>
			</li>
			<li>
				<a href="<?= esc_url( get_page_link(1124))?>" class="<?= get_permalink( get_queried_object_id())==esc_url( get_page_link(1124)) ? 'active' : ''?>">
					<i class="fa fa-envelope-o"></i>
					<strong>Liên hệ</strong>
				</a>
			</li>
		</ul>
	</nav>
	</div>;<?php
}