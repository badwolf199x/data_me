/*js*/
$(document).ready(function(){
var content_wrap=$('.product-pview ol.flex-control-nav.flex-control-thumbs').height();
var flex_width=$('.product-pview ol.flex-control-nav.flex-control-thumbs li:first-child').height()+20;
var li_all_element=$('.product-pview ol.flex-control-nav.flex-control-thumbs li');	
function change_img()
{
	for(var i=1;i<li_all_element.length+1;i++)
		{
         if($('ol.flex-control-nav.flex-control-thumbs li:nth-child('+i+') img').hasClass('flex-active'))
			 {
				 if(i<3){ $('.product-pview ol.flex-control-nav.flex-control-thumbs').scrollTop(0); }
				 else { $('.product-pview ol.flex-control-nav.flex-control-thumbs').scrollTop(((i-2)*flex_width)-10); }
			 }
		}
}
		$('ol.flex-control-nav.flex-control-thumbs li').click(function(){ change_img(); });
		$('ul.flex-direction-nav li').click(function(){ change_img(); });
})

/*css*/
.product-pview .flex-viewport {
    height: 798.08px;
    width: 70%;
}
.product-pview ol.flex-control-nav.flex-control-thumbs {
    overflow-y: scroll!important;
    display: flex;
    width: 30%;
    flex-wrap: wrap;
    height: 798.08px;
    margin: 0!important;
    padding-left: 40px!important;
}
.product-pview ol.flex-control-nav.flex-control-thumbs::-webkit-scrollbar {
  display:none;
}
.product-pview .woocommerce-product-gallery.woocommerce-product-gallery--with-images {
	justify-content:space-between;
    display: flex;
}
.woocommerce  div.product .product-pview div.images a.woocommerce-product-gallery__trigger {
    left: 59%;
}
.woocommerce .product-pview .flex-control-thumbs li {
    padding-left: 0!important;
    padding-right: 0!important;
    width: 100%!important;
    height: 33.333%;
}
.woocommerce .product-pview .flex-control-thumbs li img,.woocommerce div.product .product-pview div.images img
{
    width: 100%!important;
    height: 100%!important;
    object-fit: cover;
}
.product-pview ul.flex-direction-nav li {
    list-style: none;
    right: 15%;
    padding: 0;
    position: absolute;
    font-size: 0;
}
.product-pview ul.flex-direction-nav li a:after {
	border-radius: 50%;
    font-size: 22px;
    content: "";
    display: flex;
    width: 36px;
    height: 36px;
    color: #000;
    background: #dcdcdc;
    font-family: "Font Awesome 5 Free";
    font-weight: 900;
    justify-content: center;
    align-items: center;
}
.product-pview ul.flex-direction-nav li.flex-nav-next {
    bottom: 0px;
}
.product-pview ul.flex-direction-nav li.flex-nav-next a:after{
	  content:"\f078";
}
.product-pview ul.flex-direction-nav li.flex-nav-prev {
    top: -36px;
}
.product-pview ul.flex-direction-nav li.flex-nav-prev a:after{
	  content:"\f077";
}
.product-pview .flex-viewport > figure, .product-pview .flex-viewport > figure > div.flex-active-slide {
    height: 100%!important;
}

