/*** Sort and Filter Users ***/
/*post author company*/
function new_modify_company_table( $column ) {
    $column['posted_by'] = 'Posted by';
    return $column;
}
add_filter( 'manage_company_posts_columns', 'new_modify_company_table' );
//
function new_modify_company_table_short( $column ) {
    $column['posted_by'] = 'posted_by';
    return $column;
}
add_filter( 'manage_edit-company_sortable_columns', 'new_modify_company_table_short');
function mycpt_custom_orderby( $query ) {
  if ( 'posted_by' === $query->get( 'orderby') ) {
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', '_company_email' );
  }
}
add_action( 'pre_get_posts', 'mycpt_custom_orderby' );
//
function new_modify_company_table_row(  $column_name, $post_ID ) {
    switch ($column_name) {
        case 'posted_by' :
            $author_id = get_post_field ('post_author', $post_ID);
            $display_name = get_the_author_meta( 'display_name' , $author_id ); 
            echo '<a href="'.get_home_url().'/wp-admin/user-edit.php?user_id='.$author_id.'">'.$display_name.'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_company_posts_custom_column', 'new_modify_company_table_row', 10, 3 );