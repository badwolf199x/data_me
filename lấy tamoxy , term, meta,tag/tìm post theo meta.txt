$args = array(
    'post_type' => 'job_listing',
    'post_status'=>'publish',
    'meta_key' => array('_application','_company_name','_job_location','_company_tagline','_company_website','_company_twitter'),                  
    'meta_value' => 'Spa',                
    'meta_compare' => 'LIKE', 
    'nopaging' => true,
    'order' => 'ASC',  
    'orderby' => 'meta_value',
);
$location_select=array();
$post_query = new WP_Query( $args );
if($post_query->have_posts()):
                            while ( $post_query->have_posts() ) { ?>
                        <?php
                        $post_query->the_post();
                            $post_id=get_the_ID();
                                $location_select[] = get_post_meta( $post_id, '_job_location', true );                     
                            }
endif; wp_reset_query();