$clean_page_meta_values = get_post_meta( $id_page, '_jobhunt_page_metabox', true );
                    $page_meta_values = maybe_unserialize( $clean_page_meta_values );
                    $site_content_page_title = isset( $page_meta_values['page_title'] ) && ! empty( $page_meta_values['page_title'] ) ? $page_meta_values['page_title'] : get_the_title($id_page);