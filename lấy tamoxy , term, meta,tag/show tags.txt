/*show tags*/
.show_tags a
{
	padding: 0px 17px;
background: #E9E9E9;
font-family: SFU Futura;
font-weight: 400;
font-size: 17px;
line-height: 40px;
text-transform: capitalize;
color: #000000;
margin: 0px 20px 0 0;
}

if ( ! function_exists( 'show_tags' ) ) {
function show_tags($args,$content) {
    $tags = get_tags();
    if ( $tags ) :
        foreach ( $tags as $tag ) : ?>
            <a href="<?php echo esc_url( get_tag_link( $tag->term_id ) ); ?>" title="<?php echo esc_attr( $tag->name ); ?>"><?php echo esc_html( $tag->name ); ?></a>
        <?php endforeach; ?>
    <?php endif;
}
add_shortcode('show_tags', 'show_tags');
}