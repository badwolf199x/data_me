If you want to get slug of the post from the loop then use:

global $post;
echo $post->post_name;


If you want to get slug of the post outside the loop then use:

$post_id = 45; //specify post id here
$post = get_post($post_id); 
$slug = $post->post_name;


You can get that using the following methods:

<?php $post_slug = get_post_field( 'post_name', get_post() ); ?>


Or You can use this easy code:

<?php
    global $post;
    $post_slug = $post->post_name;
?>