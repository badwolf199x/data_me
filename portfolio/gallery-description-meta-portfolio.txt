/* Portfolio Shortcode*/
/*Portfolio description*/
add_shortcode ('portfolio_description', 'portfolio_description' );
function portfolio_description() {
if ( ! defined( 'ABSPATH' ) ) {
	die; // If this file is called directly, abort.
}
$options = Filterable_Portfolio_Helper::get_options();
?>
<h2><?php echo esc_attr( $options['project_description_text'] ); ?></h2>
			<?php echo get_the_content();  
}
/*Portfolio meta*/
add_shortcode ('portfolio_meta', 'portfolio_meta' );
function portfolio_meta() {
	$template = FILTERABLE_PORTFOLIO_TEMPLATES . '/portfolio-meta.php';			load_template( $template, false );
}
/*Portfolio gallery*/
add_shortcode ('portfolio_gallery', 'portfolio_gallery' );
function portfolio_gallery($args,$content) 
{
if(!isset($args['class'])){$class="gallery_p";} else {$class=$args['class'];}
$project_images = Filterable_Portfolio_Helper::get_portfolio_images_ids();
$image_size     = $option['project_image_size'];
?>
	<ul class="wp-block-gallery columns-3 is-cropped">
		<?php $i=0; foreach ( $project_images as $image_id ): ?>
			<li class="blocks-gallery-item">
				<figure>
					<a index="<?= $i ?>" href="<?php echo wp_get_attachment_image_url( $image_id, $image_size ); ?>">
						<?php echo wp_get_attachment_image( $image_id, $image_size ); ?>
					</a>
				</figure>
			</li>
		<?php $i++;endforeach; ?>
	</ul>
<script type="text/javascript">jQuery(function($){
		 $('.<?=$class?> .blocks-gallery-item a').click(function(){
		var index=$(this).attr('index');
   		var box = $(".swiper-wrapper .swiper-zoom-container");
	function box_check()
	{ 
		$('.swiper-container').remove();
        $('.dialog-message.dialog-lightbox-message.animated.zoomIn').append('<div class="swiper-container swiper-container-initialized swiper-container-horizontal"></div>');
		$('.dialog-message.dialog-lightbox-message.animated.zoomIn .swiper-container').append('<div class="swiper-wrapper"></div>');
		$('.swiper-wrapper').after('<div class="elementor-swiper-button elementor-swiper-button-next elementor-lightbox-prevent-close" aria-label="Next slide" tabindex="0" role="button"><i class="eicon-chevron-right"></i></div><div class="elementor-swiper-button elementor-swiper-button-prev elementor-lightbox-prevent-close" aria-label="Previous slide" tabindex="0" role="button"><i class="eicon-chevron-left"></i></div>');
		<?php foreach ( $project_images as $image_id ): ?>
		$('.swiper-wrapper').append('<div class="swiper-slide elementor-lightbox-item" ><div class="swiper-zoom-container"><img class="elementor-lightbox-image elementor-lightbox-prevent-close swiper-lazy swiper-lazy-loaded" src="<?= wp_get_attachment_image_url( $image_id, 'large' )?>"></div></div>');
		<?php  endforeach; ?>
  var swiper = new Swiper('.swiper-container', {
  slidesPerView: 1,
  spaceBetween: 0,
  direction:	'horizontal',
  initialSlide: index,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.elementor-swiper-button-next',
    prevEl: '.elementor-swiper-button-prev',
  },
  zoom: {
  containerClass: 'swiper-zoom-container',
  maxRatio:	1.5,
  minRatio:	1,	
  toggle: true,	
  zoomedSlideClass: 'swiper-slide-zoomed'
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
  });

	}
if(!box)
{
    setTimeout( box_check , 10 );
} 
else
{
	 setTimeout( box_check , 0 );
}	
    }); 
})
</script>
		<?php 
}
/* End of Portfolio Shortcode */