function send_admin_email($post_id, $post, $update) {
 if($post->post_type=='job_listing')
 {
   $count = count(get_posts(array('post_type' => 'job_listing','post_status' => 'pending','nopaging' => true)));
   $job_name=array();
   $jobs_company=array();
   foreach (get_posts(array('post_type' => 'job_listing','post_status' => 'pending','nopaging' => true)) as $value => $job) {
    $job_name[]=$job->post_title;
    $jobs_company[]= get_user_meta($job->post_author)['company_name'][0];
   }
   $job_out="";
   for($i=0;$i<count($job_name);$i++) {
    $job_out.='<br>'.$jobs_company[$i].' ----- '.$job_name[$i];
   }
  if($count>1)
  {
    $to = get_option('admin_email');
    $subject = 'Job approval';
    $message = $count.' jobs are in queue for review and approval.'.$job_out;
    wp_mail($to, $subject, $message );
  }
  else
  {
    $to = get_option('admin_email');
    $subject = 'Job approval';
    $message = '1 job are in queue for review and approval.'.$job_out;
    wp_mail($to, $subject, $message );
  }
}
}
add_action( 'wp_insert_post', 'send_admin_email', 10, 3 );
add_filter('wp_mail_content_type', function( $content_type ) {
            return 'text/html';
});