 if ( ! function_exists( 'tin_tuc_new_carousel' ) ) {
function tin_tuc_new_carousel($args,$content)
{
 /* if(!isset($args['cat_id'])){$cat_id=102;} else {$cat_id=$args['cat_id'];}?>
    <?php if(!isset($args['numb_show'])){$numb=12;} else {$numb=$args['numb_show'];}?>
    <?php if(!isset($args['numb_show_tablet'])){$numb_t=6;} else {$numb_t=$args['numb_show_tablet'];}?>
    <?php if(!isset($args['numb_show_mobile'])){$numb_m=3;} else {$numb_m=$args['numb_show_mobile'];}?>*/
    $args = array(
'post_type'=>'post',
'post_status'=>'publish',
'posts_per_page' => 9,
'orderby' => 'publish_date',
'order' => 'DESC');
$post_query = new WP_Query( $args );
$var_i=1;
?>
<div class="wrap_ajax_projects swiper-container-project swiper-container">
    <div class="slick-prev"><i class="fas fa-chevron-left"></i></div>
    <div class="ajax_projects swiper-wrapper">
        <?php   while ( $post_query->have_posts() ) { 

                        $post_query->the_post();

                            $post_id=get_the_ID();

                                 if($var_i==1)
                                 {
                                    echo '<div class="wrap-grid swiper-slide">';
                                 }
                                 elseif($var_i%3==1)
                                {
                                    echo '</div><div class="wrap-grid swiper-slide">';
                                }
                            ?>
                        <div class="wrap-post swiper-slide-item">
                                    <div class="elementor-post__thumbnail"><img class="img-post" src="<?=get_the_post_thumbnail_url( esc_attr( $post_id ), 'large' ) ?>"/>
                                    </div>
                            <div class="elementor-post__text">
                            <h3 class="elementor-post__title"><?=get_the_title( esc_attr( $post_id ) ) ?>
                            </h3>
                            </div>
                        </div> <?php      $var_i++;    }
                                    echo '</div>';
                     ?>               
    </div>
    <div class="slick-next"><i class="fas fa-chevron-right"></i></div>
</div>
<div class="swiper-pagination"></div>
<script type="text/javascript">
    jQuery(function($){
  var swiper = new Swiper('.swiper-container', {
 slidesPerView: 1,
  spaceBetween: 0,
  direction:    'horizontal',
  initialSlide: 0,
  PreventInteractionOnTransition: true,
  navigation: {
    nextEl: '.slick-next',
    prevEl: '.slick-prev',
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
  },
  keyboard: {
    enabled: true,
    onlyInViewport: false,
  }
   });
    })
    </script>
<?php 
}
add_shortcode( 'tin_tuc_new_carousel', 'tin_tuc_new_carousel' );
}