if ( ! function_exists( 'media_gallery_page' ) ) {
function media_gallery_page($args,$content)
{
if($_GET["page_num"]>0){ $page=$_GET["page_num"]; }else{ $page=1; }
$post_content= get_post(1);
$content = $post_content->post_content;
wp_reset_query();
preg_match('/ids=".*"/i', $content, $text);
$id_img=str_replace('ids="','',$text[0]);
$id_img=str_replace('"','',$id_img);
$id_imgs=explode(",",$id_img);
$count=ceil(count($id_imgs)/10)+1;
$url_new=get_page_link();
$center=0;
?>
<div class="wrap-gallery">
<div class="image-page"> <?php
for($i=$page*10-10;$i<$page*10;$i++)
{ 
    if(wp_get_attachment_image_url( $id_imgs[$i], 'large')!="") : ?>
<div class="wrap-img <?= ($i==0 || $i==9) ? 'wrap-img-570-360' : 'wrap-img-270-165' ?>">
    <div class="img <?= ($i==0 || $i==9) ? 'img-570-360' : 'img-270-165' ?>" link-data="<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'large') : 'unset'?>" style="background-image:url(<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'large') : 'unset'?>)"></div>
    <div class="<?= ($i==0 || $i==9) ? 'overlay-570-360' : 'overlay-270-165' ?>"></div>
</div> <?php endif;
} ?>
</div> 
<div id="wrap-gallery-page"> <?php
    if( $page!=1) : ?> <a class="truoc-do">←</a> <?php endif; 
   for($i=1;$i<$count;$i++)
   {
    if( ($i==1 && $page!=1) || ($i==$count-1&&$page!=$count-1) || $i==$page-1 || $i==$page+1 ) : ?><a class="page-num"><?=$i?></a> <?php
    elseif($i==$page) : ?> <span class="current-num"><?= $page ?></span> <?php
    else : if($center<2 && ($i==$page-2 || $i==$count-2)) : ?> <span class="center-num">...</span> <?php $center++; endif ; endif;
   }
    if( $page!=$count-1) : ?> <a class="xem-tiep">→</a> <?php endif; ?>
</div>
</div>
<div class="wrap-zoom-img"><img class="img-zoom"></div>
<script type="text/javascript">
jQuery(function($){
    var page=1;
    var count='<?= $count; ?>';
    
    function action()
    {
    $('.wrap-img').click(function(){
        var data_link=$(this).find('.img').attr('link-data');
        $('.img-zoom').attr('src',data_link);
        $('.wrap-zoom-img').addClass('show');
    });
    $('.wrap-zoom-img').click(function(){
        $(this).removeClass('show');
    });
    $('a.xem-tiep').click(function(){
        page++;if(page>count){page=count}; load_gallery(page);
    });
    $('a.truoc-do').click(function(){
        page--;if(page<1){page=1}; load_gallery(page);
    });
    $('a.page-num').click(function(){
        page=$(this).text(); load_gallery(page);
    });
    if($('.image-page > div').length<4){ $('.image-page').css('display','flex'); }
    }
     function load_gallery(n)
    {    
    $.ajax({
                    type : "post", 
                    dataType : "json", 
                    url : '<?php echo admin_url('admin-ajax.php');?>', 
                    data : {
                        action: "load_gallery", 
                        page: page,
                    },
                    context: this,
                    beforeSend: function(){
                    },
                    success: function(response) {
                        if(response.success)
                    {
                        $('.wrap-gallery').html(response.data);
                        action();
                    }
                    else {
                            alert('Đã có lỗi xảy ra');
                        }
                    },
                    error: function(){
                    }
                })
    }
    action();
})
</script>
<?php
}
add_shortcode( 'media_gallery_page', 'media_gallery_page' );
function load_gallery()
{
ob_start();
$page =  (isset($_POST['page']))?esc_attr($_POST['page']) : '1';
$post_content= get_post(1);
$content = $post_content->post_content;
wp_reset_query();
preg_match('/ids=".*"/i', $content, $text);
$id_img=str_replace('ids="','',$text[0]);
$id_img=str_replace('"','',$id_img);
$id_imgs=explode(",",$id_img);
$count=ceil(count($id_imgs)/10)+1;
$url_new=get_page_link();
$center=0;
?>
<div class="image-page"> <?php
for($i=$page*10-10;$i<$page*10;$i++)
{ 
    if(wp_get_attachment_image_url( $id_imgs[$i], 'large')!="") : ?>
<div class="wrap-img <?= ($i==0 || $i==9) ? 'wrap-img-570-360' : 'wrap-img-270-165' ?>">
    <div class="img <?= ($i==0 || $i==9) ? 'img-570-360' : 'img-270-165' ?>" link-data="<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'large') : 'unset'?>" style="background-image:url(<?= (wp_get_attachment_image_url( $id_imgs[$i], 'full')!="") ? wp_get_attachment_image_url( $id_imgs[$i], 'large') : 'unset'?>)"></div>
    <div class="<?= ($i==0 || $i==9) ? 'overlay-570-360' : 'overlay-270-165' ?>"></div>
</div> <?php endif;
} ?>
</div> 
<div id="wrap-gallery-page"> <?php
    if( $page!=1) : ?> <a class="truoc-do">←</a> <?php endif; 
   for($i=1;$i<$count;$i++)
   {
    if( ($i==1 && $page!=1) || ($i==$count-1&&$page!=$count-1) || $i==$page-1 || $i==$page+1 ) : ?><a class="page-num"><?=$i?></a> <?php
    elseif($i==$page) : ?> <span class="current-num"><?= $page ?></span> <?php
    else : if($center<2 && ($i==$page-2 || $i==$count-2)) : ?> <span class="center-num">...</span> <?php $center++; endif ; endif;
   }
    if( $page!=$count-1) : ?> <a class="xem-tiep">→</a> <?php endif; ?>
</div> <?php
    $result = ob_get_clean();
    wp_send_json_success($result); 
    die();
}
add_action( 'wp_ajax_load_gallery', 'load_gallery' );
add_action( 'wp_ajax_nopriv_load_gallery', 'load_gallery' );
}