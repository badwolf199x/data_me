function portfolio_gallery() 
{
	$project_images = Filterable_Portfolio_Helper::get_portfolio_images_ids();
$image_size     = $option['project_image_size'];
?>
	<ul class="wp-block-gallery columns-3 is-cropped">
		<?php foreach ( $project_images as $image_id ): ?>
			<li class="blocks-gallery-item">
				<figure>
					<a href="<?php echo wp_get_attachment_image_url( $image_id, $image_size ); ?>">
						<?php echo wp_get_attachment_image( $image_id, $image_size ); ?>
					</a>
				</figure>
			</li>
		<?php endforeach; ?>
	</ul>
		<?php 
}