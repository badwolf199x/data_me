function ash_relative_time($post_id) { 
$date_type=get_option("date_format");
$post_date = get_the_time('U',$post_id);
$delta = time() - $post_date;
if ( $delta < 60 ) {
    echo 'Just Now';
}
elseif ($delta > 60 && $delta < 120){
    echo '1 minute ago';
}
elseif ($delta > 120 && $delta < (60*60)){
    echo strval(round(($delta/60),0)), ' minutes ago';
}
elseif ($delta > (60*60) && $delta < (120*60)){
    echo 'About an hour ago';
}
elseif ($delta > (120*60) && $delta < (24*60*60)){
    echo strval(round(($delta/3600),0)), ' hours ago';
}
else {
    echo get_the_time($date_type,$post_id);
}}