<?php
/**
 * The template for displaying section before footer.
 *
 * @package ECOMCX Theme
 */
?>

<section class="before-footer">
    <div class="container d-flex flex-wrap">
    	<div class="before-footer-item d-none d-lg-block col-lg-6 col-xl-4">
    		<div class="before-footer-item__icon">
    		    <div class="item-row d-flex justify-content-between align-items-center">
    		        <div class="wrap-icon"> 
    			        <i class="bi bi-truck"></i>
    		        </div>
    		    	<div class="item-row__label d-flex flex-wrap justify-content-center">
    			    	<strong>FREEESHIP</strong> với hóa đơn từ 499k
    		    	</div>
    		    </div>
    		    <div class="item-row d-flex justify-content-between align-items-center">
    			    <div class="wrap-icon"> 
    			        <i class="bi bi-cart-plus"></i>
    		        </div>
    		    	<div class="item-row__label d-flex flex-wrap justify-content-center">
    	    			<strong>Đặt là Ship ngay</strong> với các đơn nội thành Hà Nội
        			</div>
        		</div>
    		    <div class="item-row d-flex justify-content-between align-items-center">
    	    		<div class="wrap-icon"> 
        			    <i class="bi bi-tag"></i>
        		    </div>
    			    <div class="item-row__label d-flex flex-wrap justify-content-center">
    		    		<strong>Hàng chính hãng</strong> bảo hành toàn quốc
    	    		</div>
        		</div>
        		<div class="item-row d-flex justify-content-between align-items-center">
    		    	<div class="wrap-icon"> 
    	    		    <i class="bi bi-box2"></i>
        		    </div>
        			<div class="item-row__label d-flex flex-wrap justify-content-center">
    				    <strong>Đổi trả linh hoạt</strong> trong vòng 14 ngày
    			    </div>
    		    </div>
    	    </div>
        </div>
        <div class="before-footer-item d-none d-lg-block col-lg-6 col-xl-4">
        	<h4 class="item-title text-uppercase">
        		Blog
            </h4>
            <ul class="item-content type-blog"> <?php 
                $args_item= array( 
                    'posts_per_page' => 3,
                    'post_type' => 'post',
                    'post_status'   => 'publish',
                );
                $items = new WP_Query( $args_item);
                if ( $items->have_posts() ) :
                    while ( $items->have_posts() ) : $items->the_post(); ?>
                    	<li class="item d-flex align-items-stretch">
                    		<a class="item-thumbnail" href="<?php the_permalink() ?>">
                    			<?php the_post_thumbnail('thumbnail') ?>
                    		</a>
                    		<div class="item-info d-flex flex-wrap">
                    			<a class="item-name" href="<?php the_permalink() ?>">
                    			    <?php the_title(); ?>
                    			</a>
                    			<span class="item-date"><?= get_the_date(); ?></span>
                    		</div>
                    	</li>
                    <?php endwhile; 
                endif; 
                wp_reset_postdata(); ?>
            </ul>
        </div>
        <div class="before-footer-item d-none d-lg-block col-lg-6 col-xl-4">
        	<h4 class="item-title text-uppercase">
        		NEWS ARRIVAL
            </h4>
            <ul class="item-content type-product"> <?php 
                $args_item= array( 
                    'posts_per_page' => 3,
                    'post_type' => 'product',
                    'post_status'   => 'publish',
                );
                $items = new WP_Query( $args_item);
                if ( $items->have_posts() ) :
                    while ( $items->have_posts() ) : $items->the_post(); global $product?>
                    	<li class="item d-flex align-items-stretch">
                    		<a class="item-thumbnail" href="<?php the_permalink() ?>">
                    			<?php the_post_thumbnail('thumbnail') ?>
                    		</a>
                    		<div class="item-info d-flex flex-wrap">
                    			<a class="item-name" href="<?php the_permalink() ?>">
                    			    <?php the_title(); ?>
                    			</a>
                    			<span class="item-price"><?php woocommerce_template_loop_price() ?></span>
                    		</div>
                    	</li>
                    <?php endwhile; 
                endif; 
                wp_reset_postdata(); ?>
            </ul>
        </div>
        <div class="before-footer-item d-none d-lg-block col-lg-6 col-xl-4">
        	<h4 class="item-title text-uppercase">
        		ON SALE!
            </h4>
            <ul class="item-content type-product"> <?php 
                $args_item= array( 
                    'posts_per_page' => 3,
                    'post_type' => 'product',
                    'post_status'   => 'publish',
                    'meta_query'     => array(
                    	'relation' => 'OR',
                    	array(
                    		'key'           => '_sale_price',
                            'value'         => 0,
                            'compare'       => '>',
                            'type'          => 'numeric'
                        )
                    )
                );
                $items = new WP_Query( $args_item);
                if ( $items->have_posts() ) :
                    while ( $items->have_posts() ) : $items->the_post(); global $product?>
                    	<li class="item d-flex align-items-stretch">
                    		<a class="item-thumbnail" href="<?php the_permalink() ?>">
                    			<?php the_post_thumbnail('thumbnail') ?>
                    		</a>
                    		<div class="item-info d-flex flex-wrap">
                    			<a class="item-name" href="<?php the_permalink() ?>">
                    			    <?php the_title(); ?>
                    			</a>
                    			<span class="item-price"><?php woocommerce_template_loop_price() ?></span>
                    		</div>
                    	</li>
                    <?php endwhile; 
                endif; 
                wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>
</section>
