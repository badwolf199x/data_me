<?php
/**
 * The template for displaying siderbar.
 *
 * @package ECOMCX Theme
 */
if( !function_exists('show_list_category_blog'))
{
    function show_list_category($parent,$current_term_id)
    {
        $args_cat = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent,
        'exclude_tree' =>$current_term_id,
        'taxonomy' => 'category'
        );
        $subcats = get_categories($args_cat); 
        foreach ($subcats as $sc): $args_cat['parent']=$sc->term_id; $count_childs=count(get_categories($args_cat)); ?>
            <li class="cat-item cat-item-<?= $sc->term_id ?>">
                <?= ($count_childs==0) ? '<span class="no-child"></span>' : '<span class="child-indicator"><i class="bi bi-chevron-right"></i></span>' ?>
                <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
                    <?= $sc->name ?>
                    <span class="count">(<?=$sc->count?>)</span>
                </a>
                <?php  if($count_childs>0) : ?>
                    <ul class="children"> 
                        <?php show_list_category($sc->term_id,$current_term_id) ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach;
    }
}
$queried_object = get_queried_object();
$term_slug = $queried_object->slug;

$id_setting_footer=2573; 
$data_footer=get_fields($id_setting_footer);
?>
<div class="siderbar-section search">
	<h2 class="title sidebar-title">
        Tìm kiếm
    </h2>
	<div class="siderbar-section-content">
        <form role="search" method="get" id="searchform" class="searchform" action="<?= esc_url( home_url( '/'.$term_slug ) ) ?>">
            <input type="text" placeholder="Nhập từ khóa..." value="" name="s" id="s" autocomplete="off"/>
            <input type="submit" id="searchsubmit" value="" />
        </form>
    </div>
</div>
<div class="siderbar-section menu-blog">
	<h2 class="title sidebar-title">
		Blog Trang Trang Cosmetic
	</h2>
	<ul class="siderbar-section-content">
		<?php show_list_category(0,'')?>
	</ul>
</div>
<div class="siderbar-section recent-post">
	<h2 class="title sidebar-title">
		Bài viết mới
	</h2>
	<div class="siderbar-section-content type-blog">
		<ul>
		<?php   $args_recent = array(
                                'posts_per_page' => 5,
                                'ignore_sticky_posts'=>true,
                        );
                $recent_query = new WP_Query( $args_recent ); 
                if ( $recent_query->have_posts() ) :
                    while ( $recent_query->have_posts() ) : $recent_query->the_post(); ?>
                        <li class="item d-flex align-items-stretch">
                            <a class="item-thumbnail" href="<?php the_permalink() ?>">
                                <?php the_post_thumbnail('thumbnail') ?>
                            </a>
                            <div class="item-info d-flex flex-wrap">
                                <a class="item-name" href="<?php the_permalink() ?>">
                                    <?php the_title(); ?>
                                </a>
                                <span class="item-date"><?= get_the_date(); ?></span>
                            </div>
                        </li>
                    <?php endwhile; 
                endif; 
                wp_reset_postdata(); ?>
        </ul>
	</div>
</div>
<div class="siderbar-section follow-us">
	<h2 class="title sidebar-title">
		Follow us
	</h2>
	<div class="follow-icon">
		<ul class="social">
                    <li>
                        <a <?= ($data['facebook_link']) ? 'href="'.$data['facebook_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-facebook-main.svg" alt="icon logo facebook" title="social facebook">
                            <img src="/wp-content/uploads/2021/12/icon-facebook-graycase.svg" alt="icon logo facebook graycase" title="social facebook">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['messanger_link']) ? 'href="'.$data['messanger_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-messenger-main.svg" alt="icon logo messenger" title="social messenger">
                            <img src="/wp-content/uploads/2021/12/icon-messenger-graycase.svg" alt="icon logo messenger graycase" title="social messenger">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['zalo_link']) ? 'href="'.$data['zalo_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-zalo-main.svg" alt="icon logo zalo" title="social zalo">
                            <img src="/wp-content/uploads/2021/12/icon-zalo-graycase.svg" alt="icon logo zalo graycase" title="social zalo">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['youtobe_link']) ? 'href="'.$data['youtobe_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-youtube-main.svg" alt="icon logo youtube" title="social youtube">
                            <img src="/wp-content/uploads/2021/12/icon-youtube-graycase.svg" alt="icon logo youtube graycase" title="social youtube">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['instagram_link']) ? 'href="'.$data['instagram_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-instagram-main.svg" alt="icon logo instagram" title="social instagram">
                            <img src="/wp-content/uploads/2021/12/icon-instagram-graycase.svg" alt="icon logo instagram graycase" title="social instagram">
                        </a>
                    </li>
                </ul>
	</div>
</div>
