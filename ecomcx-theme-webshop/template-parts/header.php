<?php
/**
 * The template for displaying header.
 *
 * @package ECOMCX
 */

$header_logo = wp_get_attachment_image_src(get_option('logo_monochrome'),'full')[0];

$site_name = get_bloginfo( 'name' );

$header_nav_menu = wp_nav_menu( [
	'theme_location' => 'menu-1',
	'fallback_cb' => false,
	'echo' => false,
] );
$header_top = wp_nav_menu( [
    'theme_location' => 'menu-2',
    'fallback_cb' => false,
    'echo' => false,
] );
$sidebar_menu_mobile = wp_nav_menu( [
    'theme_location' => 'menu-8',
    'fallback_cb' => false,
    'echo' => false,
] );
$year = date_i18n ('Y');
if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ))
{
    $cart=WC()->cart;
    $cart_count = $cart->cart_contents_count; 
    $cart_subtotal= $cart->get_cart_subtotal();
    $cart_url = wc_get_cart_url(); 
}
function show_list_category_nav($parent)
        {
        $args_cat = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent,
        'taxonomy' => 'product_cat'
        );
        $subcats = get_categories($args_cat); 
        foreach ($subcats as $sc): $args_cat['parent']=$sc->term_id; $count_childs=count(get_categories($args_cat)); ?>
            <li class="cat-item cat-item-<?= $sc->term_id ?> <?= ($count_childs==0) ? '' : 'menu-item-has-children' ?>">
            	<div class="wrap-li d-flex justify-content-between">
                    <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
                        <?= $sc->name ?>
                    </a>
                    <?php  if($count_childs>0) : ?>
                        <i class="bi bi-chevron-right"></i>
                    <?php endif; ?>
                </div>
                <?php  if($count_childs>0) : ?>
                    <ul class="sub-menu subnav-children"> 
                        <?php show_list_category_nav($sc->term_id) ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach;
        }
?>
<header class="site-header">
    <div class="header-top d-lg-block d-none">
        <div class="container">
            <div class="wrap-content d-flex justify-content-between align-items-center tex-uppercase">
                <?= $header_top ?>
            </div>
        </div>
    </div>
    <div class="header-middle">
        <div class="container d-flex justify-content-between align-items-center">
            <div class="header-middle-item wrap-icon-sidebar d-lg-none d-md-block">
                <i class="bi bi-list"></i>
            </div>
            <div class="header-middle-item wrap-tel d-lg-block d-none">
                <div class="wrap-item">
                    <i class="bi bi-telephone-fill"></i><a title="(Tư vấn, Đặt hàng)" href="tel:0965224390">0965224390</a>
                </div>
                <div class="wrap-item">
                    <i class="bi bi-telephone-fill"></i><a title="(Khiếu nại, phản ánh)" href="tel:0984179791">0984179791</a>
                </div>
            </div> 
            <div class="header-middle-item wrap-site-logo d-flex justify-content-center align-items-center">
                <div class="site-logo">
                    <a href="<?php echo site_url() ?>">
                        <img src="<?php echo $header_logo ?>" alt="<?php echo $site_name ?>">
                    </a>
                </div>
                <div class="search-product d-lg-block d-none">
                    <?php 
                    if(in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ))
                    { 
                        echo get_product_search_form(); 
                    } 
                    ?>
                </div>
            </div>
            <div class="header-middle-item wrap-icon d-flex justify-content-end align-items-center">
                <div class="d-lg-none icon-search">
                    <i class="bi bi-search"></i>
                </div>
                <div class="user-account">
                    <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                        <i class="bi bi-person"></i>
                    </a>
                </div>
                <ul id="mini-cart">
                    <li>
                        <a class="menu-item cart-contents" href="<?php echo $cart_url; ?>" title="giỏ hàng">
                        <i class="bi bi-bag"></i>
                            <span class="cart-contents-count d-flex justify-content-center align-items-center"><?php echo $cart_count; ?></span>
                        </a>
                        <?php if(get_option ('mini_cart')==1) : ?>
                        <span id="drop-cart" class="header-quickcart"> 
                            <?php  get_template_part( 'template-parts/mini-cart' ); ?> 
                        </span>
                        <?php endif; ?>
                    </li>
                </ul>
                <div class="cart-subtotal">
                    <?= $cart_subtotal; ?>
                </div>
            </div>    
        </div>
    </div>
    <div class="header-bottom d-lg-flex justify-content-between align-items-center d-none">
        <?= $header_nav_menu ?>
    </div>
</header>

<!-- Site Sidebar Menu-->
<div  class="site-sidebar menu-custom">
    <div  class="sidebar-container">
        <div class="sidebar-logo text-center">
                <?php the_custom_logo(); ?>             
        </div>
        <div class="sidebar-menu">
            <?= $sidebar_menu_mobile ?>
        </div>
        <div class="sidebar-menu">
            <div class="wrap-content-nav-drop">
                <ul class="menu">
                    <?php show_list_category_nav(0); ?>
        	   </ul>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Site Sidebar Menu-->
