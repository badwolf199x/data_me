<?php
/**
 * The template for displaying homepage pages.
 *
 * Template Name: Trang chủ
 * 
 * @package ECOMCX Theme
 */
/*huy style woo*/
add_filter( 'woocommerce_enqueue_styles', 'ecx_dequeue_styles_woo' );
function ecx_dequeue_styles_woo( $enqueue_styles ) {
unset( $enqueue_styles['woocommerce-general'] ); // Remove woocommerce.css
unset( $enqueue_styles['woocommerce-layout'] ); // Remove woocommerce-layout.css
unset( $enqueue_styles['woocommerce-smallscreen'] ); // Remove the woocommerce-smallscreen.css
return $enqueue_styles;
}?>

<?php get_header();
//setting riêng
$id_setting_r=68; 
$data=get_fields($id_setting_r);
//data banner
$section_banner=$data['section_banner'];
$megamenu_left=$section_banner['megamenu_left'];
$banner_middle_top=$section_banner['banner_middle_top'];
$banner_middle_bottom=$section_banner['banner_middle_bottom'];
$banner_right=$section_banner['banner_right'];
//data show product sale
$section_sale_product=$data['section_sale_product'];
$args_products_sale = array( 
    'post_type' => 'product',
    'post_status'   => 'publish',
    'posts_per_page' => $section_sale_product['product_count'], 
    'orderby'        =>'meta_value_num date',
    'order'        =>'DESC',
    'meta_key' => '_featured_product',
    'meta_query'     => array(
        'relation' => 'OR',
        array(
            'key'           => '_sale_price',
            'value'         => 0,
            'compare'       => '>',
            'type'          => 'numeric'
        )
    )
);
$products_sale = new WP_Query( $args_products_sale);
//data show product
for($i=1;$i<=8;$i++)
{
    if($data['section_show_product_'.$i]!='')
    {
        $section_show_product[$i]=$data['section_show_product_'.$i];
    }
}
$count_section_show_product=count($section_show_product);
?>
<div class="main-index">

<!-- Begin Home banner -->
    <section id="banner-home">
        <div class="container flex-wrap align-items-stretch d-flex justify-content-between">
            <div class="megamenu-left">
                <div class="megamenu-header d-flex justify-content-between"><strong>Shop</strong><a href="<?= get_permalink( wc_get_page_id( 'shop' ) ) ?>">Xem thêm</a></div>
                <ul class='megamenu-wrap'>
                <?php for ($i=1; $i <= $megamenu_left['item_count']; $i++) : 
                    $item_rank=$megamenu_left['mega_menu_'.$i];
                    $type=$item_rank['item_type'];
                    if($type=='taxonomy_product') :
                        $item=$item_rank['item_content_tax_product'];
                        if($item['select_tax']!='') :
                            $parent=$item['select_tax'];
                            $label=($item['tax_label']!='') ? $item['tax_label'] : get_term_by( 'id', $parent, 'product_cat' )->name;
                            $out='';
                            $out.= mega_menu_taxonomy($parent,'product_cat',$label,1,$item_rank['show_featured_img']);
                            echo $out;
                        endif;
                    elseif ($type=='taxonomy_post') :
                        $item=$item_rank['item_content_tax_blog'];
                        if($item['select_tax']!='') :
                            $parent=$item['select_tax'];
                            $label=($item['tax_label']!='') ? $item['tax_label'] : get_term_by( 'id', $parent,'category' )->name;
                            $out='';
                            $out.= mega_menu_taxonomy($parent,'category',$label,1,false);
                            echo $out;
                        endif;
                    elseif ($type=='page_parent') : 
                        $item=$item_rank['item_content_page_list'];
                        if($item['select_page']!='') :
                            $parent=$item['select_page'];
                            $label=($item['page_label']!='') ? $item['page_label'] : get_the_title($parent);
                            $out='';
                            $out.= mega_menu_page($parent,$label,1);
                            echo $out;
                        endif;
                    elseif ($type=='custom_link') :
                        $item=$item_rank['item_content_link'];
                        if($item['header_link']!='' ) : ?>
                            <li class="megamenu-custom-link">
                                <?= ($item['link_url']!='') ? '<a href="'.$item['link_url'].'" >' : ''?> 
                                    <?= $item['header_link'] ?>
                                <?= ($item['link_url']!='') ? '</a>' : ''?> 
                            </li>
                        <?php endif;
                    endif;
                endfor; ?>
                </ul>
            </div>
            <div class="banner-middle">
                <div class="banner-middle-top">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <?php for ($i=1; $i <= $banner_middle_top['item_count']; $i++) : ?>
                                <div class="swiper-slide">
                                    <?= wp_get_attachment_image($banner_middle_top['banner_'.$i]['id'],'large',false,['class'=>'banner-img','alt'=>'Banner Image'.$i])?>      
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="banner-middle-bottom d-flex justify-content-start">
                    <?php for ($i=1; $i <= $banner_middle_bottom['item_count']; $i++) :
                        $item=$banner_middle_bottom['bottom_'.$i];
                        if(isset($item['label']) && isset($item['select_image'])) : ?>
                            <div class="middle-bottom">
                                <div class="item-wrap position-relative d-flex align-items-between flex-wrap justify-content-center">
                                    <?= (isset($item['url_link'])) ? '<a class="item__link d-flex align-items-between flex-wrap justify-content-center" href="'.$item['url_link'].'">' : '' ?>
                                    <div class="item__img d-flex align-items-center">
                                        <?= wp_get_attachment_image($item['select_image'],'medium',false) ?> 
                                    </div>
                                    <h5 class="item__label text-center">
                                        <?= $item['label'] ?>
                                    </h5>
                                    <?= (isset($item['url_link'])) ? '</a>' : ''; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="banner-right d-xl-block d-flex flex-wrap">
                <?php for ($i=1; $i <= $banner_right['item_count']; $i++) :
                    $item=$banner_right['bottom_'.$i];
                    if(isset($item['label']) && isset($item['select_image'])) : ?>
                        <div class="banner-right-item align-items-center d-flex justify-content-start position-relative">
                            <div class="item-wrap">
                                <?= (isset($item['url_link'])) ? '<a class="item__link" href="'.$item['url_link'].'">' : '' ?>
                                    <div class="item__img">
                                        <?= wp_get_attachment_image($item['select_image']['id'],'medium',false) ?> 
                                    </div>
                                    <div class="item__body">
                                        <h4 class="item__label"><?= $item['label'] ?></h4>
                                        <?= (isset($item['url_link'])) ? '<h5 class="item__bt-text icon-chevron-right-after">'.$item['button_text'].'</h5>' : '' ?>
                                    </div>
                                <?= (isset($item['url_link'])) ? '</a>' : '' ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    </section>
<!-- End of Home banner -->

<!-- Begin Home slider sale -->
    <section id="slider-sale">
        <div class="container">
            <div class="section-header d-flex justify-content-between flex-wrap">
                <div class="section-label-wrap d-flex align-items-start justify-content-between"> 
                    <h2 class="section-label ">
                        <?= $section_sale_product['section_label'] ?>
                    </h2>
                    <div class="date-time-count-down d-flex">
                        <?= $section_sale_product['count_count_label'] ?>
                        <div id="time-end" data-time="<?= $section_sale_product['section_count_down'] ?>">
                        </div>
                    </div>
                </div>
                <div class="header-link-wrap">
                    <a class="header-link icon-chevron-right-after" href="<?= $section_sale_product['button_link'] ?>">
                        <?= $section_sale_product['button_text'] ?>
                    </a>
                </div>
            </div>
            <div class="swiper ecx-product-loop">
                <div class="swiper-button-prev"></div>
                    <div class="swiper-wrapper">
                        <?php   if ( $products_sale->have_posts() ) :
                            while ( $products_sale->have_posts() ) :
                                $products_sale->the_post(); ?>
                                <div class="swiper-slide"> 
                                    <?php wc_get_template_part( 'content', 'product' ); ?>
                                </div>
                            <?php endwhile; 
                            do_action( 'woocommerce_after_shop_loop' ); 
                        else :
                                do_action( 'woocommerce_no_products_found' );
                        endif; 
                        wp_reset_postdata(); ?>
                    </div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </section>
<!-- End of Home banner -->

<?php for($i=1;$i<=$count_section_show_product;$i++) : 
      $data_section=$section_show_product[$i];                         
      if(($data_section['select_product']!='' && $data_section['select_type']=='custom') || ($data_section['select_category']!='' && $data_section['select_type']=='category') || $data_section['select_type']=='new') : ?>
<!-- Begin show product -->
    <section id="show-product-<?= $i ?>" class="show-product">
        <div class="container">
            <div class="section-header d-flex justify-content-between flex-wrap">
                <div class="section-label-wrap d-flex align-items-start justify-content-between"> 
                    <h2 class="section-label ">
                        <?= $data_section['section_label'] ?>
                    </h2>
                </div>
                <div class="header-link-wrap"> <?php 
                    for($j=1;$j<=$data_section['button_count'];$j++) : 
                        if($data_section['button_'.$j]['high_light']==true) : 
                            if($data_section['button_'.$j]['button_link']=='' && $data_section['button_'.$j]['button_link']==null) : ?>
                                <span class="header-link high-light">
                                    <?= $data_section['button_'.$j]['button_text'] ?>
                                </span> <?php
                            else : ?>
                                <a class="header-link icon-chevron-right-after" href="<?= $data_section['button_'.$j]['button_link'] ?>">
                                    <?= $data_section['button_'.$j]['button_text'] ?>
                                </a> <?php
                            endif;
                        else : ?>
                            <a class="header-link" href="<?= $data_section['button_'.$j]['button_link'] ?>">
                                <?= $data_section['button_'.$j]['button_text'] ?>
                            </a> <?php 
                        endif;
                    endfor; ?>
                </div>
            </div>
            <div class="ecx-product-show-wrap-<?=$i?> justify-content-between d-flex flex-wrap align-items-stretch"> <?php 
                if($data_section['enable_banner']==true) : ?>
                    <div class="ecx-product-show-banner justify-content-between d-flex flex-wrap d-lg-block" > <?php 
                        for($j=1;$j<=$data_section['banner_count'];$j++) : ?>
                            <div class="product-show-banner-<?=$j?>-wrap" style="<?=($data_section['banner_count']==1) ? 'width:100%': ''; ?>">
                                <?= wp_get_attachment_image($data_section['select_banner_'.$j]['id'],'medium',false,['class'=>'banner-img','alt'=>'Banner Image Product Show '.$i.'-'.$j])?>
                            </div> <?php
                        endfor; ?>
                    </div> <?php 
                endif ?>
                <div class="ecx-product-show <?= ($data_section['enable_banner']==true) ? 'product-col-4' : 'product-col-6' ?>">
                    <div class="ecx-product-loop justify-content-start d-flex flex-wrap align-items-stretch">   <?php 
                        if($data_section['select_type']=='custom') :  
                                $product_ids=($data_section['select_product']);
                                $args_products_custom = array(
                                    'posts_per_page' => 12,
                                    'post_status'   => 'publish',
                                    'post_type' => 'product',
                                    'post__in' => $product_ids, 
                                    'orderby'        =>'meta_value_num date',
                                    'order'        =>'DESC',
                                    'meta_key'=>'_featured_product',
                                    'meta_query' => array(                  'relation' => 'AND',
                                        array(
                                            'key' => '_price',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_stock',
                                            'value' => 0,
                                            'compare' => '>')
                                            ),
                                );
                                $products_show = new WP_Query( $args_products_custom);
                        elseif($data_section['select_type']=='category') :
                                $category_id=$data_section['select_category'];
                                $args_products_category = array( 
                                    'posts_per_page' => 12,
                                    'post_status'   => 'publish',
                                    'post_type' => 'product', 
                                    'orderby'        =>'meta_value_num date',
                                    'order'        =>'DESC',
                                    'tax_query' => array(                     
                                        'relation' => 'OR',                     
                                        array(
                                            'taxonomy' => 'product_cat',                
                                            'field' => 'id',                    
                                            'terms' => $category_id,    
                                            'include_children' => true,           
                                            'operator' => 'IN'                    
                                        ),
                                    ),
                                    'meta_key'=>'_featured_product',
                                    'meta_query' => array(                  'relation' => 'AND',
                                        array(
                                            'key' => '_price',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_stock',
                                            'value' => 0,
                                            'compare' => '>')
                                            ),
                                );
                                $products_show = new WP_Query( $args_products_category);
                        elseif($data_section['select_type']=='new') :
                                $args_products_new = array( 
                                    'posts_per_page' => $data_section['select_count_product_new'],
                                    
                                    'post_type' => 'product', 
                                    'post_status'   => 'publish',
                                    'orderby'        =>'date',
                                    'order'        =>'DESC',
                                    'meta_query' => array(                  'relation' => 'AND',
                                        array(
                                            'key' => '_price',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_stock',
                                            'value' => 0,
                                            'compare' => '>'),
                                        array(
                                            'key' => '_thumbnail_id',
                                            'value' => 0,
                                            'compare' => '>')
                                            ),
                                );
                                $products_show = new WP_Query( $args_products_new);
                        endif;
                        if ( $products_show->have_posts() ) :
                            while ( $products_show->have_posts() ) :
                                $products_show->the_post(); ?>
                                    <?php wc_get_template_part( 'content', 'product' ); ?>
                            <?php endwhile; 
                        else :
                                do_action( 'woocommerce_no_products_found' );
                        endif; 
                        woocommerce_reset_loop();
                        wp_reset_query();
                        wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- End of show product -->
<?php endif; endfor; ?>

</div>

<?php get_footer(); ?>