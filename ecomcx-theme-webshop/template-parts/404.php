<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package HelloElementor
 */

?>
<main class="site-main container error404" role="main">
		<header class="page-header">
			<h1 class="entry-title"><?php esc_html_e( 'The page can&rsquo;t be found.', 'ecomcx-theme' ); ?></h1>
		</header>
	<div class="page-content">
		<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'ecomcx-theme' ); ?></p>
	</div>

</main>
