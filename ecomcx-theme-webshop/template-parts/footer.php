<?php
/**
 * The template for displaying footer.
 *
 * @package HelloElementor
 */

$footer_nav_support = wp_nav_menu( [
    'theme_location' => 'menu-3',
    'fallback_cb' => false,
    'echo' => false,
] );

$footer_nav_about_us = wp_nav_menu( [
    'theme_location' => 'menu-4',
    'fallback_cb' => false,
    'echo' => false,
] );

$footer_nav_terms_policies = wp_nav_menu( [
    'theme_location' => 'menu-5',
    'fallback_cb' => false,
    'echo' => false,
] );

$year = date_i18n ('Y');

$id_setting_r=2573; 
$data=get_fields($id_setting_r);

$hotline_num=get_option('hotline_numb');
$count_text=array('Zero','One','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten');
?>
<!-- Begin before footer -->
        <?php get_template_part( 'template-parts/section-chung/before_footer' ); ?>
<!-- End of before footer  -->
<footer id="site-footer" class="footer" role="contentinfo">
    <div class="site-footer container d-block d-lg-flex">
        <div class=" col-12 col-lg-4 col-xl-5 main-footer-wrap col-lg-left">
            <div class="footer-site-logo">
                <?php the_custom_logo(); ?>
            </div>
            <div class="footer-call-us d-none d-md-block">
                <div class="media d-flex">
                    <span class="media-left call-us-icon media-middle">
                        <i class="bi bi-headset"></i></span>
                    <div class="media-body">
                        <span class="call-us-text">Got Questions ? Call us 24/7!</span>
                        <a class="call-us-number" href="tel:<?= $hotline_num?>"><?= $hotline_num?></a>
                    </div>
                </div>
            </div>
            <div class="main-footer__content d-none d-md-block">
                <?=$data['contact_info']?>
            </div>
            <div class="footer-follow">
                <ul class="social">
                    <li>
                        <a <?= ($data['facebook_link']) ? 'href="'.$data['facebook_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-facebook-main.svg" alt="icon logo facebook" title="social facebook">
                            <img src="/wp-content/uploads/2021/12/icon-facebook-graycase.svg" alt="icon logo facebook graycase" title="social facebook">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['messanger_link']) ? 'href="'.$data['messanger_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-messenger-main.svg" alt="icon logo messenger" title="social messenger">
                            <img src="/wp-content/uploads/2021/12/icon-messenger-graycase.svg" alt="icon logo messenger graycase" title="social messenger">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['zalo_link']) ? 'href="'.$data['zalo_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-zalo-main.svg" alt="icon logo zalo" title="social zalo">
                            <img src="/wp-content/uploads/2021/12/icon-zalo-graycase.svg" alt="icon logo zalo graycase" title="social zalo">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['youtobe_link']) ? 'href="'.$data['youtobe_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-youtube-main.svg" alt="icon logo youtube" title="social youtube">
                            <img src="/wp-content/uploads/2021/12/icon-youtube-graycase.svg" alt="icon logo youtube graycase" title="social youtube">
                        </a>
                    </li>
                    <li>
                        <a <?= ($data['instagram_link']) ? 'href="'.$data['instagram_link'].'"' : ''?> target="_blank">
                            <img src="/wp-content/uploads/2021/12/icon-instagram-main.svg" alt="icon logo instagram" title="social instagram">
                            <img src="/wp-content/uploads/2021/12/icon-instagram-graycase.svg" alt="icon logo instagram graycase" title="social instagram">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="footer-menu-accordion" class="col-12 col-lg-8 col-xl-7 menu-footer-wrap col-lg-right">
            <div class="menu-footer__content d-md-flex"> <?php 
                for($i=1;$i<=3;$i++) : ?>
                <div class="footer-col footer-block">
                    <div class="accordion-item">
                        <h4 class="footer-title tex-capitalize d-none d-md-block">
                            <?=$data['label_menu_'.$i]?>
                        </h4>
                        <h4 class="accordion-header  d-md-none" id="accordion-heading<?= $count_text[$i] ?>">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?= $count_text[$i] ?>" aria-expanded="true" aria-controls="flush-collapse<?= $count_text[$i] ?>" href="#flush-collapse<?= $count_text[$i] ?>">
                                <?=$data['label_menu_'.$i]?>
                            </button>
                        </h4>
                        <div id="flush-collapse<?= $count_text[$i] ?>" class="accordion-collapse collapse show" aria-labelledby="accordion-heading<?= $count_text[$i] ?>" data-bs-parent="#footer-menu-accordion">
                            <div class="accordion-body menufooter-content">
                                <?php if($i==1) : echo $footer_nav_support;
                                    elseif ($i==2) : echo $footer_nav_about_us;
                                    elseif ($i==3) : echo $footer_nav_terms_policies;
                                    endif; ?>
                            </div>
                        </div>
                    </div>
                </div> <?php
                endfor; ?>
            </div>
            <div class="menu-footer__announced d-none d-md-block">
                <a href="http://online.gov.vn/Home/WebDetails/71198" target="_blank">
                    <img src="/wp-content/uploads/2022/02/logo-thong-bao-bo-cong-thuong.webp" alt="Logo Đã thông báo Bộ Công Thương" width="80%" height="auto">
                </a>
            </div>
        </div>          
    </div>
    <div class="copyright-footer">
        <div class="d-flex container">
            <div class="d-none d-md-block col-md-3 col-lg-2 col-xl-4 copyright-menu">
                <?php echo $footer_nav_menu_copyright ?>
            </div>
            <div class="d-block col-12 col-md-9 d-lg-flex justify-content-between col-lg-10 col-xl-8 copyright-text text-right">
                <div class="line">© <?php echo $year ?> <a href="<?php echo site_url() ?>"><?= get_bloginfo('name')?></a>.</div> 
                <div class="line">Designed by <a href="https://ecomcx.com"> ECOMCX</a></div>
            </div>
        </div>
    </div>
</footer>

