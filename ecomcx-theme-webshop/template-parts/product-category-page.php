<?php
/**
 * The template for displaying categoryies pages.
 *
 * Template Name: Categoryies page
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); 
if( !function_exists('show_list_category_page'))
{
    function show_list_category_page($parent)
    {
        $args_cat = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent,
        'taxonomy' => 'product_cat'
        );
        $subcats = get_categories($args_cat); 
        $count_ul=1;
        foreach ($subcats as $sc): 
            $args_cat['parent']=$sc->term_id; 
            $count_childs=count(get_categories($args_cat)); 
            if(($count_ul==1 || $count_ul%2==1) && $parent==0) : ?>
                <ul class="table-categoryies col-12 col-md-6 col-lg-3">
            <?php endif; ?>
                <li class="cat-item cat-item-<?= $sc->term_id ?>">
                    <?= ($count_childs==0) ? '<span class="no-child"></span>' : '<span class="child-indicator"></span>' ?>
                    <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
                         <?= $sc->name ?>
                    </a>
                    <?php  if($count_childs>0) : ?>
                        <ul class="children"> 
                            <?php show_list_category_page($sc->term_id) ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php if(($count_ul==2 || $count_ul%2==0 || $count_ul==count($subcats)) && $parent==0) : ?>
                </ul>
            <?php endif;
            $count_ul++; 
        endforeach;
    }
}
?>
<div class="container">
<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
</div>
<!-- Begin content categoryies page -->
<section id="archive" class="section-content-categoryies-page position-relative container d-flex flex-wrap">
        <div class="title-categoryies-page line-after col-12">
            <?php the_title('<h1 class="ecx-post-title text-capitalize text-center">', '</h1>'); ?>
        </div>
        <div class="col-12 content-categoryies-page d-flex flex-wrap">
                <?php show_list_category_page(0) ?>
        </div>
    </section>
<!-- End content categoryies page -->
<?php get_footer(); ?>