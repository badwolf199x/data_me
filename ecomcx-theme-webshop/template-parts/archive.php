<?php
/**
 * The template for displaying archive pages.
 *
 * @package ECOMCX Theme
 */
?>
<div class="main-index">
    <!-- Begin content archive -->
        <section id="archive" class="section-archive position-relative">
            <div class="container d-flex flex-wrap">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            	<div class="col-12 col-lg-8 content-archive">
            		<div class="wrap-content-post flex-wrap justify-content-between d-flex">
            		    <?php if(have_posts()):
                            while ( have_posts() ) : the_post(); ?>
                            <article class="ecx-post ecx-flex-item">
                                <a href="<?php the_permalink() ?>">
                                    <div class="ecx-post-thumbnail">
                                        <?php the_post_thumbnail() ?>
                                    </div>
                                </a>
                                <div class="ecx-post-info">
                                    <h3 class="ecx-post-title">
                                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <div class="ecx-post-meta-data">
                                        <div class="ecx-post-date">
                                            <i class="bi bi-clock"></i>&nbsp;<?php echo get_the_date(); ?>
                                        </div>
                                        <div class="ecx-post-description">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <a class="ecx-post-readmore" href="<?php the_permalink() ?>">Xem thêm
                                            <i class="bi bi-arrow-right-short"></i>
                                        </a>
                                    </div>
                                </div>
                            </article>
                        <?php endwhile; 
                        else:
                            if(!is_search()):
                            echo '<h3 class="empty_post">Chuyên mục chưa có bài viết, vui lòng quay lại sau.</h3>';
                            else :
                            echo '<h3 class="empty_post">Không tìm thấy nội dung chứa từ khóa.</h3>';
                            endif;
                        endif; ?>
                    </div>
                        <?php global $wp_query;
                           $max_page=ceil(($wp_query->found_posts-$wp_query->offset)/get_query_var('posts_per_page'));
                           if($max_page>1) 
                            {   ?>
                                <div class="ecx-pagination">
                                    <?php   
                                            $big = 999999999;
                                            echo paginate_links( array(
                                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                                'format' => '?paged=%#%',
                                                'prev_text'    => '←',
                                                'next_text'    => '→',
                                                'current' => max( 1, get_query_var('paged') ),
                                                'total' => $max_page,
                                                'type'         => 'list',
                                                'end_size'     => 3,
                                                'mid_size'     => 1,
                                            ) );
                                    ?>
                                </div>
                    <?php   }   ?>
                </div>
                <div class="col-12 col-lg-4 sidebar">
                    <?php get_template_part( 'template-parts/section-chung/sidebar' ); ?>
                </div>
            </div>
        </section>
    <!-- End content archive -->
</div>