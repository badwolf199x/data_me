<?php
/**
 * The template for displaying brand pages.
 *
 * Template Name: Brand page
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); 
$thuong_hieu_terms=get_terms(['taxonomy' => 'pa_thuong-hieu','hide_empty' => false,]);
    $a_z = range('A', 'Z');
    $a1_9 = range('1', '9');
    $count=0;
    foreach ($thuong_hieu_terms as $key => $term) 
    {
        $key_first=ucfirst(substr($term->name, 0, 1));
        if(in_array($key_first, $a_z))
        {
            $thuong_hieu_info[$key_first][$count]['id']=$term->term_id;
            $thuong_hieu_info[$key_first][$count]['name']=ucfirst($term->name);
            $thuong_hieu_info[$key_first][$count]['slug']=$term->slug;
            $count++;
        }
        elseif(in_array($key_first, $a1_9))
        {
            $thuong_hieu_info['123'][$count]['id']=$term->term_id;
            $thuong_hieu_info['123'][$count]['name']=ucfirst($term->name);
            $thuong_hieu_info['123'][$count]['slug']=$term->slug;
            $count++;
        }
    }

?>
<div class="container">
<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
</div>
<!-- Begin content brand page -->
<section id="archive" class="section-content-brand-page position-relative container d-flex flex-wrap">
        <div class="title-brand-page line-after col-12">
            <?php the_title('<h1 class="ecx-post-title text-capitalize text-center">', '</h1>'); ?>
        </div>
        <div class=" col-12 drop-list-brand">
            <select name="cat" id="cat" class="postform">
                <option value="0" selected="selected">Hiển thị tất cả thương hiệu</option>
                <?php foreach ($thuong_hieu_terms as $key => $term) : ?>
                <option class="thuong-hieu-<?= $term->term_id; ?>" value="<?= $term->slug;  ?>"><?= ucfirst($term->name);  ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-12 content-brand-page">
            <div class="filter-button">
                <?php  foreach ($thuong_hieu_info as $key => $value) : ?>
                    <a class="button-filter text-uppercase" href="#<?= $key ?>">
                        <?= $key ?>
                    </a>
                <?php endforeach; ?>
            </div>
            <div class="table-brand d-flex flex-wrap">
                <?php  $num=0; foreach ($thuong_hieu_info as $key => $value) :   ?>
                    <div class="col-brand col-1 col-md-6 col-lg-3">
                        <div class="label-col-brand" id="<?=$key?>">
                            <?= $key ?>
                        </div>
                        <?php foreach ($value as $key => $data) : ?>
                            <div class="content-col-brand">
                                <a href="/thuong-hieu/<?=$data['slug']?>"> 
                                    <?= $data['name'] ?>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<!-- End content brand page -->
<?php get_footer(); ?>