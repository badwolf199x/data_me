<?php
/**
 * The template for displaying contact pages.
 *
 * Template Name: Contact us
 * 
 * @package ECOMCX Theme
 */
?>
<?php get_header(); 
$id_setting_r=2658; 
$data=get_fields($id_setting_r);
$map_iframe=$data['map_iframe'];
$contact_content=$data['contact_content'];
?>
<div class="container">
<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
</div>
<!-- Begin content contact -->
    <section id="archive" class="section-content-contact position-relative container d-flex flex-wrap">
    	<div class="title-contact line-after col-12">
        	<?php the_title('<h1 class="ecx-post-title text-capitalize text-center">', '</h1>'); ?>
        </div>
        <div class=" flex-wrap align-items-center d-flex col-lg-6 col-12 content-contact">
            <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_grey d-flex">
                <span class="vc_sep_holder vc_sep_holder_l">
                    <span class="vc_sep_line"></span>
                </span>
                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-left">
                    <i class="bi bi-envelope-open-fill"></i>
                </div>
                <span class="vc_sep_holder vc_sep_holder_r">
                    <span class="vc_sep_line"></span>
                </span>
            </div>
		           <div class="contact-info">
		            	<?= $contact_content; ?>
		            </div>
                	<div class="contact-form">
		            	<?= do_shortcode('[contact-form-7 id="8" title="Form liên hệ"]'); ?>
		            </div>
        </div>
        <div class="col-lg-6 col-12 ecomcx-maps">
            <?= $map_iframe; ?>
        </div>
    </section>
<!-- End content contact -->

<?php get_footer(); ?>