<?php
/**
 * The template for displaying mini cart.
 *
 * @package ECOMCX Theme
 */
if ( ! function_exists( 'ecx_render_mini_cart_item' ) ) {
    function ecx_render_mini_cart_item( $cart_item_key, $cart_item ) {
        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $is_product_visible = ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) );

        if ( ! $is_product_visible ) {
            return;
        }

        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
        $product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
        ?>
        <div class="mini-cart__product <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

            <div class="product-thumbnail">
                <?php
                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                if ( ! $product_permalink ) :
                    echo wp_kses_post( $thumbnail );
                else :
                    printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
                endif;
                ?>
            </div>

            <div class="product-name" data-title="Product">
                <?php
                if ( ! $product_permalink ) :
                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                else :
                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                endif;

                do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

                // Meta data.
                echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
                ?>
            </div>

            <div class="product-price" data-title="Price">
                <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
            </div>

            <div class="product-remove">
                <?php
                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                    '<a href="%s" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s"></a>',
                    esc_url( wc_get_cart_remove_url( $cart_item_key ) ),'Remove this item',
                    esc_attr( $product_id ),
                    esc_attr( $cart_item_key ),
                    esc_attr( $_product->get_sku() )
                ), $cart_item_key );
                ?>
            </div>
        </div>
        <?php
    }
}

$cart_items = WC()->cart->get_cart();

if ( empty( $cart_items ) ) { ?>
    <div class="mini-cart__empty-message">Chưa có sản phẩm nào trong giỏ</div>
<?php } else { ?>
    <div class="mini-cart__contents">
        <?php
        do_action( 'woocommerce_before_mini_cart_contents' );

        foreach ( $cart_items as $cart_item_key => $cart_item ) {
            ecx_render_mini_cart_item( $cart_item_key, $cart_item );
        }

        do_action( 'woocommerce_mini_cart_contents' );
        ?>
    </div>

    <div class="mini-cart__subtotal">
        <strong><?php echo __( 'Subtotal', 'woocommerce' ); // phpcs:ignore WordPress.WP.I18n ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?>
    </div>
    <div class="mini-cart__button">
        <a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="cart-button view-cart">
            <span class="text-upercase">Xem giỏ hàng</span>
        </a>
        <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="cart-button checkout">
            <span class="text-upercase">Thanh toán</span>
        </a>
    </div>
    <?php
} // empty( $cart_items )