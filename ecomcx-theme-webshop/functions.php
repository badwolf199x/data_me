<?php
/**
 * Theme functions and definitions
 *
 * @package Ecomcx-theme
 */

define( 'ECOMCX_THEME_VERSION', '1.0' );

if ( ! isset( $content_width ) ) {
  $content_width = 800; // Pixels.
}

if ( ! function_exists( 'ecomcx_theme_setup' ) ) {
	/**
	 * Set up theme support.
	 *
	 * @return void
	 */
	function ecomcx_theme_setup() {
		$hook_result = apply_filters_deprecated( 'ecomcx_theme_load_textdomain', [ true ], '2.0', 'ecomcx_theme_load_textdomain' );
		if ( apply_filters( 'ecomcx_theme_load_textdomain', $hook_result ) ) {
			load_theme_textdomain( 'Ecomcx-theme', get_template_directory() . '/languages' );
		}

		$hook_result = apply_filters_deprecated( 'ecomcx_theme_register_menus', [ true ], '2.0', 'ecomcx_theme_register_menus' );
		if ( apply_filters( 'ecomcx_theme_register_menus', $hook_result ) ) {
			register_nav_menus( [ 'menu-1' => __( 'Header', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-2' => __( 'Top header', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-3' => __( 'Footer Hỗ Trợ Khách Hàng', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-4' => __( 'Footer About Us', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-5' => __( 'Footer Chương Trình & Chính Sách', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-6' => __( 'Sidebar blog', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-7' => __( 'Sidebar product', 'Ecomcx-theme' ) ] );
			register_nav_menus( [ 'menu-8' => __( 'Sidebar menu mobile', 'Ecomcx-theme' ) ] );
		}

		$hook_result = apply_filters_deprecated( 'ecomcx_theme_add_woocommerce_support', [ true ], '2.0', 'ecomcx_theme_add_woocommerce_support' );
		if ( apply_filters( 'ecomcx_theme_add_woocommerce_support', $hook_result ) ) {
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'title-tag' );
			add_theme_support(
				'html5',
				[
					'search-form',
					'comment-form',
					'comment-list',
					'gallery',
					'caption',
				]
			);
			add_theme_support(
				'custom-logo',
				[
					'height'      => 100,
					'width'       => 350,
					'flex-height' => true,
					'flex-width'  => true,
				]
			);

			/*
			 * Editor Style.
			 */
			add_editor_style( 'classic-editor.css' );

			/*
			 * Gutenberg wide images.
			 */
			add_theme_support( 'align-wide' );

			/*
			 * WooCommerce.
			 */
			$hook_result = apply_filters_deprecated( 'ecomcx_theme_add_woocommerce_support', [ true ], '2.0', 'ecomcx_theme_add_woocommerce_support' );
			if ( apply_filters( 'ecomcx_theme_add_woocommerce_support', $hook_result ) ) {
				// WooCommerce in general.
				add_theme_support( 'woocommerce' );
				// Enabling WooCommerce product gallery features (are off by default since WC 3.0.0).
				// zoom.
				add_theme_support( 'wc-product-gallery-zoom' );
				// lightbox.
				add_theme_support( 'wc-product-gallery-lightbox' );
				// swipe.
				add_theme_support( 'wc-product-gallery-slider' );
			}
		}
	}
}
add_action( 'after_setup_theme', 'ecomcx_theme_setup' );

function wp_enqueue_color_picker( $hook_suffix ) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker');
}
add_action('admin_enqueue_scripts','wp_enqueue_color_picker');


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles',999);
function my_theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style',
            get_stylesheet_directory_uri() . '/style.css',
        );
	    wp_enqueue_style( 'child-style',
            get_stylesheet_directory_uri() . '/ecomcx-style.css',
        );
}

add_action ('wp_enqueue_scripts', 'child_theme_script');
function child_theme_script () {
	wp_enqueue_script( 'ecomcx-theme-scripts', get_template_directory_uri() . '/js/ecomcx-scripts.js', array( 'jquery' ), '1.12.4', true );
    wp_enqueue_media();
    wp_register_script('script-child', get_stylesheet_directory_uri() . '/asset/js/js.js', array('jquery'),'1.0.0',true);
    wp_enqueue_script('script-child');
    }

function website(){ echo get_option('site');}
add_action('wp_enqueue_scripts','website');
add_action('admin_enqueue_scripts','website');
add_action( 'admin_enqueue_scripts', 'child_theme_add_admin_stylesheet' );

function child_theme_add_admin_stylesheet() {
    wp_register_style( 'my-style-admin', get_stylesheet_directory_uri() . '/ecomcx-style-admin.css',array() , '1.0.0', 'all' );
    wp_enqueue_style( 'my-style-admin');
}

add_action ('admin_enqueue_scripts', 'child_theme_script_admin');
function child_theme_script_admin () {
    wp_enqueue_media();
    wp_register_script('script-admin', get_stylesheet_directory_uri() . '/asset/js/js-admin.js', array('jquery'),'1.0.0',true);
    wp_enqueue_script('script-admin');
    }

/* Enable Upload SVG file */
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

/* End of Enable upload SVG file*/

/* Tự động thêm Title, Alt-Text, Caption & Description cho hình ảnh*/
add_action( 'add_attachment', 'ttv_set_image_meta_image_upload' );
function ttv_set_image_meta_image_upload( $post_ID ) {

    // Check if uploaded file is an image, else do nothing

    if ( wp_attachment_is_image( $post_ID ) ) {

        $my_image_title = get_post( $post_ID )->post_title;

        // Sanitize the title:  capitalize first letter of every word (other letters lower case):
        $my_image_title = ucwords( strtolower( $my_image_title ) );

        // Create an array with the image meta (Title, Caption, Description) to be updated
        // Note:  comment out the Excerpt/Caption or Content/Description lines if not needed
        $my_image_meta = array(
            'ID'        => $post_ID,            // Specify the image (ID) to be updated
            'post_title'    => $my_image_title,     // Set image Title to sanitized title
            'post_excerpt'  => $my_image_title,     // Set image Caption (Excerpt)
            'post_content'  => $my_image_title,     // Set image Description (Content)
        );

        // Set the image Alt-Text
        update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );

        // Set the image meta (e.g. Title, Excerpt, Content)
        wp_update_post( $my_image_meta );

    } 
}
/*End of Tự động thêm Title, Alt-Text, Caption & Description cho hình ảnh*/

/* Change excerpt post */

//Thay đổi độ dài excerpt post
function ecx_custom_excerpt_length( $length ) {
	return 35;
}
add_filter( 'excerpt_length', 'ecx_custom_excerpt_length', 999 );

//Thay dấu kết dòng
function ecx_new_excerpt_more( $more ) {
	return ' ...';
}
add_filter('excerpt_more', 'ecx_new_excerpt_more');

/* End of Change excerpt post*/

require_once dirname( __FILE__ ) . '/ecomcx-admin/ecomcx-admin.php';
require get_stylesheet_directory()  . '/includes/ecomcx-setting.php';
require get_stylesheet_directory() . '/includes/hotline/hotline.php';
require get_stylesheet_directory()  . '/includes/duplicate-post.php';
require get_stylesheet_directory()  . '/includes/autocomplete_search.php';
require get_stylesheet_directory()  . '/includes/post_type_setting.php';
require get_stylesheet_directory()  . '/includes/drop_nav_categories_mobile_ajax.php';
require get_stylesheet_directory()  . '/includes/woo/megamenu.php';
require get_stylesheet_directory()  . '/includes/woo/featured_archive.php';
require get_stylesheet_directory()  . '/includes/woo/custom_metabox_woo.php';
require get_stylesheet_directory()  . '/includes/woo/function_woo.php';
require get_stylesheet_directory()  . '/includes/woo/function_add_product_content.php';
require get_stylesheet_directory()  . '/includes/woo/viewed_product.php';
require get_stylesheet_directory()  . '/ecx-woo-viet/ecx-woo-address-selectbox.php';
