<?php 
//sale badge
add_filter( 'woocommerce_sale_flash', 'display_on_sale_badge', 20, 3 );
if( !function_exists('display_on_sale_badge'))
{
	function display_on_sale_badge( $html, $post, $product ) 
	{
        $regular=$product->get_regular_price();
        $discount_price=adp_functions()->getDiscountedProductPrice( $product, 1 ,true)[0];
        if ( ! $product->is_on_sale() && $discount_price==$regular )
        { 
        		return;
        }
        elseif ( $product->is_type( 'simple' ) ) 
        {
	        $sale_price=$product->get_sale_price();
	        if($sale_price='')
	        {
		        $sale_price=$discount_price;
	        }
            $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
        } 
        elseif ( $product->is_type( 'variable' ) ) 
        {
            $max_percentage = 0;
            foreach ( $product->get_children() as $child_id ) 
          {
            $variation = wc_get_product( $child_id );
            $price = $variation->get_regular_price();
            $sale = $variation->get_sale_price();
		    if($sale=='')
		    {
			    $sale=adp_functions()->getDiscountedProductPrice( $variation, 1 ,true);
		    }
            if ( $price != 0 && $price!=null && ! empty( $sale ) )
            { 
            	$percentage = ( $price - $sale ) / $price * 100;
            }
            if ( $percentage > $max_percentage ) 
            {
                $max_percentage = $percentage;
            }
          }
        }
		$custom_saleflash_text = get_post_meta( get_the_ID(), '_saleflash_text', true );
        if( !empty( $custom_saleflash_text ) && $product->is_in_stock() )
        { ?>
            <span class="ecx-product-badge custom"><?= $custom_saleflash_text?></span>  <?php
            if ( $max_percentage > 0 ) echo "<span class='ecx-product-badge onsale'>-" . round($max_percentage) . "%</span>"; 
        }
        else
        { 
        	if ( $max_percentage > 0 ) echo "<span class='ecx-product-badge onsale'>-" . round($max_percentage) . "%</span>";       
        }
    }
}

//change position thumbnail product
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 11 );

//add tag
add_action( 'woocommerce_before_shop_loop_item', 'product_tag',9);
if( !function_exists('product_tag'))
{
	function product_tag() 
	{
		global $product; ?>
		<div class="ecx-product-categories">
		    <?= wc_get_product_category_list($product->id); ?>
		</div> <?php
    }
}

//hãng sản xuất
add_action( 'woocommerce_after_shop_loop_item', 'product_brand',11);
if( !function_exists('product_brand'))
{
	function product_brand() 
	{
		global $product; $brands=wp_get_post_terms( $product->id, 'pa_thuong-hieu' ) ;
		if(count($brands)>0) : ?>
		<b class="label-brand">Brand</b>   <?php 
	    endif;?>
		<div class="ecx-product-brand">
		    <?php   ;
		        foreach ($brands as $key => $brand) : ?>
		        	<a href="<?= get_term_link( $brand->term_id , $brand->taxonomy )?>">
		        	    <span class="brand-name"><?= $brand->name ?></span>
		            </a>
		        <?php endforeach; ?>
		</div> <?php
    }
}
//change position breadcrumb 
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
//change position result count
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//change position sale badge single product
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
//remove position loop rating single product
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
//Filer WooCommerce Flexslider options - Add Navigation Arrows
if( !function_exists('ecx_update_woo_flexslider_options'))
{
    function ecx_update_woo_flexslider_options( $options ) 
    {
    	$options['directionNav'] = true;
    	return $options;
    }
    add_filter( 'woocommerce_single_product_carousel_options', 'ecx_update_woo_flexslider_options' );
}
//add line product single
add_action('woocommerce_single_product_summary','space_line',11);
add_action('woocommerce_single_product_summary','space_line',31);
function space_line()
{ ?>
    <div class="space-line"></div>
<?php }