<?php 
function mega_menu_page($id,$label,$loop)
{
    $parent=$id;
    $label_first=$label;
    $loop=$loop;
    $args_page = array(
        'nopaging' => true,
        'order'          => 'ASC',
        'orderby' => 'date', 
        'post_parent'    => $parent,
        'post_type'      => 'page'
    );
    $out='';
    $childs=get_children( $args_page,ARRAY_A );
    if($loop!=1)
    {
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_the_permalink($parent).'">'.get_the_title($parent).'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
                $out.= mega_menu_page($child['ID'],'',$loop);
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_the_permalink($parent).'">'.get_the_title($parent).'</a>';
            $out.='</li>';
        }
    }
    else
    {
        $loop++;
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_the_permalink($parent).'">'.$label_first.'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
                $out.= mega_menu_page($child['ID'],'',$loop);
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_the_permalink($parent).'">'.$label_first.'</a>';
            $out.='</li>';
        }
    }
    return $out;
}
function mega_menu_taxonomy($id,$tax,$label,$loop,$show_img)
{
    $show_img=$show_img;
    $parent=$id;
    $taxonomy=$tax;
    $label_first=$label;
    $loop=$loop;
    $args_tax = array(
        'parent'    => $parent,
        'hide_empty'=> 0,
        'taxonomy'  => $taxonomy,
        'orderby' => 'menu_order', 
        'order' => 'ASC',
    );
    $out='';
    $childs=get_categories( $args_tax );
    if($loop!=1)
    {
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.get_term_by( 'id', $parent, $taxonomy )->name.'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
               $out.= mega_menu_taxonomy($child->term_id,$taxonomy,'',$loop,false);
            }
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.get_term_by( 'id', $parent, $taxonomy )->name.'</a>';
            $out.='</li>';
        }
    }
    else
    {
        $loop++;
        if(count($childs)>0)
        {
            $out.='<li class="megamenu-list has-childs">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.$label_first.'<i class="bi bi-chevron-right"></i></a>';
            $out.='<ul class="megamenu-list sub-childs">';
            foreach ($childs as $key => $child) 
            {
               $out.= mega_menu_taxonomy($child->term_id,$taxonomy,'',$loop,false);
            }
            /*if($show_img==true) :
                $id_thumbnail=get_term_meta( $parent, 'thumbnail_id', true );
                if($id_thumbnail!=null && $id_thumbnail!='') :
                    $out.='<div class="product-category__thumbnail">'.wp_get_attachment_image($id_thumbnail,'medium',false).'</div>';
                endif;
            endif;*/
            $out.='</ul>';
            $out.='</li>';
        }
        else
        {
            $out.='<li class="megamenu_page_list">';
            $out.='<a href="'.get_term_link($parent,$taxonomy).'">'.$label_first.'</a>';
            $out.='</li>';
        }
    }
    return $out;
}