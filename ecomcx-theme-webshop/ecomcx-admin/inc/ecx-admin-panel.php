<?php
/**
* ECOMCX Admin file
*
* by Phi Tran
*/

/* Thêm logo vào trang quản trị WordPress*/
    function ecx_admin_logo() {
        echo '<a href="https://ecomweb.net" target="blank"><img src="'. get_stylesheet_directory_uri() .'/ecomcx-admin/inc/templates/images/logo-admin.png'.'"atl="Thiết kế Website cao cấp"></a>';
    }
    
    add_action( 'admin_notices', 'ecx_admin_logo' );
/* Copyright Admin panel */
    function ecx_admin_footer_credits( $text ) {
        $text = '<div class="ecx_admin_footer_credits"> <a href="https://ecomweb.net" target="blank">Powerd by ECOMCX - Thiết kế Website cao cấp</a></div>';
        return $text;
    }
    
    add_filter( 'admin_footer_text', 'ecx_admin_footer_credits' );
