<?php
/**
* ECOMCX Admin file
*
* by Phi Tran
*/

/*Show welcome panel for all user*/
add_filter('user_has_cap', 'ecx_user_has_cap');
 function ecx_user_has_cap($capabilities){
 global $pagenow;
      if ($pagenow == 'index.php')
           $capabilities['edit_theme_options'] = 1;
      return $capabilities;
 }
add_action( 'load-index.php', 'show_welcome_panel' );

function show_welcome_panel() {
    $user_id = get_current_user_id();

    if ( 1 != get_user_meta( $user_id, 'show_welcome_panel', true ) )
        update_user_meta( $user_id, 'show_welcome_panel', 1 );
}

$PrivateRole = get_role('author');
$PrivateRole -> add_cap('read_private_pages');

$PrivateRole = get_role('editor');
$PrivateRole -> add_cap('read_private_pages');

$PrivateRole = get_role('subscriber');
$PrivateRole -> add_cap('read_private_pages');
