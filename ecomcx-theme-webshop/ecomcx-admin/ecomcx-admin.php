<?php

/*Update CSS within in Admin*/
    function ecx_admin_style() {
        wp_register_style('ecomcx-admin-style', get_stylesheet_directory_uri() .'/ecomcx-admin/inc/templates/ecx-style.css',array() , '1.0.0', 'all');
        wp_enqueue_style('ecomcx-admin-style');
    }
       add_action('admin_enqueue_scripts', 'ecx_admin_style');
       
/*Update CSS Font Awesome*/
    function font_awesome_style() {
        wp_register_style('ecx_font_awesome_style', 'https://use.fontawesome.com/releases/v5.6.3/css/all.css');
        wp_enqueue_style('ecx_font_awesome_style');
    }
    
    add_action('admin_enqueue_scripts', 'font_awesome_style');
/*Login page */
    require_once dirname( __FILE__ ) . '/inc/ecx-login-page.php';

/* Admin panel */
    require_once dirname( __FILE__ ) . '/inc/ecx-admin-panel.php';

/* Welcome panel */
    require_once dirname( __FILE__ ) . '/inc/ecx-admin-welcome-pannel.php'; 

/* Show welcome panel for all user */
    require_once dirname( __FILE__ ) . '/inc/ecx-admin-dashboard-show-all.php'; 


