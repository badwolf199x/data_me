<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
    $max_page   = isset( $total ) ? $total : wc_get_loop_prop( 'total_pages' );
    $paged = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' );
?>
<form id="ecx-order-wrap" class="woocommerce-ordering d-flex flex-wrap align-items-center justify-content-between" method="get">
	<div class="shop-view-switcher">
		<i class="bi bi-funnel-fill d-lg-none filter-button"></i>
		<i class="bi bi-grid-3x3-gap-fill d-none d-lg-inline-block"></i>
		<i class="bi bi-justify d-none d-lg-inline-block"></i>
		<i class="bi bi-list-columns-reverse d-none d-lg-inline-block"></i>
		<i class="bi bi-list-task d-none d-lg-inline-block"></i>
	</div>
	<select name="orderby" class="orderby" aria-label="<?php esc_attr_e( 'Shop order', 'woocommerce' ); ?>">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<div class="pagination d-flex flex-wrap align-items-center justify-content-between">
		<?= ($paged!=1) ? '<a class="button prev-page" href="'.get_previous_posts_page_link( $max_page ).'">←</a>' : '' ?>
	    <input type="text" name="paged" value="<?=$paged?>" /> of <?=' '.$max_page?>
        <?= ($paged!=$max_page) ? '<a class="button next-page" href="'.get_next_posts_page_link( $max_page ).'">→</a>' : '' ?>
    </div>
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
</form>
