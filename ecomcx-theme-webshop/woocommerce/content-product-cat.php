<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product-cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
$args = array(
	'hierarchical' => 1,
    'show_option_none' => '',
    'hide_empty' => 0,
    'parent' => $term_id,
    'taxonomy' => 'product_cat'
   );
$subcats = get_categories($args); 
if (count($subcats)>0) : ?>
<section id="sub-category">
	<div class="section-header d-flex justify-content-between flex-wrap ">
        <div class="section-label-wrap d-flex align-items-start justify-content-between col-12"> 
            <h2 class="section-label ">
                <?php woocommerce_page_title(); ?>&nbsp;Categories                    
            </h2>
        </div>
    </div>
    <div class="section-content d-flex flex-wrap">
        <?php foreach ($subcats as $sc): ?>
        	<li class='col-2dot4'>
	            <a href="<?=get_term_link( $sc->slug, $sc->taxonomy )?>">
		            <?php woocommerce_subcategory_thumbnail($sc->term_id) ?>
		            <h2 class="woocommerce-loop-category__title">
			            <?= $sc->name; ?>
		            </h2>
	            </a>
            </li>
        <?php endforeach; ?>
    </div>
</section>
<?php endif;

